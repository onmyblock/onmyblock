<?php /* Smarty version Smarty-3.1.11, created on 2013-06-25 13:09:06
         compiled from "/home/myblock/public/layout/pages/howitworks.tpl" */ ?>
<?php /*%%SmartyHeaderCode:110856340151c9f8e26a3282-99253572%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '363b207fe30799fd3aff50475eefa70bd273361b' => 
    array (
      0 => '/home/myblock/public/layout/pages/howitworks.tpl',
      1 => 1371590922,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '110856340151c9f8e26a3282-99253572',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51c9f8e2741818_15619345',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51c9f8e2741818_15619345')) {function content_51c9f8e2741818_15619345($_smarty_tpl) {?><!doctype html>
<!--[if lt IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html lang="es" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="es" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title>OnMyBlock</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
    <!-- //metas para facebook -->
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content=" ">
    <meta property="og:site_name" content="">

    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<link rel="stylesheet" href="/media/css/main.css?v=1">
    <link rel="stylesheet" href="/media/css/jquery.vegas.css" type="text/css" media="all">    
    

	<script src="/media/js/libs/modernizr-2.0.6.min.js"></script>
    <script>
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-XXXXXXX-X']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
</head>
<body>
<header>

    <article>
        <div class="logo"></div>
        <?php echo $_smarty_tpl->getSubTemplate ('../header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    </article>

</header>    

<div id="wrapper">

    <section id="section-5">
        
        <article>
            
            <div class="boxhowitworks">


            <h1>How It Works</h1>

            <span>What Is OnMyBlock?</span><br/>
            
            OnMyBlock is a website and iphone app that helps college students find the best place to live, in the shortest time possible, with the help of their friends and classmates. Off-campus housing has always been passed through the social and college networks and now OnMyBlock makes it easier than ever to get the lowdown on the best student rentals.<br/><br/>
            
      

            <span>Use OnMyBlock To:</span><br/>
            <p>
            - Explore awesome places to live<br/>

            - Search for houses on a really big map<br/>

            - Figure out where friends and classmates live<br/>

            - Connect with current and future roommates<br/>

            - Rate, badge, like and comment on awesome pads<br/>

            - Save houses to your Wishlist<br/>
            </p>

                 
            </div>            



        </article>
    </section>
</div>

<?php echo $_smarty_tpl->getSubTemplate ('../footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script src="/media/js/libs/jquery-1.9.0.min.js"></script>
<script src="/media/js/main.js"></script>
<script src="//connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript" src="/media/js/libs/jquery.vegas.js"></script>
<script src="/media/js/libs/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="/media/js/init.js?v=1"></script>
<script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=561406290571363";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

</body>
</html><?php }} ?>