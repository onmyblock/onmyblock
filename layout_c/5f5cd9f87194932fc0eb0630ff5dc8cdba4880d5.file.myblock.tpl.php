<?php /* Smarty version Smarty-3.1.11, created on 2013-06-25 10:59:44
         compiled from "/home/myblock/public/layout/pages/myblock.tpl" */ ?>
<?php /*%%SmartyHeaderCode:50934022651c9da900d27e8-00085459%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5f5cd9f87194932fc0eb0630ff5dc8cdba4880d5' => 
    array (
      0 => '/home/myblock/public/layout/pages/myblock.tpl',
      1 => 1371748895,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '50934022651c9da900d27e8-00085459',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'user' => 0,
    'onmyblock' => 0,
    'place' => 0,
    'array' => 0,
    'network' => 0,
    'plike' => 0,
    'stats' => 0,
    'neighborhood' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51c9da90394851_40461869',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51c9da90394851_40461869')) {function content_51c9da90394851_40461869($_smarty_tpl) {?><!doctype html>
<!--[if lt IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html lang="es" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="es" class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<title>OnMyBlock - My Block</title>
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="author" content="">
		<!-- //metas para facebook -->
		<meta property="og:title" content="">
		<meta property="og:description" content="">
		<meta property="og:type" content="website">
		<meta property="og:url" content="">
		<meta property="og:image" content=" ">
		<meta property="og:site_name" content="">

		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
		<link rel="stylesheet" href="/media/css/main.css?v=1">    
		<link rel="stylesheet" type="text/css" href="/media/css/jquery.fancybox.css"/>
		
		<script src="/media/js/libs/modernizr-2.0.6.min.js"></script>
		<script>
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<style type="text/css">
			.myblock a{
				color: #000 !important;
			}

			header .menu ul li{
				padding-top: 9px !important;
			}

		</style>
	</head>
	<body>
		<header>
			<article>
				<div class="logo"></div>     		 
				<?php echo $_smarty_tpl->getSubTemplate ('../header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

			</article>
		</header>    
		<div id="wrapper">
			<section id="section-4">
				<article>
					<div id="dataprofile">
						<div class="imgprofile">
							<img src="https://graph.facebook.com/<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
/picture?type=large" />
						</div>
						<div class="idname"><?php echo $_smarty_tpl->tpl_vars['user']->value['first_name'];?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value['last_name'];?>
</div>

						<div class="accesos">
							<ul>
								<li id="favorites"><a class="on" href="javascript:">Favorites</a></li>
								<li id="profile"><a href="javascript:">Profile</a></li>
								<li id="places"><a onclick="fbLogout()" href="javascript:">Places</a></li>
							</ul>
						</div>
					</div>
					<div id="contentprofile">
						<div class="listlinks">
							<div class="link01">
								<div class="imglink01">
									<a href="browse">Look for houses to add them to your Wish List!</a>
								</div>
							</div>
							<div class="link01">
								<div class="imglink02">
									<a href="add-house">Add your house and talk about your living experience!</a>
								</div>           
							</div>
					  
							<div class="link01">
								<div class="imglink03">
									<a href="">See where your friends want to live next year!</a>
								</div>
							</div>
						</div>  
						<div class="contentaccesos">
							<div id="contfavorite">
								<!--div class="goto">
									<a href="">Go to Browse Page</a>
								</div-->
								<?php  $_smarty_tpl->tpl_vars['place'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['place']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['user']->value['favorites']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['pl']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['place']->key => $_smarty_tpl->tpl_vars['place']->value){
$_smarty_tpl->tpl_vars['place']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['pl']['iteration']++;
?>
									<div id="fav01-<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['pl']['iteration'];?>
" class="popsfavorites">                  
										<div class="imgfv">
											<a href="<?php echo $_smarty_tpl->tpl_vars['onmyblock']->value;?>
/place?id=<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
">
												<img src="https://s3-us-west-1.amazonaws.com/onmyblock/places/<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
/profile.jpg">
											</a>
										</div>
										
										<div class="datafavor">
											<div id="<?php echo $_smarty_tpl->getVariable('smarty')->value['foreach']['pl']['iteration'];?>
" data="<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
" class="closepop"></div>
											<ul>
												<li><span>$<?php echo $_smarty_tpl->tpl_vars['place']->value[6];?>
</span></li>
												<li class="icon01"><span><?php echo $_smarty_tpl->tpl_vars['place']->value[4];?>
</span></li>
												<li class="icon02"><span><?php if (isset($_smarty_tpl->tpl_vars['place']->value[5])){?><?php echo $_smarty_tpl->tpl_vars['place']->value[5];?>
<?php }else{ ?>N/A<?php }?></span></li>
											</ul>
										</div>
									</div>
								<?php } ?>	
							</div>
						
							
							
							<div style="display:none;"  id="contprofile">
								<div class="bgdata">
									<div class="datinter">
										<div class="dataprofile">
											<div class="minipic">
												<img src="https://graph.facebook.com/<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
/picture?type=square" />
											</div>
											<div class="idnamemini">
												<?php echo $_smarty_tpl->tpl_vars['user']->value['first_name'];?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value['last_name'];?>

											</div>
										</div>
										<div class="dataacount">
											<div class="email">
												Email: <span><?php echo $_smarty_tpl->tpl_vars['user']->value['email'];?>
</span>
											</div>
										  
											<div class="networks">
												Networks: <span><?php  $_smarty_tpl->tpl_vars["array"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["array"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['user']->value['networks']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["array"]->key => $_smarty_tpl->tpl_vars["array"]->value){
$_smarty_tpl->tpl_vars["array"]->_loop = true;
?>
																<?php  $_smarty_tpl->tpl_vars["network"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["network"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['array']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars["network"]->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars["network"]->key => $_smarty_tpl->tpl_vars["network"]->value){
$_smarty_tpl->tpl_vars["network"]->_loop = true;
 $_smarty_tpl->tpl_vars["network"]->index++;
 $_smarty_tpl->tpl_vars["network"]->first = $_smarty_tpl->tpl_vars["network"]->index === 0;
?>
																<?php if ($_smarty_tpl->tpl_vars['network']->first){?>
																<?php }?>
																<?php echo $_smarty_tpl->tpl_vars['network']->value;?>

																<?php } ?><?php } ?></span>
											</div>
											
											<div class="mylikes">
												My Likes:
												<div class="listlikes">
												  <ul>
													<?php  $_smarty_tpl->tpl_vars['plike'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['plike']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['user']->value['place_likes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['plike']->key => $_smarty_tpl->tpl_vars['plike']->value){
$_smarty_tpl->tpl_vars['plike']->_loop = true;
?>
														<li><a href="javascript:"><?php echo $_smarty_tpl->tpl_vars['plike']->value['name'];?>
</a></li>
													<?php } ?>
												  </ul> 
												</div>
											</div>
										</div>
									</div>
								</div>                
							</div>
							<div style="display:none;" id="contplaces">
								<div class="addyour">
									<a href="add-house">Add Your House Now!</a>
								</div>
							</div>
						</div>
					</div>
					<div id="dataright">
						<div class="data01">
							<ul>
								<li><?php echo $_smarty_tpl->tpl_vars['stats']->value['friends_on_onmyblock']['count'];?>
 Friends on OnMyBlock</li>
								<li class="noborder"><?php echo $_smarty_tpl->tpl_vars['stats']->value['total_places']['count'];?>
 Properties on OnMyBlock</li>
							</ul>
						</div>
					
						<span>Top Neighborhoods in San Diego</span>

						<div class="data01">
							<ul>
								<?php  $_smarty_tpl->tpl_vars['neighborhood'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['neighborhood']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['stats']->value['top_neighborhoods']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['neighborhood']->key => $_smarty_tpl->tpl_vars['neighborhood']->value){
$_smarty_tpl->tpl_vars['neighborhood']->_loop = true;
?>
									<li><?php echo $_smarty_tpl->tpl_vars['neighborhood']->value[1];?>
 Places in <a href="javascript:" class="neighborhoodName"><?php echo $_smarty_tpl->tpl_vars['neighborhood']->value[0];?>
</a></li>	
								<?php } ?>
							</ul>
						</div>
					</div>
				</article>      
			</section>
		</div>
		
		
		<?php echo $_smarty_tpl->getSubTemplate ('../footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script type="text/javascript" src="/media/js/libs/jquery.fancybox.js"></script>
		<script src="/media/js/libs/jquery.easing.1.3.js" type="text/javascript"></script>
		<script src="http://maps.google.com/maps/api/js?libraries=places&region=uk&language=en&sensor=false"></script>
		<script src="/media/js/initr.js?v=1"></script>

		<script>
			(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_US/all.js#xfbmsl=1&appId=561406290571363";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
			
			$(".neighborhoodName").click(function(){
				var neighborhood = $(this).text();
				
				localStorage.setItem('neighborhood', neighborhood+', San Diego, CA, United States');
				localStorage.setItem('pageBack', 1);
				
				var url = "<?php echo $_smarty_tpl->tpl_vars['onmyblock']->value;?>
/map";    
				$(location).attr('href',url);
			});
		</script>


	</body>
</html><?php }} ?>