<?php /* Smarty version Smarty-3.1.11, created on 2013-06-25 10:03:17
         compiled from "/home/myblock/public/layout/pages/place_map.tpl" */ ?>
<?php /*%%SmartyHeaderCode:58475367951c9cd55222d17-35099234%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '372b7d8e8b93b409ad8ca7e390282b09e1ee6a65' => 
    array (
      0 => '/home/myblock/public/layout/pages/place_map.tpl',
      1 => 1371254119,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '58475367951c9cd55222d17-35099234',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'type' => 0,
    'place' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51c9cd552d3102_26388305',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51c9cd552d3102_26388305')) {function content_51c9cd552d3102_26388305($_smarty_tpl) {?>
<body style="overflow:hidden">
<link rel="stylesheet" href="https://onmyblock.com/media/css/style.css" type="text/css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="https://onmyblock.com/media/js/gmap3.min.js"></script>
<?php if ($_smarty_tpl->tpl_vars['type']->value=='map'){?>

<script>
$(document).ready(function() {
	$("#place_map").gmap3({
		action: 'addMarker',
		address: "<?php echo $_smarty_tpl->tpl_vars['place']->value['street'];?>
 <?php echo $_smarty_tpl->tpl_vars['place']->value['route'];?>
, <?php echo $_smarty_tpl->tpl_vars['place']->value['city'];?>
, <?php echo $_smarty_tpl->tpl_vars['place']->value['state'];?>
 <?php echo $_smarty_tpl->tpl_vars['place']->value['postal'];?>
",
		map:{
			center: true,
			zoom: 14,
			mapTypeId: google.maps.MapTypeId.MAP,
			mapTypeControl: true,
			navigationControl: true,
			scrollwheel: true,
			streetViewControl: false
		},
		marker:{
			options:{
				icon:new google.maps.MarkerImage('https://onmyblock.com/media/images/map_icon.png',
				new google.maps.Size(24, 36),
				new google.maps.Point(0,0),
				new google.maps.Point(12, 32)),
				shadow:new google.maps.MarkerImage('https://onmyblock.com/media/images/map_icon_shadow.png',
				new google.maps.Size(19, 13),
				new google.maps.Point(0,0),
				new google.maps.Point(1, 10)),
				draggable: false
				}
		}

	});
});
</script>
<div id="place_map" style="width:600px; height:420px;"></div>

<?php }elseif($_smarty_tpl->tpl_vars['type']->value=='streetview'){?>

<script>
$(document).ready(function() {
var streetViewService = new google.maps.StreetViewService();
var STREETVIEW_MAX_DISTANCE = 50;
var latLng = new google.maps.LatLng(<?php echo $_smarty_tpl->tpl_vars['place']->value['latitude'];?>
, <?php echo $_smarty_tpl->tpl_vars['place']->value['longitude'];?>
);
streetViewService.getPanoramaByLocation(latLng, STREETVIEW_MAX_DISTANCE, function (streetViewPanoramaData, status) {
    if (status === google.maps.StreetViewStatus.OK) {
    $("#place_street_na").remove();
    $('#place_street_view').gmap3({ action:'init',
	zoom: 14,
	mapTypeId: google.maps.MapTypeId.ROADMAP,
	center: [<?php echo $_smarty_tpl->tpl_vars['place']->value['latitude'];?>
, <?php echo $_smarty_tpl->tpl_vars['place']->value['longitude'];?>
]},
	{ action:'setStreetView',
		id: 'place_street_view',
		options:{
			position: [<?php echo $_smarty_tpl->tpl_vars['place']->value['latitude'];?>
, <?php echo $_smarty_tpl->tpl_vars['place']->value['longitude'];?>
],
			pov: {
				heading: 0,
				pitch: 0,
				zoom: 1
				}}
	});
    } else {
    $("#street_view_tab").remove();
    }
});
});
</script>
<div id="place_street_na">Streetview is Not Available For This House</div>
<div id="place_street_view" style="width:600px; height:420px;"></div>

<?php }?>
</body><?php }} ?>