<?php /* Smarty version Smarty-3.1.11, created on 2013-06-25 16:21:02
         compiled from "/home/myblock/public/layout/pages/careers.tpl" */ ?>
<?php /*%%SmartyHeaderCode:121104845851ca25de841047-02767019%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2c16f92ba5831d3d49ff3e70c39277f7d9b82a71' => 
    array (
      0 => '/home/myblock/public/layout/pages/careers.tpl',
      1 => 1371591199,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '121104845851ca25de841047-02767019',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'user' => 0,
    'onmyblock' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51ca25de8e6184_09329872',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51ca25de8e6184_09329872')) {function content_51ca25de8e6184_09329872($_smarty_tpl) {?><!doctype html>
<!--[if lt IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html lang="es" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="es" class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<title>OnMyBlock</title>
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="author" content="">
		<!-- //metas para facebook -->
		<meta property="og:title" content="">
		<meta property="og:description" content="">
		<meta property="og:type" content="website">
		<meta property="og:url" content="">
		<meta property="og:image" content=" ">
		<meta property="og:site_name" content="">

		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
		<link rel="stylesheet" href="/media/css/main.css?v=1">
		<link rel="stylesheet" href="/media/css/jquery.vegas.css" type="text/css" media="all"> 

		

		<script src="/media/js/libs/modernizr-2.0.6.min.js"></script>
		<script>
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
	</head>
	<body>
		<header id="header">
			<article>
				<div class="logo"></div>
				<?php if ($_COOKIE['omb_session']=='loggedin'){?>
					<div id="contentlogin">

						<div class="topminipic">
							<img src="https://graph.facebook.com/<?php echo $_smarty_tpl->tpl_vars['user']->value['username'];?>
/picture?square">
						</div>

						<div class="idnametop">
							<a href="javascript:"><?php echo $_smarty_tpl->tpl_vars['user']->value['first_name'];?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value['last_name'];?>
</a>
						</div>

						<div style="display:none;" class="despledat">
							<ul>
								<li><a href="">Profile</a></li>
								<li><a href="">Logout</a></li>
							</ul>
						</div>
					</div>

					<div class="menu">            
						<ul>
							<li class="hide home"><a href="home">HOME</a></li>
							<li class="myblock"><a class="on" href="myblock">MYBLOCK</a></li>
							<li class="browse"><a href="browse">BROWSE</a></li>
							<li class="map"><a href="map">MAP</a></li>  
						</ul>
					</div>
				<?php }else{ ?>
					<div class="works"><a href="howitworks.html">How it Works</a></div>
				<?php }?>
			</article>
		</header>
		<div id="wrapper">
			<section id="section-5">
				<article>
					<div class="boxhowitworks">
						<h1>Company Careers</h1>
						<h2>Join Our Team!</h2>
						<span>We build OnMyBlock with one mission in mind</span><br/>
							Make finding the right college pad an exciting and fun experience. After all, we are college students ourselves who too enjoy looking for great college pads.<br/><br/>
						<p>
							- iOS Developer - San Diego, CA<br/>

							- Web Designer - San Diego, CA<br/>

							- Campus Founder - University of San Diego<br/>

							- Campus Founder - San Diego State<br/>

							- Campus Founder - UCSD<br/>

							- Campus Founder - Point Loma University<br/>
						</p>
					</div>
				</article>
			</section>
		</div>

		<?php echo $_smarty_tpl->getSubTemplate ('../footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


		<script src="/media/js/libs/jquery-1.9.0.min.js"></script>
		<script src="/media/js/main.js"></script>
		<script src="//connect.facebook.net/en_US/all.js"></script>
		<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['onmyblock']->value;?>
/media/js/libs/jquery.vegas.js"></script>
		<script src="/media/js/libs/jquery.easing.1.3.js" type="text/javascript"></script>
		<script src="/media/js/init.js?v=1"></script>
		<script>
			(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=561406290571363";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>

	</body>
</html><?php }} ?>