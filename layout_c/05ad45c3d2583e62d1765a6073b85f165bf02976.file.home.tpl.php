<?php /* Smarty version Smarty-3.1.11, created on 2013-06-25 09:50:56
         compiled from "/home/myblock/public/layout/pages/home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:174544813551c9ca709ee926-03749433%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '05ad45c3d2583e62d1765a6073b85f165bf02976' => 
    array (
      0 => '/home/myblock/public/layout/pages/home.tpl',
      1 => 1371678816,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '174544813551c9ca709ee926-03749433',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51c9ca70a9a2c2_29823200',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51c9ca70a9a2c2_29823200')) {function content_51c9ca70a9a2c2_29823200($_smarty_tpl) {?><!doctype html>
<!--[if lt IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html lang="es" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="es" class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<title>OnMyBlock</title>
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="author" content="">
		<!-- //metas para facebook -->
		<meta property="og:title" content="">
		<meta property="og:description" content="">
		<meta property="og:type" content="website">
		<meta property="og:url" content="">
		<meta property="og:image" content=" ">
		<meta property="og:site_name" content="">

		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
		<link rel="stylesheet" href="/media/css/main.css?v=1">
		<link rel="stylesheet" href="/media/css/bootstrap.css"/>
		<link rel="stylesheet" href="/media/css/jquery.vegas.css" type="text/css" media="all">    
		<link rel="stylesheet" type="text/css" href="/media/css/jquery.fancybox.css"/>

		<script src="/media/js/libs/modernizr-2.0.6.min.js"></script>
		<script>
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
		
	</head>
	<body>
		<header id="header">
			<article>
				<div class="logo"></div>
				<?php if ($_COOKIE['omb_session']=='loggedin'){?>
					<div id="contentlogin">

						<div class="topminipic" style="float:left;">
							<img src="https://graph.facebook.com/<?php echo $_smarty_tpl->tpl_vars['user']->value['username'];?>
/picture?square">
						</div>

						<div class="idnametop" style="float:left;">
							<a href="javascript:"><?php echo $_smarty_tpl->tpl_vars['user']->value['first_name'];?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value['last_name'];?>
</a>
						</div>

						<div style="display:none;" class="despledat">
							<ul>
								<li><a href="myblock">Profile</a></li>
								<li><a class="logout" href="javascript:">Logout</a></li>
							</ul>
						</div>
					</div>

					<div class="menu">            
						<ul>
							<li class="hide home"><a href="home">HOME</a></li>
							<li class="myblock"><a class="on" href="myblock">MYBLOCK</a></li>
							<li class="browse"><a href="browse">BROWSE</a></li>
							<li class="map"><a href="map">MAP</a></li>  
						</ul>
					</div>
				<?php }else{ ?>
					<div class="works"><a href="howitworks">How it Works</a></div>
				<?php }?>
			</article>
		</header>    

		<div id="wrapper">

			<section id="section-1">
				<article>
					<div class="box1" <?php if ($_COOKIE['omb_session']=='loggedin'){?>style="left:50%;margin-left:-298px;"<?php }?>>
						<div class="title01">Find Your Next College Pad.</div>
							<div class="slides_search" style="padding: 2px;margin-left:2%;"> 
							<form id="form-1" class="jqTransform jqtransformdone" enctype="multipart/form-data" action="map" method="post">
								<div class="width-1 w-4">
									<select name="test" id="school" class="select01">
										<option selected value="Where Do You Go to School?"></option>
										<option value="2">University of San Diego</option>
									</select>
								</div>
								<input id="slide_submit_search" class="blue-button" type="button" value="Go!">
								<div class="error"></div>
							</form>
						</div> 
					</div>
					<?php if ($_COOKIE['omb_session']!='loggedin'){?>
					<div class="box2">
						<div class="fblogin">
							<div id="fb-root"></div>
							
							<div class="conterfbprincipal">
								<div class="conterimgfb"></div>
								<div class="conterfbcode" onclick="fbLogin()">
									<fb:login-button show-faces="true" width="400">----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</fb:login-button>
								</div>
							</div>
							<div class="likefb"> 
								<div class="fb-like" data-href="https://www.facebook.com/OnMyBlock" data-send="false" data-width="286" data-show-faces="true" data-font="arial"></div>
							</div>
						</div>               
					</div>
					<?php }?>
				</article>
			</section>
		</div>

		<?php echo $_smarty_tpl->getSubTemplate ('../footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


		<script src="/media/js/libs/jquery-1.9.0.min.js"></script>
		<script src="https://connect.facebook.net/en_US/all.js"></script>
		<script src="/media/js/libs/jquery.cookie.js"></script> 
		<script type="text/javascript" src="/media/js/libs/jquery.vegas.js"></script>
		<script type="text/javascript" src="/media/js/libs/bootstrap-dropdown.js"></script>
		<script type="text/javascript" src="/media/js/libs/bootstrap-button.js"></script>
		<script type="text/javascript" src="/media/js/libs/jquery.fancybox.js"></script>
		<script src="/media/js/init.js?v=1"></script>
		<script src="/media/js/main.js"></script>
		<script>
			$(document).ready(function(){	
				$('.myblock a').removeClass('on');
			});
		</script>


	</body>
</html><?php }} ?>