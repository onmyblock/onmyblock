<?php /* Smarty version Smarty-3.1.11, created on 2013-06-25 10:03:07
         compiled from "/home/myblock/public/layout/pages/browse.tpl" */ ?>
<?php /*%%SmartyHeaderCode:147899378051c9cd4b9f70d4-95091693%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '366ca52704bf5681b4a69790eff0f30115616c76' => 
    array (
      0 => '/home/myblock/public/layout/pages/browse.tpl',
      1 => 1371600653,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '147899378051c9cd4b9f70d4-95091693',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'page' => 0,
    'filename' => 0,
    'user' => 0,
    'places' => 0,
    'place' => 0,
    'time' => 0,
    'cant' => 0,
    'random' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51c9cd4c007689_70266610',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51c9cd4c007689_70266610')) {function content_51c9cd4c007689_70266610($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ('../header4.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<script type="text/javascript">
var pag=1;
var datFilter="";
var search="";
	
$(document).ready(function(){
		
	$('.popup').click(function(){
		var idPlace = $(this).attr("id");
		
		$.fancybox({
			href: 'browse_popup?id='+idPlace,
			type: 'ajax',
			scrolling: 'no',
			hideOnContentClick: true
		});
	});
	
	$('.filter').click(function(){
		
		pag=1;
		pagTwo=0;
		
		datFilter = $(this).attr("data-option-value");
		
		search = $("#searchTextField").val();
		 
		$('#browse_container').isotope('destroy');
		$('#browse_container').empty();
		
		$.ajax({
			type: 	"POST",
			url:	"browse_items",
			data:	{ dataFilter:datFilter,pagina:pagTwo,valSearch:search } 
		}).done(function(data) {
			var $container = $('#browse_container');
			
			$container.isotope({
				resizable: true,
				masonry: {
					columnWidth: colW
				},
				animationEngine: 'best-available',
				itemSelector: '.browse_item',
				filter: '*'
			});
			
			$newData = $(data);
			$('#browse_container').append($newData).isotope('appended', $newData); 
		});
		pagTwo++;
	});

	$(window).scroll(function(){
		if ($(window).scrollTop() == $(document).height() - $(window).height()){
			
			$.ajax({
				type: 	"POST",
				url:	"browse_items",
				data:	{ dataFilter:datFilter,pagina:pag,valSearch:search } 
			}).done(function(data) {
				$newData = $(data);
				$('#browse_container').append($newData).isotope('appended', $newData);
			});
			
			pag++;
		}
	});	
	
	/* $("#searchTextField").keyup(function(event){
		if(event.keyCode == 13)
		{ */
	/* $("#btnSearch").on("click",function(){
		pag=1;
		pagTwo=0;
		
		var valSearch = $("#searchTextField").val();
		
		datFilter = $(this).attr("data-option-value");
		
		$('#browse_container').empty();
		
		$.ajax({
			type: 	"POST",
			url:	"browse_items",
			data:	{ dataFilter:datFilter,pagina:pagTwo,val_search:valSearch } 
		}).done(function(data) {
			$newData = $(data);
			$('#browse_container').append($newData).isotope('appended', $newData);
		});
		pagTwo++;
	}); */
			
		/* }
	}); */
	

	var input = document.getElementById('searchTextField');     
	var autocomplete = new google.maps.places.Autocomplete(input, {
		types: ["geocode"],
		componentRestrictions: {country: 'USA'}
	}); 

	var status=1;
	
	function callNotification(status)
	{
		if(status==1)
		{
			$.ajax({
				type:"POST",
				url:"svcNotification",
				data: {
					type: "result"
				}
			}).done(function(data) {
				if(data!=0)
				{
					$('#target-domm').css('display','block');

					$("#numNotificationm").empty();

					$("#numNotificationm").text($.trim(data));
				} else {
					$('#target-domm').css('display','none');
				}
			});
		}
	}

	setInterval(function() {
		$floatBar = $('.floatbar');
		
		if($(document).scrollTop() > 60){
			if(!$floatBar.hasClass('floating')){
				$floatBar.hide();
				$floatBar.addClass('floating');
				$floatBar.css({'top':'0','position':'fixed'}, function(){});
				$floatBar.fadeIn('normal');
				$(".menuBtnn").css("display","block"); 
				
				callNotification(status);

				status=0;
			}
		} else {
			if($floatBar.hasClass('floating')){
				$floatBar.hide();
				$floatBar.removeClass('floating');
				$floatBar.css({'position':'relative'}, function(){});
				$floatBar.fadeIn('normal');
				$(".menuBtnn").css("display","none"); 

				status=1;
			}
		}
		
	}, 100);
});

</script>
<div class="floatbar" style="z-index:100"><div class="homeHeaderBackground"><div id="top_container" class="menuBtnn homeHeader" style="display:none;height: 64px;text-align: center;"><div style="margin: auto;"><div id="logo" style="width: 9%;margin-right: 115px;"></div><?php if ($_smarty_tpl->tpl_vars['page']->value!='register'){?><div class="menu_buttons" style="width: 393px;"><?php if ($_COOKIE['omb_session']=='loggedin'){?><a <?php if ($_smarty_tpl->tpl_vars['filename']->value=='myblock'){?> class="selected" <?php }?> style="margin-right: 14px;" href="myblock"><span class="fs1" style="margin-left: 13px;" data-icon="&#x25;"></span>MYBLOCK<div id="target-domm" style="margin-left: 115px;display:none;width:16px;height:16px;background: #d16394;border-radius:10px;font-size:10px;color: white;left: 9%;margin-top: -41px;margin-left: 106px;float:left;text-align:center;" ><p id="numNotificationm" class="notransformar" style="margin-top: -4px;"></p></div></a><?php }else{ ?><a <?php if ($_smarty_tpl->tpl_vars['filename']->value=='home'){?> class="selected" <?php }?> href="/"><span class="fs1" style="margin-left: 24px;" data-icon="&#x25;"></span>Home</a><?php }?><a <?php if ($_smarty_tpl->tpl_vars['filename']->value=='browse'){?> class="selected" <?php }?> href="browse" style="margin-right: 7px;"><span class="fs1" style="margin-left: 10px;" data-icon="&#x30;" style="font-size: 15px top:-1px;"></span>BROWSE</a><a <?php if ($_smarty_tpl->tpl_vars['filename']->value=='map'){?> class="selected" <?php }?> href="map"><span class="fs1" data-icon="&#x2f;" style="top:-1px;"></span>MAP</a></div><?php if ($_COOKIE['omb_session']=='loggedin'){?><div id="user_login_container"><ul id="user_login"><li><a href="#"><img src="https://graph.facebook.com/<?php echo $_smarty_tpl->tpl_vars['user']->value['username'];?>
/picture?square" /><h6><?php echo $_smarty_tpl->tpl_vars['user']->value['first_name'];?>
 <?php echo $_smarty_tpl->tpl_vars['user']->value['last_name'];?>
</h6></a><ul><li><a href="myblock"><h5>Profile</h5></a></li><li><a onclick="fbLogout()"><h5>Logout</h5></a></li></ul></li></ul></div><?php }else{ ?><a id="buttonFb" class="buttonFb blue-button" style="width: 130px; margin-top: 19px; margin-left: 50px; text-align:center; padding:5px; font-size:12px;">Login with Facebook</a><?php }?><?php }?></div></div></div><div id="front_menu" class="browe_top_menu " style="width: 100%;margin-top: 0px;margin-left:0px;left:0px;text-align: center;"><div id="options" class="browse_options" style="width: 930px;margin: auto;" ><ul class="browse_nav" data-option-key="filter" style="overflow: visible;width: 770px;" ><li style="width: 120px;"><a id="new" loc="#new" data-option-value="" class="filter selected">New Pads</a></li><li style="width: 120px;"><a id="pupolar" loc="#popular" data-option-value="popular" class="filter">Popular Pads</a></li><li style="width: 120px;"><a id="beach" loc="#beach" data-option-value="beach" class="filter">Beaches</a></li><li style="width: 120px;"><a id="school" loc="#school" data-option-value="school" class="filter">Close to School</a></li><!--<li style="width: 120px;"><a id="friends" class="tabs" loc="#friends" data-option-value="friends">Friends</a></li>--></ul><div style="float: left;margin-top: 3px;width: 290px;margin-left: 20px;" ><input type="text" placeholder="Please enter city or neighborhood" style="width: 282px;" name="search" id="searchTextField" /><div style="width: 19px;height: 19px;margin-top: -21px;margin-left: 266px;cursor: pointer;"><img id="btnSearch" src="https://onmyblock.com/media/images/lupa.png" class="filter" data-option-value="browse_search" style="margin-top: 2px" /></div></div></div></div></div><div style="margin-bottom:30px;margin-top: 6px;"><div id="browse_container"><?php  $_smarty_tpl->tpl_vars['place'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['place']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['places']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['disp']['iteration']=0;
foreach ($_from as $_smarty_tpl->tpl_vars['place']->key => $_smarty_tpl->tpl_vars['place']->value){
$_smarty_tpl->tpl_vars['place']->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['disp']['iteration']++;
?> <?php $_smarty_tpl->tpl_vars['photo'] = new Smarty_variable(explode(",",$_smarty_tpl->tpl_vars['place']->value[4]), null, 0);?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['place']->value['height'];?>
<?php $_tmp1=ob_get_clean();?><?php if ($_tmp1>80){?><div style="height:<?php echo $_smarty_tpl->tpl_vars['place']->value['height'];?>
px; background-image:url(https://s3-us-west-1.amazonaws.com/onmyblock/places/<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
/profile.jpg?time=<?php echo $_smarty_tpl->tpl_vars['time']->value;?>
);background-size: 315px <?php echo $_smarty_tpl->tpl_vars['place']->value['height'];?>
px;background-repeat:no-repeat;" class="browse_item <?php if ($_smarty_tpl->tpl_vars['place']->value[4]<2){?> 1br<?php }elseif($_smarty_tpl->tpl_vars['place']->value[4]==2){?> 2br<?php }elseif($_smarty_tpl->tpl_vars['place']->value[4]==3){?> 3br<?php }elseif($_smarty_tpl->tpl_vars['place']->value[4]>3){?> 4br <?php }?><?php if ($_smarty_tpl->tpl_vars['place']->value[5]<1000){?> 0-1<?php }elseif($_smarty_tpl->tpl_vars['place']->value[5]>=1000&&$_smarty_tpl->tpl_vars['place']->value[5]<2000){?> 1-2<?php }elseif($_smarty_tpl->tpl_vars['place']->value[5]>=2000&&$_smarty_tpl->tpl_vars['place']->value[5]<3000){?> 2-3<?php }elseif($_smarty_tpl->tpl_vars['place']->value[5]>=3000){?> 3-more <?php }?>" place_id="<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
"><div class="browse_box" id="<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
"><div class="dataname" style="top:5px;"><?php if ($_smarty_tpl->tpl_vars['place']->value[4]>0){?><?php echo $_smarty_tpl->tpl_vars['place']->value[4];?>
br /<?php }?> $<?php echo $_smarty_tpl->tpl_vars['place']->value[5];?>
 </div><div id="<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
" style="z-index:9999 !important;" class="popup"></div><div class="browse_item_header_other" style="height:200px; position:relative; width:auto; margin-left: 180px;" user_id="<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
" place_id="<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
"><!--input type="button" class="blue-button" style="float:right; padding:7px; font-size:14px;" value="$<?php echo $_smarty_tpl->tpl_vars['place']->value[5];?>
" /--><div class="tooltip" style="float:left; width:80px; position:absolute; z-index:99999;"><span id="imgm_<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
" class="tooltip1"><?php if ($_smarty_tpl->tpl_vars['place']->value['favorited']==1){?>Added to Favorites<?php }else{ ?>Add to Favorites<?php }?></span><a class="tipsy"  style="width:20px;height:24px;" id="<?php if ($_smarty_tpl->tpl_vars['place']->value['favorited']==1){?>Added to Favorites<?php }else{ ?>Add to Favorites<?php }?>" user_id="<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
" place_id="<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
" ><?php if ($_smarty_tpl->tpl_vars['place']->value['favorited']==1){?><input type="button" class="myblock_tipsy" data-animation="1" style="background: #68ab64; background:url(https://onmyblock.com/media/images/icons/myblock_w.png) 4px 2px no-repeat #68ab64; opacity: 0.6;" id="<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
" /><?php }else{ ?><input type="button" class="myblock_tipsy" data-animation="0" id="<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
"/><?php }?></a></div><div class="a-btn tooltip2" id="<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
" style="position:relative; margin-left:50px;float:left; width:80px; height:30px; z-index:99999;" ><span id="imgv_<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
" class="tooltip2"><?php if ($_smarty_tpl->tpl_vars['place']->value['liked']==1){?>Liked<?php }else{ ?>Like<?php }?></span><a class="tipsy"  style="width:24px;height:24px;" id="<?php if ($_smarty_tpl->tpl_vars['place']->value['liked']==1){?>Liked<?php }else{ ?>Like<?php }?>" user_id="<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
" place_id="<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
" ><?php if ($_smarty_tpl->tpl_vars['place']->value['liked']==1){?><input type="button" class="browselike" data-animation="1" style="background:#d16394; background:url(https://onmyblock.com/media/images/thumb_up_w.png) 9px 5px no-repeat #d16394; opacity: 0.6;" id="<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
"/><?php }else{ ?><input type="button" class="browselike" data-animation="0" id="<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
"/><?php }?></a></div><!--<input type="button" class="browse_like" title="Like this House"  <?php if ($_smarty_tpl->tpl_vars['place']->value['liked']==1){?>id="browse_liked"<?php }?>/><input type="button" class="browse_fav" title="Add to Wishlist" <?php if ($_smarty_tpl->tpl_vars['place']->value['favorited']==1){?>id="browse_favorited"<?php }?>/>--></div><!--div class="browse_sweet_border">Sweet</div--><!--a class="browse_sweet <?php if ($_smarty_tpl->tpl_vars['place']->value['liked']==1){?>browse_sweetnd<?php }?>" style="margin-top:<?php echo $_smarty_tpl->tpl_vars['place']->value['height']/2-25;?>
px;"></a--><div class="dataname"><?php echo $_smarty_tpl->tpl_vars['place']->value[6];?>
 <?php echo $_smarty_tpl->tpl_vars['place']->value[7];?>
 <br /> <?php echo $_smarty_tpl->tpl_vars['place']->value[1];?>
</div><div class="browse_item_info"><div class="bi"><div class="bi0"><div class="fs1" data-icon="&#x2a;"></div><?php echo $_smarty_tpl->tpl_vars['place']->value['views'];?>
</div><div class="bi1" ><div class="fs1" style="height: 20px; background:url(https://onmyblock.com/media/images/thumb_up3.png) 9px 3px no-repeat; margin-right: 17px;" ></div><p id="p<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
" style="margin-top: -21px; margin-left: 10px;"><?php echo $_smarty_tpl->tpl_vars['place']->value['likes'];?>
</p></div><div class="bi2"><div class="fs1" data-icon="&#x22;"></div><?php echo $_smarty_tpl->tpl_vars['place']->value['comments'];?>
</div></div></div></div></div><?php }?><?php if ($_smarty_tpl->tpl_vars['cant']->value==$_smarty_tpl->getVariable('smarty')->value['foreach']['disp']['iteration']){?><?php if ($_smarty_tpl->tpl_vars['random']->value==1){?><div style="height:210px; background:url(https://onmyblock.com/media/images/capa.png) no-repeat; background-size: 280px 210px; width: 280px; text-align: center;" class="browse_item"  place_id="0"><div style="margin-top: 15%;"><div style="width: 20px;height: 90px;float: left;"></div><div style="height: 90px;width: 20px;float: right;"></div><span style="font-size: 16px; color: black;">Do you like where you live?</span><br/><br/><span style="color:#51443c;">How's the landlord? What are the neighbors like? Share your story.</span></div><div <?php if ($_COOKIE['omb_session']=='loggedin'){?> onClick="window.location='add-house'"<?php }else{ ?> onClick="openPopup()" <?php }?> style="margin-top: 20px;"><a id="buttonFb" class="buttonFb blue-button" style="width: 130px; text-align:center; padding:5px; font-size:12px;">Add Your House</a></div></div><?php }else{ ?><div style="height:210px; background:url(https://onmyblock.com/media/images/capa.png) no-repeat; background-size: 280px 210px; width: 280px; text-align: center;" class="browse_item" place_id="0"><div style="margin-top: 14%;text-align: center;"><div style="width: 20px;height: 90px;float: left;"></div><div style="height: 90px;width: 20px;float: right;"></div><span style="font-size: 16px; margin: auto; color: black;">Find your next college pad through the people you know.</span><hr style="border: none; background-color: none; height: 0px;"><span style="margin: auto; padding: 6px;color:#51443c;">Find out where your classmates and friends live.</span></div><div <?php if ($_COOKIE['omb_session']=='loggedin'){?> id="optionsf" <?php }else{ ?> onClick="openPopup()"  <?php }?>  data-option-value="friends" style="margin-top: 20px;" ><a id="buttonFb" class="buttonFb blue-button" style="width: 160px; text-align:center; padding:5px; font-size:12px;">See Your Friend´s Houses</a></div></div><?php }?><?php }?><?php } ?></div></div>
<script type="text/javascript">
$(document).ready(function() {
	
	$(".myblock_tipsy").live('click', function(){	
		
		var sel = $(this).attr('id');
		var data = $(this).attr('data-animation');
		
		var antes = $('#imgm_'+sel).text();
		
		if($.cookie("omb_session")=="loggedin")
		{
			if(data==0){
				
				$("#imgm_"+sel).text("");
				$("#imgm_"+sel).text("Added to Favorites");	

			}else{
				$("#imgm_"+sel).text("");
				$('#imgm_'+sel).text('Add to Favorites');
			}
		}
	});
	
	
	$(".browselike").live('click', function() {
		
		var sel = $(this).attr('id');
		var data = $(this).attr('data-animation');
		
		var antes = $('#imgv_'+sel).text();
		
		if($.cookie("omb_session")=="loggedin")
		{
			if(data == 0){
				$("#imgv_"+sel).text("");
				$('#imgv_'+sel).text('Liked');
			}else{
				//alert('olakase');
				$("#imgv_"+sel).text("");
				$('#imgv_'+sel).text('Like');
			}
		}
	});

});
</script> 
<?php echo $_smarty_tpl->getSubTemplate ('../footer4.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
 
<?php }} ?>