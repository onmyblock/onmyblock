<?php /* Smarty version Smarty-3.1.11, created on 2013-06-25 12:18:46
         compiled from "/home/myblock/public/layout/pages/place.tpl" */ ?>
<?php /*%%SmartyHeaderCode:177125211951c9ca80a49e71-97758004%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2ce4f223f230706bc2a129ea61288b328564b426' => 
    array (
      0 => '/home/myblock/public/layout/pages/place.tpl',
      1 => 1372187921,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '177125211951c9ca80a49e71-97758004',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51c9ca80e97ef1_03248989',
  'variables' => 
  array (
    'place' => 0,
    'viewer' => 0,
    'photo' => 0,
    'time' => 0,
    'onmyblock' => 0,
    'pl' => 0,
    'likes' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51c9ca80e97ef1_03248989')) {function content_51c9ca80e97ef1_03248989($_smarty_tpl) {?><!doctype html>
<!--[if lt IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html lang="es" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="es" class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<title><?php echo $_smarty_tpl->tpl_vars['place']->value['title'];?>
</title>
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="author" content="">
		<!-- //metas para facebook -->
		<meta property="og:title" content="">
		<meta property="og:description" content="">
		<meta property="og:type" content="website">
		<meta property="og:url" content="">
		<meta property="og:image" content=" ">
		<meta property="og:site_name" content="">

		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
		<link rel="stylesheet" href="/media/css/main.css?v=1">
		<link rel="stylesheet" href="/media/css/coin-slider-styles.css" />
		

		<script src="/media/js/libs/modernizr-2.0.6.min.js"></script>
		<script>
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<style type="text/css">
			.cs-prev{
				background: url(http://54.241.12.168/media/images/pg_prev.png) no-repeat 0 0 !important;
				color: #FFFFFF !important;
				padding: 0 10px !important;
				width: 263px !important;
				height: 397px !important;
				background-position:4px 168px !important;
				margin-top: 9px !important;				
			}

			.cs-next{
				background: url(http://54.241.12.168/media/images/pg_next.png) no-repeat 0 0 !important;
				color: #FFFFFF !important;
				padding: 0 10px !important;
				width: 264px !important;
				height: 397px !important;
				background-position:235px 168px !important;
				margin-top: 9px !important;				
			} 

		</style>
	</head>
	<body>
		<header>
			<article>
				<div class="logo"></div>
				<?php echo $_smarty_tpl->getSubTemplate ('../header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

			</article>
		</header>    

		<div id="wrapper">
			<section id="section-6">	
				<article>
					<div id="boxdata">
						<div class="datatabs">
							<div id="mainplace">
								<div class="titleplace">
									<span><?php echo $_smarty_tpl->tpl_vars['place']->value['name'];?>
</span>
									<a href=""><?php echo $_smarty_tpl->tpl_vars['place']->value['neighborhood'];?>
</a>
								</div>
								<div id="<?php echo $_smarty_tpl->tpl_vars['place']->value['id'];?>
" class="sociallog">
									<div data="1" style="display:<?php if ($_smarty_tpl->tpl_vars['viewer']->value['wishlisted']!=1){?>block<?php }else{ ?>none<?php }?>;" class="addfav btnFav" ><a href="javascript:">Add to Favorites</a></div>
									<div data="0" style="display:<?php if ($_smarty_tpl->tpl_vars['viewer']->value['wishlisted']==1){?>block<?php }else{ ?>none<?php }?>;" class="bgadd"><a href="javascript:">Added to Favorites</a></div>
									<div data="1" style="display:<?php if ($_smarty_tpl->tpl_vars['viewer']->value['liked']!=1){?>block<?php }else{ ?>none<?php }?>;" class="likebtn  btnLike" ><a href="javascript:">Like</a></div>
									<div data="0" style="display:<?php if ($_smarty_tpl->tpl_vars['viewer']->value['liked']==1){?>block<?php }else{ ?>none<?php }?>;" class="bgliked "><a href="javascript:">Liked</a></div>
								</div>
								<!--div class="sociallog" >
									<div class="addfav btnFav" data-placeid="<?php echo $_smarty_tpl->tpl_vars['place']->value['id'];?>
" data="" ><a href="">Add to Favorites</a></div>
									<div class="likebtn btnLike" data-placeid="<?php echo $_smarty_tpl->tpl_vars['place']->value['id'];?>
" data="" ><a href="">Like</a></div>
								</div-->
							</div>
							<div class="tabsline">
								<a id="datphotos" class="on" href="javascript:">Photos</a>
								<a id="datmap" href="javascript:">Map</a>
								<a id="datstreet" href="javascript:">From the Street</a>
								<!--a id="datedit" href="javascript:">Edit Photos</a-->     
							</div>
							<div id="contenttabsline">
								<div class="datphotos">
									<div id="coin-slider">
									   <?php  $_smarty_tpl->tpl_vars['photo'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['photo']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['place']->value['photos']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['photo']->key => $_smarty_tpl->tpl_vars['photo']->value){
$_smarty_tpl->tpl_vars['photo']->_loop = true;
?>
										   <img src="https://s3-us-west-1.amazonaws.com/onmyblock/places/<?php echo $_smarty_tpl->tpl_vars['place']->value['id'];?>
/<?php echo $_smarty_tpl->tpl_vars['photo']->value[0];?>
?time=<?php echo $_smarty_tpl->tpl_vars['time']->value;?>
"  />
									   <?php } ?>
									</div>     
								</div>
								
								<div style="display:none;" class="datmap">
									<div class="showp" id="map" style="width:550px; height:420px;"></div> 
								</div>
								<div style="display:none;" class="datstreet">
									<div class="showp" id="place_street_view" style="width:550px; height:420px;"></div>
								</div>
								<div style="display:none;" class="datedit">
									<div class="bgedit">
										<div class="imgeditprof">
											<img src="images/profile.jpg" />
										</div>
										<div class="dataeditpro">
											<form enctype="multipart/form-data" method="post" action="photos?place_id=1010037142">
												<input id="upload_photo" type="file" name="file" />
												<input class="silver-button" type="submit" style="float:right;" value="Upload">
											</form>
											<a class="edit_photo_profile" target="_parent" href="place?id=1010037142">Save</a>
											<span>Max Size: 4MB</span>

										</div>

									</div>
								</div>
							</div>    
						</div>

						<div id="carrusel">
							<div class="namecarrusel">Neighbors</div>
							<div class="moveimages">
								<ul>
									<?php  $_smarty_tpl->tpl_vars['pl'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['pl']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['place']->value['neighbors']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['pl']->key => $_smarty_tpl->tpl_vars['pl']->value){
$_smarty_tpl->tpl_vars['pl']->_loop = true;
?>
									<li class="noborder">
										<a href="<?php echo $_smarty_tpl->tpl_vars['onmyblock']->value;?>
/place?id=<?php echo $_smarty_tpl->tpl_vars['pl']->value['id'];?>
"> 
											<img src="https://s3-us-west-1.amazonaws.com/onmyblock/places/<?php echo $_smarty_tpl->tpl_vars['pl']->value['id'];?>
/profile.jpg"  height="412px" />
											<div class="dat-1"><?php echo $_smarty_tpl->tpl_vars['pl']->value['street'];?>
 <?php echo $_smarty_tpl->tpl_vars['pl']->value['route'];?>
</div>
											<div class="dat-2">$<?php echo $_smarty_tpl->tpl_vars['pl']->value['price'];?>
-<?php echo $_smarty_tpl->tpl_vars['pl']->value['rooms'];?>
br</div>
											<div class="dat-3"><?php echo $_smarty_tpl->tpl_vars['pl']->value['distance'];?>
mi away</div>
										</a>
									</li>
									<?php } ?>
								</ul>
							</div>
						</div>
						<div id="datatabs02">
							<div class="tabsline tabsline2">
								<a id="datphotos2" class="on" href="javascript:">Wall</a>
								<a id="datmap2" href="javascript:">Real Estate Facts</a>
								<a id="datstreet2" href="javascript:">Description</a>
								<!--a id="datedit2" href="javascript:">Edit Info</a-->     
							</div>
							<div id="contenttabsline2">
								<div class="datphotos2">
									<input type="text" placeholder="Give a review on this house!" size="80" />
									<div class="btnpost">Post</div>

									<div id="dataposted">

										<div class="alldata">    

											<!--div class="imgface">
												<img src="images/picture.jpg" />
											</div>

											<div class="contentpost">
												<div class="usertitle">
													<a class="nameuser" href="">Ronald Mori</a>
												</div>
												<span id="contposted">Hola</span>
											</div>

											<div class="likes">
												<div class="silike">

												</div>
												
												<div class="numlike">
													0
												</div>

											</div>
											<div class="now">Now</div>

											<div style="display:none;" id="replydat">
												<input class="inpreply" type="text" size="40" />
												<div class="btnpost">Reply</div>
											</div-->

										</div>

										<!--div class="nopost hidden">
											This place has no entries. Be the fisrt
										</div-->


									</div>
								</div>
								<div style="display:none;" class="datmap2">
									<div class="col01">
										Street: <?php echo $_smarty_tpl->tpl_vars['place']->value['street'];?>
 <?php echo $_smarty_tpl->tpl_vars['place']->value['route'];?>
<br/>
										City: <?php echo $_smarty_tpl->tpl_vars['place']->value['city'];?>
, <?php echo $_smarty_tpl->tpl_vars['place']->value['state'];?>
 <br/>
										Postal Code: <?php echo $_smarty_tpl->tpl_vars['place']->value['postal'];?>
 <br/>
										Neighborhood: <?php echo $_smarty_tpl->tpl_vars['place']->value['neighborhood'];?>
 <br/>
									</div>

									<div class="col02">
										Rent: <?php echo $_smarty_tpl->tpl_vars['place']->value['price'];?>
 <br/>
										Bedrooms: <?php echo $_smarty_tpl->tpl_vars['place']->value['rooms'];?>
 <br/>
										Bathrooms: <?php echo $_smarty_tpl->tpl_vars['place']->value['bathrooms'];?>
 <br/>
										Realtor: <?php echo $_smarty_tpl->tpl_vars['place']->value['realtor']['phone'];?>
 <br/>                       
									</div>
								</div>
								<div style="display:none;" class="datstreet2">
									<div class="col03">
										<?php echo $_smarty_tpl->tpl_vars['place']->value['description'];?>

									</div>
								</div>
								<div style="display:none;" class="datedit2">
									<div class="titleform">House Information</div>    

									<div id="formedit">

										<div class="colleft">
											<div>House Name:</div>
											<input id="housename" type="text" value="<?php echo $_smarty_tpl->tpl_vars['place']->value['name'];?>
" />

											<div>Neighborhood:</div>
											<input id="neighborhood" type="text" value="<?php echo $_smarty_tpl->tpl_vars['place']->value['neighborhood'];?>
" />

											<div>Unit Number:</div>
											<input id="number" type="text" value="<?php echo $_smarty_tpl->tpl_vars['place']->value['name'];?>
"  />

											<div>House Size (ft):</div>
											<input id="housesize" maxlength="4" type="text" value="<?php echo $_smarty_tpl->tpl_vars['place']->value['size'];?>
"  />
										</div>


										<div class="colright">
											<div>Monthly Rent:</div>
											<input id="monthly" maxlength="4" type="text"  value="<?php echo $_smarty_tpl->tpl_vars['place']->value['size'];?>
"  />

											<div>Number of Bedrooms:</div>
											<input id="numbed" maxlength="1" type="text" value="" />

											<div>Number of Bathrooms:</div>
											<input id="numbad" maxlength="1" type="text" value="" />
										</div>

										<div class="description">Description:</div>
											<textarea id="normal"><?php echo $_smarty_tpl->tpl_vars['place']->value['description'];?>
</textarea>

										<div class="titleform">Realtor Information</div>

											<div class="colleft">
												<div>Realtor Name:</div>
												<input id="namerealtor" maxlength="40" type="text" value="<?php echo $_smarty_tpl->tpl_vars['place']->value['realtor']['name'];?>
" />

												<div>Phone Number:</div>
												<input id="numphone" maxlength="20" type="text" value="<?php echo $_smarty_tpl->tpl_vars['place']->value['realtor']['phone'];?>
" />                                        
											</div>

											<div class="colright">
												<div>Email Address:</div>
												<input id="email" maxlength="40" type="text" value="<?php echo $_smarty_tpl->tpl_vars['place']->value['realtor']['email'];?>
" />
												<div class="error"></div>

												<div>Website:</div>
												<input id="website" maxlength="60" type="text" value="<?php echo $_smarty_tpl->tpl_vars['place']->value['realtor']['link'];?>
" />

											</div>
									   
										<div class="titleform">Address</div>
										<div class="sumitform">
											<div>Address:</div>
											<input id="address_value" class="place_edit_text" value="" name="address" placeholder="4502 New Hampshire St, San Diego, CA 92116" />

											<a id="find-address" class="btnfind">Find</a>

											<div id="save" class="save">Save</div>
											<div id="cancel" class="cancel">Cancel</div>
										</div> 
									</div>
								</div>
							</div>   
						</div>
					</div>

					<div id="boxdatarigth">
						<div id="dataviews">
							<div class="datalog">
								<div class="icon1">
									<span id="base1">$<?php echo $_smarty_tpl->tpl_vars['place']->value['price'];?>
</span>  
								</div>
								<div class="icon2">                          
									<span id="base1"><?php echo $_smarty_tpl->tpl_vars['place']->value['rooms'];?>
</span>
								</div>
								<div class="icon3">
									<span id="base1"><?php if (isset($_smarty_tpl->tpl_vars['place']->value['bathrooms'])){?><?php echo $_smarty_tpl->tpl_vars['place']->value['bathrooms'];?>
<?php }else{ ?>N/A<?php }?></span>
								</div>
							</div>

							<div class="datuser">
								<ul>
									<li><div class="nolikes"><?php if ($_smarty_tpl->tpl_vars['likes']->value['total']<1){?> No Likes <?php }elseif($_smarty_tpl->tpl_vars['likes']->value['total']==1){?>1 Like<?php }else{ ?><?php echo $_smarty_tpl->tpl_vars['likes']->value['total'];?>
 Likes<?php }?></div></li>
									<li><div class="views"><div id="numviews"><?php echo $_smarty_tpl->tpl_vars['place']->value['views'];?>
</div> Views</div></li>
									<li><div class="nocomm"><a href="">No comments</a></div></li>
									<li><div class="nobadges"><a href=""><?php if ($_smarty_tpl->tpl_vars['place']->value['badges']!=0){?><?php echo $_smarty_tpl->tpl_vars['place']->value['views'];?>
<?php }else{ ?>No<?php }?> badges</a></div></li>
									<li><div class="sharefb"><a href="">Share On Facebook</a></div></li>
									<li><div class="linkdat">omb.cm/FG</div></li>
								</ul>
							</div>
						</div>

						<div id="information">
							<div class="namecarrusel">Realtor Information</div>

							<div class="datarealtor">
							   Post to: <a target="_blank" href="http://sandiego.craigslist.org/<?php echo $_smarty_tpl->tpl_vars['place']->value['type'];?>
">Craigslist 1 month ago</a><br/><br/>
							   <span>Phone:</span><br/><br/>
							   <div id="numberphone">(619) 225-8200</div> 
							</div>
						</div>

						<div id="badges">
							<div class="namebadges">Badges</div>                    

							<div class="databadges">
								Give a review on this house!
							   <div class="listbadges">
									<ul id="<?php echo $_smarty_tpl->tpl_vars['place']->value['id'];?>
">
										<li class="bad01 bdgs tooltip" data="0" data-status="0" ><span>Fraternity House</span></li>                                
										<li class="border"></li>                         
										<li class="bad02 bdgs tooltip" data="1" data-status="0" ><span>Sorority House</span></li>

										<li class="bad03 bdgs tooltip" data="2" data-status="0" ><span>Party House</span></li>
										<li class="border"></li>                                
										<li class="bad04 bdgs tooltip" data="3" data-status="0" ><span>Nerd House</span></li>

										<li class="bad05 bdgs hide tooltip" data="4" data-status="0" ><span>Beach House</span></li>
										<li class="border bdgs hide"></li>                                      
										<li class="bad06 bdgs hide tooltip" data="5" data-status="0" ><span>Apartment Complex</span></li>
										
										<li class="bad07 bdgs hide tooltip" data="6" data-status="0" ><span>Luxury Living</span></li>
										<li class="border hide"></li>                
										<li class="bad08 bdgs hide tooltip" data="7" data-status="0" ><span>Close to School</span></li>                               
									</ul>
									<div class="viewmore">view more</div>
									<div class="viewless hide">view less</div>
							   </div>                        
							</div>
						</div>
						<div id="likesplace01" >
							<div class="namelikeplace">Likes</div>                    
							<div class="dataplace">
							   Be the first to Like this place!
							</div>   
							<div style="display:<?php if ($_smarty_tpl->tpl_vars['viewer']->value['liked']!=1){?>block<?php }else{ ?>none<?php }?>;" class="likebtn" >
								<a class="btnLike" data-placeid="<?php echo $_smarty_tpl->tpl_vars['place']->value['id'];?>
" href="javascript:">Like</a>
							</div>
							<div style="display:<?php if ($_smarty_tpl->tpl_vars['viewer']->value['liked']==1){?>block<?php }else{ ?>none<?php }?>;" class="bgliked">
                        		<a href="javascript:" data-placeid="<?php echo $_smarty_tpl->tpl_vars['place']->value['id'];?>
">Liked</a>
                    		</div>
						</div>
						<!--div id="deleteplace">
							<div class="namedeleteplace">Delete Place</div>                                        
							<a href="javascript;">Delete</a>            
						</div-->  
					</div>
				</article>
			</section>
		</div>
		<footer>
			<article>
			<div class="sublogo">OnMyBlock</div>        
				<nav>
					<ul>
						<li class="nocircle"><a id="howitworkslk" href="howitworks">How it Works</a></li>
						<li><a href="help">Help</a></li>
						<li><a href="team">Team</a></li>
						<li><a href="careers">Careers</a></li>
						<li><a href="privacy">Privacy Policy</a></li>
						<li><a href="terms">Terms of Service</a></li> 
					</ul>
				</nav>
			</article>
		</footer>    

		<script src="/media/js/libs/jquery-1.9.0.min.js"></script>
		<script src="//connect.facebook.net/en_US/all.js"></script>		
		<script src="/media/js/libs/coin-slider.js"></script> 
		<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript" src="/media/js/libs/gmap3.min.js"></script>
		<script src="/media/js/initr.js?v=1"></script>
        <script src="/media/js/main.js"></script>
		<script>
			(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=561406290571363";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>
		
		<script>
		$(document).ready(function(){
			$('#coin-slider').coinslider({ width: 500, height: 310, navigation: true, delay: 5000, links : false }); 
		});
		</script>

		<script type="text/javascript">
			$('#datmap').click(function(){

				$('.datmap').fadeIn('slow');
				$('.datmap').show();

				$('#datmap').addClass('on');

				$('#datphotos').removeClass('on');
				$('#datstreet').removeClass('on');
				$('#datedit').removeClass('on');

				$('.datphotos').hide();
				$('.datstreet').hide();
				$('.datedit').hide();   
				
				$("#map").gmap3({
					action: 'addMarker',
					address: "<?php echo $_smarty_tpl->tpl_vars['place']->value['street'];?>
 <?php echo $_smarty_tpl->tpl_vars['place']->value['route'];?>
, <?php echo $_smarty_tpl->tpl_vars['place']->value['city'];?>
, <?php echo $_smarty_tpl->tpl_vars['place']->value['state'];?>
 <?php echo $_smarty_tpl->tpl_vars['place']->value['postal'];?>
",
					map:{
						center: true,
						zoom: 14,
						mapTypeId: google.maps.MapTypeId.MAP,
						mapTypeControl: true,
						navigationControl: true,
						scrollwheel: true,
						streetViewControl: false
					},
					marker:{
						options:{
							icon:new google.maps.MarkerImage('https://onmyblock.com/media/images/map_icon.png',
							new google.maps.Size(24, 36),
							new google.maps.Point(0,0),
							new google.maps.Point(12, 32)),
							shadow:new google.maps.MarkerImage('https://onmyblock.com/media/images/map_icon_shadow.png',
							new google.maps.Size(19, 13),
							new google.maps.Point(0,0),
							new google.maps.Point(1, 10)),
							draggable: false
							}
					}

				});

			});
			
			$('#datstreet').click(function(){

				$('.datstreet').fadeIn('slow');
				$('.datstreet').show();

				$('#datstreet').addClass('on');

				$('#datphotos').removeClass('on');
				$('#datmap').removeClass('on');
				$('#datedit').removeClass('on');

				$('.datphotos').hide();
				$('.datmap').hide();
				$('.datedit').hide();   
				
				var streetViewService = new google.maps.StreetViewService();
				var STREETVIEW_MAX_DISTANCE = 50;
				var latLng = new google.maps.LatLng(<?php echo $_smarty_tpl->tpl_vars['place']->value['latitude'];?>
, <?php echo $_smarty_tpl->tpl_vars['place']->value['longitude'];?>
);
				streetViewService.getPanoramaByLocation(latLng, STREETVIEW_MAX_DISTANCE, function (streetViewPanoramaData, status) {
					if (status === google.maps.StreetViewStatus.OK) {
						$("#place_street_na").remove();
						$('#place_street_view').gmap3({ action:'init',
						zoom: 14,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						center: [<?php echo $_smarty_tpl->tpl_vars['place']->value['latitude'];?>
, <?php echo $_smarty_tpl->tpl_vars['place']->value['longitude'];?>
]},
							{ action:'setStreetView',
								id: 'place_street_view',
								options:{
									position: [<?php echo $_smarty_tpl->tpl_vars['place']->value['latitude'];?>
, <?php echo $_smarty_tpl->tpl_vars['place']->value['longitude'];?>
],
									pov: {
										heading: 0,
										pitch: 0,
										zoom: 1
									}
								}
							});
						} else {
							$("#street_view_tab").remove();
						}
				}); 
			});
			
			$('#datphotos').click(function(){

				$('.datphotos').fadeIn('slow');
				$('.datphotos').show();

				$('#datphotos').addClass('on');

				$('#datmap').removeClass('on');
				$('#datstreet').removeClass('on');
				$('#datedit').removeClass('on');

				$('.datmap').hide();
				$('.datstreet').hide();
				$('.datedit').hide();   

			});
			
			$('#datedit').click(function(){

				$('.datedit').fadeIn('slow');
				$('.datedit').show();

				$('#datedit').addClass('on');

				$('#datphotos').removeClass('on');
				$('#datmap').removeClass('on');
				$('#datstreet').removeClass('on');

				$('.datphotos').hide();
				$('.datmap').hide();
				$('.datstreet').hide();   

			});
			
		</script>

	</body>
</html><?php }} ?>