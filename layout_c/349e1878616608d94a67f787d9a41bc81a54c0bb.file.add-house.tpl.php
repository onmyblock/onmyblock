<?php /* Smarty version Smarty-3.1.11, created on 2013-06-25 17:27:21
         compiled from "/home/myblock/public/layout/pages/add-house.tpl" */ ?>
<?php /*%%SmartyHeaderCode:162010752451c9d40c0a9406-32514141%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '349e1878616608d94a67f787d9a41bc81a54c0bb' => 
    array (
      0 => '/home/myblock/public/layout/pages/add-house.tpl',
      1 => 1372206381,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '162010752451c9d40c0a9406-32514141',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51c9d40c3fa503_36813469',
  'variables' => 
  array (
    'step' => 0,
    'address' => 0,
    'slow' => 0,
    'placeId' => 0,
    'place' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51c9d40c3fa503_36813469')) {function content_51c9d40c3fa503_36813469($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ('../header4.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<link rel="stylesheet" href="https://onmyblock.com/media/css/savecss/sexyalertbox.css" type="text/css"/>
<script src="https://onmyblock.com/media/js/libs/savejs/sexyalertbox.v1.2.jquery.mini.js"></script>
<?php if ($_smarty_tpl->tpl_vars['step']->value==2){?>

<script>
$(document).ready(function() {

	statusImg = localStorage.status;
	
	if(statusImg==1)
	{
		Sexy.alert('Please upload at least one picture of the house!'); 
	}
	

	$("#add_house_map").gmap3({
		action: 'addMarker',
		address: "<?php echo $_smarty_tpl->tpl_vars['address']->value['street'];?>
 <?php echo $_smarty_tpl->tpl_vars['address']->value['route'];?>
 <?php echo $_smarty_tpl->tpl_vars['address']->value['city'];?>
, <?php echo $_smarty_tpl->tpl_vars['address']->value['state'];?>
 <?php echo $_smarty_tpl->tpl_vars['address']->value['postal'];?>
",
		map:{
			center: true,
			zoom: 14,
			mapTypeId: google.maps.MapTypeId.MAP,
			mapTypeControl: false,
			navigationControl: false,
			scrollwheel: false,
			streetViewControl: false
		},
		marker:{
			options:{
				icon:new google.maps.MarkerImage('https://onmyblock.com/media/images/map_icon.png',
				new google.maps.Size(24, 36),
				new google.maps.Point(0,0),
				new google.maps.Point(12, 32)),
				shadow:new google.maps.MarkerImage('https://onmyblock.com/media/images/map_icon_shadow.png',
				new google.maps.Size(19, 13),
				new google.maps.Point(0,0),
				new google.maps.Point(1, 10)),
				draggable: false
				}
		}

	});
	
	$("#BoxAlertBtnOk").on("click",function(){
		url = "http://54.241.12.168/add-house";
	
		//$(location).attr('href',url);
	});
});
</script>
<?php }?>
<script>
$(document).ready(function() {

	var statusPlace = "<?php echo $_smarty_tpl->tpl_vars['slow']->value;?>
";

	if(statusPlace==1)
	{
		Sexy.alert('House already exists at this location! You can view the house profile if you would like.<a class="wishlistBlank" href="https://onmyblock.com/place?id=<?php echo $_smarty_tpl->tpl_vars['placeId']->value;?>
" target="_blank"  style="margin-left: 68px;margin-top: 42px;float: right;position: absolute;left: 188px;top: 20px;width:117px;"><p style="margin-left: 14px;" class="noselect">View Profile</p></a> '); 
	}

	var input = document.getElementById('searchTextField');     
	var autocomplete = new google.maps.places.Autocomplete(input, {
		types: ["geocode"],
		componentRestrictions: {country: 'USA'}
	});
	
	
});
</script>
<div id="main" style="text-align:center;width: 100%;"><div style="margin: auto;border-top: 3px solid #CCC;margin-top: 30px;width: 920px;"><h6 style="margin: auto;margin-top: -14px;background: #f6f6f6;width: 366px;color: black;font-size: 45px;" >Create a House</h6></div><?php if ($_smarty_tpl->tpl_vars['step']->value<2){?><div style="position:relative; height: 206px; margin: auto; width: 454px; padding:186px 0;"><?php if ($_smarty_tpl->tpl_vars['step']->value==1){?><!--h3> We're sorry, but the address is not correct. Please try again.</h3--><br /><?php }?><form method="post" action="add-house" id="form-1" class="jqTransform"><input type="text" style="width:350px;padding: 6px 5px;" id="searchTextField" value=""  name="address"><input type="submit" class="blue-button" style="padding: 8px 14px; top:1px;" value="Continue"><h5 style="color:#ababab; font-size:11px; margin-left:10px;">Example: 1234 Main st. San Diego</h5></form></div><?php }elseif($_smarty_tpl->tpl_vars['step']->value==2){?><div id="add_house_map" style="width:100%; height:420px; position:relative; top:-36px; border-bottom:1px solid #ccc;"></div><div style="position:relative;margin-left:50%; left:-400px; padding-bottom:30px;"><form method="post" action="add-house"><input type="text" style="width:350px;float: center;" id="add_house_address" value="<?php echo $_smarty_tpl->tpl_vars['address']->value['street'];?>
 <?php echo $_smarty_tpl->tpl_vars['address']->value['route'];?>
, <?php echo $_smarty_tpl->tpl_vars['address']->value['city'];?>
, <?php echo $_smarty_tpl->tpl_vars['address']->value['state'];?>
 <?php echo $_smarty_tpl->tpl_vars['address']->value['postal'];?>
 " name="address"><input type="submit" class="blue-button" style="padding:8px 15px; top:-4px; right:170px;" value="Change"></form><div style="margin: 10px 20px;"><form method="post" action="add-house"><!--input type="radio" name="resident" value="current"><h5 style="display:inline-block; padding-right:10px; padding-left:5px;">I live here</h5><input type="radio" name="resident" value="past"><h5 style="display:inline-block; padding-right:10px; padding-left:5px;">I used to live here</h5><input type="radio" name="resident" checked="true" value="none"><h5 style="display:inline-block; padding-right:10px; padding-left:5px;">I don't live here</h5--><div style="width:600px; margin-top:50px;"><h1 style="display:inline-block;">House Information </h1><h5 style="display:inline-block; padding-top:3px;">(Optional)</h5><br /><div class="place_edit_co"><div class="place_edit_item"><h5>House Name: </h5><input type="text" class="place_edit_text" name="house_name" value="<?php echo $_smarty_tpl->tpl_vars['place']->value['name'];?>
"/></div><div class="place_edit_item"><h5>Unit Number: </h5><input type="text" class="place_edit_text" name="house_unit" value="<?php echo $_smarty_tpl->tpl_vars['place']->value['unit'];?>
"/></div><div class="place_edit_item"><h5>House Size (ft): </h5><input type="text" class="place_edit_text" name="house_size" value="<?php echo $_smarty_tpl->tpl_vars['place']->value['size'];?>
"/></div><div class="place_edit_item"><h5>Neighborhood: </h5><input type="text" class="place_edit_text" name="house_neighborhood" value="<?php echo $_smarty_tpl->tpl_vars['address']->value['neighborhood'];?>
"/></div></div><div class="place_edit_co"><div class="place_edit_item"><h5>Monthly Rent: </h5><input type="text" class="place_edit_text" name="house_price" value="<?php echo $_smarty_tpl->tpl_vars['place']->value['price'];?>
"/></div><div class="place_edit_item"><h5>Number of Bedrooms: </h5><input type="text" class="place_edit_text" name="house_rooms" value="<?php echo $_smarty_tpl->tpl_vars['place']->value['rooms'];?>
"/></div><div class="place_edit_item"><h5>Number of Bathrooms: </h5><input type="text" class="place_edit_text" name="house_bathrooms" value="<?php echo $_smarty_tpl->tpl_vars['place']->value['bathrooms'];?>
"/></div></div><h1 style="display:inline-block;">Realtor Information </h1><h5 style="display:inline-block; padding-top:3px;">(Optional)</h5><br /><div class="place_edit_co"><div class="place_edit_item"><h5>Realtor Name: </h5><input type="text" class="place_edit_text" name="realtor_name" value="<?php echo $_smarty_tpl->tpl_vars['place']->value['realtor']['name'];?>
"/></div><div class="place_edit_item"><h5>Phone Number: </h5><input type="text" class="place_edit_text" name="realtor_phone" value="<?php echo $_smarty_tpl->tpl_vars['place']->value['realtor']['phone'];?>
"/></div></div><div class="place_edit_co"><div class="place_edit_item"><h5>Email Address: </h5><input type="text" class="place_edit_text" name="realtor_email" value="<?php echo $_smarty_tpl->tpl_vars['place']->value['realtor']['email'];?>
"/></div><div class="place_edit_item"><h5>Website: </h5><input type="text" class="place_edit_text" name="realtor_link" value="<?php echo $_smarty_tpl->tpl_vars['place']->value['realtor']['link'];?>
"/></div><input type="text" name="create_form" value="create" style="display:none" /><input type="text" name="address" value="<?php echo $_smarty_tpl->tpl_vars['address']->value['street'];?>
 <?php echo $_smarty_tpl->tpl_vars['address']->value['route'];?>
 <?php echo $_smarty_tpl->tpl_vars['address']->value['city'];?>
, <?php echo $_smarty_tpl->tpl_vars['address']->value['state'];?>
 <?php echo $_smarty_tpl->tpl_vars['address']->value['postal'];?>
" style="display:none" /></div><h1 style="display:inline-block;">Images </h1><h5 style="display:inline-block; padding-top:3px;">(Required)</h5><iframe id="photo_iframe" src="/photos_temp"  scrolling="no" marginwidth="0" marginheight="0" frameborder="0" vspace="0" hspace="0" style="overflow:visible; width:100%;height: 454px;"></iframe><input type="submit" class="blue-button" value="Create" style="width:80px; margin-left:50%; left:-40px; position:relative" /></form></div></div></form></div><?php }?></div><?php echo $_smarty_tpl->getSubTemplate ('../footer4.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>
<?php }} ?>