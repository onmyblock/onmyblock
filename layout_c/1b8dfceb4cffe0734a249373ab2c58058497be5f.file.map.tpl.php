<?php /* Smarty version Smarty-3.1.11, created on 2013-06-25 10:05:00
         compiled from "/home/myblock/public/layout/pages/map.tpl" */ ?>
<?php /*%%SmartyHeaderCode:77041473851c9cdbc5b1e85-94627743%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1b8dfceb4cffe0734a249373ab2c58058497be5f' => 
    array (
      0 => '/home/myblock/public/layout/pages/map.tpl',
      1 => 1371594096,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '77041473851c9cdbc5b1e85-94627743',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'rooms' => 0,
    'price' => 0,
    'focus' => 0,
    'center' => 0,
    'zoom' => 0,
    'places' => 0,
    'place' => 0,
    'pricemax' => 0,
    'pricemin' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51c9cdbc6b5d25_49951587',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51c9cdbc6b5d25_49951587')) {function content_51c9cdbc6b5d25_49951587($_smarty_tpl) {?><!DOCTYPE html>

<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/media/css/main.css?v=1" />  
    <link rel="stylesheet" href="/media/css/style.css" />
	<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
	<script src="https://maps.google.com/maps/api/js?libraries=places&region=uk&language=en&sensor=false"></script>
	<link type="image/x-icon" href="/media/images/map_icon_tab.png" rel="shortcut icon" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" ></script>
	<script src="/media/js/libs/gmap3.min.js"></script>
	<script src="/media/js/main.js"></script>
	<script src="/media/js/libs/jquery-ui-1.10.3.custom.min.js"></script>
	<link rel="stylesheet" href="/media/css/jquery-ui-1.10.3.custom.min.css" type="text/css"/>
	
	<style type="text/css">
		article{margin: 0 auto;margin-left: -500px;}
		header{background: #F6F6F6;}
	</style>
	
</head>	
	<body>
		
		<header>
			<article>
				<a href="/"><div class="logo"></div></a>
				<?php echo $_smarty_tpl->getSubTemplate ('../header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

			</article>
		</header> 


<script>

$(document).ready(function() {

	backMap = localStorage.getItem('backMap');
	var pageBack = localStorage.getItem('pageBack');
	
	if(pageBack==1)
	{
		var neighborhoodPopular = localStorage.getItem('neighborhood');
		
		getAddress(neighborhoodPopular);
		
		localStorage.setItem('pageBack', 0);
	}
	
	if(backMap==1)
	{
		localStorage.setItem('backMap', 0);
		
		var search = JSON.parse(localStorage.placeSearch);
		
		getAddress(search.inputSearch);
	}
	
	localStorage.setItem('price', "");
	localStorage.setItem('room', "");
	
	var r = '<?php echo $_smarty_tpl->tpl_vars['rooms']->value;?>
';
	var p = '<?php echo $_smarty_tpl->tpl_vars['price']->value;?>
';
	
	
	var cadena="0,0,0,0";
	
	if(r!='')
	{
		array = cadena.split(",");
		
		if(r==1){
			array[0]=1;
		}
		if(r==2){
			array[1]=2;
		}
		if(r==3){
			array[2]=3;
		}
		if(r==4){
			array[3]=4;
		}
	
		var cadenaFinal = array[0]+","+array[1]+","+array[2]+","+array[3];
	
		localStorage.setItem('price', cadenaFinal);
	}
	
	if(p!='')
	{
		array = cadena.split(",");
	
		if(p==1){
			array[0]=1;
		}
		if(p==2){
			array[1]=2;
		}
		if(p==3){
			array[2]=3;
		}
		if(p==4){
			array[3]=4;
		}
	
		var cadenaFinal = array[0]+","+array[1]+","+array[2]+","+array[3];
		
		localStorage.setItem('room', cadenaFinal);
	}
		
	$(".room").on("click",function(){
		svcPlaces($(this));
	});
	
	/* if($(document).getUrlParam("location") && !$(document).getUrlParam("latitude")){
		getAddress($('#searchTextField').val());
	} else { */
		
		

		$("#searchTextField").keyup(function(event){
			if(event.keyCode == 13){
				getAddress($('#searchTextField').val());
			}
		});

		$('#slide_submit_search').click(function(){
			getAddress($('#map_search_city').val());
		});

		$('#map').gmap3({
			action: 'init',
			options: {
				
				<?php if ($_smarty_tpl->tpl_vars['focus']->value){?>
				center: [<?php echo $_smarty_tpl->tpl_vars['center']->value['latitude'];?>
, <?php echo $_smarty_tpl->tpl_vars['center']->value['longitude'];?>
],
				zoom: <?php echo $_smarty_tpl->tpl_vars['zoom']->value;?>
,
				<?php }else{ ?>
				center: [32.761904, -117.175755],
				zoom: 13,
				<?php }?>
				
				//minZoom: 11,
				mapTypeId: google.maps.MapTypeId.MAP,
				mapTypeControl: false,
				streetViewControl: false
			}
		}, {
			action: 'addMarkers',
			radius: 100,
			markers: [
			
			<?php  $_smarty_tpl->tpl_vars['place'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['place']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['places']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['place']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['place']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['place']->key => $_smarty_tpl->tpl_vars['place']->value){
$_smarty_tpl->tpl_vars['place']->_loop = true;
 $_smarty_tpl->tpl_vars['place']->iteration++;
 $_smarty_tpl->tpl_vars['place']->last = $_smarty_tpl->tpl_vars['place']->iteration === $_smarty_tpl->tpl_vars['place']->total;
?>
			<?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['place']->value[1];?>
<?php $_tmp1=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['place']->value[2];?>
<?php $_tmp2=ob_get_clean();?><?php ob_start();?><?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
<?php $_tmp3=ob_get_clean();?><?php if ($_tmp1!=''&&$_tmp2!=''&&$_tmp3!=''){?>
			{
				lat: <?php echo $_smarty_tpl->tpl_vars['place']->value[1];?>
,
				lng: <?php echo $_smarty_tpl->tpl_vars['place']->value[2];?>
,
				tag:  'bye',
				data: '<iframe src="map_popup?id=<?php echo $_smarty_tpl->tpl_vars['place']->value[0];?>
" scrolling="no" style="width:400px; overflow:hidden;"></iframe>'
			}
			<?php }?>
			<?php if (!$_smarty_tpl->tpl_vars['place']->last){?>,<?php }?>
			<?php } ?>
			
			],
			marker: {
				
				options: {
						icon:new google.maps.MarkerImage('https://onmyblock.com/media/images/map_icon.png',
						new google.maps.Size(24, 36),
						new google.maps.Point(0,0),
						new google.maps.Point(12, 32)),
						shadow:new google.maps.MarkerImage('https://onmyblock.com/media/images/map_icon_shadow.png',
						new google.maps.Size(19, 13),
						new google.maps.Point(0,0),
						new google.maps.Point(1, 10)),
						draggable: false
				},
				events: {
					click: function(marker, event, data) {

						var map = $(this).gmap3('get'),
							infowindow = $(this).gmap3({
								action: 'get',
								name: 'infowindow'
							});
						if (infowindow) {
							infowindow.close();
							infowindow.open(map, marker);
							infowindow.setContent(data);
				
						} else {
							$(this).gmap3({
								action: 'addinfowindow',
								anchor: marker,
								options: {
									content: data
								}
							});
						}
						
						map.panTo(marker.getPosition());
					}
				}
			},
			clusters: {
				4: {
					content: "<div class='cluster cluster-1'>CLUSTER_COUNT</div>",
					width: 46,
					height: 46
				},  
				20: {
					content: "<div class='cluster cluster-1'>CLUSTER_COUNT</div>",
					width: 46,
					height: 46
				},
				50: {
					content: "<div class='cluster cluster-1'>CLUSTER_COUNT</div>",
					width: 46,
					height: 46
				}
			},
			cluster:{
				radius: 50,
				events: {
					click:function(cluster, event, data) {
						cluster.map.panTo(data.latLng);
						cluster.map.setZoom(cluster.map.getZoom()+2); 
					}
				}
			}
		});
		
		$('#map').gmap3({
			action: 'addMarkers',
			markers: [{
				lat: 32.7719176,
				lng: -117.18749939999998,
				tag: "hi",
				data: "down"
			}], 
			marker: {
				options: {
						icon:new google.maps.MarkerImage('https://onmyblock.com/media/images/iconMapUniversity.png',
						new google.maps.Size(36, 36),
						new google.maps.Point(0,0),
						new google.maps.Point(6, 10)),
						shadow:new google.maps.MarkerImage('https://onmyblock.com/media/images/map_icon_shadow.png',
						new google.maps.Size(19, 13),
						new google.maps.Point(0,0),
						new google.maps.Point(1, 10)) ,
						draggable: false
				},
				events:{
					/* scroll: function(marker, event, data){
						
						var zoom = marker.map.getZoom();
						
						alert(zoom); */
						/* $.each( marker, function( key, value ) {
						  alert( key + ": " + value );
						}); */
						/* $('#map').gmap3({
							action: 'clear',
							tag: "hi"
						}); */
					/* } */
				}
			}
		});
	/* } */


	var input = document.getElementById('searchTextField');     
	var autocomplete = new google.maps.places.Autocomplete(input, {
		types: ["geocode"],
		componentRestrictions: {country: 'USA'}
	});

	var cadena="0,0,0,0";
	var i=1;
	var j=1;
	
	function svcPlaces(e)
	{
		$('#lightLoad').css('display','block');
	
		var valPrice = "";
		var valRoom = "";
		var value = e.attr('data-option-value');
		var type = e.attr('data-type');
		
		var $optionSet = e.parents('.browse_sorting_price');
		
		if (e.hasClass('selected')) 
		{
			e.removeClass('selected');
				
			if("room"==type)
			{				
				arrayRoom = localStorage.getItem('room').split(",");
				
				if(value==1){
					arrayRoom[0]=0;
				}
				if(value==2){
					arrayRoom[1]=0;
				}
				if(value==3){
					arrayRoom[2]=0;
				}
				if(value==4){
					arrayRoom[3]=0;
				}
				
				var cadenaRoom = arrayRoom[0]+","+arrayRoom[1]+","+arrayRoom[2]+","+arrayRoom[3];
				
				localStorage.setItem('room', cadenaRoom);
			}
			
		}else
		{
			e.addClass('selected');
			
			if("room"==type)
			{
				if(j==1)
				{
					arrayRoom = cadena.split(",");
					
					j++;
				}
				else
				{
					arrayRoom = localStorage.getItem('room').split(",");
				}
				
				if(value==1){
					arrayRoom[0]=1;
				}
				if(value==2){
					arrayRoom[1]=2;
				}
				if(value==3){
					arrayRoom[2]=3;
				}
				if(value==4){
					arrayRoom[3]=4;
				}
				
				var cadenaRoom = arrayRoom[0]+","+arrayRoom[1]+","+arrayRoom[2]+","+arrayRoom[3];
				
				localStorage.setItem('room', cadenaRoom);
			}
		}
		
		valRoom = localStorage.getItem('room');
		
		
		$.ajax({
			type: "POST",
			url: "svcPlaces",
			data: {
				rooms: valRoom,
				price: valPrice
			}
		}).done(function(data) {

			$('#map').gmap3("clear");
			
			addMarkers(data);
			
			
			$('#lightLoad').css('display','none');
		});
	}
	
	function addMarkers(rs)
	{
		arrayRs = rs.split("|");

		function objectTmp(lat, lng, tag, data)
		{
			this.lat = lat
			this.lng = lng
			this.tag = tag
			this.data = data
		} 
		
		var newArray = new Array();
		
		$.each(arrayRs, function() {
			tmp = this;
			
			array = tmp.split(",");
			
			if(typeof array[3] != "undefined" && typeof array[1] != "undefined" && array[0] != "undefined" )
			{
				myObjectTmp = new objectTmp(array[0], array[1], "bye", array[3]);
				
				newArray.push(myObjectTmp);
			}
		});
		
		
		$('#map').gmap3({
			action: 'addMarkers',
			markers: newArray, 
			radius: 100,
			marker: {
				options: {
						icon:new google.maps.MarkerImage('https://onmyblock.com/media/images/map_icon.png',
						new google.maps.Size(24, 36),
						new google.maps.Point(0,0),
						new google.maps.Point(12, 32)),
						shadow:new google.maps.MarkerImage('https://onmyblock.com/media/images/map_icon_shadow.png',
						new google.maps.Size(19, 13),
						new google.maps.Point(0,0),
						new google.maps.Point(1, 10)),
						draggable: false
				},
				events: {
					click: function(marker, event, data) {
						var map = $(this).gmap3('get'),
							infowindow = $(this).gmap3({
								action: 'get',
								name: 'infowindow'
							});
						if (infowindow) {
							infowindow.close();
							infowindow.open(map, marker);
							infowindow.setContent(data);
						} else {
							$(this).gmap3({
								action: 'addinfowindow',
								anchor: marker,
								options: {
									content: data
								}
							});
						}
					}
				},
				clusters: {
					10: {
						content: '<div class="cluster cluster-1">CLUSTER_COUNT</div>',
						width: 46,
						height: 46
					}
				},
				cluster:{
					radius: 100,
					events: {
						click:function(cluster, event, data) {
							cluster.map.panTo(data.latLng);
							cluster.map.setZoom(cluster.map.getZoom()+2); 
						}
					}
				}
			}
		});	
	}
		
	function centerMap(lat,lng, zoom)
	{
		var latLng = new google.maps.LatLng (lat,lng);
		
		$('#map').gmap3("get").setCenter(latLng);
		
		$('#map').gmap3("get").setZoom(zoom);
	}
	
	$("#btnClose").on("click",function(){
		$("#frame_app").fadeOut("slow");
	});

	function getAddress(input) {
		
		var myArray = input.split(",");
		
		var countArray = myArray.length;
		
		switch(countArray)
		{
			case 1:
					return false;
				break;
			case 2:
					zoom=7;
				break;
			case 3:
					zoom=10;
				break;
			case 4:
					zoom=14;
				break;
		}
		
		geocoder = new google.maps.Geocoder();

		geocoder.geocode({
			'address': input
		}, function(results, status) {
		
			if (status == google.maps.GeocoderStatus.OK) {
				$('#latitude').val(results[0].geometry.location.lat());
				$('#longitude').val(results[0].geometry.location.lng());
				
				lat = results[0].geometry.location.lat(); 
				lng = results[0].geometry.location.lng();
				
				searchLocal = {inputSearch: input, latitude: lat, longitude: lng};
				
				localStorage.placeSearch = JSON.stringify(searchLocal);
				
				$.ajax({
					type: "POST",
					url: "svcPlaces",
					data: {
						location: input,
						latitude: lat,
						longitude: lng
					}
				}).done(function(data) {
					/* $('#map').gmap3({
						action: 'clear',
						tag: "bye"
					}); */ 
					
					centerMap(lat,lng,zoom);
					
					//addMarkers(data);
				});
			} else {
			  alert("We're sorry, but the address is not correct, please try again.");
			}
			
		}); 
	}
	
	var priceMax = '<?php echo $_smarty_tpl->tpl_vars['pricemax']->value;?>
';
	var priceMin = '<?php echo $_smarty_tpl->tpl_vars['pricemin']->value;?>
';
	
    $( "#slider-range" ).slider({
		range: true,
		min: 500,
		max: 5000,
		step: 5,
		values: [ 1500, 4000 ],
		slide: function( event, ui ) {
			$( "#nummin" ).val( "$" + ui.values[ 0 ] );
			
			var priceMax = ui.values[ 1 ];
			
			if(priceMax==5000)
			{
				priceMax="5000+";
			}
			
			$( "#nummax" ).val( "$" + priceMax );
			localStorage.setItem('price', ui.values[ 0 ]+"-"+ui.values[ 1 ]);
			
			
		},
		stop: function( event, ui ) {
			dataStrong();
		}
    });
	
	$('#lightLoad').css('display','none');
	
	function dataStrong()
	{
		valPrice = localStorage.getItem('price');
		valRoom = "";
		
		$('#lightLoad').css('display','block');
		
		$.ajax({
			type: "POST",
			url: "svcPlaces",
			data: {
				rooms: valRoom,
				price: valPrice
			}
		}).done(function(data) {
			
			$('#map').gmap3("clear");
			
			addMarkers(data);
			$('#lightLoad').css('display','none');
		});
	}
	
    $( "#nummin" ).val( "$" + $( "#slider-range" ).slider( "values", 0 )  );
    $( "#nummax" ).val( "$" + $( "#slider-range" ).slider( "values", 1 ) );
	
	
	
}); 

</script>


<div id="main" style="background:#232323;">
	<div class="map_search" style="height:42px;">
		<div class="map_search_internal">
			<form  id="form-1" class="jqTransform" action="" method="get" enctype="multipart/form-data">
				<!--input type="text" border="0" id="map_search_city" name="location"  style="height:20px;" placeholder="Please enter city or neighborhood"/-->
				<input type="text" border="0" id="searchTextField" class="slides_search_city" name="location"  style="height:20px;" placeholder="Please enter city or neighborhood"/>
				<input type="text" id="latitude" name="latitude" style="display:none;" />
				<input type="text" id="longitude" name="longitude" style="display:none;" />

			</form>
			
			<div style="float:left;margin-top: 1px;" class="browse_sorting_container">
				<span style="padding: 5px 12px;margin-top: -5px;margin-right: -4px;color: white !important;">Rooms</span>
				<div style="display:inline-block;">
					<div id="sorting_price" class="browse_sorting_price">
						<ul class="browse_sorting" >
							<a data-option-value="1" data-type="room" class="<?php if ($_smarty_tpl->tpl_vars['rooms']->value==1){?>selected<?php }?> room icon-action align-text-left">1br</a>
							<a data-option-value="2" data-type="room" class="<?php if ($_smarty_tpl->tpl_vars['rooms']->value==2){?>selected<?php }?> room icon-action align-text-center">2br</a>
							<a data-option-value="3" data-type="room" class="<?php if ($_smarty_tpl->tpl_vars['rooms']->value==3){?>selected<?php }?> room icon-action align-text-center">3br</a>
							<a data-option-value="4" data-type="room" class="<?php if ($_smarty_tpl->tpl_vars['rooms']->value==4){?>selected<?php }?> room icon-action align-text-right">4br+</a>
						</ul>
					</div>
				</div>
			</div>
			
			<span id="price" style="margin-top: -1px;margin-left: 8px;position: relative;padding: 5px 14px;background: #d16394;color: #fff !important;border-left: 1px solid #dbdbdb;font-weight: 600;border: black;float: left;">Price</span>  
			<div class="layout-slider" style="width: 210px;position: relative;height: 30px;float: left;padding-left: 11px;padding-right: 13px;margin-top: -6px;">
				<input type="text" id="nummin" style="border: 0; color: white; font-weight: bold;background:transparent;width: 44px;text-align: center;font-size: 10px;background: #aba3a3;border-radius: 4px;" />
				<input type="text" id="nummax" style="border: 0; color: white; font-weight: bold;background:transparent;width: 44px;text-align: center;font-size: 10px;background: #aba3a3;border-radius: 4px;margin-left: 111px;" />
				<div id="slider-range" style="margin-top: 3px;"></div>
			</div>
		</div>
	</div>
	<div style="width:100%; text-align:center;position: absolute; margin-top: -30px;">
		<div id="frame_app" style="position:relative;background:url(https://onmyblock.com/media/images/v0-1/frame_app_map.png) no-repeat; width:669px; height: 58px;z-index: 100; margin: auto; ">
			<a href="https://itunes.apple.com/us/app/onmyblock/id599371074?ls=1&mt=8" target="_blank" ><div style="background:url(https://onmyblock.com/media/images/v0-1/btn_app.png) no-repeat;width:616px;height:51px;margin-top: 6px;margin-left: 20px;position:fixed;"></div></a>
			<div id="btnClose" style="background:url(https://onmyblock.com/media/images/v0-1/btn_close.png) no-repeat;width:19px;height:19px;float: right;margin-right: 13px;margin-top: 18px;"></div>
		</div>
    </div>
	<div id="lightLoad" style="text-align:center;z-index: 1;height: 100%;background: black;width: 100%;position: absolute;margin-top: -36px;opacity: 0.2;">
		<div  style="background-image: url('https://onmyblock.com/media/images/wait.gif');height: 46px;width: 50px;position: absolute;z-index: 2;margin-left: 50%;margin-top: 20%;" ></div>
	</div>
	<div id="map"></div>
</div>

	
<?php echo $_smarty_tpl->getSubTemplate ('../footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbmsl=1&appId=561406290571363";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>



</body>
</html>
<?php }} ?>