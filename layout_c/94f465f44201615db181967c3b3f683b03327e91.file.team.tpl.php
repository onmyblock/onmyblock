<?php /* Smarty version Smarty-3.1.11, created on 2013-06-25 17:05:55
         compiled from "/home/myblock/public/layout/pages/team.tpl" */ ?>
<?php /*%%SmartyHeaderCode:119406211951ca3063611502-49437701%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '94f465f44201615db181967c3b3f683b03327e91' => 
    array (
      0 => '/home/myblock/public/layout/pages/team.tpl',
      1 => 1371591113,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '119406211951ca3063611502-49437701',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51ca30636851c9_45660792',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51ca30636851c9_45660792')) {function content_51ca30636851c9_45660792($_smarty_tpl) {?><!DOCTYPE html>

<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="description" content="Find your next college place to live.">
    <link rel="stylesheet" href="/media/css/main.css?v=1">  
    <link rel="stylesheet" href="/media/css/style.css"> 
	<link type="image/x-icon" href="/media/images/map_icon_tab.png" rel="shortcut icon">	
	<script src="/media/js/libs/jquery-1.9.0.min.js"></script>
	<script src="/media/js/main.js"></script>
	
	<style type="text/css">
		article{margin: 0 auto;margin-left: -500px;}
		header{background: #f6f6f6;}
	</style>
	
</head>	
	<body>
		
		<header>
			<article>
				<a href="/"><div class="logo"></div></a>
				<?php echo $_smarty_tpl->getSubTemplate ('../header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

			</article>
		</header>  
		<div id="main" >
			<div class="container_24" >
				
				<div style="border-top: 3px solid #CCC;text-align:center;margin-top: 30px;width: 920px;">
					<h6 style="margin: auto;margin-top: -14px;background: white;width: 588px;color: black;font-size: 45px;" >Meet the OnMyBlock Team</h6>
				</div>
			
				<div style=" font-size:14px; width: 920px;height: 980px;color: #0a0a0a;margin-top: 40px;">
					
					<div style="width:50%;height:464px;float:left;padding-top: 10px;">
						<div style="height: 160px;width: 100%;">
							<div style="float: left;height: 100%;width: 40%;text-align: center;">
								<div style="width: 160px;height: 156px;border:1px solid #b5b3b4;">
									<div style="background-image: url('http://onmyblock.com/media/images/team/jordan.png');background-repeat:no-repeat;width:150px;height:147px;margin: auto;margin-top: 4px;"></div>
								</div>
							</div>
							<div  style="float: left;height: 100%;width: 60%;text-align: left;">
								<div style="margin-top: 16%;">
									<span style="font-size: 25px;">Jordan Jadallah</span><br/><br/>Co-Founder
								</div>
							</div>
						</div>
						<div style="height:250px; width:100%;display:inline-block; padding-top:20px;text-align: justify;clear: both;line-height: 25px;">
							<div style="margin-right:20px;">
							Jordan Jadallah is a junior at the University of San Diego majoring in economics with a minor in math. Jordan has experience with programming, started his own business in high school and has worked at several start up companies in the past. He got connected with Morgan Schwanke in his freshman year about the frustration of finding off campus housing, and they decided to develop a solution together. This development eventually turned into OnMyBlock. Jordan is from Hillsborough, California.
							</div>
							<a href="http://www.linkedin.com/profile/view?id=111447289&trk=tab_pro">
								<div class="linkd" style="background-image: url('http://onmyblock.com/media/images/team/linkedinGray.png');background-repeat:no-repeat;width: 56px; height: 56px;margin-top: 25px;"></div>
							</a>
						</div>
					</div> 
					<div style="width:50%;height:464px;float:left;padding-top: 10px;">
						<div style="height: 160px;width: 100%;">
							<div style="float: left;height: 100%;width: 40%;text-align: center;">
								<div style="width: 160px;height: 156px;border:1px solid #b5b3b4;">
									<div style="background-image: url('http://onmyblock.com/media/images/team/morgan.png');background-repeat:no-repeat;width:150px;height:147px;margin: auto;margin-top: 4px;"></div>
								</div>
							</div>
							<div  style="float: left;height: 100%;width: 60%;text-align: left;">
								<div style="margin-top: 16%;">
									<span style="font-size: 25px;">Morgan Schwanke</span><br/><br/>Co-Founder
								</div>
							</div>
						</div>
						<div style="height:250px; width:100%;display:inline-block; padding-top:20px;text-align: justify;clear: both;line-height: 25px;">
							<div style="">
							Morgan Schwanke is a senior at the University of San Diego studying Interdisciplinary Humanities with an emphasis in art history. Morgan is also the President of Associated Students. After going through the frustrating process of finding off-campus housing his freshman year, Morgan started a real estate service called Student Realty San Diego. After helping over 80 students find off-campus housing, he connected with Jordan Jadallah to move forward with developing OnMyBlock. He also has his real estate license ((DRE# 01909554). Morgan is from Menlo Park, California.
							</div>
							<a href="http://www.linkedin.com/profile/view?id=62171347&locale=en_US&trk=tyah">
								<div class="linkd" style="background-image: url('http://onmyblock.com/media/images/team/linkedinGray.png');background-repeat:no-repeat;width: 56px; height: 56px;"></div>
							</a>
						</div>
					</div>
					
					<div style="width:50%;height:390px;float:left;padding-top: 10px;">
						<div style="height: 160px;width: 100%;">
							<div style="float: left;height: 100%;width: 40%;text-align: center;">
								<div style="width: 160px;height: 156px;border:1px solid #b5b3b4;">
									<div style="background-image: url('http://onmyblock.com/media/images/team/gino.png');background-repeat:no-repeat;width:150px;height:147px;margin: auto;margin-top: 4px;"></div>
								</div>
							</div>
							<div  style="float: left;height: 100%;width: 60%;text-align: left;">
								<div style="margin-top: 16%;">
									<span style="font-size: 25px;">Gino Ferrand</span><br/><br/>Lead Developer
								</div>
							</div>
						</div>
						<div style="height:250px; width:100%;display:inline-block; padding-top:20px;text-align: justify;clear: both;line-height: 25px;">
							<div style="margin-right:20px;">
							Gino Ferrand is an alum of the University of San Diego and also an entrepreneur. Gino leads Tecla Labs, a web and iPhone development firm in Lima, Peru. He also participated in the USD V2 Business Plan Competition in 2012. Gino leads a team of five web and iPhone developers who have helped build out and manage OnMyBlock's website and iPhone app. Gino lives in Lima, Peru.
							</div>
							<a href="http://www.linkedin.com/profile/view?id=95614353&locale=en_US&trk=tyah">
								<div class="linkd" style="background-image: url('http://onmyblock.com/media/images/team/linkedinGray.png');background-repeat:no-repeat;width: 56px; height: 56px;"></div>
							</a>
						</div>
					</div>
					
					<div style="width:50%;height:390px;float:left;padding-top: 10px;">
						<div style="height: 160px;width: 100%;">
							<div style="float: left;height: 100%;width: 40%;text-align: center;">
								<div style="width: 160px;height: 156px;border:1px solid #b5b3b4;">
									<div style="background-image: url('http://onmyblock.com/media/images/team/mike.png');background-repeat:no-repeat;width:150px;height:147px;margin: auto;margin-top: 4px;"></div>
								</div>
							</div>
							<div  style="float: left;height: 100%;width: 60%;text-align: left;">
								<div style="margin-top: 16%;">
									<span style="font-size: 25px;">Michael Stuhmer</span><br/><br/>Business Development 
								</div>
							</div>
						</div>
						<div style="height:250px; width:100%;display:inline-block; padding-top:20px;text-align: justify;clear: both;line-height: 25px;">
							<div style="margin-right:20px;">
							Michael Stuhmer is a senior at the University of San Diego majoring in Business Adminstration and Real Estate. Michael joined the OnMyBlock to help plan, build and execute the expansion of OnMyBlock to other campuses in the upcoming months. He has a strong background in the real estate market. Michael is from Las Vegas, Nevada.
							</div>
							<a href="http://www.linkedin.com/profile/view?id=243683178&authType=NAME_SEARCH&authToken=JtIe&locale=en_US&srchid=f850a762-524f-4216-9541-1a82be4940fb-0&srchindex=1&srchtotal=1&goback=.fps_PBCK_michael+stuhmer_*1_*1_*1_*1_*1_*1_*2_*1_Y_*1_*1_*1_false_1_R_*1_*51_*1_*51_true_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2&pvs=ps&trk=pp_profile_name_link ">
								<div class="linkd" style="background-image: url('http://onmyblock.com/media/images/team/linkedinGray.png');background-repeat:no-repeat;width: 56px; height: 56px;"></div>
							</a>
						</div>
					</div>  
				</div>
				
				<div style="border-top: 3px solid #CCC;text-align:center;margin-top: 30px;">
					<h6 style="margin: auto;margin-top: -14px;background: white;width: 588px;color: black;font-size: 45px;" >Campus Founders</h6>
				</div>
				
				<div style=" font-size:14px; width: 920px;height: 980px;color: #0a0a0a;margin-top: 40px;">
					<div style="width:50%;height:300px;float:left;padding-top: 10px;">
						<div style="height: 160px;width: 100%;">
							<div style="float: left;height: 100%;width: 40%;text-align: center;">
								<div style="width: 160px;height: 156px;border:1px solid #b5b3b4;">
									<div style="background-image: url('http://onmyblock.com/media/images/team/chris.jpg');background-repeat:no-repeat;width:150px;height:147px;margin: auto;margin-top: 4px;"></div>
								</div>
							</div>
							<div  style="float: left;height: 100%;width: 60%;text-align: left;">
								<div style="margin-top: 16%;">
									<span style="font-size: 25px;">Christopher Shields</span><br/><br/>
									University of San Diego
								</div>
							</div>
						</div>
					</div>
					<div style="width:50%;height:300px;float:left;padding-top: 10px;">
						<div style="height: 160px;width: 100%;">
							<div style="float: left;height: 100%;width: 40%;text-align: center;">
								<div style="width: 160px;height: 156px;border:1px solid #b5b3b4;">
									<div style="background-image: url('http://onmyblock.com/media/images/team/chelsea.jpg');background-repeat:no-repeat;width:150px;height:147px;margin: auto;margin-top: 4px;"></div>
								</div>
							</div>
							<div  style="float: left;height: 100%;width: 60%;text-align: left;">
								<div style="margin-top: 16%;">
									<span style="font-size: 25px;">Chelsea Casserly</span><br/><br/>
									University of San Diego
								</div>
							</div>
						</div>
					</div>
					<div style="width:50%;height:200px;float:left;padding-top: 10px;"> 
						<div style="height: 160px;width: 100%;">
							<div style="float: left;height: 100%;width: 40%;text-align: center;">
								<div style="width: 160px;height: 156px;border:1px solid #b5b3b4;">
									<div style="background-image: url('http://onmyblock.com/media/images/team/mikes.jpg');background-repeat:no-repeat;width:150px;height:147px;margin: auto;margin-top: 4px;"></div>
								</div>
							</div>
							<div  style="float: left;height: 100%;width: 60%;text-align: left;">
								<div style="margin-top: 16%;">
									<span style="font-size: 25px;">Mike Sussman</span><br/><br/>
									University of San Diego
								</div>
							</div>
						</div>
					</div>	
					<div style="width:50%;height:200px;float:left;padding-top: 10px;">
						<div style="height: 160px;width: 100%;">
							<div style="float: left;height: 100%;width: 40%;text-align: center;">
								<div style="width: 160px;height: 156px;border:1px solid #b5b3b4;">
									<div style="background-image: url('http://onmyblock.com/media/images/team/vincent.png');background-repeat:no-repeat;width:150px;height:147px;margin: auto;margin-top: 4px;"></div>
								</div>
							</div>
							<div  style="float: left;height: 100%;width: 60%;text-align: left;">
								<div style="margin-top: 16%;">
									<span style="font-size: 25px;">Vincent Nasca</span><br/><br/>
									University of San Diego
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>
		
<?php echo $_smarty_tpl->getSubTemplate ('../footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>



<script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbmsl=1&appId=561406290571363";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>



</body>
</html><?php }} ?>