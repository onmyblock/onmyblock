<?php /* Smarty version Smarty-3.1.11, created on 2013-06-25 13:10:32
         compiled from "/home/myblock/public/layout/pages/help.tpl" */ ?>
<?php /*%%SmartyHeaderCode:62371947051c9f9388ff943-14465594%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8b759ba9dc845f7acdcbb06c3853806ebd104d60' => 
    array (
      0 => '/home/myblock/public/layout/pages/help.tpl',
      1 => 1371590983,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '62371947051c9f9388ff943-14465594',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'onmyblock' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_51c9f93897b7e0_20956086',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_51c9f93897b7e0_20956086')) {function content_51c9f93897b7e0_20956086($_smarty_tpl) {?><!doctype html>
<!--[if lt IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html lang="es" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="es" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title>OnMyBlock</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
    <!-- //metas para facebook -->
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content=" ">
    <meta property="og:site_name" content="">

    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<link rel="stylesheet" href="/media/css/main.css?v=1">
    <link rel="stylesheet" href="/media/css/jquery.vegas.css" type="text/css" media="all">    
    
	<script src="/media/js/libs/modernizr-2.0.6.min.js"></script>
    <script>
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-XXXXXXX-X']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
</head>
<body>
<header>

    <article>
        <div class="logo"></div>
        <?php echo $_smarty_tpl->getSubTemplate ('../header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

    </article>

</header>    

<div id="wrapper">
    <section id="section-5">
        <article>
            <div class="boxhowitworks boxhelp">
            <h1>Help</h1>

            <h2>Getting Started</h2>

            <h2>General information</h2>
            
            <span>When are you expanding to my school?</span><br/>
            We are expanding geographically, starting in San Diego. We hope to be in many college towns by the end of the school year. To request that OnMyBlock comes to your campus, please click here!<br/><br/>

            <span>Who can see where I live?</span><br/>
            You have complete control over sharing who see?s where you live. You can let your college network see where you live...or only your friends. To control who sees where you live, please edit your privacy settings.<br/><br/>

            <span>What types of places do you list?</span><br/>
            We list just about anything you can find online including: apartments, resident halls, houses, studio?s and more!<br/><br/>

            <span>What's the difference between Save to Wishlist and Like?</span><br/>
            Save to Wish List adds houses to a tab in the ?MyBlock? section so that you can go back and review the houses you want to learn more about. Liking a house is a social way to share with your friends how you feel about different places!<br/><br/>

            <span>How can I check OnMyBlock using my phone?</span><br/>
            Our iPhone app is comming soon!<br/><br/>

            <span>Can I be a Campus Founder?</span><br/>
            Absolutely. Email us at jordan@onmyblock.com with your resume, a short description of yourself, how you are involved on campus, and why you would be a good campus founder.<br/><br/>          
      

            <h2>Profile and Account Settings</h2>

            <span>How do I change my personal settings?</span><br/>
            Advanced settings are comming soon.<br/><br/>

            <span>Personal Help</span><br/>
            Not finding the right answer to your questions, email us at: info@onmyblock.com
 
            </div>            

        </article>

    </section>
</div>


<?php echo $_smarty_tpl->getSubTemplate ('../footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<script src="/media/js/libs/jquery-1.9.0.min.js"></script>
<script src="/media/js/main.js"></script>
<script src="//connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['onmyblock']->value;?>
/media/js/libs/jquery.vegas.js"></script>
<script src="/media/js/libs/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="/media/js/init.js?v=1"></script>
<script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=561406290571363";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

</body>
</html><?php }} ?>