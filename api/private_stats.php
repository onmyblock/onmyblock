<?php

$page = $_GET['page'];

if($page == 'users'){


		$total = mysqli_fetch_array($db->query("select count(*) from users"));
		$day = mysqli_fetch_array($db->query("select count(*) from users where UNIX_TIMESTAMP(date) > (unix_timestamp()- 86400)"));
		$week = mysqli_fetch_array($db->query("select count(*) from users where UNIX_TIMESTAMP(date) > (unix_timestamp()- 604800)"));
		$month = mysqli_fetch_array($db->query("select count(*) from users where UNIX_TIMESTAMP(date) > (unix_timestamp()- 2592000)"));
		$users = mysqli_fetch_all($db->query("select id, first_name, last_name from users order by date desc"));

		$output = array(
			'total' => $total[0],
			'day' => $day[0],
			'week' => $week[0],
			'month' => $month[0],
			'userImage0' => "https://graph.facebook.com/{$users[0][0]}/picture?width=250&height=180",
			'userImage1' => "https://graph.facebook.com/{$users[1][0]}/picture?width=250&height=180",
			'userImage2' => "https://graph.facebook.com/{$users[2][0]}/picture?width=250&height=180",
			'userImage3' => "https://graph.facebook.com/{$users[3][0]}/picture?width=250&height=180",
			'userImage4' => "https://graph.facebook.com/{$users[4][0]}/picture?width=250&height=180",
			'userName0' => $users[0][1]." ".$users[0][2],
			'userName1' => $users[1][1]." ".$users[1][2],
			'userName2' => $users[2][1]." ".$users[2][2],
			'userName3' => $users[3][1]." ".$users[3][2],
			'userName4' => $users[4][1]." ".$users[4][2],
			
		);

	echo json_encode($output);

}

if($page == 'places'){


		$total = mysqli_fetch_array($db->query("select count(*) from places"));
		$day = mysqli_fetch_array($db->query("select count(*) from places where UNIX_TIMESTAMP(date) > (unix_timestamp()- 86400)"));
		$week = mysqli_fetch_array($db->query("select count(*) from places where UNIX_TIMESTAMP(date) > (unix_timestamp()- 604800)"));
		$month = mysqli_fetch_array($db->query("select count(*) from places where UNIX_TIMESTAMP(date) > (unix_timestamp()- 2592000)"));
		$realtor = mysqli_fetch_array($db->query("select count(*) from places where realtor <> ''"));
		$photos = mysqli_fetch_array($db->query("select count(distinct place) from photos"));
		$both = mysqli_fetch_array($db->query("select count(distinct place) from photos join places on places.id = photos.place where places.realtor <> ''"));

		$places = mysqli_fetch_all($db->query("select places.id, street, route, realtors.phone from places join photos on places.id = photos.place left join realtors on realtors.id = places.realtor where places.realtor > 0 group by realtors.phone order by places.date desc limit 3"));


		$output = array(
			'total' => $total[0],
			'day' => $day[0],
			'week' => $week[0],
			'month' => $month[0],
			'realtor' => $realtor[0],
			'photos' => $photos[0],
			'both' => $both[0],
			'placeId0' => "https://s3-us-west-1.amazonaws.com/onmyblock/places/{$places[0][0]}/profile.jpg",
			'placeId1' => "https://s3-us-west-1.amazonaws.com/onmyblock/places/{$places[1][0]}/profile.jpg",
			'placeId2' => "https://s3-us-west-1.amazonaws.com/onmyblock/places/{$places[2][0]}/profile.jpg",
			'placeTitle0' => $places[0][1]." ".$places[0][2],
			'placeTitle1' => $places[1][1]." ".$places[1][2],
			'placeTitle2' => $places[2][1]." ".$places[2][2],
			'placePhone0' => "(".$places[0][3][0].$places[0][3][1].$places[0][3][2].") ".$places[0][3][3].$places[0][3][4].$places[0][3][5]."-".$places[0][3][6].$places[0][3][7].$places[0][3][8].$places[0][3][9],
			'placePhone1' => "(".$places[1][3][0].$places[1][3][1].$places[1][3][2].") ".$places[1][3][3].$places[1][3][4].$places[1][3][5]."-".$places[1][3][6].$places[1][3][7].$places[1][3][8].$places[1][3][9],
			'placePhone2' => "(".$places[2][3][0].$places[2][3][1].$places[1][3][2].") ".$places[2][3][3].$places[2][3][4].$places[2][3][5]."-".$places[2][3][6].$places[2][3][7].$places[2][3][8].$places[2][3][9],
		);


	echo json_encode($output);

}


if($page == 'interactions'){


		$placeLikes = mysqli_fetch_array($db->query("select count(*) from place_likes where UNIX_TIMESTAMP(date) > (unix_timestamp()- 86400)"));
		$placeFavorites = mysqli_fetch_array($db->query("select count(*) from favorites where UNIX_TIMESTAMP(date) > (unix_timestamp()- 86400)"));
		$placeRatings = mysqli_fetch_array($db->query("select count(*) from ratings where UNIX_TIMESTAMP(date) > (unix_timestamp()- 86400)"));

		$likes7 = mysqli_fetch_array($db->query("select count(*) from place_likes where UNIX_TIMESTAMP(date) > (unix_timestamp()- 604800)"));
		$favorites7 = mysqli_fetch_array($db->query("select count(*) from favorites where UNIX_TIMESTAMP(date) > (unix_timestamp()- 604800)"));
		$ratings7 = mysqli_fetch_array($db->query("select count(*) from ratings where UNIX_TIMESTAMP(date) > (unix_timestamp()- 604800)"));

		$total24 = $placeLikes[0] + $placeFavorites[0] + $placeRatings[0];
		$total7 = $likes[0] + $favorites7[0] + $ratings7[0];

		$output = array(
			'placeLikes' => "".$placeLikes[0],
			'placeFavorites' => "".$placeFavorites[0],
			'placeRatings' => "".$placeRatings[0],
			'total24' => "".$total24,
			'total7' => "".$total7,

		);


	echo json_encode($output);

}

if($page == 'views'){


		$placeViews = mysqli_fetch_array($db->query("select count(*) from views where type = 1"));
		$userViews = mysqli_fetch_array($db->query("select count(*) from views where type = 0"));

		$views24 = mysqli_fetch_array($db->query("select count(*) from views where UNIX_TIMESTAMP(date) > (unix_timestamp()- 86400)"));
		$views7 = mysqli_fetch_array($db->query("select count(*) from views where UNIX_TIMESTAMP(date) > (unix_timestamp()- 604800)"));
		$views30 = mysqli_fetch_array($db->query("select count(*) from views where UNIX_TIMESTAMP(date) > (unix_timestamp()- 18144000)"));

		$output = array(
			'placeViews' => $placeViews[0],
			'userViews' => $userViews[0],
			'views24' => $views24[0],
			'views7' => $views7[0],
			'views30' => $views30[0]
		);


	echo json_encode($output);

}



?>