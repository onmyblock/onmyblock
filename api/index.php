<?php

/* 
error_reporting(E_ALL);
ini_set('display_errors', '1');
*/

require('../classes/database.class.php');

$db = new Database();

$object = $_GET['object'];

$array_param = explode('/',$object);

//    Private Stats

if($_GET['object'] == 'private'){

	include_once('private_stats.php');

}


//    OUTPUT

if($_GET['object'] == 'registered'){

	$output = mysqli_fetch_array($db->query("select exists (select 1 from users where id = {$_GET['user_id']}) as result"));
	
	if($output['result']==1)
	{
		$output = mysqli_fetch_array($db->query("select if (STRCMP(status,'inactive'),'1','2') as result from users where id = {$_GET['user_id']}"));
	}
	
	echo $output['result'];

}

if($_GET['object'] == 'place'){

	include_once('communicators/output_place.php');

}


 
if($array_param[0] == 'posts'){

	include_once('communicators/output_posts.php');

}


if($_GET['object'] == 'user'){

	include_once('communicators/output_user.php');

}

if($_GET['object'] == 'geo'){

	include_once('communicators/output_geo.php');

}


if($_GET['object'] == 'residents'){

	include_once('communicators/output_residents.php');

}


if($_GET['object'] == 'favorites'){

	include_once('communicators/output_favorites.php');

}


if($_GET['object'] == 'likes'){

	include_once('communicators/output_likes.php');

}

if($_GET['object'] == 'badge'){

	include_once('communicators/output_badge.php');

}

if($_GET['object'] == 'realtor'){

	include_once('communicators/output_realtor.php');

}

if($_GET['object'] == 'rating'){

	include_once('communicators/output_rating.php');

}

if($_GET['object'] == 'browse'){

	include_once('communicators/output_browse.php');

}





//    INPUT


if($_GET['object'] == 'in_like'){

	include_once('communicators/input_like.php');

}

if($_GET['object'] == 'in_favorite'){

	include_once('communicators/input_favorite.php');

}

if($_GET['object'] == 'in_badge'){

	include_once('communicators/input_badge.php');

}

if($array_param[0] == 'in_posts'){

	include_once('communicators/input_posts.php');

}

if($array_param[0] == '3taps'){
	
	include_once('communicators/input_threetaps.php');

}

if($array_param[0] == 'input'){
	
	include_once('communicators/input_input.php');
}

if($array_param[0] == 'friends'){
	
	include_once('communicators/input_friends.php');

}

//  STREAM


if($_GET['object'] == 'stream'){

	include_once('communicators/stream.php');

}


?>