<?php

	$id = $_GET['place_id'];
	
	if($id)
	{
		$current = $db->query("select users.id from users join residents on residents.place = {$id} and residents.current = 1 where users.id = residents.user");
		$past = $db->query("select users.id from users join residents on residents.place = {$id} and residents.current = 0 where users.id = residents.user");

		if($current)
		{
			$sql['current'] = mysqli_fetch_all($current);
		} 
		else
		{
			$sql['current'] = null;
		}
		
		if($past)
		{
			$sql['past'] = mysqli_fetch_all($past);
		}
		else
		{
			$sql['past'] = null;
		}
			
		if(!$sql['current'] && !$sql['past'])
		{
			$sql = 0;
		}


		if(!isset($sql['current'][0])){

			unset($sql['current']);
		}

		if(!isset($sql['past'][0])){

			unset($sql['past']);
		}

		print(json_encode($sql));
	}
	
	$user_id = $_GET['user_id'];
	
	if($user_id)
	{
		$sql = mysqli_fetch_all($db->query("select pl.place, p.neighborhood, p.city, p.state, p.street, p.route
											from residents as pl
											join places as p
											on p.id=pl.place
											where pl.user = {$user_id}"));
											
											
		$output = array();
	
		foreach($sql as $user){

			$array = array('place_id' => $user[0],
						   'neighborhood' => $user[1],
						   'city' => $user[2],
						   'state' => $user[3],
						   'street' => $user[4],
						   'route' => $user[5]);
						   
			array_push($output,$array);
		}
		
		print(json_encode($output));
	}
?>