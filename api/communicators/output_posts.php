<?php

	$array_param = explode('/',$_GET['object']);
	
	$nameFunction = $array_param[1];
	
	$nameFunction($array_param);
	
	function countComment($array_param)
	{
		$place_id = $array_param[2];
		
		if(is_numeric($place_id))
		{
			$db = new Database();
			
			$sql = mysqli_fetch_array($db->query("select count(*) from posts where place = {$place_id}"));
			
			foreach($sql as $item)
			{
				$output = array('amount'=>$item);
				
				break;
			}
			
			print(json_encode($output)); 
					
		}else{
			print('{"amount":"0"}');
		}		
	}
	
	function registryByPlaceid($array_param)
	{
		$place_id = $array_param[2];
		$limitF = $array_param[3];
		$limitI = $array_param[4];
				
		$query = "select po.user, us.first_name, us.last_name, po.entry , UNIX_TIMESTAMP(po.date)
				  from posts as po
				  join users as us
				  on po.`user`=us.id
				  where place = {$place_id}
				  order by po.date desc";
		
		if(is_numeric($limitF))
		{
			if(!is_numeric($limitI))
			{
				$limitI=0;
			}
			$limitI = $limitI*10;
			$query = $query." limit {$limitI},{$limitF}";
		}
		
		$db = new Database();
		
		$sql = mysqli_fetch_all($db->query($query));
		
		$output = array();
		
		foreach($sql as $item)
		{
			$arrayItem = array('user'=>$item[0],
							   'first_name'=>$item[1],
							   'last_name'=>$item[2],
							   'entry'=>$item[3],
							   'date'=>$item[4]
							  );
			
			array_push($output,$arrayItem);
		}
		
		print(json_encode($output));
	}
	
	
	
	
	