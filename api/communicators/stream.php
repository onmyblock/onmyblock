<?php


$limit = 10*$_GET['load'];

if($_GET['type'] == 'network'){

	/*$stream = $db->query("select events.id as event_id, events.user as user_id, events.action, events.target as place_id, UNIX_TIMESTAMP(events.date) as time

from events

left join friends on friends.user = {$_GET['user_id']} and friends.friend = events.user
join affiliations as firstAff on firstAff.user = {$_GET['user_id']}
join affiliations as secondAff on secondAff.network = firstAff.network
where events.user = secondAff.user and friends.friend is null and events.user <> {$_GET['user_id']} and events.action <> 5 and events.action <> 9 and  events.action <> 10 and events.action <> 11 and events.action <> 15 and events.action <> 16
order by events.id desc limit {$limit}, 10");*/

$stream = $db->query("select events.id as event_id, events.user as user_id, events.action, events.target as place_id, UNIX_TIMESTAMP(events.date) as time

from events

left join friends on friends.user = {$_GET['user_id']} and friends.friend = events.user
join affiliations as firstAff on firstAff.user = {$_GET['user_id']}
join affiliations as secondAff on secondAff.network = firstAff.network
where events.user = secondAff.user or events.user = friends.friend and events.user <> {$_GET['user_id']} and events.action <> 5 and events.action <> 9 and  events.action <> 10 and events.action <> 11 and events.action <> 15 and events.action <> 16
group by events.id 
order by events.date desc 
limit {$limit}, 10");


} elseif ($_GET['type'] == 'friends'){

	$stream = $db->query("select events.id as event_id, events.user as user_id, events.action, events.target as place_id, UNIX_TIMESTAMP(events.date) as time

    from events
    join friends on friends.user = {$_GET['user_id']}

where events.user = friends.friend and events.user <> {$_GET['user_id']} and events.action <> 5 and events.action <> 9 and  events.action <> 10 and events.action <> 11 and events.action <> 15 and events.action <> 16
order by events.date desc limit {$limit} ,10");

}


$i = 0;
while($event = mysqli_fetch_assoc($stream))
{
	$event_details = $db->cache->get("event_{$event['event_id']}");

	if(!isset($event_details['user_id'])){
		$user = $db->get_user($event['user_id']);
		$place = ($event['place_id'] > 0 ? $db->get_place($event['place_id']) : null);
		$photos = ($event['place_id'] > 0 && $event['action'] < 13 ? mysqli_fetch_all($db->query("select dir from photos where place = {$event['place_id']} and dir not like '%profile%' limit 3")) : null);
		
		$details = null;

		switch ($event['action']){

		case 2: // badge

			$badge = mysqli_fetch_array($db->query("select badge from badges where user = {$event['user_id']} and place = {$event['place_id']} and UNIX_TIMESTAMP(date) = {$event['time']}"));
			$details['badge'] = $badge[0];

			break;

		case 3: // posted on house wall

			$post = mysqli_fetch_array($db->query("select entry from posts where user = {$event['user_id']} and place = {$event['place_id']} and UNIX_TIMESTAMP(date) = {$event['time']}"));
			$details['post'] = $post[0];

			break;


		}

		$x = 0;
		foreach($photos as $photo){
			$details['photos'][$x] = $photo[0];
			$x++;
		}


		$event_details = array(
			'user_id' => $event['user_id'],
			'first_name' => $user['first_name'],
			'last_name' => $user['last_name'],
			'event_type' => $event['action'],
			'place_id' => $event['place_id'],
			'place_name' => $place['name'],
			'place_street' => $place['street'],
			'place_route' => $place['route'],
			'time' => $event['time'],
			'details' => $details,
		);

		$db->cache->add("event_{$event['event_id']}", $event_details, false, 86400);

	}
	
	$output[$i] = $event_details;
	$i++;
}

echo(json_encode($output));

?>