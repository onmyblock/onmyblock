<?php

	$latitude = $_GET['latitude'];
	$longitude = $_GET['longitude'];
	$radius = $_GET['radius'];
	$limit = $_GET['limit'];
	
	$sql = mysqli_fetch_all($db->query("SELECT id, name, latitude, longitude, price, size, rooms, bathrooms, type ,((ACOS(SIN({$latitude} * PI() / 180) * SIN(`latitude` * PI() / 180) + COS({$latitude} * PI() / 180) * COS(`latitude` * PI() / 180) * COS(({$longitude} - `longitude`) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS distance FROM places WHERE (`latitude` BETWEEN ({$latitude} - {$radius}) AND ({$latitude} + {$radius}) AND `longitude` BETWEEN ({$longitude} - {$radius}) AND ({$longitude} + {$radius})) group by distance ORDER BY distance ASC, date desc limit {$limit};")); 


	$i = 0;
	$output = array();
	foreach($sql as $place)

		$output[$i++] = array(
			'place_id' => $place[0],
			'name' => $place[1],
			'latitude' => $place[2],
			'longitude' => $place[3],
			'distance' => $place[9],
			'rating' => null,
			'price' => $place[4],
			'size' => $place[5],
			'rooms' => $place[6],
			'bathrooms' => $place[7],
			'type' => $place[8]
		);

	print(json_encode($output));
	
?>