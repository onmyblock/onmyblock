<?php

$cat = $_GET['category'];
$user_id = $_GET['user_id'];
$load = $_GET['load']*10;
$placeId = $_GET['place_id'];
$location = $_GET['text'];

if($cat != '' && $user_id != ''){

if($cat == 'list'){
	$output = array(
	0 => 'All Pads',
	1 => 'Popular Pads',
	2 => 'People',
	3 => 'Beaches',
	4 => 'Close To School'
	);
}

elseif($cat == 0){

	$criterio = trim($location);
	
	if($criterio!='')
	{
		$arrayCriterio = explode(',',$criterio);
	}
	
	$places = array();
	
	foreach($arrayCriterio as $item)
	{
		$item = trim($item);
		$route = '';
		
		$query .= "( city like '%{$item}%' or neighborhood like '%{$item}%' or state like '%{$item}%' or route like '%{$item}%' ";
		
		$arraystreet = explode(' ',$item);
			
		for($i=1; $i<count($arraystreet); $i++)
		{
			$route.=$arraystreet[$i].' ';
		}
		
		$route = trim($route);
		
		if(is_numeric($arraystreet[0]))
		{
			$query .= " or street={$arraystreet[0]} ";
		}
		
		if($route)
		{
			$query .= " or route like '%{$route}%' ";
		}
		
		$query .= ") and ";
	}
	 
	//$query = rtrim($query,"and ");
	//group by places.latitude
	$queryNew = "select places.id, neighborhood, city, state, rooms, price, street, route, dir as photo from places join photos where {$query} places.id = photos.place and dir like '%profile%'  order by date desc limit {$load}, 10";
		
	//echo $queryNew;	
		
	$query = $db->query($queryNew);
	
	if($query!=null)
	{
		$i = 0;
		while($item = mysqli_fetch_assoc($query)){
			$output[$i] = $item;
			$views = mysqli_fetch_array($db->query("select count(*) from views where type = 1 and page = {$output[$i]['id']}"));
			$output[$i]['views'] = $views[0];
			$sweets = mysqli_fetch_array($db->query("select count(*) from place_likes where place = {$output[$i]['id']}"));
			$output[$i]['sweets'] = $sweets[0];
			$like = mysqli_fetch_array($db->query("select * from place_likes where place = {$output[$i]['id']} and user = {$user_id}"));
			if($like[0]){$varLike=1;}else{$varLike=0;}
			$output[$i]['like'] = $varLike;
			$posts = mysqli_fetch_array($db->query("select count(*) from posts where personal = 0 and place = {$output[$i]['id']}"));
			$output[$i]['posts'] = $posts[0];
			$wishlisted = mysqli_fetch_array($db->query("select exists (select 1 from favorites where user = {$user_id} and place = {$output[$i]['id']})"));
			$output[$i++]['wishlisted'] = $wishlisted[0];
		}
	}
}

elseif($cat == 1){
	
	$criterio = trim($location);
	
	if($criterio!='')
	{
		$arrayCriterio = explode(',',$criterio);
	}
	
	$places = array();
	
	foreach($arrayCriterio as $item)
	{
		$item = trim($item);
		$route = '';
		
		$query .= "( city like '%{$item}%' or neighborhood like '%{$item}%' or state like '%{$item}%' ";
		
		$arraystreet = explode(' ',$item);
			
		for($i=1; $i<count($arraystreet); $i++)
		{
			$route.=$arraystreet[$i].' ';
		}
		
		$route = trim($route);
		
		if(is_numeric($arraystreet[0]))
		{
			$query .= " or street={$arraystreet[0]} ";
		}
		
		if($route)
		{
			$query .= " or route like '%{$route}%' ";
		}
		
		$query .= ") and ";
	}
	
	$queryNew="select places.id, neighborhood, city, state, rooms, price, street, route, dir as photo from places join views on views.page = places.id join place_likes on place_likes.place = places.id join photos on photos.place = places.id where {$query} places.id = photos.place and dir like '%profile%' group by places.latitude order by count(views.page) desc limit {$load}, 10";
	
	$query = $db->query($queryNew);
	
	if($query!=null)
	{
		$i = 0;
		while($item = mysqli_fetch_assoc($query)){
			$output[$i] = $item;
			$views = mysqli_fetch_array($db->query("select count(*) from views where type = 1 and page = {$output[$i]['id']}"));
			$output[$i]['views'] = $views[0];
			$sweets = mysqli_fetch_array($db->query("select count(*) from place_likes where place = {$output[$i]['id']}"));
			$output[$i]['sweets'] = $sweets[0];
			$like = mysqli_fetch_array($db->query("select * from place_likes where place = {$output[$i]['id']} and user = {$user_id}"));
			if($like[0]){$varLike=1;}else{$varLike=0;}
			$output[$i]['like'] = $varLike;
			$posts = mysqli_fetch_array($db->query("select count(*) from posts where personal = 0 and place = {$output[$i]['id']}"));
			$output[$i]['posts'] = $posts[0];
			$wishlisted = mysqli_fetch_array($db->query("select exists (select 1 from favorites where user = {$user_id} and place = {$output[$i]['id']})"));
			$output[$i++]['wishlisted'] = $wishlisted[0];
		}
	}
}

elseif($cat == 2){
	
	$criterio = trim($location);
	
	if($criterio!='')
	{
		$arrayCriterio = explode(',',$criterio);
	}
	
	$places = array();
	
	foreach($arrayCriterio as $item)
	{
		$item = trim($item);
		$route = '';
		
		$query .= "( city like '%{$item}%' or neighborhood like '%{$item}%' or state like '%{$item}%' ";
		
		$arraystreet = explode(' ',$item);
			
		for($i=1; $i<count($arraystreet); $i++)
		{
			$route.=$arraystreet[$i].' ';
		}
		
		$route = trim($route);
		
		if(is_numeric($arraystreet[0]))
		{
			$query .= " or street={$arraystreet[0]} ";
		}
		
		if($route)
		{
			$query .= " or route like '%{$route}%' ";
		}
		
		$query .= ") and ";
	}
	
	$queryNew = "select places.id, neighborhood, city, state, rooms, price, street, route, dir as photo from places join friends on friends.user = {$user_id} join residents on residents.user = friends.friend join photos on photos.place = residents.place where {$query} places.id = residents.place and dir like '%profile%' order by places.date desc limit {$load}, 10";

	$query = $db->query($queryNew);
	
	if($query!=null)
	{
		$i = 0;
		while($item = mysqli_fetch_assoc($query)){
			$output[$i] = $item;
			$views = mysqli_fetch_array($db->query("select count(*) from views where type = 1 and page = {$output[$i]['id']}"));
			$output[$i]['views'] = $views[0];
			$sweets = mysqli_fetch_array($db->query("select count(*) from place_likes where place = {$output[$i]['id']}"));
			$output[$i]['sweets'] = $sweets[0];
			$like = mysqli_fetch_array($db->query("select * from place_likes where place = {$output[$i]['id']} and user = {$user_id}"));
			if($like[0]){$varLike=1;}else{$varLike=0;}
			$output[$i]['like'] = $varLike;
			$posts = mysqli_fetch_array($db->query("select count(*) from posts where personal = 0 and place = {$output[$i]['id']}"));
			$output[$i]['posts'] = $posts[0];
			$wishlisted = mysqli_fetch_array($db->query("select exists (select 1 from favorites where user = {$user_id} and place = {$output[$i]['id']})"));
			$output[$i++]['wishlisted'] = $wishlisted[0];
		}
	}
}

elseif($cat == 3){
	
	$criterio = trim($location);
	
	if($criterio!='')
	{
		$arrayCriterio = explode(',',$criterio);
	}
	
	$places = array();
	
	foreach($arrayCriterio as $item)
	{
		$item = trim($item);
		$route = '';
		
		$query .= "( city like '%{$item}%' or neighborhood like '%{$item}%' or state like '%{$item}%' ";
		
		$arraystreet = explode(' ',$item);
			
		for($i=1; $i<count($arraystreet); $i++)
		{
			$route.=$arraystreet[$i].' ';
		}
		
		$route = trim($route);
		
		if(is_numeric($arraystreet[0]))
		{
			$query .= " or street={$arraystreet[0]} ";
		}
		
		if($route)
		{
			$query .= " or route like '%{$route}%' ";
		}
		
		$query .= ") and ";
	}
	
	$queryNew = "select places.id, neighborhood, city, state, rooms, price, street, route, dir as photo from places join photos where {$query} places.id = photos.place and latitude < 32.9 and latitude > 32.7 and longitude < -117.24 and dir like '%profile%' group by places.latitude order by date desc limit {$load}, 10";
	
	//echo $queryNew;
	
	$query = $db->query($queryNew);
	
	if($query!=null)
	{
		$i = 0;
		while($item = mysqli_fetch_assoc($query)){
			$output[$i] = $item;
			$views = mysqli_fetch_array($db->query("select count(*) from views where type = 1 and page = {$output[$i]['id']}"));
			$output[$i]['views'] = $views[0];
			$sweets = mysqli_fetch_array($db->query("select count(*) from place_likes where place = {$output[$i]['id']}"));
			$output[$i]['sweets'] = $sweets[0];
			$like = mysqli_fetch_array($db->query("select * from place_likes where place = {$output[$i]['id']} and user = {$user_id}"));
			if($like[0]){$varLike=1;}else{$varLike=0;}
			$output[$i]['like'] = $varLike;
			$posts = mysqli_fetch_array($db->query("select count(*) from posts where personal = 0 and place = {$output[$i]['id']}"));
			$output[$i]['posts'] = $posts[0];
			$wishlisted = mysqli_fetch_array($db->query("select exists (select 1 from favorites where user = {$user_id} and place = {$output[$i]['id']})"));
			$output[$i++]['wishlisted'] = $wishlisted[0];
		}
	}
}

elseif($cat == 4){

	$criterio = trim($location);
	
	if($criterio!='')
	{
		$arrayCriterio = explode(',',$criterio);
	}
	
	$places = array();
	
	foreach($arrayCriterio as $item)
	{
		$item = trim($item);
		$route = '';
		
		$query .= "( city like '%{$item}%' or neighborhood like '%{$item}%' or state like '%{$item}%' ";
		
		$arraystreet = explode(' ',$item);
			
		for($i=1; $i<count($arraystreet); $i++)
		{
			$route.=$arraystreet[$i].' ';
		}
		
		$route = trim($route);
		
		if(is_numeric($arraystreet[0]))
		{
			$query .= " or street={$arraystreet[0]} ";
		}
		
		if($route)
		{
			$query .= " or route like '%{$route}%' ";
		}
		
		$query .= ") and ";
	}
	
	$queryNew = "select places.id, neighborhood, city, state, rooms, price, street, route, dir as photo from places join photos where {$query} places.id = photos.place and latitude > 32.73 and latitude < 32.81 and longitude > -117.214 and longitude < -117.13 and dir like '%profile%' group by places.latitude order by date desc limit {$load}, 10";
	
	$query = $db->query($queryNew);
	
	if($query!=null)
	{
		$i = 0;
		while($item = mysqli_fetch_assoc($query)){
			$output[$i] = $item;
			$views = mysqli_fetch_array($db->query("select count(*) from views where type = 1 and page = {$output[$i]['id']}"));
			$output[$i]['views'] = $views[0];
			$sweets = mysqli_fetch_array($db->query("select count(*) from place_likes where place = {$output[$i]['id']}"));
			$output[$i]['sweets'] = $sweets[0];
			$like = mysqli_fetch_array($db->query("select * from place_likes where place = {$output[$i]['id']} and user = {$user_id}"));
			if($like[0]){$varLike=1;}else{$varLike=0;}
			$output[$i]['like'] = $varLike;
			$posts = mysqli_fetch_array($db->query("select count(*) from posts where personal = 0 and place = {$output[$i]['id']}"));
			$output[$i]['posts'] = $posts[0];
			$wishlisted = mysqli_fetch_array($db->query("select exists (select 1 from favorites where user = {$user_id} and place = {$output[$i]['id']})"));
			$output[$i++]['wishlisted'] = $wishlisted[0];
		}
	}
}
	
	if(isset($placeId))
	{
		foreach($output as $key => $value)
		{
			$searchKey = array_search($placeId, $value);
			
			if($searchKey=='id')
			{
				$out = $output[$key];
				break;
			}else
			{
				$out = '{}';
			}
		}
		
		if($out!='{}')
		{
			$out=json_encode($out);
		}
		
		print($out);
	}
	else{
		if($output!=null)
		{
			print(json_encode($output));
		}else{
			print(json_encode('{}'));
		}
	}

}

?>