<?php
	$id = $_GET['place_id'];

	$sql = mysqli_fetch_array($db->query("select * from places where id = {$id}"));
	$views = mysqli_fetch_array($db->query("select count(*) as views from views where type = 1 and page = {$id}"));
	$photos = mysqli_fetch_array($db->query("SELECT group_concat(dir) as photos FROM photos where place = {$id}"));

	$output = array(
		'name' => $sql['name'],
		'realtor_id' => $sql['realtor'],
		'signup_date' => $sql['date'],
		'street' => $sql['street'],
		'route' => $sql['route'],
		'state' => $sql['state'],
		'unit' => $sql['unit'],
		'city' => $sql['city'],
		'neighborhood' => $sql['neighborhood'],
		'postal' => $sql['postal'],
		'latitude' => $sql['latitude'],
		'longitude' => $sql['longitude'],
		'views' => $views['views'],
		'type' => $sql['type'],
		'photos' => explode(',', $photos[0]),
		'size' => $sql['size'],
		'rooms' => $sql['rooms'],
		'bathrooms' => $sql['bathrooms'],
		'price' => $sql['price']

	);

	print(json_encode($output));
?>