<?php

require_once("sdk.class.php");

function dynamoDB_add($table, $attrs) {
    $dynamodb = new AmazonDynamoDB();
    $response = $dynamodb->batch_write_item(array(
        'RequestItems' => array(
            $table => array(
                array(
                    'PutRequest' => array(
                        'Item' => $dynamodb->attributes($attrs)
                    )
                ),
            )
        )
            ));

    if ($response->isOK()) {
        return true;
    } else {
        return false;
    }
}

function dynamoDB_get($table, $key, $range, $attrs) {

    $dynamodb = new AmazonDynamoDB();
    $response = $dynamodb->get_item(array(
        'TableName' => $table,
        'Key' => $dynamodb->attributes(array(
            'HashKeyElement' => $key,
            'RangeKeyElement' => $range,
        )),
        'AttributesToGet' => $attrs,
        'ConsistentRead' => 'true'
            ));

    return $response->body->Item->to_array();
}

function dynamoDB_update($table, $key, $range, $attr, $update) {

    $dynamodb = new AmazonDynamoDB();
    $response = $dynamodb->update_item(array(
        'TableName' => $table,
        'Key' => $dynamodb->attributes(array(
            'HashKeyElement' => $key,
            'RangeKeyElement' => $range,
        )),
        'AttributeUpdates' => array(
            $attr => array(
                'Action' => AmazonDynamoDB::ACTION_PUT,
                'Value' => array(AmazonDynamoDB::TYPE_STRING => $update)
            ),
        ),
            ));

    if ($response->isOK()) {
        return true;
    } else {
        return false;
    }
}

function dynamoDB_update_array($table, $key, $range, $updates) {

    $dynamodb = new AmazonDynamoDB();
    $response = $dynamodb->update_item(array(
        'TableName' => $table,
        'Key' => $dynamodb->attributes(array(
            'HashKeyElement' => $key,
            'RangeKeyElement' => $range,
        )),
        'AttributeUpdates' => $updates
            ));

    if ($response->isOK()) {
        return true;
    } else {
        return false;
    }
}

function dynamoDB_delete($table, $key, $range) {

    $dynamodb = new AmazonDynamoDB();
    $response = $dynamodb->delete_item(array(
        'TableName' => $table,
        'Key' => $dynamodb->attributes(array(
            'HashKeyElement' => $key,
            'RangeKeyElement' => $range,
        )),
            ));
    if ($response->isOK()) {
        return true;
    } else {
        return false;
    }
}

?>