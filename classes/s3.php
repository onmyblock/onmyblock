<?php

require_once "sdk.class.php";


function store_file($file, $type, $dir)
{
	$s3 = new AmazonS3();
	$response = $s3->create_object(
		'onmyblock', $dir, array(
			'body' => $file,
			'acl' => AmazonS3::ACL_PUBLIC,
			'contentType' => $type,
			'headers' => array(
				'Cache-Control' => 'max-age=2629000',
			),
		)
	);
	if ($response->isOK())
	{
		return 'http://s3.cdn.onmyblock.com/' . $dir;
	} else
	{
		return false;
	}
}



function delete_file($uri)
{
	$bucket = 'onmyblock';
	$s3 = new AmazonS3();
	$response = $s3->delete_object($bucket, $uri);

	// Success?
	if ($response->isOK())
	{
		return true;
	} else
	{
		return false;
	}
}


?>
