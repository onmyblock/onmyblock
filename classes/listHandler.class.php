<?php

class listHandler {

    public function addItem($itemList, $item) {
        if (strlen($itemList) < 1) {
            return $item;
        } else {
            $newList = explode(",", $itemList);
            $newList[count($newList)] = $item;
            return implode(",", $newList);
        }
    }

    public function removeItem($itemList, $item) {
        if (strlen($itemList) < 1) {
            return false;
        } else {
            $newList = explode(",", $itemList);
            if (count($newList) == 1) {
                return "";
            }
            unset($newList[array_search($item, $newList)]);
            $newList = array_values($newList);
            return implode(",", $newList);
        }
    }

}

$lh = new listHandler();
?>