<?php

require 'facebook.php';
require 'database.class.php';

class User
{

	public $fb = null;
	public $id = null;
	public $db = null;

	function __construct()
	{
		$this->fb = new Facebook(array(
				'appId' => '246041178762859',
				'secret' => '4be4fdad0049242924bd683040d758a3',
				'cookie' => true
			));

		$this->id = $this->fb->getUser();
		$this->db = new Database();

	}


	public function is_loggedin()
	{
		if($this->fb->getUser())
		{
			try{
				return true;

			} catch (FacebookApiException $e) {
				error_log($e);
				return false;
			}
		}

	}

	public function is_user()
	{
		if($this->id!=0)
		{
			$result = $this->db->cache->get("is_user_{$this->id}");
			if(!$result){
				$result = mysqli_fetch_array($this->db->query("select exists (select 1 from users where id = {$this->id})"));
				$this->db->cache->add("is_user_{$this->id}", $result, false, 3600);
			}
			return $result[0];
		}else
		{
			return 3;
		}
	}
	
	public function largo()
	{
		$this->db->cache->delete("user_{$this->id}");
	}

	public function get_info()
	{
		return $this->db->get_user($this->id);
	}


	public function get_links()
	{
		$links = array(
			'login' => $this->fb->getLoginUrl(array('scope' => 'user_photos, email, publish_actions')),
			'logout' => $logoutUrl = $this->fb->getLogoutUrl()
		);
		return $links;
	}


	public function get_networks()
	{

		$fql = "SELECT affiliations FROM user WHERE uid=".$this->id;
		$response  =  $this->fb->api(array('method' => 'fql.query', 'query' =>$fql));

		if(isset($response[0]['affiliations'][0]['nid']))
		{
			$networks = $response[0]['affiliations'];
			return $networks;
		} else
		{
			return false;
		}
	}
	
	public function test()
	{
		$user = $this->fb->api('/me');
		
		print_r($user);
	}
	
	public function verification_user()
	{
		$result=false;
		
		if($this->id)
		{
			$user = $this->fb->api('/me');
		
			foreach($user['education'] as $item)
			{
				switch($item["school"]["id"])
				{
					case 105614112804725 :
							$result = true;
						break;
						
					case 36689795929 :
							$result = true;
						break;
				}
				
				switch($item["school"]["name"])
				{
					case "University of San Diego" :
							$result = true;
						break;
						
					case "San Diego" :
							$result = true;
						break;
						
					case "Universidad de San Diego" :
							$result = true;
						break;
						
					case "USD" :
							$result = true;
						break;
				}
			}
		}
		
		return $result;
	}
	
	public function authentication_manual()
	{
		$return = false;
		
		$array_requiere = array('sam.redmond@menloschool.org',
								'micah@onmyblock.com','nmethakul@sandiego.edu',
								'spencerhandly@gmail.com ','kforey@sandiego.edu',
								'ashusko@sandiego.edu','aaronschwei-13@sandiego.edu',
								'adrianh@sandiego.edu','sam@jadallah.com',
								'cwalsey@gmail.com','lexysf@gmail.com',
								'eric@jadallah.com','gs.schwanke@gmail.com',
								'tyrepstad@yahoo.com','natalieatran@gmail.com',
								'lhogan02@gmail.com','wsbenham@gmail.com',
								'isalambing@gmail.com','breannab@sandiego.edu',
								'ltoscanarodriguez@sandiego.edu','rlataitis@comcast.net',
								'braydenfabris@gmail.com','ccs987@yahoo.com',
								'sas@schwanke-aia.com','theschwank@gmail.com'
		);
		
		if($this->id)
		{
			$user = $this->fb->api('/me'); 
			
			$key = array_search($user['email'],$array_requiere);
			
			if(is_numeric($key))
			{
				$return = true;
			}
		}
		
		return $return; 
	}
	
	public function add_user()
	{ 
		//$this->db->cache->delete("is_user_{$this->id}");
		
		$user = $this->fb->api('/me');

		if($user['id']){ 

			$networks = $user['education'];
			
			$db = new mysqli(DB_server, DB_username, DB_password, DB_name);

			$query = "insert into users (`id`, `username`, `first_name`, `last_name`, `status`, `date`, `gender`, `email`) values (?, ?, ?, ?, 'new', now(), ?, ?)";

			$stmt = $db->prepare($query);
			
			mysqli_stmt_bind_param($stmt, "dsssss", $user['id'], $user['username'], $user['first_name'], $user['last_name'], $user['gender'], $user['email']);
			
			$result = mysqli_stmt_execute($stmt);

			if($result)
			{
				if($networks != false) 
				{
					foreach ($networks as $network)
					{
						if(!isset($network['year']))
							{$network['year'] = 0;}
						if(!isset($network['status']))
							{$network['status'] = null;}
							
						$this->db->query("insert into networks (`id`, `name`, `type`) values ({$network["school"]["id"]}, '{$network["school"]["name"]}', '{$network["type"]}');");
						$this->db->query("insert into affiliations (`user`, `network`, `year`, `status`) values ({$user['id']}, {$network["school"]["id"]}, {$network['year']}, '{$network['status']}');"); 
					}

				}

				$friends = $this->fb->api('/' . $this->id . '/friends');

				$friendsQuery = '';

				foreach ($friends['data'] as $value)
				{
					$friendsQuery = $friendsQuery.'('.$this->id.', '.$value['id'].'), ';
				}

				$this->db->query("insert into friends (`user`, `friend`) values ".substr($friendsQuery, 0, -2).";");
				return true;
			} else
			{
				return false;
			} 

 		} else {
			return false;
		}   
	} 

}


?>
