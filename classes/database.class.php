<?php

require_once 'config.php';

class Database
{

	public $connection = null;
	public $cache = null;

	function __construct()
	{
		$this->connection = new mysqli(DB_server, DB_username, DB_password, DB_name);
		$this->cache = new Memcache;
		$this->cache->addServer('localhost', 11211);
	}

	public function query($sql)
	{
		return $this->connection->query($sql);
	}
	
	public function returnQueryId($sql)
	{
		$this->connection->query($sql);
	
		return $this->connection->insert_id;
	}

	public function get_connection(){
		return $this->connection;
	}

	public function get_user_roommates($id)
	{
		return mysqli_fetch_all($this->query("select users.* from users join residents on users.id = residents.user and users.id <> {$id} and residents.current = 1 where residents.place = (select group_concat(place) from residents where user = {$id} and current = 1)"));
	}


	public function get_user_favorites($id)
	{

		return mysqli_fetch_all($this->query("select place from favorites where user = {$id}"));

	}

	public function get_user_friends($id)
	{

		$result = mysqli_fetch_all($this->query("select friend from friends where user = {$id} order by rand() limit 24"));
		$friends = array();

		$i = 0;
		foreach($result as $friend)
		{
			$friends[$i++] = $friend[0];
		}

		return $friends;

	}
	
	public function get_residents_general($id, $user)
	{	
		$query = "select users.id from users join residents on residents.place = {$id} where users.id = residents.user and residents.user != {$user}";
		
		$regs = $this->query("select users.id from users join residents on residents.place = {$id} where users.id = residents.user and residents.user != {$user}");
		
		$result = mysqli_fetch_all($regs);
		
		return $result;
	}

	public function get_residents($id)
	{
		$current = $this->query("select users.id from users join residents on residents.place = {$id} and residents.current = 1 where users.id = residents.user");
		$past = $this->query("select users.id from users join residents on residents.place = {$id} and residents.current = 0 where users.id = residents.user");

		if
		($current)
			{$result['current'] = mysqli_fetch_all($current);} else
			{$result['current'] = false;}
		if
		($past)
			{$result['past'] = mysqli_fetch_all($past);} else
			{$result['past'] = false;}
		if
		(!$result['current'] && !$result['past'])
			{return 0;}

		return $result;

	}


	public function add_place_visit($user, $place){

		$visit = $this->cache->get("place_visit_{$user}_{$place}");

		if($visit != 'visited'){
			$this->query("call insert_visit({$user}, 1, {$place})");
			$this->cache->add("place_visit_{$user}_{$place}", 'visited', false, 3600);
		}

	}


	public function is_place($id){

		$place = $this->cache->get("is_place_{$id}");

		if(!isset($place[0])){
			$place = mysqli_fetch_array($this->query("select count(*) from places where id = {$id}"));
			$this->cache->add("is_place_{$id}", $place, false, 86400);
		}

		if($place[0] > 0){
			return true;
		} else {
			return false;
		}
	}

	public function get_user($id)
	{

		$user = $this->cache->get("user_{$id}");

		if(!$user){
			$user = mysqli_fetch_array($this->query("select * from users where id = {$id}"));
			$this->cache->add("user_{$id}", $user, false, 86400);
		}
		return $user;

	}

	public function get_place($id)
	{

		$place = $this->cache->get("place_{$id}");

		if(!$place){

			$place = mysqli_fetch_array($this->query("select *, UNIX_TIMESTAMP(date) as epoch_date from places where id = {$id}"));
			$short = mysqli_fetch_array($this->query("select short from short_url where place_id = {$id}"));
			
			/* if($place['type'] != ''){
				$place['type'] = explode(",", $place['type']);
				$place['type'] = $place['type'][1];
			} */
			
			$place['date'] = date("d F Y", (strtotime($place['date'])-28800));
			$place['short'] = $short[0];


			$comments = mysqli_fetch_array($this->query("select count(*) as comments from posts where personal = 0 and place = {$id}"));
			$place['comments'] = $comments['comments'];

			$place['current_residents'] = mysqli_fetch_all($this->query("select users.id, users.first_name, users.last_name from users join residents on residents.place = {$id} and residents.current = 1 where residents.user = users.id"));
			$place['past_residents'] = mysqli_fetch_all($this->query("select users.id, users.first_name, users.last_name from users join residents on residents.place = {$id} and residents.current = 0 where residents.user = users.id"));
			
			$place['pending_residents'] = mysqli_fetch_all($this->query("select users.id, users.first_name, users.last_name, residents_queue.status from users join residents_queue on residents_queue.place = {$id} where residents_queue.user = users.id"));
			

			$place['realtor'] = mysqli_fetch_array($this->query("select realtors.id, realtors.name, realtors.email, realtors.phone, realtors.link from realtors join places on places.id = {$id} where places.realtor = realtors.id"));

			if($place['realtor']['phone'] > 1000000000){
				$place['realtor']['phone'] = "(".$place['realtor']['phone'][0].$place['realtor']['phone'][1].$place['realtor']['phone'][2].") ".$place['realtor']['phone'][3].$place['realtor']['phone'][4].$place['realtor']['phone'][5]."-".$place['realtor']['phone'][6].$place['realtor']['phone'][7].$place['realtor']['phone'][8].$place['realtor']['phone'][9]; } else {
				$place['realtor']['phone'] = '';
			}

			$views = mysqli_fetch_array($this->query("select count(*) as views from views where page = {$id} and type = 1"));

			if($place['name'] == ''){$place['name'] = $place['street']." ".$place['route'];}

			$badges = mysqli_fetch_array($this->query("select count(*) as badges from badges where place = {$id}"));

			$posts = mysqli_fetch_all($this->query("select posts.user as user_id, users.first_name, users.last_name, UNIX_TIMESTAMP(posts.date) as date, posts.entry as entry, posts.id, posts.reply, posts.reply_to from posts join users on users.id = posts.user where posts.place = {$id} and personal = 0  order by posts.id desc limit 6"));

			$i = 0;
			foreach($posts as $post){
				if($post[6]){$reply = mysqli_fetch_array($this->query("select entry from posts where id = {$post[7]}")); $posts[$i][8] = $reply[0];}
				$i++;
			}

			$i = 0;
			foreach($posts as $post){

				if((time() - $post[3]) > 5) {
					$posts[$i++][3] = $this->time_format($post[3])."ago";
				} else {
					$posts[$i++][3] = "Now";
				}
			}

			$photos = mysqli_fetch_all($this->query("select dir from photos where place = {$id} and dir not like '%profile%' order by `order` desc"));
			$top_badge = mysqli_fetch_array($this->query("SELECT badge, COUNT(badge) AS users FROM badges where place = {$id} GROUP BY badge ORDER BY users desc limit 1"));



			$badge = array(
				0 => " (Fraternity House)",
				1 => " (Sorority House)",
				2 => " (Party House)",
				3 => " (Nerd House)",
				4 => " (Beach House)",
				5 => " (Apartment Complex)",
				6 => " (Luxury Living)",
				7 => " (Close to School)"
			);
			
			$badge_color = array(
				0 => "#65b1cf",
				1 => "#e153bc",
				2 => "#b87726",
				3 => "#adadad",
				4 => "#98c667",
				5 => "#829fb1",
				6 => "#edb689",
				7 => "#65b1cf"
			);

			$place['views'] = $views['views'];
			$place['badges'] = $badges['badges'];
			$place['badge'] = $top_badge['badge'];
			$place['posts'] = $posts;
			$place['badge_color'] = $badge_color[$top_badge['badge']];
			$place['photos'] = $photos;
			$place['title'] = 'OnMyBlock - '.$place['name'].$badge[$top_badge['badge']];

			$rand = rand(0, 10);

			$neighbors_query = $this->query("SELECT places.id, name, street, route, price, size, rooms, bathrooms, type ,((ACOS(SIN({$place['latitude']} * PI() / 180) * SIN(`latitude` * PI() / 180) + COS({$place['latitude']} * PI() / 180) * COS(`latitude` * PI() / 180) * COS(({$place['longitude']} - `longitude`) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS distance FROM places join photos on places.id = photos.place WHERE (`latitude` BETWEEN ({$place['latitude']} - 10) AND ({$place['latitude']} + 10) AND `longitude` BETWEEN ({$place['longitude']} - 10) AND ({$place['longitude']} + 10)) and places.id <> {$id} group by street ORDER BY distance ASC, date desc limit $rand, 4;");

			$i = 0;
			while($neighbor = mysqli_fetch_assoc($neighbors_query)){
				if($neighbor['distance'] < 5){
					$place['neighbors'][$i] = $neighbor;
					$place['neighbors'][$i++]['distance'] = round($neighbor['distance']*10)/10;
				}
			}

			$this->cache->add("place_{$id}", $place, false, 86400);
		}
		
		return $place;

	}

	public function time_format($tm,$rcs = 0) {
		$cur_tm = time(); $dif = $cur_tm-$tm;
		$pds = array('second','minute','hour','day','week','month','year','decade');
		$lngh = array(1,60,3600,86400,604800,2630880,31570560,315705600);
		for($v = sizeof($lngh)-1; ($v >= 0)&&(($no = $dif/$lngh[$v])<=1); $v--); if($v < 0) $v = 0; $_tm = $cur_tm-($dif%$lngh[$v]);

		$no = floor($no); if($no <> 1) $pds[$v] .='s'; $x=sprintf("%d %s ",$no,$pds[$v]);
		if(($rcs == 1)&&($v >= 1)&&(($cur_tm-$_tm) > 0)) $x .= time_ago($_tm);
		return $x;
	}

	public function geo_search($latitude, $longitude, $radius, $limit)
	{
		return mysqli_fetch_all($this->query("

		SELECT id,name,latitude,longitude,price,size,rooms,bathrooms, type ,((ACOS(SIN({$latitude} * PI() / 180) * SIN(`latitude` * PI() / 180) + COS({$latitude} * PI() / 180) * COS(`latitude` * PI() / 180) * COS(({$longitude} - `longitude`) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS `distance`

		FROM `places`

		WHERE
		(`latitude` BETWEEN ({$latitude} - {$radius}) AND ({$latitude} + {$radius})

  		AND `longitude` BETWEEN ({$longitude} - {$radius}) AND ({$longitude} + {$radius}))

		ORDER BY `distance` ASC
		limit {$limit};

		"));
	}

	public function get_user_networks($id){

		return mysqli_fetch_all($this->query("select networks.name from networks join affiliations on user = {$id} where networks.id = affiliations.network"));

	}

	public function get_user_places($id){

		return mysqli_fetch_all($this->query("select * from places join residents on user = {$id} where place = id"));

	}
	
	public function deleteMemCached()
	{
		Memcached::quit();
	}


}

?>