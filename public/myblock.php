<?php

if($_COOKIE['omb_session'] == 'loggedout' || $_COOKIE['omb_session'] == '')
{
	header('Location: home');
}

$user_info = $user->get_info();

// Networks
$user_info['networks'] = mysqli_fetch_all($user->db->query("select name from networks join affiliations on affiliations.user = {$user->id} where networks.id = affiliations.network"));


// Places
$user_info['places'] = mysqli_fetch_all($user->db->query("select * from places join residents on user = {$user->id} where place = id"));


// Roommates
$user_info['roommates'] = mysqli_fetch_all($user->db->query("select users.* from users join residents on users.id = residents.user and users.id <> {$user->id} and residents.current = 1 where residents.place = (select group_concat(place) from residents where user = {$user->id} and current = 1)"));


// Favorites 
$user_info['favorites'] = mysqli_fetch_all($user->db->query("select places.id, places.name, places.street, places.route, places.rooms, places.bathrooms, places.price from places join favorites on user = {$user->id} where favorites.place = places.id order by favorites.date desc"));


// City
$user_info['city_temp'] = mysqli_fetch_all($user->db->query("select city from places join residents on user = {$user->id} and current = 1 where id = place"));

//likes
$user_info['place_likes'] = mysqli_fetch_all($user->db->query("select p.id, p.street, p.route from place_likes as pl join places as p on p.id=pl.place where user={$user->id}"));


$i = 0;
foreach($user_info['city_temp'] as $city){

	$user_info['city'][$i] = $user_info['city_temp'][$i++][0];
	
}

$i = 0;
foreach($user_info['favorites'] as $place){

	$views = mysqli_fetch_array($user->db->query("select count(*) as views from views where type = 1 and page = {$place[0]}"));
	$user_info['favorites'][$i]['views'] = $views['views'];

	if($user_info['favorites'][$i][1] == ''){
		$user_info['favorites'][$i][1] = $user_info['favorites'][$i][2]." ".$user_info['favorites'][$i][3];
	}
	$i++;
}


$i = 0;
foreach($user_info['place_likes'] as $like){

	$user_info['place_likes'][$i]['id'] = $user_info['place_likes'][$i][0];
	$user_info['place_likes'][$i]['name'] = $user_info['place_likes'][$i][1]." ".$user_info['place_likes'][$i][2];
	
	$i++;
}

$i = 0;
foreach($user_info['places'] as $place){

	$user_info['places'][$i]['id'] = $user_info['places'][$i][0];
	$user_info['places'][$i]['name'] = $user_info['places'][$i][2];
	$user_info['places'][$i]['street'] = $user_info['places'][$i][4];
	$user_info['places'][$i]['route'] = $user_info['places'][$i][5];
	$user_info['places'][$i]['state'] = $user_info['places'][$i][6];
	$user_info['places'][$i]['city'] = $user_info['places'][$i][8];
	$user_info['places'][$i]['postal'] = $user_info['places'][$i][10];
	$user_info['places'][$i]['photos'] = mysqli_fetch_all($user->db->query("select dir from photos where place = {$user_info['places'][$i]['id']} and dir not like '%profile%' order by id desc limit 3"));
	$user_info['places'][$i]['residents'] = $user->db->get_residents($user_info['places'][$i][0]);
	$user_info['places'][$i]['current'] = mysqli_fetch_array($user->db->query("select current from residents where user = {$user->id} and place = {$user_info['places'][$i][0]}"));


	if($user_info['places'][$i]['name'] == ''){

		$user_info['places'][$i]['name'] = $user_info['places'][$i]['street']." ".$user_info['places'][$i]['route'];
	}
	$i++;
}


$title = $user_info['first_name'].' '.$user_info['last_name'];


$stats = array(
	"profile_views" => mysqli_fetch_array($user->db->query("select count(id) as count from views where page = {$user->id}")),
	"friends_on_onmyblock" => mysqli_fetch_array($user->db->query("select count(*) as count from friends join users on users.id = friends.friend where user = {$user->id}")),
	"total_places" => mysqli_fetch_array($user->db->query("select count(*) as count from places where state = 'ca'")),
	"top_neighborhoods" => mysqli_fetch_all($user->db->query("select neighborhood, count(id) as count from places where neighborhood <> '' and status=1 and (city like '%Mission Beach%' or neighborhood like '%Mission Beach%' or route like '%Mission Beach%' 
														or city like '%Pacific Beach%' or neighborhood like '%Pacific Beach%' or route like '%Pacific Beach%' 
														or city like '%Morena%' or neighborhood like '%Morena%' or route like '%Morena%' 
														or city like '%Linda Vista%' or neighborhood like '%Linda Vista%' or route like '%Linda Vista%' 
														or city like '%Ocean Beach%' or neighborhood like '%Ocean Beach%' or route like '%Ocean Beach%' 
														or city like '%Point Loma%' or neighborhood like '%Point Loma%' or route like '%Point Loma%' 
														or city like '%La Jolla%' or neighborhood like '%La Jolla%' or route like '%La Jolla%' 
														or city like '%Mission Valley%' or neighborhood like '%Mission Valley%' or route like '%Mission Valley%' 
														or city like '%University Heights%' or neighborhood like '%University Heights%' or route like '%University Heights%' 
														or city like '%Hillcrest%' or neighborhood like '%Hillcrest%' or route like '%Hillcrest%' 
														or city like '%North Park%' or neighborhood like '%North Park%' or route like '%North Park%' 
														or city like '%Downtown%' or neighborhood like '%Downtown%' or route like '%Downtown%' ) group by neighborhood order by count(id) desc limit 4")),
	"popular_places" => mysqli_fetch_all($user->db->query("select places.id, places.name, places.street, places.route, count(views.page) as rank
from places 
join views on views.page = places.id 
join place_likes on place_likes.place = places.id
join posts on posts.place = places.id
join photos on photos.place = places.id
group by places.id order by rank desc limit 10")),
	"liked_places" => mysqli_fetch_all($user->db->query("select places.id from places join place_likes on place_likes.place = places.id and place_likes.user = {$user->id} order by rand() limit 4"))
);
shuffle($stats['popular_places']);
$stats['popular_places'] = array_slice($stats['popular_places'], 0, 3);

$user->db->query("update user_notification set number_notification=0 where user = {$user->id}");


$smarty->assign('stats', $stats);
$smarty->assign('user', $user_info);
$smarty->assign('links', $user->get_links());
$smarty->assign('title', $title);
$smarty->assign('filename', 'myblock');


$smarty->display('pages/myblock.tpl');




?>
