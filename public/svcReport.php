<?php

	$arrayPage = explode('/',$_GET['page']);

	switch($arrayPage[1])
	{
		case 'selectPlaces':
				selectPlaces($user->db, $arrayPage[2], $arrayPage[3]);
			break;
			
		case 'selectPlaceId':
				selectPlaceId($user->db);
			break;
			
		case 'updatePlace':
				updatePlace($user->db);
			break;
			
		case 'selectUsers':
				selectUsers($user->db);
			break;
			
		case 'selectUserId':
				selectUserId($user->db);
			break;
		
		case 'permissions':
				permissions($user->db, $user->id);
			break;
			
		case 'selectnoPlaces':
				selectnoPlaces($user->db);
			break;
			
		case 'updateNoPlace':
				updateNoPlace($user->db);
			break;
	}

	function selectPlaces($db, $idUniversity, $date)
	{
		$page = (int) $_POST['page'];
		$rp = (int) $_POST['rp'];

		$limitFinal = $page*$rp;
		$limitInit = $limitFinal-$rp;
		
		switch($idUniversity)
		{
			case 1:
					$where = "where city like '%College wes%' or neighborhood like '%College wes%' 
								or city like '%Baja Drive%' or neighborhood like '%Baja Drive%' 
								or city like '%SDSU%' or neighborhood like '%SDSU%' 
								or city like '%College area%' or neighborhood like '%College area%'
								or city like '%Del Cerro%' or neighborhood like '%Del Cerro%'
								or city like '%Mission Valley%' or neighborhood like '%Mission Valley%'
								or city like '%Kensington%' or neighborhood like '%Kensington%'
								or city like '%La Mesa%' or neighborhood like '%La Mesa%'
								or city like '%Allied Gardens%' or neighborhood like '%Allied Gardens%'";
				break;
			case 2:
					$where = "where city like '%Old Towne Orange%' or neighborhood like '%Old Towne Orange%' 
								or city like '%Chapman and Tus%' or neighborhood like '%Chapman and Tus%' 
								or city like '%Costa Mesa%' or neighborhood like '%Costa Mesa%' 
								or city like '%Villa Park%' or neighborhood like '%Villa Park%' 
								or city like '%Glassell%' or neighborhood like '%Glassell%'
								or city like '%Collins%' or neighborhood like '%Collins%'
								or city like '%Tustin%' or neighborhood like '%Tustin%'";
				break;
			case 3:
					$where = "where city like '%Menlo%' or neighborhood like '%Menlo%' 
								or city like '%Irvine Are%' or neighborhood like '%Irvine Are%' 
								or city like '%Ellendale%' or neighborhood like '%Ellendale%' 
								or city like '%USC%' or neighborhood like '%USC%' 
								or city like '%University Villag%' or neighborhood like '%University Villag%'
								or city like '%Adams & Ellendale%' or neighborhood like '%Adams & Ellendale%'
								or city like '%W 29th St%' or neighborhood like '%W 29th St%'";
				break;
			case 4:
					$where = "where city like '%Westwood Village%' or neighborhood like '%Westwood Village%' 
								or city like '%Beverly Hills%' or neighborhood like '%Beverly Hills%' 
								or city like '%Westwood Hills%' or neighborhood like '%Westwood Hills%' 
								or city like '%UCLA%' or neighborhood like '%UCLA%' 
								or city like '%Greenfield%' or neighborhood like '%Greenfield%'";
				break;
			case 5:
					$where = "where city like '%San Luis Obispo%' or neighborhood like '%San Luis Obispo%' 
								or city like '%Downtown SLO%' or neighborhood like '%Downtown SLO%' 
								or city like '%Grover Beach%' or neighborhood like '%Grover Beach%' 
								or city like '%UCLA%' or neighborhood like '%UCLA%' 
								or city like '%Pismo Beach%' or neighborhood like '%Pismo Beach%'";
				break;
			case 6:
					$where = "where city like '%Marina Del Ray%' or neighborhood like '%Marina Del Ray%' 
								or city like '%La Tijera%' or neighborhood like '%La Tijera%' 
								or city like '%Manchester%' or neighborhood like '%Manchester%' 
								or city like '%Westchester%' or neighborhood like '%Westchester%' 
								or city like '%Culver City%' or neighborhood like '%Culver City%'";
				break;
			case 7:
					$where = "where city like '%Isla Vista%' or neighborhood like '%Isla Vista%' 
								or city like '%Goleta%' or neighborhood like '%Goleta%' 
								or city like '%Fortuna%' or neighborhood like '%Fortuna%' 
								or city like '%Goleta Beach%' or neighborhood like '%Goleta Beach%'";
				break;
			case 8:
					$where = "where city like '%Santa Clara%' or neighborhood like '%Santa Clara%' 
								or city like '%San Jose%' or neighborhood like '%San Jose%' 
								or city like '%Milpitas%' or neighborhood like '%Milpitas%' ";
				break;
				
			case 9:
					$where = "where city like '%Stanford%' or neighborhood like '%Stanford%' 
								or city like '%Palo Alto%' or neighborhood like '%Palo Alto%' 
								or city like '%Menlo Park%' or neighborhood like '%Menlo Park%' 
								or city like '%Mountain View%' or neighborhood like '%Mountain View%' 
								or city like '%Redwood City%' or neighborhood like '%Redwood City%'";
				break;
				
			case 10:
					$where = "where city like '%Richmond%' or neighborhood like '%Richmond%' 
								or city like '%Inner sunset%' or neighborhood like '%Inner sunset%' 
								or city like '%UCSF%' or neighborhood like '%UCSF%' 
								or city like '%Parkside%' or neighborhood like '%Parkside%'";
				break;
				
			case 11:
					$where = "where `status` = 1 and (postal=92109 or postal=92109 or postal=92110  or postal=92107 or postal=92106 or postal=92037 or postal=92108 or postal=92116 or postal=92103 or postal=92104 or postal=92101)";
				break;
				
			case 12:
					$where = "where DATE(date) ='{$date}' ";
				break;
				
			default:
					$where = "where city like '%Mission Beach%' or neighborhood like '%Mission Beach%' 
								or city like '%Pacific Beach%' or neighborhood like '%Pacific Beach%' 
								or city like '%Morena%' or neighborhood like '%Morena%' 
								or city like '%Linda Vista%' or neighborhood like '%Linda Vista%'
								or city like '%Ocean Beach%' or neighborhood like '%Ocean Beach%'
								or city like '%Point Loma%' or neighborhood like '%Point Loma%'
								or city like '%La Jolla%' or neighborhood like '%La Jolla%'
								or city like '%Mission Valley%' or neighborhood like '%Mission Valley%'
								or city like '%University Heights%' or neighborhood like '%University Heights%'
								or city like '%Hillcrest%' or neighborhood like '%Hillcrest%'
								or city like '%North Park%' or neighborhood like '%North Park%'
								or city like '%Downtown%' or neighborhood like '%Downtown%'
								or city like '%College wes%' or neighborhood like '%College wes%' 
								or city like '%Baja Drive%' or neighborhood like '%Baja Drive%' 
								or city like '%SDSU%' or neighborhood like '%SDSU%' 
								or city like '%College area%' or neighborhood like '%College area%'
								or city like '%Old Towne Orange%' or neighborhood like '%Old Towne Orange%' 
								or city like '%Chapman and Tus%' or neighborhood like '%Chapman and Tus%' 
								or city like '%Costa Mesa%' or neighborhood like '%Costa Mesa%' 
								or city like '%Villa Park%' or neighborhood like '%Villa Park%' 
								or city like '%Glassell%' or neighborhood like '%Glassell%'
								or city like '%Collins%' or neighborhood like '%Collins%'
								or city like '%Menlo%' or neighborhood like '%Menlo%' 
								or city like '%Irvine Are%' or neighborhood like '%Irvine Are%' 
								or city like '%Ellendale%' or neighborhood like '%Ellendale%' 
								or city like '%USC%' or neighborhood like '%USC%' 
								or city like '%University Villag%' or neighborhood like '%University Villag%'
								or city like '%Adams & Ellendale%' or neighborhood like '%Adams & Ellendale%'
								or city like '%W 29th St%' or neighborhood like '%W 29th St%'
								or city like '%Westwood Village%' or neighborhood like '%Westwood Village%' 
								or city like '%Beverly HIlls%' or neighborhood like '%Beverly HIlls%' 
								or city like '%Westwood Hills%' or neighborhood like '%Westwood Hills%' 
								or city like '%UCLA%' or neighborhood like '%UCLA%' 
								or city like '%Greenfield%' or neighborhood like '%Greenfield%'
								or city like '%San Luis Obispo%' or neighborhood like '%San Luis Obispo%' 
								or city like '%Downtown SLO%' or neighborhood like '%Downtown SLO%' 
								or city like '%Grover Beach%' or neighborhood like '%Grover Beach%' 
								or city like '%UCLA%' or neighborhood like '%UCLA%' 
								or city like '%Pismo Beach%' or neighborhood like '%Pismo Beach%'
								or city like '%Marina Del Ray%' or neighborhood like '%Marina Del Ray%' 
								or city like '%La Tijera%' or neighborhood like '%La Tijera%' 
								or city like '%Manchester%' or neighborhood like '%Manchester%' 
								or city like '%Westchester%' or neighborhood like '%Westchester%' 
								or city like '%Culver City%' or neighborhood like '%Culver City%'
								or city like '%Isla Vista%' or neighborhood like '%Isla Vista%' 
								or city like '%Goleta%' or neighborhood like '%Goleta%' 
								or city like '%Fortuna%' or neighborhood like '%Fortuna%' 
								or city like '%Goleta Beach%' or neighborhood like '%Goleta Beach%'
								or city like '%Santa Clara%' or neighborhood like '%Santa Clara%' 
								or city like '%San Jose%' or neighborhood like '%San Jose%' 
								or city like '%Milpitas%' or neighborhood like '%Milpitas%'
								or city like '%Stanford%' or neighborhood like '%Stanford%' 
								or city like '%Palo Alto%' or neighborhood like '%Palo Alto%' 
								or city like '%Menlo Park%' or neighborhood like '%Menlo Park%' 
								or city like '%Mountain View%' or neighborhood like '%Mountain View%' 
								or city like '%Redwood City%' or neighborhood like '%Redwood City%'
								or city like '%Richmond%' or neighborhood like '%Richmond%' 
								or city like '%Inner sunset%' or neighborhood like '%Inner sunset%' 
								or city like '%UCSF%' or neighborhood like '%UCSF%' 
								or city like '%Parkside%' or neighborhood like '%Parkside%'";
				break;
		}
		
		
		
		if($_POST['sortname']=='iso')
		{
			$order = "order by p.date desc ";
		}else{
			$order = "order by p.{$_POST['sortname']} {$_POST['sortorder']} ";
		}
		
		if($_POST['query']!="")
		{
			$rsPlaces = mysqli_fetch_all($db->query("select p.id, DATE(p.date) as date, p.route, p.city, p.neighborhood, p.type, p.street from places as p where city like '%{$_POST['query']}%' or neighborhood like '%{$_POST['query']}%' or route like '%{$_POST['query']}%' {$order} limit ".$limitInit.",".$rp.";"));
			
			$total = mysqli_fetch_row($db->query("select count(*) from places as p where city like '%{$_POST['query']}%' or neighborhood like '%{$_POST['query']}%' or route like '%{$_POST['query']}%' {$order} ;"));
		}else{
			$rsPlaces = mysqli_fetch_all($db->query("select p.id, DATE(p.date) as date, p.route, p.city, p.neighborhood, p.type, p.street from places as p {$where} {$order} limit ".$limitInit.",".$rp.";"));
											 //echo "select p.id, DATE(p.date) as date, p.route, p.city, p.neighborhood, p.type, p.street from places as p {$where} {$order} limit ".$limitInit.",".$rp.";";
			$total = mysqli_fetch_row($db->query("select count(*) from places {$where}"));
		}
		//print_r($rsPlaces);
		foreach($rsPlaces as $item)
		{
			$jsonString .= '{"id":"'.$item[0].'","cell":{"idPlace":"'.$item[0].'","date":"'.$item[1].'","route":"'.$item[2].'","city":"'.$item[3].'","neighborhood":"'.$item[4].'","type":"'.$item[5].'","street":"'.$item[6].'"}},';
		}

		$jsonString = rtrim($jsonString, ",");

		echo '{"page":'.$_POST['page'].',"total":'.$total[0].',"rows":['.$jsonString.'],"post":[]}';
	}
	
	function selectnoPlaces($db)
	{
		$page = (int) $_POST['page'];
		$rp = (int) $_POST['rp'];

		$limitFinal = $page*$rp;
		$limitInit = $limitFinal-$rp;

		$rsPlaces = mysqli_fetch_all($db->query("select p.id, DATE(p.date) as date, p.route, p.city, p.neighborhood, p.type, p.codCity from places as p where p.status=99 order by p.date desc limit ".$limitInit.",".$rp.";"));

		$total = mysqli_fetch_row($db->query("select count(*) from places  where places.status=99"));

		foreach($rsPlaces as $item)
		{
			if($item[5] != ''){
				$placeArray = explode(",", $item[5]);
				$stringUrl = $placeArray[1];
			}
			
			switch($item[6])
			{
				case 1:
						$fUrl = "http://sandiego.craigslist.org/".$stringUrl;
					break;
					
				case 2:
						$fUrl = "http://losangeles.craigslist.org/".$stringUrl;
					break;
					
				case 3:
						$fUrl = "http://sfbay.craigslist.org/".$stringUrl;
					break;
					
				case 4:
						$fUrl = "http://santabarbara.craigslist.org/".$stringUrl;
					break; 
					
				case 5:
						$fUrl = "http://slo.craigslist.org/".$stringUrl;
					break;
					
			}
		
			$jsonString .= '{"id":"'.$item[0].'","cell":{"idPlace":"'.$item[0].'","date":"'.$item[1].'","route":"'.$item[2].'","city":"'.$item[3].'","neighborhood":"'.$item[4].'","type":"'.$fUrl.'"}},';
		}

		$jsonString = rtrim($jsonString, ",");

		echo '{"page":'.$_POST['page'].',"total":'.$total[0].',"rows":['.$jsonString.'],"post":[]}';
	}
	
	function selectPlaceId($db)
	{
		$idPlace = $_POST['placesId'];
		
		$rsPlace = mysqli_fetch_row($db->query("select id, street, route, city, neighborhood from places where id={$idPlace}"));
				
		$array = array('id'=>$rsPlace[0],
					   'street'=>$rsPlace[1],
					   'route'=>$rsPlace[2],
					   'city'=>$rsPlace[3],
					   'neighborhood'=>$rsPlace[4]
					);
					
		$json = json_encode($array);
		
		print_r($json);
	}

	function updatePlace($db)
	{
		$idPlace = $_POST['placesId'];
		$neighborhood = $_POST['neighborhoodEdit'];
		
		$db->query("update places set neighborhood='{$neighborhood}' where id={$idPlace}");
	}
	
	function updateNoPlace($db)
	{
		$idPlace = $_POST['placesId'];
		$street = $_POST['streetEdit'];
		$route = $_POST['routeEdit'];
		$city = $_POST['cityEdit'];
		$neighborhood = $_POST['neighborhoodEdit'];
		
		$db->query("update places set street='{$street}',route='{$route}',city='{$city}',neighborhood='{$neighborhood}' where id={$idPlace}");
	}

	function selectUsers($db)
	{
		$page = (int) $_POST['page'];
		$rp = (int) $_POST['rp'];

		$limitFinal = $page*$rp;
		$limitInit = $limitFinal-$rp;

		$rsUsers = mysqli_fetch_all($db->query("select u.id, u.first_name, u.last_name, u.gender, u.email from users as u order by u.date desc limit ".$limitInit.",".$rp.";"));
		
		$total = mysqli_fetch_row($db->query("select count(*) from users"));
		
		foreach($rsUsers as $item)
		{
			if($item[0]!=1 or $item[0]!=597597919 or $item[0]!=1348620030 or $item[0]!=1310384220)
			{
				$rsItem = mysqli_fetch_all($db->query("select networks.`name` from affiliations join networks on affiliations.network=networks.id where affiliations.`user`={$item[0]}"));
				
				$jsonString .= '{"id":"'.$item[0].'","cell":{"id":"'.$item[0].'","first_name":"'.$item[1].'","last_name":"'.$item[2].'","gender":"'.$item[3].'","email":"'.$item[4].'","school":"'.$rsItem[0][0].'"}},';
			}
		}
		
		$jsonString = rtrim($jsonString, ",");

		echo '{"page":'.$page.',"total":'.$total[0].',"rows":['.$jsonString.'],"post":[]}';
	}
	
	function selectUserId($db)
	{
		$idUser = $_POST['userId'];
		
		$query  = "select u.id, u.first_name, u.last_name, u.gender, u.email from users as u where u.id={$idUser}";
		
		$rsUserAdminStatus = mysqli_fetch_row($db->query("select exists (select 1 from user_admin where user_id = {$idUser} and status = 1) as result"));
		
		$rsUser = mysqli_fetch_row($db->query($query));
				
		$array = array('id'=>$rsUser[0],
					   'name'=>$rsUser[1].' '.$rsUser[2],
					   'email'=>$rsUser[4],
					   'status'=>$rsUserAdminStatus[0]
					  );
					
		$json = json_encode($array);
	
		print_r($json);  
	}
	
	function permissions($db, $user_id)
	{
		$idUser = $_POST['userId'];
		$check = $_POST['check'];
		
		$rsUser = mysqli_fetch_row($db->query("select exists (select 1 from user_admin where user_id = {$idUser}) as result"));
		
		if($rsUser[0]==0)
		{			
			$db->query("insert into user_admin (user_id, status) value ({$idUser}, {$check})");
		}else{
			$db->query("update user_admin set status={$check} where user_id = {$idUser}");
		}
	}




