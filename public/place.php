<?php

$place_id = $_GET['id'];


if($_POST['edit_form'] == 'update'){

	$user->db->cache->delete("place_{$place_id}");

	$name = '';
	$unit = '';
	$size = '';
	$price = '';
	$rooms = '';
	$bathrooms = '';
	$neighborhood = '';
		
	if($_POST['house_name']!='')
	{
		$name = "name = '{$_POST['house_name']}'";
	}
	
	if($_POST['house_unit']!='')
	{
		$unit = ",unit = {$_POST['house_unit']}";
	}
	
	if($_POST['house_size']!='')
	{
		$size = ",size = {$_POST['house_size']}";
	}
	
	if($_POST['house_price']!='')
	{
		$price = ",price = '{$_POST['house_price']}'";
	}
	
	if($_POST['house_rooms']!='')
	{
		$rooms = ",rooms = {$_POST['house_rooms']}";
	}
	
	if($_POST['house_bathrooms']!='')
	{
		$bathrooms = ",bathrooms = {$_POST['house_bathrooms']}";
	}
	
	if($_POST['house_neighborhood']!='')
	{
		$neighborhood = ",neighborhood = '{$_POST['house_neighborhood']}'";
	}
	
	if($_POST['description']!='')
	{
		$description = ",description = '{$_POST['description']}'";
	}
	
	$address = $_POST['address'];

	if($address != ''){

		// get address info
		require('../classes/geo_parser.php');
		$geo = get_ggeocoder_json($_POST['address']);
		$street =  $geo->find_address_components('street_number');
		$route =  $geo->find_address_components('route');
		$neighborhood =  $geo->find_address_components('neighborhood');
		$city =  $geo->find_address_components('locality');
		$state =  $geo->find_address_components('administrative_area_level_1');
		$postal_code =  $geo->find_address_components('postal_code');

		if($geo->results['status'] == 'OK' && $street->short_name != '' && $route->short_name != '' && $state->short_name == 'CA')
		{
			$query = "update places set name='', street={$street->short_name}, route='{$route->short_name}', neighborhood='{$neighborhood->short_name}', city='{$city->short_name}', state='{$state->short_name}', postal={$postal_code->short_name}, latitude={$geo->results['latitude']}, longitude={$geo->results['longitude']} where id={$place_id}";
			
			$place_sql = $user->db->query($query); 
		}
	}else
	{
		$query = "update places set {$name} {$unit} {$size} {$price} {$rooms} {$bathrooms} {$neighborhood} {$description} where id={$place_id}";
		
		$place_sql = $user->db->query($query);
		
	}
	
	if($_POST['realtor_id'] > 1){
		$query = 'update realtors set name = ?, phone = ?, email = ?, link = ? where id = ?';

		$stmt = $user->db->connection->prepare($query);
		$realtor_phone = preg_replace("/[^0-9]/", '', $_POST['realtor_phone']);
		mysqli_stmt_bind_param($stmt, "sissi", $_POST['realtor_name'], $realtor_phone, $_POST['realtor_email'], $_POST['realtor_link'], $_POST['realtor_id']);
		$realtor_sql = $stmt->execute();
	} else {
		$query = 'insert into realtors values (null, ?, ?, ?, ?)';

		$stmt = $user->db->connection->prepare($query);
		if(strlen($_POST['realtor_phone']) > 9){
			$realtor_phone = preg_replace("/[^0-9]/", '', $_POST['realtor_phone']);
		}
		mysqli_stmt_bind_param($stmt, "ssss", $_POST['realtor_name'], $_POST['realtor_email'], $realtor_phone, $_POST['realtor_link']);
		$realtor_sql = $stmt->execute();
		$user->db->query("update places set realtor = {$user->db->connection->insert_id} where id = {$place_id}");

	}
	if($realtor_sql || $place_sql){
		$smarty->assign('edit_saved', true);
	} else {
		$smarty->assign('edit_saved', false);
	}

}


if(isset($_POST['entry']) && strlen($_POST['entry']) > 2){

	$user->db->cache->delete("place_{$place_id}");
	$db = new mysqli(DB_server, DB_username, DB_password, 'main');
	$query = 'insert into posts values (null, ?, now(), 0, ?, ?, 0, null)';
	$stmt = $db->prepare($query);
	$entry = strip_tags($_POST['entry']);
	mysqli_stmt_bind_param($stmt, "dis", $user->id, $place_id, $entry);
	$stmt->execute();
}

if(isset($_POST['reply']) && strlen($_POST['reply']) > 2){

	$user->db->cache->delete("place_{$place_id}");
	$db = new mysqli(DB_server, DB_username, DB_password, 'main');
	$query = 'insert into posts values (null, ?, now(), 0, ?, ?, 1, ?)';
	$stmt = $db->prepare($query);
	$entry = strip_tags($_POST['reply']);
	mysqli_stmt_bind_param($stmt, "disi", $user->id, $place_id, $entry, $_POST['post_id']);
	$stmt->execute();
}


if($place_id != '') {
	
	$user->db->add_place_visit($user->id, $place_id);
	
	if($user->db->is_place($place_id)) {
		$place = $user->db->get_place($place_id);
	} else {
		header("location:https://onmyblock.com/browse");
	}

	if($user->is_loggedin() and $_COOKIE['omb_session'] == 'loggedin'){ 

		$viewer['owner'] = mysqli_fetch_array($user->db->query("select exists (select 1 from events where user = {$user->id} and action = 0 and target = {$place_id})"));
		$viewer['owner'] = $viewer['owner'][0];
		
		if(!$viewer['owner']){
			$viewer['owner'] = mysqli_fetch_array($user->db->query("select exists (select 1 from residents where user = {$user->id} and current = 1 and place = {$place_id})"));
			$viewer['owner'] = $viewer['owner'][0];
		}

		$viewer['liked'] = mysqli_fetch_array($user->db->query("select exists (select 1 from place_likes where user = {$user->id} and place = {$place_id})"));
		$viewer['liked'] = $viewer['liked'][0];

		$viewer['wishlisted'] = mysqli_fetch_array($user->db->query("select exists (select 1 from favorites where user = {$user->id} and place = {$place_id})"));
		$viewer['wishlisted'] = $viewer['wishlisted'][0];
		
		$viewer['resident'] = mysqli_fetch_array($user->db->query("select current from residents where user = {$user->id} and place = {$place_id}"));
		$viewer['current_pending'] = mysqli_fetch_array($user->db->query("select status from residents_queue where user ={$user->id} and place = {$place_id}"));
		
		
		if($viewer['resident'][0] == '1'){
			$viewer['resident']['current'] = 1;
		} elseif ($viewer['resident'][0] == '0'){
			$viewer['resident']['past'] = 1;
		}


		if($viewer['current_pending'][0] > 0){
			$viewer['current_pending'] = 1;
		} 
		
		$smarty->assign('viewer', $viewer);
	}
	
	$dataUser = mysqli_fetch_row($user->db->query("select * from users where id = {$user->id}"));
	
	$posts = mysqli_fetch_all($user->db->query("select * from posts where place = {$place_id}"));
	
	$arrayFinal = array();
	
	$cantPosts=0;
	
	foreach($posts as $item)
	{
		$usdata = mysqli_fetch_row($user->db->query("select * from users where id = ".$item[1]));
		
		$postsArray = array('userFb'=>$usdata[1],
							'userName'=>$usdata[2].' '.$usdata[3],
							'post'=>$item[5]
							);
							
		array_push($arrayFinal, $postsArray);
		
		$cantPosts++;
	}
	
	$likes['users'] = mysqli_fetch_all($user->db->query("select users.id, users.first_name, users.last_name from users join place_likes on place_likes.place = {$place_id} where place_likes.user = users.id"));
	
	$likes['total'] = count($likes['users']);
	
	$smarty->assign('listing_date', $user->db->time_format($place['epoch_date'])."ago");
	$smarty->assign('likes', $likes);
	$smarty->assign('title', $place['title']);
	$smarty->assign('place', $place);
	$smarty->assign('filename', 'place');
	$smarty->assign('dataUser', $dataUser);
	$smarty->assign('arrayFinal', $arrayFinal);
	$smarty->assign('cantPosts', $cantPosts);
	
	$smarty->display('pages/place.tpl');

}

?>
