function initialize() {

	var pos = new google.maps.LatLng(-12.05, -76.90);
	var opciones = {
		zoom : 12,
		center: pos,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	var div = document.getElementById('map');
	var map = new google.maps.Map(div, opciones);
	var infowindow = new google.maps.InfoWindow({
		content: '',
		maxWidth: 400
	});
	
	 
	var marcadores = [
		{
			position:{
				lat : -12.085904775672402,
				lng : -76.97600841522217
				
			},
			contenido: "<div id=\"content\">"+
				"<h2>Jockey Plaza</h2><br>"+
					"<div id=\"bodyContent\">" +
						"<p><b>Direccion:</b>Av. Javier Prado Este #4200 - Monterrico , Santiago de Surco.<br><br> <b>Referencia</b>: Piso 2 del Centro Comercial Jockey Plaza, saliendodel Food Court, a la izquierda se encuentra el<br> modulo de Samsung</p>" +
						
							
					"</div>"+
			"</div>"
			
		},
		
		{
			position:{
				lat : -11.993945638985883,
				lng : -77.0614743232727
				
			},
			contenido: "<div id=\"content\">"+
				"<h2>Mega Plaza</h2><br>"+
					"<div id=\"bodyContent\">" +
						"<p><b>Direccion:</b>Av. Alfredo Mendiola #3698, Independencia.<br><br> <b>Referencia</b>:Entra Principal del Mall Plaza, antes de llegar al patio de comidas, entre la entrada de Tottus y Ripley,<br> se encuentra el modulo de samsung.</p>" +
						
							
					"</div>"+
			"</div>"
			
		},
		
		{
			position:{
				lat : -12.111564601330272,
				lng : -77.0120358467102
				
			},
			contenido:"Hola estoy en el Open Plaza Angamos!"
			
			 
			
		},
		
			
				
		{
			position:{
				lat : -12.077176097218517,
				lng : -77.08273887634277
			},
			contenido:"Hola, estoy en Plaza San Miguel!"
			
		}  
		
		
							
	];
	for (var i = 0, j = marcadores.length; i < j; i++) {
		var contenido = marcadores[i].contenido;
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(marcadores[i].position.lat, marcadores[i].position.lng),
			map: map,
			icon:'images/icon-map.png',
			shadow:'images/icon-map-sombra.png'
				
		});
		(function(marker, contenido){                      
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.setContent(contenido);
				infowindow.open(map, marker);
			});
		})(marker,contenido);
	
	}
}