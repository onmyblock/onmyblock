<?php 

	$dataFilter = $_POST['dataFilter'];
	
	$page = $_POST['pagina'];
	
	if($page == 0){
	
		$limit = "limit 24";
		
	} elseif($page == 1){
	
		$limit = "limit 24, 24";
		
	} else {
		$li = $page*24+24;
		$limit = "limit {$li}, 24";
	}
	
	if($dataFilter == 'beach') {
		$query = "select places.id, neighborhood, city, state, rooms, price, street, route from places join photos where places.id = photos.place and latitude < 32.9 and latitude > 32.7 and longitude < -117.24 and status = 1 and (city like '%Mission Beach%' or neighborhood like '%Mission Beach%' 
														or city like '%Pacific Beach%' or neighborhood like '%Pacific Beach%' 
														or city like '%Morena%' or neighborhood like '%Morena%' 
														or city like '%Linda Vista%' or neighborhood like '%Linda Vista%'
														or city like '%Ocean Beach%' or neighborhood like '%Ocean Beach%'
														or city like '%Point Loma%' or neighborhood like '%Point Loma%'
														or city like '%La Jolla%' or neighborhood like '%La Jolla%'
														or city like '%Mission Valley%' or neighborhood like '%Mission Valley%'
														or city like '%University Heights%' or neighborhood like '%University Heights%'
														or city like '%Hillcrest%' or neighborhood like '%Hillcrest%'
														or city like '%North Park%' or neighborhood like '%North Park%'
														or city like '%Downtown%' or neighborhood like '%Downtown%') group by places.latitude order by date desc {$limit}";
	} elseif($dataFilter == 'school') {
		$query = "select places.id, neighborhood, city, state, rooms, price, street, route from places join photos where places.id = photos.place and latitude > 32.73 and latitude < 32.81 and longitude > -117.214 and longitude < -117.13 and status = 1 and (city like '%Mission Beach%' or neighborhood like '%Mission Beach%' 
														or city like '%Pacific Beach%' or neighborhood like '%Pacific Beach%' 
														or city like '%Morena%' or neighborhood like '%Morena%' 
														or city like '%Linda Vista%' or neighborhood like '%Linda Vista%'
														or city like '%Ocean Beach%' or neighborhood like '%Ocean Beach%'
														or city like '%Point Loma%' or neighborhood like '%Point Loma%'
														or city like '%La Jolla%' or neighborhood like '%La Jolla%'
														or city like '%Mission Valley%' or neighborhood like '%Mission Valley%'
														or city like '%University Heights%' or neighborhood like '%University Heights%'
														or city like '%Hillcrest%' or neighborhood like '%Hillcrest%'
														or city like '%North Park%' or neighborhood like '%North Park%'
														or city like '%Downtown%' or neighborhood like '%Downtown%') group by places.latitude order by date desc {$limit}";
	} elseif($dataFilter == 'popular') {
		$query = "select places.id, neighborhood, city, state, rooms, price, street, route from places join views on views.page = places.id join place_likes on place_likes.place = places.id join photos on photos.place = places.id where places.id = photos.place and dir like '%rofile%' and status = 1 and (city like '%Mission Beach%' or neighborhood like '%Mission Beach%' 
														or city like '%Pacific Beach%' or neighborhood like '%Pacific Beach%' 
														or city like '%Morena%' or neighborhood like '%Morena%' 
														or city like '%Linda Vista%' or neighborhood like '%Linda Vista%'
														or city like '%Ocean Beach%' or neighborhood like '%Ocean Beach%'
														or city like '%Point Loma%' or neighborhood like '%Point Loma%'
														or city like '%La Jolla%' or neighborhood like '%La Jolla%'
														or city like '%Mission Valley%' or neighborhood like '%Mission Valley%'
														or city like '%University Heights%' or neighborhood like '%University Heights%'
														or city like '%Hillcrest%' or neighborhood like '%Hillcrest%'
														or city like '%North Park%' or neighborhood like '%North Park%'
														or city like '%Downtown%' or neighborhood like '%Downtown%') group by places.latitude order by count(views.page) desc {$limit}";
	} elseif($dataFilter == 'browse_search'){
		
		$criterio = $_POST['valSearch'];
		
		if($criterio!='')
		{
			$arrayCriterio = explode(',',$criterio);
		}
		
		if(count($arrayCriterio)>1)
		{
			array_pop($arrayCriterio);
		}
		
		$places = array();
		$j=0;
		foreach($arrayCriterio as $item)
		{
			$item = trim($item);
			
			if($j>1)
			{
				continue;
			}
			
			$arraystreet = explode(' ',$item);
			
			if(is_numeric($arraystreet[0]))
			{
				$initial=1;
				$item=$arraystreet[1].' '.$arraystreet[2];
			}else
			{
				$initial=0;
			}
			
			$query .= "( city like '%{$item}%' or neighborhood like '%{$item}%' or state like '%{$item}%'  ";
			
			/* if($j<2)
			{ */
				$query .= " or route like '%{$item}%'";
			/* } */
				
			for($i=$initial; $i<count($arraystreet); $i++)
			{
				switch($arraystreet[$i])
				{
					case 'Court':
							$arraystreet[$i] = 'Ct';
						break;
						
					case 'street':
							$arraystreet[$i] = 'St';
						break;
						
					case 'Street':
							$arraystreet[$i] = 'St';
						break;
					case 'Avenue':
							$arraystreet[$i] = 'Ave';
						break;
						
					case 'Boulevard':
							$arraystreet[$i] = 'Blvd';
						break;
						
					case 'Road':
							$arraystreet[$i] = 'Rd';
						break;
						
					case 'Lane':
							$arraystreet[$i] = 'Ln';
						break;
					
					case 'Drive':
							$arraystreet[$i] = 'Dr';
						break;
						
					case 'Highway':
							$arraystreet[$i] = 'Hwy';
						break;
					
					case 'Parkway':
							$arraystreet[$i] = 'Pkwy';
						break;
				}
	
				$route.=$arraystreet[$i].' ';
			}
			
			$route = trim($route);
			
			if($j<1)
			{
				if(is_numeric($arraystreet[0]))
				{
					$query .= " or ( street={$arraystreet[0]} and ";
				}else{
					$query .= " or ";
				}
				
				if($route)
				{					
					$query .= " route like '%{$route}%' ";
				}
				
				if(is_numeric($arraystreet[0]))
				{
					$query .= " ) ";
				}
			}
			
			$query .= ") and ";
			
			$j++;
		}
		
		$query = rtrim($query,"and ");
		  
		$querySearch="select id, neighborhood, city, state, rooms, price, street, route from places where {$query} and status = 1 group by id ORDER BY date desc {$limit}";  
		
		$query = $querySearch;
		
		if($criterio=='')
		{
			$places = mysqli_fetch_all($user->db->query("select id, neighborhood, city, state, rooms, price, street, route from places where status = 1 {$limit}"));
		}
	} else {
		$query = "select places.id, neighborhood, city, state, rooms, price, street, route from places join photos where places.id = photos.place and status = 1 and (postal=92109 or postal=92109 or postal=92110  or postal=92107 or postal=92106 or postal=92037 or postal=92108 or postal=92116 or postal=92103 or postal=92104 or postal=92101) group by places.latitude order by date desc {$limit}";
	}
	
	$places =  mysqli_fetch_all($user->db->query($query));
	
	$i = 0;
	foreach($places as $place)
	{
		$views = mysqli_fetch_array($user->db->query("select count(*) as count from views where type = 1 and page = {$places[$i][0]}"));
		$places[$i]['views'] = $views['count'];
		$likes = mysqli_fetch_array($user->db->query("select count(*) as count from place_likes where place = {$places[$i][0]}"));
		$places[$i]['likes'] = $likes['count'];
		$comments = mysqli_fetch_array($user->db->query("select count(*) as count from posts where personal = 0 and place = {$places[$i][0]}"));
		$places[$i]['comments'] = $comments['count'];
		if($place[1] == ''){$places[$i][1] = $place[2].", ".$place[3];}
		$places[$i]['category'] = rand(0,2);
		//echo $places[$i][0].'-1.jpg';
		$height = mysqli_fetch_array($user->db->query("select height from photos where place = {$places[$i][0]} and dir = 'profile.jpg' "));//or dir = '{$places[$i][0]}-1.jpg'
		
		if($height[0])
		{
			$h = 210;
		}else
		{
			$h = $height[0];
		}
		 
		$places[$i]['height'] = $height[0];
		$i++;
	}
	
	if($user->is_loggedin() or $_COOKIE['omb_session'] == 'loggedin')
	{
		$i = 0;
		
		foreach($places as $place)
		{
			$liked = mysqli_fetch_array($user->db->query("select count(*) from place_likes where user = {$user->id} and place = {$place[0]}"));
			$places[$i]['liked'] = $liked[0];
			$favorited = mysqli_fetch_array($user->db->query("select count(*) from favorites where user = {$user->id} and place = {$place[0]}"));
			$places[$i]['favorited'] = $favorited[0];

			$i++;
		}
	}
	
	
	$smarty->assign('user', $user->get_info());
	$smarty->assign('places', $places);
	
	$smarty->display('pages/browse_items.tpl');
	
	