<?php

if($_COOKIE['omb_session'] == 'loggedout' || $_COOKIE['omb_session'] == ''){
	echo '<div class="non_user_popup"></div>';
}

if($_POST['ajax_action'] != ''){
	if($_POST['ajax_action'] == 'fav'){
		$response = mysqli_fetch_array($user->db->query("select count(*) from favorites where user = {$_POST['user_id']} and place = {$_POST['place_id']}"));
		if($response[0] == 1){
			$user->db->query("delete from favorites where user = {$_POST['user_id']} and place = {$_POST['place_id']}");
			echo 0;
		} else {
			$user->db->query("insert into favorites values ({$_POST['user_id']}, {$_POST['place_id']}, now())");
			echo 1;
		}
	}
	if($_POST['ajax_action'] == 'like'){
		$response = mysqli_fetch_array($user->db->query("select count(*) from place_likes where user = {$_POST['user_id']} and place = {$_POST['place_id']}"));
		if($response[0] == 1){
			$user->db->query("delete from place_likes where user = {$_POST['user_id']} and place = {$_POST['place_id']}");
			echo 0;
		} else {
			$user->db->query("insert into place_likes values ({$_POST['user_id']}, {$_POST['place_id']}, now())");
			echo 1;
		}
	}
	if($_POST['ajax_action'] == 'favorite'){

		if($_POST['action'] == 'favorite'){$user->db->query("insert into favorites values ({$_POST['user_id']},{$_POST['place_id']}, now())");}
		if($_POST['action'] == 'unfavorite'){$user->db->query("delete from favorites where user = {$_POST['user_id']} and place = {$_POST['place_id']}");}

	}

	if($_POST['ajax_action'] == 'place_like'){


		if($_POST['action'] == 'like'){$user->db->query("insert into place_likes values ({$_POST['user_id']}, {$_POST['place_id']}, now())");}
		if($_POST['action'] == 'unlike'){$user->db->query("delete from place_likes where user = {$_POST['user_id']} and place = {$_POST['place_id']}");}
		$likes = mysqli_fetch_array($user->db->query("select count(user) as likes from place_likes where place = {$_POST['place_id']}"));
		echo $likes['likes'];

	}

	if($_POST['ajax_action'] == 'past_resident'){


		if($_POST['action'] == 'add'){$user->db->query("insert into residents values ({$_POST['user_id']}, {$_POST['place_id']}, 0, now())");}
		if($_POST['action'] == 'remove'){$user->db->query("delete from residents where user = {$_POST['user_id']} and place = {$_POST['place_id']}");}
		$user->db->cache->delete("place_{$_POST['place_id']}");

	}

	if($_POST['ajax_action'] == 'past_resident_friends'){

		$user_ids = json_decode($_POST['user_ids']);

		foreach($user_ids as $user_id){

			$result = mysqli_fetch_array($user->db->query("select exists (select 1 from users where id = {$user_id})"));

			if($result[0]){

				$user->db->query("insert into residents values ({$user_id}, {$_POST['place_id']}, 0, now())");

			} else {

				$user->db->query("insert into residents_queue values ({$user_id}, {$_POST['place_id']}, 0)");

			}

		}

		$user->db->cache->delete("place_{$_POST['place_id']}");

	}

	if($_POST['ajax_action'] == 'confirm_resident'){

		$status = mysqli_fetch_array($user->db->query("select status from residents_queue where user = {$_POST['user_id']} and place = {$_POST['place_id']}"));

		if($status[0] == 1 || $status[0] == 2){
			$delete = $user->db->query("delete from residents_queue where user = {$_POST['user_id']} and place = {$_POST['place_id']}");
			if($delete){
				$user->db->query("insert into residents values ({$_POST['user_id']}, {$_POST['place_id']}, 1, now())");
			}
		} else if($status[0] == 3 && $_POST['owner'] == '1'){
		
			$user->db->query("update residents_queue set status = 2 where user = {$_POST['user_id']} and place {$_POST['place_id']}");
			
		} else if($status[0] == 3 && $_POST['owner'] == '0'){
			
			$user->db->query("update residents_queue set status = 1 where user = {$_POST['user_id']} and place {$_POST['place_id']}");
			
		}

		$user->db->cache->delete("place_{$_POST['place_id']}");

	}

	if($_POST['ajax_action'] == 'current_resident'){


		$owner = mysqli_fetch_array($user->db->query("select user from events where action = 0 and target = {$_POST['place_id']}"));

		if($owner[0] == null){
			$user->db->query("insert into events values (null, {$_POST['user_id']}, 0, {$_POST['place_id']}, now())");
			$new_owner = true;
		}

		if($owner[0] == $_POST['user_id'] || $new_owner){
			if($_POST['action'] == 'add'){$user->db->query("insert into residents values ({$_POST['user_id']}, {$_POST['place_id']}, 1, now())");}
			if($_POST['action'] == 'remove'){$user->db->query("delete from residents where user = {$_POST['user_id']} and place = {$_POST['place_id']}");}
		} else {
			if($_POST['action'] == 'add'){$user->db->query("insert into residents_queue values ({$_POST['user_id']}, {$_POST['place_id']}, 1)");}
			if($_POST['action'] == 'remove'){
				$user->db->query("delete from residents_queue where user = {$_POST['user_id']} and place = {$_POST['place_id']}");
				$user->db->query("delete from residents where user = {$_POST['user_id']} and place = {$_POST['place_id']}");}
		}

		$user->db->cache->delete("place_{$_POST['place_id']}");

	}

	if($_POST['ajax_action'] == 'current_resident_friends'){

		$user_ids = json_decode($_POST['user_ids']);

		$from_user = mysqli_fetch_array($user->db->query("select user from events where action = 0 and target = {$_POST['place_id']}"));


		foreach($user_ids as $user_id){

			$result = mysqli_fetch_array($user->db->query("select exists (select 1 from users where id = {$user_id})"));

			if($result[0]){

				if($from_user[0] == $_POST['from_user']){
					$user->db->query("insert into residents_queue values ({$user_id}, {$_POST['place_id']}, 2)");
				} else {
					$user->db->query("insert into residents_queue values ({$user_id}, {$_POST['place_id']}, 3)");
				}

			} else {

				$user->db->query("insert into residents_queue values ({$user_id}, {$_POST['place_id']}, 4)");

			}

		}

		$user->db->cache->delete("place_{$_POST['place_id']}");

	}



} else {


?>
<link rel="stylesheet" href="https://onmyblock.com/media/css/action.css" type="text/css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<script src="https://onmyblock.com/media/js/action.js"></script>
<script src="https://onmyblock.com/media/js/main.js"></script>
<script src="https://onmyblock.com/media/js/jquery.rating.js"></script>
<script src="https://onmyblock.com/media/js/jquery.metadata.js"></script>
<link rel="stylesheet" href="https://onmyblock.com/media/icons/style.css" />
<!--[if lte IE 7]><script src="https://onmyblock.com/media/icons/lte-ie7.js"></script><![endif]-->
<script>
if(self==top)
{
window.open('', '_self', '');
window.close();
}
</script>
<?php



	$object = $_GET['object'];

	if($object == "delete_from_wishlist"){
		$user->db->query("delete from favorites where user = {$_POST['user_id']} and place = {$_POST['place_id']}");
		return;
	}

	
	
	echo "<form method='post' action='action?object={$object}&id={$_GET['id']}'>";
	

	if($object == 'browse_place_like'){

		if($_POST['action'] == 'like'){$user->db->query("insert into place_likes values ({$user->id}, {$_GET['id']}, now())");}
		if($_POST['action'] == 'unlike'){$user->db->query("delete from place_likes where user = {$user->id} and place = {$_GET['id']}");}

		$sql = mysqli_fetch_array($user->db->query("select exists (select 1 from place_likes where place = {$_GET['id']} and user = {$user->id}) as result"));

		if($sql['result']){
			echo "<input type='hidden' name='action' value='unlike' />";
			echo "<input id='unlike_button' style='float:left; height:30px; background: url(https://onmyblock.com/media/images/like_heart.png) 4px -23px no-repeat #f3f3f3; opacity:1;' type='submit' value='liked' />";
		} else {
			echo "<input type='hidden' name='action' value='like' />";
			echo "<input id='like_button' style='float:left; height:30px; background: url(https://onmyblock.com/media/images/like_heart.png) 4px 5px no-repeat #f3f3f3; opacity:0.6;' type='submit' value='like' />";
		}
	}


	if($object == 'post_like'){

		if($_POST['action'] == 'like'){$user->db->query("insert into post_likes values ({$user->id}, {$_GET['id']}, now())");}
		if($_POST['action'] == 'unlike'){$user->db->query("delete from post_likes where user = {$user->id} and post = {$_GET['id']}");}

		$sql = mysqli_fetch_array($user->db->query("select exists (select 1 from post_likes where post = {$_GET['id']} and user = {$user->id}) as result"));

		$likes = mysqli_fetch_array($user->db->query("select count(user) as likes from post_likes where post = {$_GET['id']}"));
		$likes = $likes['likes'];

		if($sql['result']){
			echo "<input type='hidden' name='action' value='unlike' />";
			echo "<input id='post_unlike' type='submit' value='{$likes}' />";
		} else {
			echo "<input type='hidden' name='action' value='like' />";
			echo "<input id='post_like' type='submit' value='{$likes}' />";
		}
	}


	if($object == 'rating'){

		if($_POST['star'] > 0){$user->db->query("insert into ratings values ({$user->id}, {$_GET['id']}, {$_POST['star']}, now())");}

		$sql = mysqli_fetch_array($user->db->query("select exists (select 1 from ratings where place = {$_GET['id']} and user = {$user->id}) as result"));
		$rating = mysqli_fetch_array($user->db->query("select round(avg(rating)) as result from ratings where place = {$_GET['id']}"));

		$raters = mysqli_fetch_array($user->db->query("select count(place) as raters from ratings where place = {$_GET['id']}"));
		echo "<div style='margin-left:49px;'>";
		for($i = 1; $i < 26; $i++){

			echo '<input name="star" type="radio" value="'.$i.'" class="star  {split:5} required" ';
			if($sql['result']){echo 'disabled="disabled" ';}
			if($i == $rating['result']){echo 'checked="checked"';}
			echo " />";
		}
		echo "</div>";

		echo "<input type='submit' hidden='hidden' style='position: absolute; top:1000px;' />";

		if($raters['raters'] == 1){
			echo "<br /><span style='text-align:center; margin-top:0; width:200px;'>by 1 person</span>";
		} elseif ($raters['raters'] > 1){
			echo "<br /><span style='text-align:center; margin-top:0; width:200px;'>by {$raters['raters']} people</span>";
		} else {
			echo "<br /><span style='text-align:center; margin-top:0; width:200px;'>no ratings</span>";
		}

	}


	if($object == 'badge'){

		if(isset($_POST['badge']))
		{
			
			$insertValue = true;
			
			$sql = mysqli_fetch_array($user->db->query("select exists (select 1 from badges where place = {$_GET['id']} and user = {$user->id} and badge = {$_POST['badge']}) as result"));
			
			if($sql['result']==1)
			{
				$user->db->query("delete FROM badges where user = {$user->id} and place = {$_GET['id']} and badge = {$_POST['badge']} ");
				
				$insertValue = false;
			}
		
			if($insertValue==true)
			{
				$user->db->query("insert into badges values ({$user->id}, {$_GET['id']}, {$_POST['badge']}, now())");
				$user->db->cache->delete("place_".$_GET['id']);
				$user->db->get_place($_GET['id']);
			}
		}

		

		$badgers = mysqli_fetch_all($user->db->query("SELECT badge, COUNT(badge) AS users FROM badges where place = {$_GET['id']} GROUP BY badge ORDER BY users desc, badge asc"));

		$badg_num = array();

		foreach($badgers as $badger){
			$badg_num[$badger[0]] = $badger[1];
		}
		
		$Fraternity = is_numeric($badg_num[0]) ? '-1' : '';
		$Sorority = is_numeric($badg_num[1]) ? '-1' : '';
		$Party = is_numeric($badg_num[2]) ? '-1' : '';
		$Nerd = is_numeric($badg_num[3]) ? '-1' : '';
		$Beach = is_numeric($badg_num[4]) ? '-1' : '';
		$Apartment = is_numeric($badg_num[5]) ? '-1' : '';
		$Luxury = is_numeric($badg_num[6]) ? '-1' : '';
		$Close = is_numeric($badg_num[7]) ? '-1' : '';

		$buttons = array(
			0 => "<button name='badge' type='submit' id='badge' class='fixedtip' title='Fraternity House' style='background:url(https://onmyblock.com/media/images/badges/0{$Fraternity}.png) no-repeat;' value='0'></button><span id='badge_stats'>{$badg_num[0]}</span>",
			1 => "<button name='badge' type='submit' id='badge' class='fixedtip' title='Sorority House' style='background:url(https://onmyblock.com/media/images/badges/1{$Sorority}.png) no-repeat;' value='1'></button><span id='badge_stats'>{$badg_num[1]}</span>",
			2 => "<button name='badge' type='submit' id='badge' class='fixedtip' title='Party House' style='background:url(https://onmyblock.com/media/images/badges/2{$Party}.png) no-repeat;' value='2'></button><span id='badge_stats'>{$badg_num[2]}</span>",
			3 => "<button name='badge' type='submit' id='badge' class='fixedtip' title='Nerd House' style='background:url(https://onmyblock.com/media/images/badges/3{$Nerd}.png) no-repeat;' value='3'></button><span id='badge_stats'>{$badg_num[3]}</span>",
			4 => "<button name='badge' type='submit' id='badge' class='fixedtip' title='Beach House' style='background:url(https://onmyblock.com/media/images/badges/4{$Beach}.png) no-repeat;' value='4'></button><span id='badge_stats'>{$badg_num[4]}</span>",
			5 => "<button name='badge' type='submit' id='badge' class='fixedtip' title='Apartment Complex' style='background:url(https://onmyblock.com/media/images/badges/5{$Apartment}.png) no-repeat;' value='5'></button><span id='badge_stats'>{$badg_num[5]}</span>",
			6 => "<button name='badge' type='submit' id='badge' class='fixedtip' title='Luxury Living' style='background:url(https://onmyblock.com/media/images/badges/6{$Luxury}.png) no-repeat;' value='6'></button><span id='badge_stats'>{$badg_num[6]}</span>",
			7 => "<button name='badge' type='submit' id='badge' class='fixedtip' title='Close to School' style='background:url(https://onmyblock.com/media/images/badges/7{$Close}.png) no-repeat;' value='7'></button><span id='badge_stats'>{$badg_num[7]}</span>"
		);



		echo "<div style='margin-top: 25px;' class='badge_container'>";

		foreach($buttons as $button){
			echo $button;
		}

		echo "</div>";

	}


?>
</form>

<?php
}
?>