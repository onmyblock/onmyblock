<html>
<head>
<link rel="stylesheet" href="https://onmyblock.com/media/css/style.css" type="text/css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" ></script>
<script src="https://connect.facebook.net/en_US/all.js"></script>
<script src="https://onmyblock.com/media/js/cufon-yui.js" ></script>
<script src="https://onmyblock.com/media/js/Kozuka_Gothic_Pro_OpenType_400.font.js" ></script>
<script src="https://onmyblock.com/media/js/cufon-replace.js" ></script>
<script src="https://onmyblock.com/media/js/jquery.isotope.min.js"></script>
<script src="https://onmyblock.com/media/js/main.js"></script>
<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script src="https://onmyblock.com/media/js/gmap3.min.js"></script>
<script src="https://onmyblock.com/media/js/coin-slider.js"></script>
<script src="https://onmyblock.com/media/js/jquery.jqtransform.js"></script>
</head>
<body class="photo_edit_container">
<script>
if(self==top)
{
window.open('', '_self', ''); 
window.close(); 
}
</script>
<?php

/*
error_reporting(E_ALL);
ini_set('display_errors', '1');
*/

$user_id = $user->id;


/* $permission = mysqli_fetch_array($user->db->query("select exists (select 1 from residents where user = {$user_id} and place = {$_GET['place_id']} and current = 0)"));

if($permission[0] || $user_id == 578652540 || $user_id == 1063140531 || $user_id == 1063140506 || $user_id == 597597919 || $user_id == 645560083 || $user_id == 100005387075389 || $user_id == 1348620030){ 
*/
	$tempPlace = mysqli_fetch_row($user->db->query("select id_user_temp, id from temp_place where id_user_temp = {$user_id} "));
	
	if($tempPlace[0] != $user_id)
	{
		$tempPlaceId = $user->db->returnQueryId("insert into temp_place ( id_user_temp )values ( {$user_id} )");
	}else
	{
		$tempPlaceId = $tempPlace[1];
	}
	
	include('../classes/s3.php');
	include('../classes/optimizer.class.php');
	
	$new_photo = $_FILES['file'];
	$statusNew_photo = 0;
	
	if($new_photo['size'] > 10000 && $new_photo['error'] == 0){
		if($new_photo['type'] == 'image/gif' || $new_photo['type'] ==  'image/jpeg' || $new_photo['type'] ==  'image/png')
		{
			$image = new Optimizer();
			$image->load($new_photo['tmp_name']);
			
			if($image->getWidth() > 600){
				$image->resizeToWidth(600);
			}
			
			$image->save('photo_temp');
			
			$extenion = array(
				'image/jpeg' => '.jpg',
				'image/png' => '.png',
				'image/gif' => '.gif'
			);
			
			$new_photo_name = time().$extenion[$new_photo['type']];
			store_file(file_get_contents('photo_temp'), $new_photo['type'], "places/{$tempPlaceId}/".$new_photo_name);
			$queryPhoto="insert into temp_photos values (null, '{$tempPlaceId}', {$user_id}, '{$new_photo_name}', '{$image->getWidth()}', '{$image->getHeight()}', null, null, null)";
			$user->db->query($queryPhoto);
			$user->db->cache->delete("place_{$tempPlaceId}");
		} else {
			echo "Error: This File Type is Not Supported.";
		}
	}else {
		if(isset($new_photo))
		{
			$statusNew_photo = 1;
		}
	}

	if($_GET['action'] == 'delete'){
		delete_file("places/{$_GET['place_id']}/{$_GET['photo']}");
		$user->db->query("delete from temp_photos where place = {$_GET['place_id']} and dir = '{$_GET['photo']}'");
		$user->db->cache->delete("place_{$_GET['place_id']}");
	}

	/*if($_GET['action'] == 'profile'){
		$image = new Optimizer();
		file_put_contents('photo_temp', file_get_contents("https://s3-us-west-1.amazonaws.com/onmyblock/places/{$_GET['place_id']}/{$_GET['photo']}"));
		$image->load('photo_temp');
		$image->resizeToWidth(280);
		$image->save('photo_temp');
		$extenion = array(
			1 => '.gif',
			2 => '.jpg',
			3 => '.png'
		);
		$type = array(
			1 => 'image/gif',
			2 => 'image/jpeg',
			3 => 'image/png'
		);
		$new_photo_name = "profile".$extenion[exif_imagetype('photo_temp')];
		store_file(file_get_contents('photo_temp'), $type[exif_imagetype('photo_temp')], "places/{$_GET['place_id']}/".$new_photo_name);
		$user->db->query("delete from photos where place = {$_GET['place_id']} && dir like '%profile%'");
		$user->db->query("insert into photos values (null, '{$_GET['place_id']}', {$user_id}, '{$new_photo_name}', '{$image->getWidth()}', '{$image->getHeight()}', null, null, null)");
		$row = mysqli_fetch_row($user->db->query("select dir from photos where place = {$_GET['place_id']} and `order` = 1 "));
		$user->db->query("update photos set `order`=0 where dir='{$row[0]}'");
		$user->db->query("update photos set `order`=1 where dir='{$_GET['photo']}'");
		$user->db->cache->delete("place_{$_GET['place_id']}");
	}*/

	$profile = mysqli_fetch_array($user->db->query("select dir from temp_photos where place = {$tempPlaceId} && dir like '%profile%'"));

	if($profile[0]){
?>			
		<div class="photo_edit_header">
			<div class="profile_photo" style="background:url(https://s3-us-west-1.amazonaws.com/onmyblock/places/<?php  echo $tempPlaceId.'/'.$profile[0].'?time='.time();  ?>);background-size: 184px 130px;">
				<div>
					<h6>Profile Picture</h6>
				</div>
			</div>
			<form style="margin-left: 25px;" action="photos?place_id=<?php  echo $tempPlaceId ?>" method="post" enctype="multipart/form-data">
				<input type="file" name="file" id="upload_photo"/>
				<input class="silver-button" type="submit" value="Upload" style="float:right;"/>
			</form>
			<a class="edit_photo_profile" href="place?id=<?php  echo $tempPlaceId ?>" style="font-size: 12px;padding: 7px 14px;margin-top: 44px;" target="_parent">Save</a>
			<span>Max Size: 4MB</span>
			
		</div>
		<?php
	} else {
?>
		<div class="photo_edit_header">
			<div class="profile_photo">
				<div>
					<h6>No Profile Picture</h6>
				</div>
			</div>
			<form action="photos_temp?place_id=<?php echo $tempPlaceId?>" method="post" enctype="multipart/form-data">
				<input type="file" name="file" id="upload_photo"/>
				<input class="silver-button" type="submit" value="Upload" style="float:right;"/>
			</form>
			<?php if($statusNew_photo == 1) { ?><strong style="float: left;margin-left: 30px;color:red;">Minimum size 300px x 300px</strong><?php } ?>
			<span style="margin-left: 70px;">Max Size: 4MB</span>
		</div>
		<?php
	}

	$photos = $user->db->query("select * from temp_photos where place = {$tempPlaceId} && dir not like '%profile%' order by id desc");
	
	while($photo = mysqli_fetch_assoc($photos)){
?>

	<div class="edit_photo">
	<img src="https://s3-us-west-1.amazonaws.com/onmyblock/places/<?php echo $photo['place']."/".$photo['dir'] ?>" />
	<a class="edit_photo_delete" href="photos_temp?place_id=<?php echo $tempPlaceId."&photo=".$photo['dir']?>&action=delete">Delete</a>
	</div>

	<?php
	}

/* } */




?>
</body>
</html>