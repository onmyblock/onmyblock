<?php


$location = $_POST['location'];
$rooms = $_POST['rooms'];
$price = $_POST['price'];

$focus = false;

$arrayPrices = explode('-',$price);

$queryprice="";

if($price!='')
{
	$queryprice = " and ( price > {$arrayPrices[0]} and price < {$arrayPrices[1]} )";
}

$arrayRooms = explode(',',$rooms);

$queryRoom="";

if($arrayRooms[0] == 1)
{
	$queryRoom .= "rooms = 1 ";
	
	if($arrayRooms[1] != 2)
	{
		if($arrayRooms[2] == 3)
		{
			$queryRoom .= "or";
		}
		
		if($arrayRooms[3] == 4)
		{
			$queryRoom .= "or";
		}
	}
} 

if($arrayRooms[1] == 2)
{
	if($arrayRooms[0] == 1)
	{
		$queryRoom .= "or";
	}
	
	$queryRoom .= " rooms = 2 ";
}

if($arrayRooms[2] == 3)
{
	if($arrayRooms[1] == 2)
	{
		$queryRoom .= "or";
	}
	
	$queryRoom .= " rooms = 3 ";
} 

if($arrayRooms[3] == 4)
{
	if($arrayRooms[2] == 3)
	{
		$queryRoom .= "or";
	}
	
	if($arrayRooms[2] != 3)
	{
		if($arrayRooms[1] == 2)
		{
			$queryRoom .= "or";
		}
	}
	
	$queryRoom .= " rooms > 3 ";	
}

if($rooms=="0,0,0,0")
{
	$queryRoom = " rooms < 999 ";
}
	
if ($location != '' && $location != 'Enter City Name, Neighborhood, Street, or Zipcode' ){
	
	$criterio = $location;
	
	if($criterio!='')
	{
		$arrayCriterio = explode(',',$criterio);
	}
	
	$places = array();
	
	foreach($arrayCriterio as $item)
	{
		$item = trim($item);
		$route = '';
		
		$query .= "( city like '%{$item}%' or neighborhood like '%{$item}%' or state like '%{$item}%' ";
		
		$arraystreet = explode(' ',$item);
			
		for($i=1; $i<count($arraystreet); $i++)
		{
			$route.=$arraystreet[$i].' ';
		}
		
		$route = trim($route);
		
		if(is_numeric($arraystreet[0]))
		{
			$query .= " or street={$arraystreet[0]} ";
		}
		
		if($route)
		{
			$query .= " or route like '%{$route}%' ";
		}
		
		$query .= ") and ";
	}
	 
	$query = rtrim($query,"and ");
	
	$queryf = "select id, latitude, longitude, name, street, route, price, size, rooms, bathrooms, type ,((ACOS(SIN({$_POST['latitude']} * PI() / 180) * SIN(`latitude` * PI() / 180) + COS({$_POST['latitude']} * PI() / 180) * COS(`latitude` * PI() / 180) * COS(({$_POST['longitude']} - `longitude`) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS distance from places where (`latitude` BETWEEN ({$latitude} - {$radius}) AND ({$latitude} + {$radius}) AND `longitude` BETWEEN ({$longitude} - {$radius}) AND ({$longitude} + {$radius})) and {$query} and status = 1 group by id ORDER BY distance ASC, date desc limit 300;";
	
	//$queryf = "SELECT id, latitude, longitude, name, street, route, price, size, rooms, bathrooms, type ,((ACOS(SIN({$_GET['latitude']} * PI() / 180) * SIN(`latitude` * PI() / 180) + COS({$_GET['latitude']} * PI() / 180) * COS(`latitude` * PI() / 180) * COS(({$_GET['longitude']} - `longitude`) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS distance FROM places WHERE (`latitude` BETWEEN ({$_GET['latitude']} - 10) AND ({$_GET['latitude']} + 10) AND `longitude` BETWEEN ({$_GET['longitude']} - 10) AND ({$_GET['longitude']} + 10)) and {$price} and {$rooms} group by distance ORDER BY distance ASC, date desc limit 300;"; 
	
	$places = mysqli_fetch_all($user->db->query($queryf));

} else {
	$big_bar = true;
	$latitude = 32.771904;
	$longitude = -117.189124;
	$radius = 70;
	$limit = 3000;
	
	if($queryRoom!="")
	{
		$queryRoom = " and ( {$queryRoom} )";
	}
	
	$query = "SELECT id, latitude, longitude, name, street, route, price, size, rooms, bathrooms, type ,((ACOS(SIN({$latitude} * PI() / 180) * SIN(`latitude` * PI() / 180) + COS({$latitude} * PI() / 180) * COS(`latitude` * PI() / 180) * COS(({$longitude} - `longitude`) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS `distance` FROM places WHERE (`latitude` BETWEEN ({$latitude} - {$radius}) AND ({$latitude} + {$radius}) AND `longitude` BETWEEN ({$longitude} - {$radius}) AND ({$longitude} + {$radius})) and status = 1 {$queryprice} {$queryRoom} ORDER BY distance ASC, date desc limit {$limit};";
	
	$places = mysqli_fetch_all($user->db->query($query));
}

$printJs = "";

foreach($places as $item)
{
	$printJs .= "{$item[1]},";
	$printJs .= "{$item[2]},";
	$printJs .= "'bye',";
	$printJs .= ''."<iframe src='map_popup?id={$item[0]}' scrolling='no' style='width:400px; overflow:hidden;'></iframe>".'';
	$printJs .= "|";
}

// $rs = rtrim($printJs, ","); 

echo $printJs;
