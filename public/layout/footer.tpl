{strip}
<footer>
	<article>
	<div class="sublogo">OnMyBlock</div>        
		<nav>
			<ul>
				<li class="nocircle"><a id="howitworkslk" href="howitworks">How it Works</a></li>
				<li><a href="help">Help</a></li>
				<li><a href="team">Team</a></li>
				<li><a href="careers">Careers</a></li>
				<li><a href="privacy">Privacy Policy</a></li>
				<li><a href="terms">Terms of Service</a></li> 
			</ul>
		</nav>
	</article>
</footer>
{/strip}