{strip}
<div id="stream" class="tab-content">
    <div class="inner">
        <div class="home_box_middle">
	        <div id="stream_options" class="stream_options">
	            <ul class="stream_nav" data-option-key="filter">
	                <li><a href="#filter" data-option-value="*" class="selected"><h6>All</h6></a></li>
	                <li><a href="#filter" data-option-value=".friends"><h6>Friends</h6></a></li>
	                <li><a href="#filter" data-option-value=".network"><h6>Network</h6></a></li>
	            </ul>
	        </div>
            <div id="stream_container" user_id="{$user.id}" class="home_box_middle_inner" style="padding-bottom:0;">
        
{foreach from=$stream item='event'}

        <div class="event {$event.sort}">
        
         <a href="profile?id={$event.user_id}"><div class="event_img"><img src="https://graph.facebook.com/{$event.user_id}/picture">
        </a></div><div class="event_content"><a href="profile?id={$event.user_id}">{$event.first_name} {$event.last_name} </a>
        
        {* added a place *}
        {if $event.event_type eq 0}
             added <a href="place?id={$event.place_id}">{if $event.place_name eq ''}{$event.place_street} {$event.place_route}{else}{$event.place_name}{/if}</a> 
         {/if}
         
         
        {* gave a house a rating *}
        {if $event.event_type eq 1}
             gave <a href="place?id={$event.place_id}">{if $event.place_name eq ''}{$event.place_street} {$event.place_route}{else}{$event.place_name}{/if}</a> a rating
         {/if}
         
         
        {* gave a house a badge *}
        {if $event.event_type eq 2}
             gave <a href="place?id={$event.place_id}"><img src="https://onmyblock.com/media/images/badges/{$event.details.badge}.png" />{if $event.place_name eq ''}{$event.place_street} {$event.place_route}{else}{$event.place_name}{/if}</a> 
         {/if}
         
         
        {* posted on a house wall *}
        {if $event.event_type eq 3}
             posted "{$event.details.post}" on <a href="place?id={$event.place_id}">{if $event.place_name eq ''}{$event.place_street} {$event.place_route}{else}{$event.place_name}{/if}</a> wall 
         {/if}
         
         
        {* liked a place *}
        {if $event.event_type eq 4}
             liked <a href="place?id={$event.place_id}">{if $event.place_name eq ''}{$event.place_street} {$event.place_route}{else}{$event.place_name}{/if}</a> 
             {if isset($event.details.photos.0)}
             <img src="https://s3-us-west-1.amazonaws.com/onmyblock/places/{$event.place_id}/{$event.details.photos.0}" style="width:260px; height:200px; margin:20px;" />
             {/if}
         {/if} 
         
         
         {* favorited a place *}
        {if $event.event_type eq 5}
             favorited <a href="place?id={$event.place_id}">{if $event.place_name eq ''}{$event.place_street} {$event.place_route}{else}{$event.place_name}{/if}</a>
        {/if}
         
                  
         
        {* updated a house name *}
        {if $event.event_type eq 6}
             updated <a href="place?id={$event.place_id}">{if $event.place_name eq ''}{$event.place_street} {$event.place_route}{else}{$event.place_name}{/if}</a> name
        {/if}
         
         
        {* added a house as a current home *}
        {if $event.event_type eq 7}
             added <a href="place?id={$event.place_id}">{if $event.place_name eq ''}{$event.place_street} {$event.place_route}{else}{$event.place_name}{/if}</a> as a current home
        {/if}
         
         
        {* added a house as a past home *}
        {if $event.event_type eq 8}
             added <a href="place?id={$event.place_id}">{if $event.place_name eq ''}{$event.place_street} {$event.place_route}{else}{$event.place_name}{/if}</a> as a past home 
         {/if}
         
         
        {* updated realtor information *}
        {if $event.event_type eq 12}
             updated realtor information
        {/if} 
        
                 
        {* added a photo to a house *}
        {if $event.event_type eq 13}
             added a photo to <a href="place?id={$event.place_id}">{if $event.place_name eq ''}{$event.place_street} {$event.place_route}{else}{$event.place_name}{/if}</a> 
         {/if}
                  
         
        {* commented on a post *}
        {if $event.event_type eq 14}
             commented on a post 
         {/if}
        
        
        {* joined  *}
        {if $event.event_type eq 17}
             joined OnMyBlock 
         {/if}
         
         
         </div><div class="event_time">{$event.time}</div></div>

        {/foreach}

        
        
            </div>
        </div>
    </div>
</div>
{/strip}