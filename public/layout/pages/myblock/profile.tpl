{strip}
    <div id="profile" class="tab-content">
        <div class="inner">
            <!--div  class="home_box_middle">
                <h1>Profile</h1>
            </div-->
            <div class="home_box_middle">
                <div class="home_box_middle_inner">
                    <a href="#" class="silver-button" style="margin-top:10px; padding:5px 10px 5px 10px; text-align:center; float:right">Update</a>

                    <div style="padding: 0 35px 0 35px;">
                        <br>
                        <br>
                        <h2><img src="https://graph.facebook.com/{$user.id}/picture?type=square"><span style="margin:20px;">{$user.first_name} {$user.last_name}</span></h2><br>
                        <hr>

                        <div style="color:#a6a6a6; font-weight:bold;">
                            Email
                        </div>

                        <div style="text-align:center">
                            {$user.email}
                        </div>
                        
                        {* --- Networks --- *}
                        {if isset($user.networks.0)}
                        <hr>
						<div style="color:#a6a6a6; font-weight:bold;">
                            Networks
                        </div>
						{foreach from=$user.networks item="array"}
						{foreach from=$array item="network"}
						{if $network@first}
  						{/if}
                        <div style="text-align:center">
                            {$network}
                        </div>
                        {/foreach}
                        {/foreach}
                        {/if}
                        {* --- *}
                        
                        
                        {* --- Places --- *}
						{foreach from=$user.places item="place"}
					    {if $place@first}
					    <hr>
                        <div style="color:#a6a6a6; font-weight:bold;">
                            My Places
                        </div>
						{/if}
                        <div style="text-align:center">
                           <a href="place?id={$place.id}">{$place.name}</a>
                        </div>
                        {/foreach}
                        {* --- *}
                        

                        {* --- Roommates --- *}
                        {foreach from=$user.roommates item="roommate"}
						{if $roommate@first}
						<hr>
                        <div style="color:#a6a6a6; font-weight:bold;">
                            My Roommates
                        </div>
                        <div style="text-align:center">
                        {/if}
                           <a target="_blank" href="profile?id={$roommate.1}"><img style="margin:5px;" width="50px" src="https://graph.facebook.com/{$roommate.1}/picture"></a>
                        {if $roommate@last}
                        </div>
                        {/if}
                        {/foreach}
                        {* --- *}
						
						
						{* --- My likes --- *}
                        {foreach from=$user.place_likes item="like"}
						{if $like@first}
						<hr>
                        <div style="color:#a6a6a6; font-weight:bold;">
                            My Likes
                        </div>
						{/if}
                        <div style="text-align:center">
                           <a href="place?id={$like.id}">{$like.name}</a>
                        </div>
                        {/foreach}
                        {* --- *}
                        
                        
					</div><br>
                </div>
            </div>
        </div>
    </div>
{/strip}