{strip}
    <div id="places" class="tab-content">
        <div class="inner">
            <!--div class="home_box_middle">
                <h1>Places</h1>
            </div-->

            	{if isset($user.places.0)}
                 <div class="home_box_middle">
                {foreach $user.places item='place'}
				{if $place.current.current}
                <div class="home_box_middle_inner">
                    <a href="place?id={$place.id}" class="silver-button" style="margin-top:10px; padding:5px 10px 5px 10px; text-align:center; float:right">Options</a>

                    <div style="padding: 0 35px 0 35px;">
                        <br>
                        <br>

                        <a href="place?id={$place.id}"><h2>{$place.name} {if $place.current.current}(Current){else}(Past){/if}</h2></a><span>{$place.street} {$place.route},   {$place.city}, {$place.state}<span style="float:right">{$place.postal}</span></span><br>
                        <br>


                        {if isset($place.photos.0)}
                         <hr>
                        <div style="color:#a6a6a6; font-weight:bold;">
                            Photos
                        </div>

                        <div style="text-align:center">
                            <br>
                            
                        {foreach $place.photos item='photo'} 
                            <img width="80px" src="https://s3-us-west-1.amazonaws.com/onmyblock/places/{$place.id}/{$photo.0}">
                        {/foreach}
                            <br>
                            <br>
                        </div>
                        {/if}
                        
                        {foreach $place.residents.current item='resident'}
                        {if $resident@first}
						<hr>
                        <div style="color:#a6a6a6; font-weight:bold;">
                            Residents
                        </div>
                        <div style="text-align:center">
                        {/if}
                           <a target="_blank" href="profile?id={$resident.0}"><img style="margin:5px;" width="50px" src="https://graph.facebook.com/{$resident.0}/picture"></a>
                        {if $resident@last}
                        </div>
                        {/if}
                        {/foreach} 
                     <br></div></div><br>
                    {else}
                    
                    
                    <dl id="places"> 
  		<dt><a>{$place.name} {if $place.current.current}(Current){else}(Past){/if}</a></dt> 
 				 <dd> 
 			   <div> 


                <div class="home_box_middle_inner" style="left:-17px;">
                    <a href="place?id={$place.id}" class="silver-button" style="margin-top:10px; padding:5px 10px 5px 10px; text-align:center; float:right">Options</a>

                    <div style="padding: 0 35px 0 35px;">
                        <br>
                        <br>

                        <a href="place?id={$place.id}"><h2>{$place.name} {if $place.current.current}(Current){else}(Past){/if}</h2></a><span style="width:400px;">{$place.street} {$place.route}, {$place.city}, {$place.state}<span style="float:right">{$place.postal}</span></span><br>
                        <br>


                         {if $place.photos.0 != ''}
                         <hr>
                        <div style="color:#a6a6a6; font-weight:bold;">
                            Photos
                        </div>

                        <div style="text-align:center">
                            <br>
                        {foreach $place.photos item='photo'} 
                            <img width="80px" src="https://s3-us-west-1.amazonaws.com/onmyblock/places/{$place.id}/{$photo.0}">
                        {/foreach}
                            <br>
                            <br>
                        </div>
                        {/if}

                        {foreach $place.residents.current item='resident'}
                        {if $resident@first}
						<hr>
                        <div style="color:#a6a6a6; font-weight:bold;">
                            Roommates
                        </div>
                        <div style="text-align:center">
                        {/if}
                           <a target="_blank" href="profile?id={$resident.0}"><img style="margin:5px;" width="50px" src="https://graph.facebook.com/{$resident.0}/picture"></a>
                        {if $resident@last}
                        </div>
                        {/if}
                        {/foreach}<br> </div></div>


 			   </div> 
			  </dd> 
			</dl>
                    {/if}
                    {/foreach}
        </div>
        {else}
        <a href="add-house" class="blue-button" style="width: 280px; margin:75px; text-align:center;">Add Your House Now!</a> 
        {/if}
			            </div>
    </div>
{/strip}