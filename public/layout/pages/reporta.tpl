<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		
		<link rel="stylesheet" href="https://onmyblock.com/media/css/responsive-nav.css" type="text/css" />
		<link rel="stylesheet" href="https://onmyblock.com/media/css/responsive-style.css" type="text/css" />
		<link rel="stylesheet" href="https://onmyblock.com/media/css/demo_page.css" type="text/css" />
		<link rel="stylesheet" href="https://onmyblock.com/media/css/demo_table.css" type="text/css" /> 
		<link rel="stylesheet" href="https://onmyblock.com/media/css/jquery-ui-datepicker-1.10.2.custom.min.css" />
		<link rel="stylesheet" href="https://onmyblock.com/media/css/kendo.common.min.css" />
		<link rel="stylesheet" href="https://onmyblock.com/media/css/kendo.default.min.css" />
			
		<script src="https://onmyblock.com/media/js/responsive-nav.js"></script>
		<script src="https://onmyblock.com/media/js/jquery.dataTables.min.js"></script>
		<script src="https://onmyblock.com/media/js/morris.min.js"></script>
		<script src="https://onmyblock.com/media/js/raphael-min.js"></script>
		<script src="https://onmyblock.com/media/js/jquery-ui-datepicker-1.10.2.custom.min.js"></script>
		
		
		<script type="text/javascript" >
			$(document).ready(function() {
			
				$('#example').dataTable();
				
				$('#tableUsers').dataTable();
				

				$("#datepicker").datepicker();
				$("#datepicker").datepicker("option","dateFormat","yy-mm-dd");
				$("#datepicker").datepicker("option", "maxDate", "+0d");

				
				$('#graphics').css('display','none');
				$('#users').css('display','none');
				$('#editPlace').css('display','none');
				
				$(".ac").on("click",function(){
				
					var desactive = $(".active").attr("data");
					
					$("#"+desactive).slideToggle("slow");
					
					$(".ac").removeClass("active");
					
					$(this).addClass("active");
					
					var active = $(this).attr("data");
					
					$("#"+active).slideToggle("slow");
				});
				
				$("#selectUniversity").change(function(){
					
					var id = $("#selectUniversity").val();
					
					url = "http://onmyblock.com/report?idUniversity="+id;
					
					$(location).attr('href',url);
					
				});
				
				$("#btnDate").on("click",function(){
					var value = $("#datepicker").val();
					
					if(value!='')
					{
						url = "http://onmyblock.com/report?idUniversity=12&date="+value;
					
						$(location).attr('href',url);
					
					}
				});
				
				$(".savePlace").on("click", function(){
				
					var idPlace = $("#idPlace").val();
					var neighborhood = $("#neighborhood").val();
					
					$.ajax({
						type: "POST",
						url: "svcPlace?idPlace="+idPlace+"&neighborhood="+neighborhood+"&action=2",
						success:  function (data) {

							$("#"+idPlace).text(neighborhood);
					
							$("#editPlace").slideToggle("slow");
							
							var active = $(".savePlace").attr("data");
							
							$("#"+active).addClass("active");
							
							$("#"+active).slideToggle("slow");

						}
					});
					
				});
				 
				$('body').on('click','#example tbody tr td a.btnEdit', function(){
					
					var idPlace = $(this).attr("id");
					
					$.ajax({
						type: "POST",
						url: "svcPlace?idPlace="+idPlace+"&action=1",
						success:  function (data) {
						
							var obj = $.parseJSON(data);
							
							$("#idPlace").val(obj.id);
							$("#street").val(obj.street);
							$("#route").val(obj.route);
							$("#city").val(obj.city);
							$("#neighborhood").val(obj.neighborhood);
							
						}
					});
					
					$("#global").slideToggle("slow");
					
					var active = $(this).attr("data");
					
					$("#"+active).slideToggle("slow"); 
				});
				
				$(".cancel").on("click",function(){
				
					$("#editPlace").slideToggle("slow");
							
					var active = $(".savePlace").attr("data");
					
					$("#"+active).addClass("active");
					
					$("#"+active).slideToggle("slow");
				});
			});
			
			
		</script>
		<style>
			#edit {
				list-style-type: none;
				margin: 0;
				padding: 0;
			}
			
			#edit li {
				margin: 10px 0 0 0;
			}
			
			#edit li label {
				display: inline-block;
				width: 90px;
				text-align: right;
				margin-right: 20px;
			}
		</style>
	</head>
	<body>
		<header style="height:60px;width:18em;margin-top: 18px;">
			<div style="background: url(http://onmyblock.com/media/images/logo-1.png) no-repeat;height:42px;width:120px;margin-left: 88px;"></div>
		</header>
		
		<div id="nav" style="margin-top: 50px;">
			<ul>
				<li class="active ac" data="global" ><a href="#">Places</a></li>
				<li class="ac" data="graphics" ><a href="#">Graphics</a></li>
				<li class="ac" data="users" ><a href="#">Users</a></li>
			</ul>
		</div>
		
		<div role="main" class="main">
			<!--a href="#nav" id="toggle">Menu</a-->
			<div id="global">
				<h2>Places</h2>
				<!--p class="intro">Report Global</p-->
				<p></p>
				
					<select name="dataUniversity" id="selectUniversity" >
						<option value="" >All Universities</option>		
						{foreach from=$university item='u' name=disp}
							<option value="{$smarty.foreach.disp.iteration}" {if $idUniversity eq $smarty.foreach.disp.iteration }selected="selected"{/if} >{$u}</option>					
						{/foreach}
					</select >
					<div>
						<span>Date: </span><input type="text" id="datepicker" value="{$date}" /><input type="button" value="Find" id="btnDate" />
					</div>
				<table cellpadding="0" cellspacing="0" border="0" class="display" id="example" width="100%">
					<thead>
						<tr>
							<th>Id</th>
							<th>Street</th>
							<th>Route</th>
							<th>City</th>
							<th>Neighborhood</th>
							<th>Date</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					{foreach from=$places item='place' } 
						<tr class="odd gradeA">
							<td>{$place.0}</td>
							<td>{$place.1}</td>
							<td>{$place.2}</td>
							<td>{$place.3}</td>
							<td id="{$place.0}" class="center">{$place.4}</td>
							<td style="width: 100px;">{$place.5}</td>
							<td>
								<a data="editPlace" href="#"  id="{$place.0}" class="btnEdit k-button k-button-icontext k-grid-add" style="height: 28px;font-size: 15px;" >Edit</a>
							</td>
						</tr>
					{/foreach}
					</tbody>
				</table>
			</div>
			
			<div id="editPlace">
				<h2>Edit</h2>
				<!--p class="intro">Report Global</p-->
				<p></p>
				
				<ul id="edit" >
					<li>
						<label for="id" class="required">Id</label>
						<input type="text" id="idPlace" name="id" disabled="disabled" class="k-textbox" required validationMessage="Please enter {0}" />
					</li>
					<li>
						<label for="street" class="required">Street</label>
						<input type="text" id="street" name="street" disabled="disabled" class="k-textbox" required validationMessage="Please enter {0}" />
					</li>
					<li>	
						<label for="route" class="required">Route</label>
						<input type="text" id="route" name="route" disabled="disabled" class="k-textbox" required validationMessage="Please enter {0}" />
					</li>
					<li>
						<label for="city" class="required">City</label>
						<input type="text" id="city" name="city" disabled="disabled" class="k-textbox" required validationMessage="Please enter {0}" />
					</li>
					<li>
						<label for="neighborhood" class="required">Neighborhood</label>
						<input type="text" id="neighborhood" name="neighborhood" class="k-textbox" required validationMessage="Please enter {0}" />
					</li>
					<li>
						<button class="k-button savePlace"  data="global"  type="submit">Save</button>
						<button class="k-button cancel"  data="global"  type="submit">Cancel</button>
					</li>
				</ul>
			</div>
			
			<div id="graphics">
				<h2>Graphics</h2>
				
				<div style="height:420px;">
					<div id="graph_productos" style="height:250px;width:940px"></div>
					<pre id="code" class="prettyprint linenums">
						{literal}
						<script type="text/javascript">
						
							var data_productos = {/literal}{$data}{literal};

							Morris.Line({
								element: 'graph_productos',
								data: data_productos,
								xkey: 'y',
								ykeys: ['a'],
								labels: ['# of Properties'],
								parseTime: false
							});
						</script>
						{/literal}
				</div>
				
			</div>
			
			<div id="users">
				<h2>Users</h2>
				
				<table cellpadding="0" cellspacing="0" border="0" class="display" id="tableUsers" width="100%">
					<thead>
						<tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Gender</th>
							<th>Email</th>
							<th>School</th>
						</tr>
					</thead>
					<tbody>
					{foreach from=$users item='user' } 
						<tr class="odd gradeA">
							<td>{$user.first_name}</td>
							<td>{$user.last_name}</td>
							<td>{$user.gender}</td>
							<td>{$user.email}</td>
							<td>{$user.school}</td>
						</tr>
					{/foreach}
					</tbody>
				</table>
			</div>
		</div>
	</body>
</html>