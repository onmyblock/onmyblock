<!doctype html>
<!--[if lt IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html lang="es" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="es" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title>OnMyBlock</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
    <!-- //metas para facebook -->
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content=" ">
    <meta property="og:site_name" content="">

    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<link rel="stylesheet" href="/media/css/main.css?v=1">
    <link rel="stylesheet" href="/media/css/jquery.vegas.css" type="text/css" media="all">    
    

	<script src="/media/js/libs/modernizr-2.0.6.min.js"></script>
    <script>
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-XXXXXXX-X']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
</head>
<body>
<header>

    <article>
        <div class="logo"></div>
        {include file='../header.tpl'}

    </article>

</header>    

<div id="wrapper">

    <section id="section-5">
        
        <article>
            
            <div class="boxhowitworks boxprivacy">


            <h1>Privacy Policy</h1>
            
            
            <span>What Information Do We Collect?</span><br/>
            We collect information from you when you register on our site, subscribe to our newsletter or fill out a form.When ordering or registering on our site, as appropriate, you may be asked to enter your: name, e-mail address, mailing address or phone number. You may, however, visit our site anonymously.<br/><br/>

            <span>What Do We Use Your Information For?</span><br/>
            Any of the information we collect from you may be used in one of the following ways:
            To personalize your experience (your information helps us to better respond to your individual needs)
            To improve our website (we continually strive to improve our website offerings based on the information and feedback we receive from you)
            To improve customer service (your information helps us to more effectively respond to your customer service requests and support needs)
            To send periodic emails
            The email address you provide may be used to send you information, respond to inquiries, and/or other requests or questions<br/><br/>

            <span>How Do We Protect Your Information?</span><br/>
            We implement a variety of security measures to maintain the safety of your personal information when you enter, submit, or access your personal information.<br/><br/>

            <span>Do We Use Cookies?</span><br/>
            Yes (Cookies are small files that a site or its service provider transfers to your computers hard drive through your Web browser (if you allow) that enables the sites or service providers systems to recognize your browser and capture and remember certain information.<br/><br/>

            <span>Do We Disclose Any Information To Outside Parties?</span><br/>
            We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect our rights or others? rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.<br/><br/>

            <span>Third Party Links</span><br/>
            Occasionally, at our discretion, we may include or offer third-party products or services on our website. These third-party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.<br/><br/>

            <span>Your Consent</span><br/>
            By using our site, you consent to this privacy policy.<br/><br/>

            <span>Changes To Our Privacy Policy</span><br/>
            If we decide to change our privacy policy, we will post those changes on this page. Contact us if there are any questions regarding this privacy policy.<br/><br/>        
            </div>            
        </article>
    </section>
</div>

{include file='../footer.tpl'}

<script src="/media/js/libs/jquery-1.9.0.min.js"></script>
<script src="/media/js/main.js"></script>		
<script src="//connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript" src="/media/js/libs/jquery.vegas.js"></script>
<script src="/media/js/libs/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="/media/js/init.js?v=1"></script>
<script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=561406290571363";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

</body>
</html>