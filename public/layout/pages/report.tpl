<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title></title>
		<link rel="stylesheet" href="https://onmyblock.com/media/css/flexigrid.css" />
		<link rel="stylesheet" href="https://onmyblock.com/media/css/responsive-nav.css" />
		<link rel="stylesheet" href="https://onmyblock.com/media/css/responsive-style.css" />
		<link rel="stylesheet" href="https://onmyblock.com/media/css/kendo.common.min.css" />
		<link rel="stylesheet" href="https://onmyblock.com/media/css/kendo.default.min.css" />
		<link rel="stylesheet" href="https://onmyblock.com/media/css/report.css" />
		<link rel="stylesheet" href="https://onmyblock.com/media/css/jquery-ui-datepicker-1.10.2.custom.min.css" />
		
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script src="https://onmyblock.com/media/js/flexigrid.js"></script>
		<script src="https://onmyblock.com/media/js/morris.min.js"></script>
		<script src="https://onmyblock.com/media/js/raphael-min.js"></script>
		<script src="https://onmyblock.com/media/js/jquery-ui-datepicker-1.10.2.custom.min.js"></script>
	</head>	
	<body>
		
		<header style="height:60px;width:18em;margin-top: 18px;">
			<div style="background: url(http://onmyblock.com/media/images/logo-1.png) no-repeat;height:42px;width:120px;margin-left: 88px;"></div>
		</header>
		
		<div id="nav" style="margin-top: 50px;">
			<ul>
				{$htmlNav}
			</ul>
		</div>
		<div role="main" class="main">
			<div id="global">
				<h2>Places</h2>
				
				<p><strong>Select University:</strong>
					<select name="dataUniversity" id="selectUniversity" style="border: 1px solid #b0aaaa;height: 28px;font-size: 12px;color: #6f6f6f;" >
						<option value="0" >All Universities</option>		
						<option value="1" >San Diego State University</option>
						<option value="2" >Chapman University</option>
						<option value="3" >University of Southern California</option>
						<option value="4" >University of California at Los Angeles</option>
						<option value="5" >Cal Poly San Luis Obispo</option>
						<option value="6" >Loyola Marymount University</option>
						<option value="7" >UCSB</option>
						<option value="8" >Santa Clara</option>
						<option value="9" >Stanford</option>
						<option value="10" >UCSF</option>
						<option value="11" >University of San Diego</option>
					</select >
				</p>
				
				<p><strong>Date:</strong>
					<input type="text" id="datepicker"  />
					<input type="button" class="k-button btnDate" value="Find" id="btnDate" />
				</p>
				
				<table id="flexPlaces" style="display:none"></table>
			</div>
			
			<div id="editPlace">
			
				<h2>Edit</h2>
				
				<p></p>
				
				<ul id="edit" >
					<li>
						<label for="id" class="required">Id</label>
						<input type="text" id="editPlaceId" name="id" disabled="disabled" class="k-textbox" required validationMessage="Please enter {0}" />
					</li>
					<li>
						<label for="street" class="required">Street</label>
						<input type="text" id="street" name="street" disabled="disabled" class="k-textbox" required validationMessage="Please enter {0}" />
					</li>
					<li>	
						<label for="route" class="required">Route</label>
						<input type="text" id="route" name="route" disabled="disabled" class="k-textbox" required validationMessage="Please enter {0}" />
					</li>
					<li>
						<label for="city" class="required">City</label>
						<input type="text" id="city" name="city" disabled="disabled" class="k-textbox" required validationMessage="Please enter {0}" />
					</li>
					<li>
						<label for="neighborhood" class="required">Neighborhood</label>
						<input type="text" id="neighborhood" name="neighborhood" class="k-textbox" required validationMessage="Please enter {0}" />
					</li>
					<li>
						<button class="k-button savePlace"  data="global"  type="submit">Save</button>
						<button class="k-button cancel"  data="global"  type="submit">Cancel</button>
					</li>
				</ul>
			</div>
			
			<div id="users">
				<h2>Users</h2>
				<table id="flexUsers" style="display:none"></table>
			</div>
			
			<div id="adminusers">
				<h2>Admin Users</h2>
				<table id="flexUsersAdmin" style="display:none"></table>
			</div>
			
			<div id="graphics">
				<h2>Graphics</h2>
				
				<div style="height:420px;">
					<div id="graph_productos" style="height:250px;width:940px"></div>
					<pre id="code" class="prettyprint linenums">
						{literal}
						<script type="text/javascript">
						
							var data_productos = {/literal}{$data}{literal};

							Morris.Line({
								element: 'graph_productos',
								data: data_productos,
								xkey: 'y',
								ykeys: ['a'],
								labels: ['# of Properties'],
								parseTime: false
							});
						</script>
						{/literal}
				</div>
				
			</div>
			
			<div id="editUser">
			
				<h2>Edit Users</h2>
				
				<p></p>
				
				<ul id="edit" >
					<li>
						<label for="id" class="required">ID</label>
						<input type="text" id="idUser" name="idUser" disabled="disabled" class="k-textbox" required validationMessage="Please enter {0}" />
					</li>
					<li>
						<label for="id" class="required">Name</label>
						<input type="text" id="nameUser" name="nameUser" disabled="disabled" class="k-textbox" required validationMessage="Please enter {0}" />
					</li>
					<li>
						<label for="email" class="required">Email</label>
						<input type="text" id="emailUser" name="emailUser" disabled="disabled" class="k-textbox" required validationMessage="Please enter {0}" />
					</li>
					{$htmlAcces}
					<li>
						<button class="k-button saveUser"  data="adminusers"  type="submit">Save</button>
						<button class="k-button cancelUser"  data="adminusers"  type="submit">Cancel</button>
					</li>
				</ul>
			</div>
			
			<div id="noPlaces">
				<h2>No Places</h2>
				<table id="flexNoPlaces" style="display:none"></table>
			</div>
			
			<div id="editNoPlace">
			
				<h2>Edit no Place</h2>
				
				<p></p>
				
				<ul id="edit" >
					<li>
						<label for="id" class="required">ID</label>
						<input type="text" id="editnoPlaceId" name="editnoPlaceId" disabled="disabled" class="k-textbox" required validationMessage="Please enter {0}" />
					</li>
					<li>
						<label for="id" class="required">Street</label>
						<input type="text" id="streetno" name="streetno" class="k-textbox" required validationMessage="Please enter {0}" />
					</li>
					<li>
						<label for="email" class="required">Route</label>
						<input type="text" id="routeno" name="routeno" class="k-textbox" required validationMessage="Please enter {0}" />
					</li>
					<li>
						<label for="email" class="required">City</label>
						<input type="text" id="cityno" name="cityno" class="k-textbox" required validationMessage="Please enter {0}" />
					</li>
					<li>
						<label for="email" class="required">Neighborhood</label>
						<input type="text" id="neighborhoodno" name="neighborhoodno" class="k-textbox" required validationMessage="Please enter {0}" />
					</li>
					<li>
						<button class="k-button savePlaceno"  data="noPlaces"  type="submit">Save</button>
						<button class="k-button cancelno"  data="noPlaces"  type="submit">Cancel</button>
					</li>
				</ul>
			</div>
			
		</div>
	</body>
		{literal}
			<script type="text/javascript">
			
			$(document).ready(function(){
				
				$("#editPlace").css('display','none');
				$("#users").css('display','none');
				$("#editUser").css('display','none');
				$("#adminusers").css('display','none');
				$("#noPlaces").css('display','none');
				$("#editNoPlace").css('display','none');
				$("#graphics").css('display','none');
				
				var navHtml = $(".active").attr("data");
				
				if(navHtml=="noPlaces")
				{
					$("#"+navHtml).css('display','block');
					
					$("#global").css('display','none');
				}
				
				$(".ac").on("click",function(){
				
					var desactive = $(".active").attr("data");
					
					$("#"+desactive).slideToggle("slow");
					
					$(".ac").removeClass("active");
					
					$(this).addClass("active");
					
					var active = $(this).attr("data");
					
					$("#"+active).slideToggle("slow");
				});
				
				$(".cancel").on("click",function(){
				
					$("#editPlace").slideToggle("slow");
							
					var active = $(".savePlace").attr("data");
					
					$("#"+active).addClass("active");
					
					$("#"+active).slideToggle("slow");
				});
				
				$(".cancelUser").on("click",function(){
				
					$("#editUser").slideToggle("slow");
							
					var active = $(".saveUser").attr("data");
					
					$("#"+active).addClass("active");
					
					$("#"+active).slideToggle("slow");
				});
				
				$(".cancelno").on("click",function(){
				
					$("#editNoPlace").slideToggle("slow");
							
					var active = $(".saveUser").attr("data");
					
					$("#"+active).addClass("active");
					
					$("#"+active).slideToggle("slow");
				});
				
				$("#datepicker").datepicker();
				$("#datepicker").datepicker("option","dateFormat","yy-mm-dd");
				$("#datepicker").datepicker("option", "maxDate", "+0d");
				
				$(".btnDate").on("click",function(){
					
					var valDate = $("#datepicker").val();
					
					if(valDate!="")
					{
						$('#flexPlaces').flexOptions({
							url: 'svcReport/selectPlaces/12/'+valDate,
							dataType: 'json',
							method: 'POST', 
							colModel : [ 
								{display: 'ID', name : 'idPlace', width : 80, sortable : false, align: 'center'},
								{display: 'Date', name : 'date', width : 60, sortable : false, align: 'center'},
								{display: 'Street', name : 'street', width : 70, sortable : false, align: 'center'},
								{display: 'Route', name : 'route', width : 180, sortable : true, align: 'center'},
								{display: 'City', name : 'city', width : 120, sortable : true, align: 'center'},
								{display: 'Neighborhood', name : 'neighborhood', width : 130, sortable : true, align: 'center', hide: false, align: 'center'},
								{display: 'Url', name : 'type', width : 180, sortable : false, align: 'center'},
							],
							title: 'Places',
							useRp: true,
							rp: 15,
							page: 1,
							showTableToggleBtn: true,
							width: 900,
							height: 400
						}).flexReload();
					}
				});
				
				$("#selectUniversity").on("change",function(event){
				
					event.preventDefault();
					
					val = $(this).val();
					
					if(val!=0)
					{
						$('#flexPlaces').flexOptions({
							url: 'svcReport/selectPlaces/'+val,
							dataType: 'json',
							method: 'POST', 
							colModel : [ 
								{display: 'ID', name : 'idPlace', width : 80, sortable : false, align: 'center'},
								{display: 'Date', name : 'date', width : 60, sortable : false, align: 'center'},
								{display: 'Street', name : 'street', width : 70, sortable : false, align: 'center'},
								{display: 'Route', name : 'route', width : 180, sortable : false, align: 'center'},
								{display: 'City', name : 'city', width : 120, sortable : false, align: 'center'},
								{display: 'Neighborhood', name : 'neighborhood', width : 130, sortable : false, align: 'center', hide: false, align: 'left'},
								{display: 'Url', name : 'type', width : 180, sortable : false, align: 'center'},
							],
							buttons : [
								{name: 'Edit', bclass: 'edit', onpress : editPlace},
								{separator: true}
							],  
							searchitems : [
								{display: 'ID', name : 'idPlace'},
								{display: 'route', name : 'route', isdefault: true}
							],
							sortname: "iso",
							sortorder: "asc",
							usepager: true,
							title: 'Places',
							useRp: true,
							rp: 15,
							page: 1,
							showTableToggleBtn: true,
							width: 900,
							height: 400
							}).flexReload();
					}
				});
				
				$(".savePlace").on("click", function(){
				
					var idPlace = $("#editPlaceId").val();
					var neighborhood = $("#neighborhood").val();
					
					$.ajax({
						type: "POST",
						url: "svcReport/updatePlace/",
						data: {placesId:idPlace,neighborhoodEdit:neighborhood},
						success:  function (data) {

							$('#flexPlaces').flexOptions({newp: 1}).flexReload();
							
							$("#editPlace").slideToggle("slow");
							
							var active = $(".savePlace").attr("data");
							
							$("#"+active).addClass("active");
							
							$("#"+active).slideToggle("slow");

						}
					});
					
				});
				
				$(".savePlaceno").on("click", function(){
				
					var idPlace = $("#editnoPlaceId").val();
					var street = $("#streetno").val();
					var route = $("#routeno").val();
					var city = $("#cityno").val();
					var neighborhood = $("#neighborhoodno").val();
					
					$.ajax({
						type: "POST",
						url: "svcReport/updateNoPlace/",
						data: {placesId:idPlace,streetEdit:street,routeEdit:route,cityEdit:city,neighborhoodEdit:neighborhood},
						success:  function (data) {

							$('#flexNoPlaces').flexOptions({newp: 1}).flexReload();
							
							$("#editNoPlace").slideToggle("slow");
							
							var active = $(".savePlaceno").attr("data");
							
							$("#"+active).addClass("active");
							
							$("#"+active).slideToggle("slow");

						}
					});
					
				});
				
				$(".saveUser").on("click", function(){
				
					var idUser = $("#idUser").val();
					
					var checkNoPlaces = $("#statusUser").val();
					
					/* if(checkNoPlaces!=1)
					{
						var checkNoPlaces = $("#checkNoPlaces").attr("value");
					} */
					
					$.ajax({
						type: "POST",
						url: "svcReport/permissions/",
						data: {userId:idUser,check:checkNoPlaces},
						success:  function (data) {

							$('#flexUsersAdmin').flexOptions({newp: 1}).flexReload();
							
							$("#editUser").slideToggle("slow");
							
							var active = $(".saveUser").attr("data");
							
							$("#"+active).addClass("active");
							
							$("#"+active).slideToggle("slow");

						}
					});
					
				});
			});
			
			$("#flexPlaces").flexigrid({
				url: 'svcReport/selectPlaces/',
				dataType: 'json',
				method: 'POST', 
				colModel : [ 
					{display: 'ID', name : 'idPlace', width : 80, sortable : false, align: 'center'},
					{display: 'Date', name : 'date', width : 60, sortable : false, align: 'center'},
					{display: 'Street', name : 'street', width : 70, sortable : false, align: 'center'},
					{display: 'Route', name : 'route', width : 180, sortable : true, align: 'center'},
					{display: 'City', name : 'city', width : 120, sortable : true, align: 'center'},
					{display: 'Neighborhood', name : 'neighborhood', width : 130, sortable : true, align: 'center', hide: false, align: 'center'},
					{display: 'Url', name : 'type', width : 180, sortable : false, align: 'center'},
				],
				buttons : [
					{name: 'Edit', bclass: 'edit', onpress : editPlace},
					{separator: true}
				],  
				searchitems : [
					{display: 'Neighborhood', name : 'neighborhood'},
					{display: 'City', name : 'city'},
					{display: 'Route', name : 'route', isdefault: true}
				],
				sortname: "iso",
				sortorder: "asc",
				usepager: true,
				title: 'Places',
				useRp: true,
				rp: 15,
				showTableToggleBtn: true,
				width: 900,
				height: 400
			});
			
			$("#flexNoPlaces").flexigrid({
				url: 'svcReport/selectnoPlaces/',
				dataType: 'json',
				method: 'POST', 
				colModel : [ 
					{display: 'ID', name : 'idPlace', width : 80, sortable : false, align: 'center'},
					{display: 'Date', name : 'date', width : 60, sortable : false, align: 'center'},
					{display: 'Route', name : 'route', width : 180, sortable : false, align: 'center'},
					{display: 'City', name : 'city', width : 120, sortable : false, align: 'center'},
					{display: 'Neighborhood', name : 'neighborhood', width : 130, sortable : false, align: 'center', hide: false, align: 'left'},
					{display: 'Url', name : 'type', width : 250, sortable : false, align: 'center'},
				],
				buttons : [
					{name: 'Edit_Place', bclass: 'edit', onpress : editNoPlace},
					{separator: true}
				],  
				searchitems : [
					{display: 'ID', name : 'idPlace'},
					{display: 'route', name : 'route', isdefault: true}
				],
				sortname: "iso",
				sortorder: "asc",
				usepager: true,
				title: 'Places',
				useRp: true,
				rp: 15,
				showTableToggleBtn: true,
				width: 900,
				height: 400
			});
			
			$("#flexUsers").flexigrid({
				url: 'svcReport/selectUsers/',
				dataType: 'json',
				method: 'POST', 
				colModel : [ 
					{display: 'First Name', name : 'first_name', width : 180, sortable : false, align: 'center'},
					{display: 'Last Name', name : 'last_name', width : 180, sortable : false, align: 'center'},
					{display: 'Gender', name : 'gender', width : 60, sortable : false, align: 'center'},
					{display: 'Email', name : 'email', width : 180, sortable : false, align: 'center'},
					{display: 'School', name : 'school', width : 130, sortable : false, align: 'center', hide: false, align: 'center'}
				],  
				searchitems : [
					{display: 'ID', name : 'first_name'},
					{display: 'route', name : 'last_name', isdefault: true}
				],
				sortname: "iso",
				sortorder: "asc",
				usepager: true,
				title: 'Users',
				useRp: true,
				rp: 15,
				showTableToggleBtn: true,
				width: 900,
				height: 400
			});
			
			$("#flexUsersAdmin").flexigrid({
				url: 'svcReport/selectUsers/',
				dataType: 'json',
				method: 'POST', 
				colModel : [ 
					{display: 'ID', name : 'id', width : 100, sortable : false, align: 'center'},
					{display: 'First Name', name : 'first_name', width : 180, sortable : false, align: 'center'},
					{display: 'Last Name', name : 'last_name', width : 180, sortable : false, align: 'center'},
					{display: 'Gender', name : 'gender', width : 60, sortable : false, align: 'center'},
					{display: 'Email', name : 'email', width : 180, sortable : false, align: 'center'},
					{display: 'School', name : 'school', width : 130, sortable : false, align: 'center', hide: false, align: 'center'}
				],
				buttons : [
					{name: 'Edit_User', bclass: 'edit', onpress : editUser}, 
					{separator: true}
				],
				searchitems : [
					{display: 'ID', name : 'first_name'},
					{display: 'route', name : 'last_name', isdefault: true}
				],
				sortname: "iso",
				sortorder: "asc",
				usepager: true,
				title: 'Users',
				useRp: true,
				rp: 15,
				showTableToggleBtn: true,
				width: 900,
				height: 400
			});
			
			function editUser(com, grid) {
				if (com == 'Edit_User')
				{
					$.each($('.trSelected', grid),
						function(key, value){
						
							var idUser = value.children[0].innerText; // in case we're changing the key
							
							$.ajax({
								type: "POST",
								url: "svcReport/selectUserId",
								data: {userId:idUser},
								success:  function (data) {
								
									var obj = $.parseJSON(data);
						
									$("#idUser").val(obj.id);
									$("#nameUser").val(obj.name);
									$("#emailUser").val(obj.email);
									
									if(obj.status==1)
									{
										$("#statusUser").attr("checked","checked");
										$("#statusUser").attr("value","0");
									}
									
								}
							});
							
							$("#adminusers").slideToggle("slow");
							
							$("#editUser").slideToggle("slow"); 
					});
				}
			}
			
			function editNoPlace(com, grid)
			{
				if (com == 'Edit_Place')
				{
					$.each($('.trSelected', grid),
					function(key, value){
						var idnoPlace = value.children[0].innerText; // in case we're changing the key
							
						$.ajax({
							type: "POST",
							url: "svcReport/selectPlaceId",
							data: {placesId:idnoPlace},
							success:  function (data) {
							
								var obj = $.parseJSON(data);
						
								$("#editnoPlaceId").val(obj.id);
								$("#streetno").val(obj.street);
								$("#routeno").val(obj.route);
								$("#cityno").val(obj.city);
								$("#neighborhoodno").val(obj.neighborhood);
								
							}
						}); 
						
						$("#noPlaces").slideToggle("slow");
						
						$("#editNoPlace").slideToggle("slow"); 
					});
				} 
			}
			
			function editPlace(com, grid) {
				if (com == 'Edit') {
					
					$.each($('.trSelected', grid),
						function(key, value){
							
							var idPlace = value.children[0].innerText; // in case we're changing the key
												
							$.ajax({
								type: "POST",
								url: "svcReport/selectPlaceId/",
								data: {placesId:idPlace},
								success:  function (data) {
								
									var obj = $.parseJSON(data);
						
									$("#editPlaceId").val(obj.id);
									$("#street").val(obj.street);
									$("#route").val(obj.route);
									$("#city").val(obj.city);
									$("#neighborhood").val(obj.neighborhood);
									
								}
							});
							
							$("#global").slideToggle("slow");
							
							$("#editPlace").slideToggle("slow"); 
								
                    }); 
				}
			}			
		</script>
		{/literal}

</html>

