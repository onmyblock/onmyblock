{strip}
{*
This is the browse page.

Variables assigned:
$user (user info)
$places (array of listed houses)
$people (array of houses associated with user friends)
*}
{include file='../header4.tpl'}
{literal}
<script type="text/javascript">
var pag=1;
var datFilter="";
var search="";
	
$(document).ready(function(){
		
	$('.popup').click(function(){
		var idPlace = $(this).attr("id");
		
		$.fancybox({
			href: 'browse_popup?id='+idPlace,
			type: 'ajax',
			scrolling: 'no',
			hideOnContentClick: true
		});
	});
	
	$('.filter').click(function(){
		
		pag=1;
		pagTwo=0;
		
		datFilter = $(this).attr("data-option-value");
		
		search = $("#searchTextField").val();
		 
		$('#browse_container').isotope('destroy');
		$('#browse_container').empty();
		
		$.ajax({
			type: 	"POST",
			url:	"browse_items",
			data:	{ dataFilter:datFilter,pagina:pagTwo,valSearch:search } 
		}).done(function(data) {
			var $container = $('#browse_container');
			
			$container.isotope({
				resizable: true,
				masonry: {
					columnWidth: colW
				},
				animationEngine: 'best-available',
				itemSelector: '.browse_item',
				filter: '*'
			});
			
			$newData = $(data);
			$('#browse_container').append($newData).isotope('appended', $newData); 
		});
		pagTwo++;
	});

	$(window).scroll(function(){
		if ($(window).scrollTop() == $(document).height() - $(window).height()){
			
			$.ajax({
				type: 	"POST",
				url:	"browse_items",
				data:	{ dataFilter:datFilter,pagina:pag,valSearch:search } 
			}).done(function(data) {
				$newData = $(data);
				$('#browse_container').append($newData).isotope('appended', $newData);
			});
			
			pag++;
		}
	});	
	
	var input = document.getElementById('searchTextField');     
	var autocomplete = new google.maps.places.Autocomplete(input, {
		types: ["geocode"],
		componentRestrictions: {country: 'USA'}
	}); 

	var status=1;
	
	function callNotification(status)
	{
		if(status==1)
		{
			$.ajax({
				type:"POST",
				url:"svcNotification",
				data: {
					type: "result"
				}
			}).done(function(data) {
				if(data!=0)
				{
					$('#target-domm').css('display','block');

					$("#numNotificationm").empty();

					$("#numNotificationm").text($.trim(data));
				} else {
					$('#target-domm').css('display','none');
				}
			});
		}
	}

	setInterval(function() {
		$floatBar = $('.floatbar');
		
		if($(document).scrollTop() > 60){
			if(!$floatBar.hasClass('floating')){
				$floatBar.hide();
				$floatBar.addClass('floating');
				$floatBar.css({'top':'0','position':'fixed'}, function(){});
				$floatBar.fadeIn('normal');
				$(".menuBtnn").css("display","block"); 
				
				callNotification(status);

				status=0;
			}
		} else {
			if($floatBar.hasClass('floating')){
				$floatBar.hide();
				$floatBar.removeClass('floating');
				$floatBar.css({'position':'relative'}, function(){});
				$floatBar.fadeIn('normal');
				$(".menuBtnn").css("display","none"); 

				status=1;
			}
		}
		
	}, 100);
});

</script>
{/literal}


<div class="floatbar" style="z-index:100">	
		
	<div class="homeHeaderBackground">
		<div id="top_container" class="menuBtnn homeHeader" style="display:none;height: 64px;text-align: center;">
			<div style="margin: auto;">
			
				<div id="logo" style="width: 9%;margin-right: 115px;"></div>
				 
				{if $page != 'register'}
					<div class="menu_buttons" style="width: 393px;">
						{if $smarty.cookies.omb_session == 'loggedin'}
							<a {if $filename == 'myblock'} class="selected" {/if} style="margin-right: 14px;" href="myblock">
								<span class="fs1" style="margin-left: 13px;" data-icon="&#x25;"></span>MYBLOCK
								<div id="target-domm" style="margin-left: 115px;display:none;width:16px;height:16px;background: #d16394;border-radius:10px;font-size:10px;color: white;left: 9%;margin-top: -41px;margin-left: 106px;float:left;text-align:center;" ><p id="numNotificationm" class="notransformar" style="margin-top: -4px;"></p></div>
							</a>
						{else}
							<a {if $filename == 'home'} class="selected" {/if} href="/"><span class="fs1" style="margin-left: 24px;" data-icon="&#x25;"></span>Home</a>
						{/if}
						<a {if $filename == 'browse'} class="selected" {/if} href="browse" style="margin-right: 7px;"><span class="fs1" style="margin-left: 10px;" data-icon="&#x30;" style="font-size: 15px top:-1px;"></span>BROWSE</a>
						<a {if $filename == 'map'} class="selected" {/if} href="map"><span class="fs1" data-icon="&#x2f;" style="top:-1px;"></span>MAP</a>
					</div>
					
					

					{if $smarty.cookies.omb_session == 'loggedin'}
						<div id="user_login_container">
							<ul id="user_login">
								<li>
									<a href="#">
										<img src="https://graph.facebook.com/{$user.username}/picture?square" />
										<h6>{$user.first_name} {$user.last_name}</h6>
									</a>
									<ul>
										<li><a href="myblock"><h5>Profile</h5></a></li>
										<li><a onclick="fbLogout()"><h5>Logout</h5></a></li>
									</ul>
								</li>
							</ul>
						</div>
					{else}
						<a id="buttonFb" class="buttonFb blue-button" style="width: 130px; margin-top: 19px; margin-left: 50px; text-align:center; padding:5px; font-size:12px;">Login with Facebook</a>
					{/if}
				{/if}
			</div>
		</div>
	</div>
	
	<div id="front_menu" class="browe_top_menu " style="width: 100%;margin-top: 0px;margin-left:0px;left:0px;text-align: center;">
		<div id="options" class="browse_options" style="width: 930px;margin: auto;" >
			<ul class="browse_nav" data-option-key="filter" style="overflow: visible;width: 770px;" > 
				<li style="width: 120px;"><a id="new" loc="#new" data-option-value="" class="filter selected">New Pads</a></li>               
				<li style="width: 120px;"><a id="pupolar" loc="#popular" data-option-value="popular" class="filter">Popular Pads</a></li>                
				<li style="width: 120px;"><a id="beach" loc="#beach" data-option-value="beach" class="filter">Beaches</a></li>
				<li style="width: 120px;"><a id="school" loc="#school" data-option-value="school" class="filter">Close to School</a></li>
				<!--<li style="width: 120px;"><a id="friends" class="tabs" loc="#friends" data-option-value="friends">Friends</a></li>--> 
			</ul>
			
			<div style="float: left;margin-top: 3px;width: 290px;margin-left: 20px;" >
				<input type="text" placeholder="Please enter city or neighborhood" style="width: 282px;" name="search" id="searchTextField" /> 
				<div style="width: 19px;height: 19px;margin-top: -21px;margin-left: 266px;cursor: pointer;">
					<img id="btnSearch" src="https://onmyblock.com/media/images/lupa.png" class="filter" data-option-value="browse_search" style="margin-top: 2px" />  
				</div>
			</div>
		</div>
	</div>
</div>

<div style="margin-bottom:30px;margin-top: 6px;">	 
    <div id="browse_container">
        {foreach from=$places item='place' name=disp } {assign var=photo value=","|explode:$place.4}
			{if {$place.height} > 80} 
			<div style="height:{$place.height}px; background-image:url(https://s3-us-west-1.amazonaws.com/onmyblock/places/{$place.0}/profile.jpg?time={$time});background-size: 315px {$place.height}px;background-repeat:no-repeat;" class="browse_item {if $place.4 < 2} 1br 
									{elseif $place.4 == 2} 2br 
									{elseif $place.4 == 3} 3br 
									{elseif $place.4 > 3} 4br {/if}
									{if $place.5 < 1000} 0-1 
									{elseif $place.5 >= 1000 && $place.5 < 2000} 1-2 
									{elseif $place.5 >= 2000 && $place.5 < 3000} 2-3 
									{elseif $place.5 >= 3000} 3-more {/if}" place_id="{$place.0}">
				<div class="browse_box" id="{$place.0}"> 
					<div class="dataname" style="top:5px;">{if $place.4 > 0}{$place.4}br /{/if} ${$place.5} </div>
					<div id="{$place.0}" style="z-index:9999 !important;" class="popup"></div>
					<div class="browse_item_header_other" style="height:200px; position:relative; width:auto; margin-left: 180px;" user_id="{$user.id}" place_id="{$place.0}">
						<!--input type="button" class="blue-button" style="float:right; padding:7px; font-size:14px;" value="${$place.5}" /-->												
						<div class="tooltip" style="float:left; width:80px; position:absolute; z-index:99999;">
							<span id="imgm_{$place.0}" class="tooltip1">{if $place.favorited eq 1}Added to Favorites{else}Add to Favorites{/if}</span>
							<a class="tipsy"  style="width:20px;height:24px;" id="{if $place.favorited eq 1}Added to Favorites{else}Add to Favorites{/if}" user_id="{$user.id}" place_id="{$place.0}" >
								{if $place.favorited eq 1}
									<input type="button" class="myblock_tipsy" data-animation="1" style="background: #68ab64; background:url(https://onmyblock.com/media/images/icons/myblock_w.png) 4px 2px no-repeat #68ab64; opacity: 0.6;" id="{$place.0}" />					
								{else}
									<input type="button" class="myblock_tipsy" data-animation="0" id="{$place.0}"/>
								{/if}
							</a>
						</div>						
						<div class="a-btn tooltip2" id="{$place.0}" style="position:relative; margin-left:50px;float:left; width:80px; height:30px; z-index:99999;" >
							<span id="imgv_{$place.0}" class="tooltip2">{if $place.liked eq 1}Liked{else}Like{/if}</span>
							<a class="tipsy"  style="width:24px;height:24px;" id="{if $place.liked eq 1}Liked{else}Like{/if}" user_id="{$user.id}" place_id="{$place.0}" > 
								{if $place.liked eq 1}
									<input type="button" class="browselike" data-animation="1" style="background:#d16394; background:url(https://onmyblock.com/media/images/thumb_up_w.png) 9px 5px no-repeat #d16394; opacity: 0.6;" id="{$place.0}"/>
								{else}
									<input type="button" class="browselike" data-animation="0" id="{$place.0}"/>
								{/if}
							</a>  
						</div>
						<!--
							<input type="button" class="browse_like" title="Like this House"  {if $place.liked eq 1}id="browse_liked"{/if}/>
							<input type="button" class="browse_fav" title="Add to Wishlist" {if $place.favorited eq 1}id="browse_favorited"{/if}/>
						-->
					</div>
					<!--div class="browse_sweet_border">Sweet</div-->
					<!--a class="browse_sweet {if $place.liked eq 1}browse_sweetnd{/if}" style="margin-top:{$place.height/2-25}px;"></a-->
					<div class="dataname">{$place.6} {$place.7} <br /> {$place.1}</div>
					<div class="browse_item_info">
						<div class="bi">
							<div class="bi0"><div class="fs1" data-icon="&#x2a;"></div>{$place.views}</div>
							<div class="bi1" ><div class="fs1" style="height: 20px; background:url(https://onmyblock.com/media/images/thumb_up3.png) 9px 3px no-repeat; margin-right: 17px;" ></div><p id="p{$place.0}" style="margin-top: -21px; margin-left: 10px;">{$place.likes}</p></div>
							<div class="bi2"><div class="fs1" data-icon="&#x22;"></div>{$place.comments}</div>
						</div>
					</div>
				</div>
			</div>
			{/if}
			{if $cant == $smarty.foreach.disp.iteration}
				{if $random == 1}
				<div style="height:210px; background:url(https://onmyblock.com/media/images/capa.png) no-repeat; background-size: 280px 210px; width: 280px; text-align: center;" class="browse_item"  place_id="0">
					<div style="margin-top: 15%;">
						<div style="width: 20px;height: 90px;float: left;"></div>
						<div style="height: 90px;width: 20px;float: right;"></div>
						<span style="font-size: 16px; color: black;">Do you like where you live?</span> 
						<br/><br/>
						<span style="color:#51443c;">How's the landlord? What are the neighbors like? Share your story.</span>
					</div>
					<div {if $smarty.cookies.omb_session == 'loggedin' } onClick="window.location='add-house'"{else} onClick="openPopup()" {/if} style="margin-top: 20px;"><a id="buttonFb" class="buttonFb blue-button" style="width: 130px; text-align:center; padding:5px; font-size:12px;">Add Your House</a></div>
				</div>
				{else}
				<div style="height:210px; background:url(https://onmyblock.com/media/images/capa.png) no-repeat; background-size: 280px 210px; width: 280px; text-align: center;" class="browse_item" place_id="0">
					<div style="margin-top: 14%;text-align: center;">
						<div style="width: 20px;height: 90px;float: left;"></div>
						<div style="height: 90px;width: 20px;float: right;"></div>
						<span style="font-size: 16px; margin: auto; color: black;">Find your next college pad through the people you know.</span>
						<hr style="border: none; background-color: none; height: 0px;">
						<span style="margin: auto; padding: 6px;color:#51443c;">Find out where your classmates and friends live.</span>
					</div>
					<div {if $smarty.cookies.omb_session == 'loggedin' } id="optionsf" {else} onClick="openPopup()"  {/if}  data-option-value="friends" style="margin-top: 20px;" ><a id="buttonFb" class="buttonFb blue-button" style="width: 160px; text-align:center; padding:5px; font-size:12px;">See Your Friend´s Houses</a></div>
				</div>
				{/if}
			{/if}
        {/foreach}
		
    </div>
</div>
{literal}
<script type="text/javascript">
$(document).ready(function() {
	
	$(".myblock_tipsy").live('click', function(){	
		
		var sel = $(this).attr('id');
		var data = $(this).attr('data-animation');
		
		var antes = $('#imgm_'+sel).text();
		
		if($.cookie("omb_session")=="loggedin")
		{
			if(data==0){
				
				$("#imgm_"+sel).text("");
				$("#imgm_"+sel).text("Added to Favorites");	

			}else{
				$("#imgm_"+sel).text("");
				$('#imgm_'+sel).text('Add to Favorites');
			}
		}
	});
	
	
	$(".browselike").live('click', function() {
		
		var sel = $(this).attr('id');
		var data = $(this).attr('data-animation');
		
		var antes = $('#imgv_'+sel).text();
		
		if($.cookie("omb_session")=="loggedin")
		{
			if(data == 0){
				$("#imgv_"+sel).text("");
				$('#imgv_'+sel).text('Liked');
			}else{
				//alert('olakase');
				$("#imgv_"+sel).text("");
				$('#imgv_'+sel).text('Like');
			}
		}
	});

});
</script> 
{/literal}
{include file='../footer4.tpl'} {/strip}
