<!doctype html>
<!--[if lt IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html lang="es" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="es" class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<title>OnMyBlock - My Block</title>
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="author" content="">
		<!-- //metas para facebook -->
		<meta property="og:title" content="">
		<meta property="og:description" content="">
		<meta property="og:type" content="website">
		<meta property="og:url" content="">
		<meta property="og:image" content=" ">
		<meta property="og:site_name" content="">

		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
		<link rel="stylesheet" href="/media/css/main.css?v=1">    
		<link rel="stylesheet" type="text/css" href="/media/css/jquery.fancybox.css"/>
		
		<script src="/media/js/libs/modernizr-2.0.6.min.js"></script>
		<script>
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<style type="text/css">
			.myblock a{
				color: #000 !important;
			}

			header .menu ul li{
				padding-top: 9px !important;
			}

		</style>
	</head>
	<body>
		<header>
			<article>
				<div class="logo"></div>     		 
				{include file='../header.tpl'}
			</article>
		</header>    
		<div id="wrapper">
			<section id="section-4">
				<article>
					<div id="dataprofile">
						<div class="imgprofile">
							<img src="https://graph.facebook.com/{$user.id}/picture?type=large" />
						</div>
						<div class="idname">{$user.first_name} {$user.last_name}</div>

						<div class="accesos">
							<ul>
								<li id="favorites"><a class="on" href="javascript:">Favorites</a></li>
								<li id="profile"><a href="javascript:">Profile</a></li>
								<li id="places"><a onclick="fbLogout()" href="javascript:">Places</a></li>
							</ul>
						</div>
					</div>
					<div id="contentprofile">
						<div class="listlinks">
							<div class="link01">
								<div class="imglink01">
									<a href="browse">Look for houses to add them to your Wish List!</a>
								</div>
							</div>
							<div class="link01">
								<div class="imglink02">
									<a href="add-house">Add your house and talk about your living experience!</a>
								</div>           
							</div>
					  
							<div class="link01">
								<div class="imglink03">
									<a href="">See where your friends want to live next year!</a>
								</div>
							</div>
						</div>  
						<div class="contentaccesos">
							<div id="contfavorite">
								<!--div class="goto">
									<a href="">Go to Browse Page</a>
								</div-->
								{foreach $user.favorites item='place' name=pl}
									<div id="fav01-{$smarty.foreach.pl.iteration}" class="popsfavorites">                  
										<div class="imgfv">
											<a href="{$onmyblock}/place?id={$place.0}">
												<img src="https://s3-us-west-1.amazonaws.com/onmyblock/places/{$place.0}/profile.jpg">
											</a>
										</div>
										
										<div class="datafavor">
											<div id="{$smarty.foreach.pl.iteration}" data="{$place.0}" class="closepop"></div>
											<ul>
												<li><span>${$place.6}</span></li>
												<li class="icon01"><span>{$place.4}</span></li>
												<li class="icon02"><span>{if isset($place.5)}{$place.5}{else}N/A{/if}</span></li>
											</ul>
										</div>
									</div>
								{/foreach}	
							</div>
						
							
							
							<div style="display:none;"  id="contprofile">
								<div class="bgdata">
									<div class="datinter">
										<div class="dataprofile">
											<div class="minipic">
												<img src="https://graph.facebook.com/{$user.id}/picture?type=square" />
											</div>
											<div class="idnamemini">
												{$user.first_name} {$user.last_name}
											</div>
										</div>
										<div class="dataacount">
											<div class="email">
												Email: <span>{$user.email}</span>
											</div>
										  
											<div class="networks">
												Networks: <span>{foreach from=$user.networks item="array"}
																{foreach from=$array item="network"}
																{if $network@first}
																{/if}
																{$network}
																{/foreach}{/foreach}</span>
											</div>
											
											<div class="mylikes">
												My Likes:
												<div class="listlikes">
												  <ul>
													{foreach $user.place_likes item='plike' }
														<li><a href="javascript:">{$plike.name}</a></li>
													{/foreach}
												  </ul> 
												</div>
											</div>
										</div>
									</div>
								</div>                
							</div>
							<div style="display:none;" id="contplaces">
								<div class="addyour">
									<a href="add-house">Add Your House Now!</a>
								</div>
							</div>
						</div>
					</div>
					<div id="dataright">
						<div class="data01">
							<ul>
								<li>{$stats.friends_on_onmyblock.count} Friends on OnMyBlock</li>
								<li class="noborder">{$stats.total_places.count} Properties on OnMyBlock</li>
							</ul>
						</div>
					
						<span>Top Neighborhoods in San Diego</span>

						<div class="data01">
							<ul>
								{foreach $stats.top_neighborhoods item='neighborhood'}
									<li>{$neighborhood.1} Places in <a href="javascript:" class="neighborhoodName">{$neighborhood.0}</a></li>	
								{/foreach}
							</ul>
						</div>
					</div>
				</article>      
			</section>
		</div>
		
		
		{include file='../footer.tpl'}

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script type="text/javascript" src="/media/js/libs/jquery.fancybox.js"></script>
		<script src="/media/js/libs/jquery.easing.1.3.js" type="text/javascript"></script>
		<script src="http://maps.google.com/maps/api/js?libraries=places&region=uk&language=en&sensor=false"></script>
		<script src="/media/js/initr.js?v=1"></script>

		<script>
			(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_US/all.js#xfbmsl=1&appId=561406290571363";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
			
			$(".neighborhoodName").click(function(){
				var neighborhood = $(this).text();
				
				localStorage.setItem('neighborhood', neighborhood+', San Diego, CA, United States');
				localStorage.setItem('pageBack', 1);
				
				var url = "{$onmyblock}/map";    
				$(location).attr('href',url);
			});
		</script>


	</body>
</html>