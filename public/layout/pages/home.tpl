<!doctype html>
<!--[if lt IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html lang="es" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="es" class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<title>OnMyBlock</title>
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="author" content="">
		<!-- //metas para facebook -->
		<meta property="og:title" content="">
		<meta property="og:description" content="">
		<meta property="og:type" content="website">
		<meta property="og:url" content="">
		<meta property="og:image" content=" ">
		<meta property="og:site_name" content="">

		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
		<link rel="stylesheet" href="/media/css/main.css?v=1">
		<link rel="stylesheet" href="/media/css/bootstrap.css"/>
		<link rel="stylesheet" href="/media/css/jquery.vegas.css" type="text/css" media="all">    
		<link rel="stylesheet" type="text/css" href="/media/css/jquery.fancybox.css"/>

		<script src="/media/js/libs/modernizr-2.0.6.min.js"></script>
		<script>
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
		
	</head>
	<body>
		<header id="header">
			<article>
				<div class="logo"></div>
				{if $smarty.cookies.omb_session eq 'loggedin'}
					<div id="contentlogin">

						<div class="topminipic" style="float:left;">
							<img src="https://graph.facebook.com/{$user.username}/picture?square">
						</div>

						<div class="idnametop" style="float:left;">
							<a href="javascript:">{$user.first_name} {$user.last_name}</a>
						</div>

						<div style="display:none;" class="despledat">
							<ul>
								<li><a href="myblock">Profile</a></li>
								<li><a class="logout" href="javascript:">Logout</a></li>
							</ul>
						</div>
					</div>

					<div class="menu">            
						<ul>
							<li class="hide home"><a href="home">HOME</a></li>
							<li class="myblock"><a class="on" href="myblock">MYBLOCK</a></li>
							<li class="browse"><a href="browse">BROWSE</a></li>
							<li class="map"><a href="map">MAP</a></li>  
						</ul>
					</div>
				{else}
					<div class="works"><a href="howitworks">How it Works</a></div>
				{/if}
			</article>
		</header>    

		<div id="wrapper">

			<section id="section-1">
				<article>
					<div class="box1" {if $smarty.cookies.omb_session eq 'loggedin'}style="left:50%;margin-left:-298px;"{/if}>
						<div class="title01">Find Your Next College Pad.</div>
							<div class="slides_search" style="padding: 2px;margin-left:2%;"> 
							<form id="form-1" class="jqTransform jqtransformdone" enctype="multipart/form-data" action="map" method="post">
								<div class="width-1 w-4">
									<select name="test" id="school" class="select01">
										<option selected value="Where Do You Go to School?"></option>
										<option value="2">University of San Diego</option>
									</select>
								</div>
								<input id="slide_submit_search" class="blue-button" type="button" value="Go!">
								<div class="error"></div>
							</form>
						</div> 
					</div>
					{if $smarty.cookies.omb_session neq 'loggedin'}
					<div class="box2">
						<div class="fblogin">
							<div id="fb-root"></div>
							
							<div class="conterfbprincipal">
								<div class="conterimgfb"></div>
								<div class="conterfbcode" onclick="fbLogin()">
									<fb:login-button show-faces="true" width="400">----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------</fb:login-button>
								</div>
							</div>
							<div class="likefb"> 
								<div class="fb-like" data-href="https://www.facebook.com/OnMyBlock" data-send="false" data-width="286" data-show-faces="true" data-font="arial"></div>
							</div>
						</div>               
					</div>
					{/if}
				</article>
			</section>
		</div>

		{include file='../footer.tpl'}

		<script src="/media/js/libs/jquery-1.9.0.min.js"></script>
		<script src="https://connect.facebook.net/en_US/all.js"></script>
		<script src="/media/js/libs/jquery.cookie.js"></script> 
		<script type="text/javascript" src="/media/js/libs/jquery.vegas.js"></script>
		<script type="text/javascript" src="/media/js/libs/bootstrap-dropdown.js"></script>
		<script type="text/javascript" src="/media/js/libs/bootstrap-button.js"></script>
		<script type="text/javascript" src="/media/js/libs/jquery.fancybox.js"></script>
		<script src="/media/js/init.js?v=1"></script>
		<script src="/media/js/main.js"></script>
		<script>
			$(document).ready(function(){	
				$('.myblock a').removeClass('on');
			});
		</script>


	</body>
</html>