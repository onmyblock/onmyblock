{literal}
<script type="text/javascript">
$('.popup').click(function(){
	var idPlace = $(this).attr("id");
	
	$.fancybox({
		href: 'browse_popup?id='+idPlace,
		type: 'ajax',
		scrolling: 'no'
	});
});
$(".myblock_tipsy").live('click', function(){	
		
	var sel = $(this).attr('id');
	var data = $(this).attr('data-animation');
	
	var antes = $('#imgm_'+sel).text();
	
	
	if(data==0){
		
		$("#imgm_"+sel).text("");
		$("#imgm_"+sel).text("Added to Favorites");	

	}else{
		$("#imgm_"+sel).text("");
		$('#imgm_'+sel).text('Add to Favorites');
	}

});


$(".browselike").live('click', function() {
	
	var sel = $(this).attr('id');
	var data = $(this).attr('data-animation');
	
	var antes = $('#imgv_'+sel).text();

	if(data == 0){
		$("#imgv_"+sel).text("");
		$('#imgv_'+sel).text('Liked');
	}else{
		//alert('olakase');
		$("#imgv_"+sel).text("");
		$('#imgv_'+sel).text('Like');
	}

});
</script>
{/literal}
{if $smarty.cookies.omb_session != 'loggedin' and $smarty.cookies.omb_access == '' }
<div class="temp_popup_container" style="margin-left: -162px;">
</div>
<div class="temp_popup" style="margin-left: -298px; background: transparent;opacity: 1;" >
	<div style="background-image: url('http://onmyblock.com/media/images/login/popup.png'); background-repeat:no-repeat; margin:auto; border-radius: 10px; width:547px; height:448px; text-align: center;">
		<div style="height: 100%; width: 100%;">
			<a onclick="fbLogin()"><div style="background-image: url('http://onmyblock.com/media/images/v0-1/btnFacebook.png'); background-repeat:no-repeat; width:295px; height:49px; margin-top: 220px; margin-left: 127px; position: absolute;" ></div></a>
			<div style="background-image: url('http://onmyblock.com/media/images/login/dont-have-a-login.png'); background-repeat:no-repeat;width:186px; height:21px;margin-top: 304px;margin-left: 125px;position: absolute;" ></div>
			<a onclick="fbLogin()"><div style="background-image: url('http://onmyblock.com/media/images/login/signUp.png'); background-repeat:no-repeat;width:186px; height:21px;margin-top: 304px;margin-left: 319px;position: absolute;" ></div></a>
			<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FOnMyBlock&amp;send=false&amp;layout=standard&amp;width=480&amp;show_faces=true&amp;action=like&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; margin-top: 360px;margin-left: 8px; width:480px; height:80px;" allowTransparency="true"></iframe>
		</div>
	</div>
	<a onclick="closePopup()"><div style="background-image: url('http://onmyblock.com/media/images/login/X.png'); background-repeat:no-repeat;position: absolute;width:15px; height:14px;margin-left: 512px;margin-top: -433px;" ></div></a>
</div>

{/if}
{foreach from=$places item='place' name=disp} {assign var=photo value=","|explode:$place.4}
	{if {$place.height} > 80}
	<div id="openpop" style="height:{$place.height}px; background:url(https://s3-us-west-1.amazonaws.com/onmyblock/places/{$place.0}/profile.jpg?time={$time});background-size: 315px {$place.height}px;" class="browse_item {if $place.4 < 2} 1br 
							{elseif $place.4 == 2} 2br 
							{elseif $place.4 == 3} 3br 
							{elseif $place.4 > 3} 4br {/if}
							{if $place.5 < 1000} 0-1 
							{elseif $place.5 >= 1000 && $place.5 < 2000} 1-2 
							{elseif $place.5 >= 2000 && $place.5 < 3000} 2-3 
							{elseif $place.5 >= 3000} 3 {/if}" place_id="{$place.0}">
		<div class="browse_box">
			<div class="dataname" style="top:5px;">{if $place.4 > 0}{$place.4}br /{/if} ${$place.5}</div>
			<div id="{$place.0}" style="z-index:9999 !important;" class="popup"></div>
					<div class="browse_item_header_other" style="height:200px; position:relative; width:auto; margin-left: 180px;" user_id="{$user.id}" place_id="{$place.0}">
						<!--input type="button" class="blue-button" style="float:right; padding:7px; font-size:14px;" value="${$place.5}" /-->												
						<div class="tooltip" style="float:left; width:80px; position:absolute; z-index:99999;">
							<span>{if $place.favorited eq 1}Added to Favorites{else}Add to Favorites{/if}</span>
							<a class="tipsy"  style="width:20px;height:24px;" id="{if $place.favorited eq 1}Added to Favorites{else}Add to Favorites{/if}" user_id="{$user.id}" place_id="{$place.0}" >
								{if $place.favorited eq 1}
									<input type="button" class="myblock_tipsy" data-animation="1" style="background: #68ab64; background:url(https://onmyblock.com/media/images/icons/myblock_w.png) 4px 2px no-repeat #68ab64; opacity: 0.6;" id="imgm_{$place.0}" />					
								{else}
									<input type="button" class="myblock_tipsy" data-animation="0" id="imgm_{$place.0}"/>
								{/if}
							</a>
						</div>						
						<div class="a-btn tooltip2" id="{$place.0}" style="position:relative; margin-left:50px;float:left; width:80px; height:30px; z-index:99999;" >
							<span>{if $place.liked eq 1}Liked{else}Like{/if}</span>
							<a class="tipsy"  style="width:24px;height:24px;" id="{if $place.liked eq 1}Liked{else}Like{/if}" user_id="{$user.id}" place_id="{$place.0}" > 
								{if $place.liked eq 1}
									<input type="button" class="browselike" data-animation="1" style="background:#d16394; background:url(https://onmyblock.com/media/images/thumb_up_w.png) 9px 5px no-repeat #d16394; opacity: 0.6;" id="imgl_{$place.0}"/>
									
									
								{else}
									<input type="button" class="browselike" data-animation="0" id="imgl_{$place.0}"/>
								{/if}
							</a>  
						</div>
						<!--
							<input type="button" class="browse_like" title="Like this House"  {if $place.liked eq 1}id="browse_liked"{/if}/>
							<input type="button" class="browse_fav" title="Add to Wishlist" {if $place.favorited eq 1}id="browse_favorited"{/if}/>
						-->
					</div>
			<!--div class="browse_sweet_border">Sweet</div-->
			<!--a class="browse_sweet {if $place.liked eq 1}browse_sweetnd{/if}" style="margin-top:{$place.height/2-25}px;"><p class="fs1" data-icon="&#x32;"></p>Sweet</a-->
			<div class="dataname">{$place.6} {$place.7} <br /> {$place.1}</div>
			<div class="browse_item_info">
				<div class="bi">
					<div class="bi0"><div class="fs1" data-icon="&#x2a;"></div>{$place.views}</div>
					<div class="bi1" ><div class="fs1" style="height: 20px; background:url(https://onmyblock.com/media/images/thumb_up3.png) 9px 3px no-repeat; margin-right: 17px;" ></div><p id="p{$place.0}" style="margin-top: -21px; margin-left: 10px;">{$place.likes}</p></div>
					<div class="bi2"><div class="fs1" data-icon="&#x22;"></div>{$place.comments}</div>
				</div>
			</div>
		</div>
	</div>
	{/if}
{/foreach}
