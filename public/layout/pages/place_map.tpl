{*
This is a page used as an iframe for the map and streetview in the house profile page.

Variables assigned:
$place (house information including address and geolocation.
$type (either map or streetview)
*}
<body style="overflow:hidden">
<link rel="stylesheet" href="https://onmyblock.com/media/css/style.css" type="text/css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="https://onmyblock.com/media/js/gmap3.min.js"></script>
{if $type eq 'map'}
{literal}
<script>
$(document).ready(function() {
	$("#place_map").gmap3({
		action: 'addMarker',
		address: "{/literal}{$place.street} {$place.route}, {$place.city}, {$place.state} {$place.postal}{literal}",
		map:{
			center: true,
			zoom: 14,
			mapTypeId: google.maps.MapTypeId.MAP,
			mapTypeControl: true,
			navigationControl: true,
			scrollwheel: true,
			streetViewControl: false
		},
		marker:{
			options:{
				icon:new google.maps.MarkerImage('https://onmyblock.com/media/images/map_icon.png',
				new google.maps.Size(24, 36),
				new google.maps.Point(0,0),
				new google.maps.Point(12, 32)),
				shadow:new google.maps.MarkerImage('https://onmyblock.com/media/images/map_icon_shadow.png',
				new google.maps.Size(19, 13),
				new google.maps.Point(0,0),
				new google.maps.Point(1, 10)),
				draggable: false
				}
		}

	});
});
</script>
<div id="place_map" style="width:600px; height:420px;"></div>
{/literal}
{elseif $type eq 'streetview'}
{literal}
<script>
$(document).ready(function() {
var streetViewService = new google.maps.StreetViewService();
var STREETVIEW_MAX_DISTANCE = 50;
var latLng = new google.maps.LatLng({/literal}{$place.latitude}, {$place.longitude}{literal});
streetViewService.getPanoramaByLocation(latLng, STREETVIEW_MAX_DISTANCE, function (streetViewPanoramaData, status) {
    if (status === google.maps.StreetViewStatus.OK) {
    $("#place_street_na").remove();
    $('#place_street_view').gmap3({ action:'init',
	zoom: 14,
	mapTypeId: google.maps.MapTypeId.ROADMAP,
	center: [{/literal}{$place.latitude}, {$place.longitude}{literal}]},
	{ action:'setStreetView',
		id: 'place_street_view',
		options:{
			position: [{/literal}{$place.latitude}, {$place.longitude}{literal}],
			pov: {
				heading: 0,
				pitch: 0,
				zoom: 1
				}}
	});
    } else {
    $("#street_view_tab").remove();
    }
});
});
</script>
<div id="place_street_na">Streetview is Not Available For This House</div>
<div id="place_street_view" style="width:600px; height:420px;"></div>
{/literal}
{/if}
</body>