{strip}
{*
This is the user profile page (not MyBlock)
It only appears if someone is viewing someone else's profile.

Variables assigned:
$user (viewer user info)
$profile (user info)
$places (places the user is associated with)
$place_likes (places the user likes)
$mutual_friends (mutual friends with the viewer)
*}
{include file='../header.tpl'}
<div id="main">
    <div class="container_24">
        <div class="home_tabs">
            <div class="grid_6">
                <div class="home_box_left">
                    <div class="home_box_left_img"><img src="https://graph.facebook.com/{$profile.id}/picture?type=large"></div>

                    <div class="white-box" style="margin-top:5px;">
                       <a href="https://www.facebook.com/profile.php?id={$profile.id}"> <h2 align="center">{$profile.first_name} {$profile.last_name}<br></h2></a>
                    </div>
                </div>
            </div>

            <div class="grid_12">
			    <div  class="home_box_middle">
			        <h1>Profile</h1>
			    </div>
			    <div class="home_box_middle">
			        <div class="home_box_middle_inner">
			            <a href="https://www.facebook.com/dialog/send?app_id=246041178762859&name=Sent%20From%20OnMyBlock&link=https://onmyblock.com&redirect_uri=https://onmyblock.com/profile?id={$profile.id}&to={$profile.id}&picture=https://onmyblock.com/images/logo.png" class="silver-button" style="margin-top:10px; padding:5px 10px 5px 10px; text-align:center; float:right">Message</a>
			
			            <div style="padding: 0 35px 0 35px;">
			                <br>
			                <br>
			                <h2><span style="margin:20px;">Hi, I'm {$profile.first_name}!</span></h2><br>
			                
			                
			                {* --- Mutual Friends --- *}
			                {if isset($mutual_friends.0.0)}
			                <hr>
							<div style="color:#a6a6a6; font-weight:bold;">
			                    Mutual Friends
			                </div>
			                 <br />
			                			                <div style="text-align:center">
							{foreach from=$mutual_friends item="friend"}
			                  <img width="50" src="https://graph.facebook.com/{$friend.0}/picture?type=square">
			                {/foreach}
			                			                </div>
			                
			                {/if}
			                {* --- *}
			                 <br />
			                
			                
			                {* --- Places --- *}
			                {if isset($places.0.0)}
			                <hr>
							<div style="color:#a6a6a6; font-weight:bold;">
			                    Places
			                </div>
			                 <br />
							{foreach from=$places item="place"}
			                <div style="text-align:center">
			                  <a href="place?id={$place.0}">  {if $place.1 == ''}{$place.2} {$place.3}{else}{$place.1}{/if} </a>
			                </div>
			                {/foreach}
			                
			                {/if}
			                {* --- *}
			                 <br />
			                 
			                
			                {* --- Liked Places --- *}
			                {if isset($place_likes.0.0)}
			                <hr>
							<div style="color:#a6a6a6; font-weight:bold;">
			                    Liked Places
			                </div>
			                 <br />
							{foreach from=$place_likes item="place"}
			                <div style="text-align:center">
			                 <a href="place?id={$place.0}">   {if $place.1 == ''}{$place.2} {$place.3}{else}{$place.1}{/if}</a>
			                </div>
			                {/foreach}
			                
			                {/if}
			                {* --- *}
			                
		
			                
			              <br />
			    </div>
			</div>
			</div>
            </div>
        </div>
    </div>
</div>
{include file='../footer.tpl'}
{/strip}