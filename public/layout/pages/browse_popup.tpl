{literal}
<style type="text/css">
.cs-prev{
	background: url(http://54.241.12.168/media/images/pg_prev.png) no-repeat 0 0 !important;
	color: #FFFFFF !important;
	padding: 0 10px !important;
	width: 263px !important;
	height: 397px !important;
	background-position:4px 168px !important;
	margin-top: 9px !important;				
}

.cs-next{
	background: url(http://54.241.12.168/media/images/pg_next.png) no-repeat 0 0 !important;
	color: #FFFFFF !important;
	padding: 0 10px !important;
	width: 264px !important;
	height: 397px !important;
	background-position:235px 168px !important;
	margin-top: 9px !important;				
}
.cs-coin-slider {
	height: 100% !important;
} 
</style>
{/literal}
<div class="temp_popup_container" style="position: absolute;">
</div>
<div class="temp_popup" style="position: absolute; margin-left: -298px; background: transparent;opacity: 1;width:547px;height:448px;" >
	<div style="background-image: url('http://onmyblock.com/media/images/login/popup.png'); background-repeat:no-repeat; margin:auto; border-radius: 10px; width:547px; height:448px; text-align: center;">
		<div style="height: 100%; width: 100%;">
			<a onclick="fbLogin()"><div style="background-image: url('http://onmyblock.com/media/images/v0-1/btnFacebook.png'); background-repeat:no-repeat; width:295px; height:49px; margin-top: 220px; margin-left: 127px; position: absolute;" ></div></a>
			<div style="background-image: url('http://onmyblock.com/media/images/login/dont-have-a-login.png'); background-repeat:no-repeat;width:186px; height:21px;margin-top: 304px;margin-left: 125px;position: absolute;" ></div>
			<a onclick="fbLogin()"><div style="background-image: url('http://onmyblock.com/media/images/login/signUp.png'); background-repeat:no-repeat;width:186px; height:21px;margin-top: 304px;margin-left: 319px;position: absolute;" ></div></a>
			<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FOnMyBlock&amp;send=false&amp;layout=standard&amp;width=480&amp;show_faces=true&amp;action=like&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; margin-top: 360px;margin-left: 8px; width:480px; height:80px;" allowTransparency="true"></iframe>
		</div>
	</div>
	<a href="javascript:" id="closePop"><div style="background-image: url('http://onmyblock.com/media/images/login/X.png'); background-repeat:no-repeat;position: absolute;width:15px; height:14px;margin-left: 512px;margin-top: -433px;" ></div></a>
</div>

<div id="boxdatapop">
	<div class="datatabspop">
		<div id="mainplacepop">
			<div class="titleplacepop">
				<a href="/place?id={$place.id}"><span style="width: 270px;height: auto;">{$place.name}</span></a>
				<a class="neighborhood" style="margin-top: 20px;" href="">{$place.neighborhood}</a>
			</div>
			<div id="{$place.id}" class="sociallogpop">
				<div data="1" style="display:{if $viewer.wishlisted neq 1}block{else}none{/if};" class="addfav fav btnFav addfavpop"><a >Add to Favorites</a></div>
				<div data="0" style="display:{if $viewer.wishlisted eq 1}block{else}none{/if};" id="favadd" class=" fav bgadd"><a href="javascript:">Added to Favorites</a></div>
				<div data="1" style="display:{if $viewer.liked neq 1}block{else}none{/if};" class="likebtn btnLike like likebtnpop"><a >Like</a></div>
				<div data="0" style="display:{if $viewer.liked eq 1}block{else}none{/if};" class="bgliked like"><a href="javascript:">Liked</a></div>
			</div>
		</div>
		<div class="viewprofileb"><a href="http://onmyblock.com/place?id={$place.id}" target="_parent">View Profile</a></div>
		<div class="tabslinepop">
			<a id="datphotospop" class="on" href="javascript:">Photos</a>
			<a id="datmappop"  href="javascript:">Map</a>
			<a id="datstreetpop" href="javascript:">From the Street</a>
		</div>

		<div id="contenttabslinepop">
			
			<div class="datphotospop">
				<div id="coin-slider">
				   {foreach from=$place.photos item='photo'}
					   <img src="https://s3-us-west-1.amazonaws.com/onmyblock/places/{$place.id}/{$photo.0}?time={$time}"  />
				   {/foreach}
				</div>     
			</div>
		
			<div style="display:none;" class="datmappop">
				<div class="show" id="map" style="width:550px; height:390px;margin-top: 48px;"></div>
			</div>
			<div style="display:none;" class="datstreetpop">
				<div class="show" id="place_street_view" style="width:550px; height:420px;">
					<iframe src="/place_map?id={$place.id}&type=streetview"  style="width: 550px; height:390px;"></iframe>				
				</div>
			</div>
		</div>    
	</div>
</div>
<script>
$(document).ready(function(){
	$('#coin-slider').coinslider({ width: 500, height: 310, navigation: true, delay: 5000, links : false }); 
});
</script>

<script>
	$('#closePop').click(function(){
		$('.fancybox-close').css('display','block');
		closePopup();
	});
	$('.addfav').click(function(){
		if($.cookie("omb_session")=="loggedin")
		{
			$('.addfav').hide();
			$('.bgadd').show();
			
			objDivParent = $(this).parent("div");
			var idPlace = objDivParent.attr("id");
			
			var valAction=$(this).attr("data");
			
			contenido("favorite", valAction, idPlace);
		} else {
			$('.fancybox-close').css('display','none');
			openPopup();
		}
	});

    $('.bgadd').click(function(){
	
		$('.addfav').show();
		$('.bgadd').hide();
		
		objDivParent = $(this).parent("div");
		var idPlace = objDivParent.attr("id");
		
		var valAction=$(this).attr("data");
		
		contenido("favorite", valAction, idPlace);
	}); 

	function crearAjax()
	{
		var xmlhttp=false;
		 try 
		{
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} 
		catch (e) 
		{  
			try 
			{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			  } 
			catch (e) 
			{
				   xmlhttp = false;
			  }
		 }

		if (!xmlhttp && typeof XMLHttpRequest!='undefined') 
		{
			  xmlhttp = new XMLHttpRequest();
		}
		return xmlhttp;
	}


	function contenido(type, valAction, idPlace)
	{
		var contenedor;
		contenedor = document.getElementById('content');
		
		ajax=crearAjax();
		
		ajax.open("GET", "svcAction/"+type+"?action="+valAction+"&place_id="+idPlace,true);
		
		ajax.onreadystatechange=function() 
		{
			if (ajax.readyState==4) // Readystate 4 significa que ya acabó de cargarlo
			{
				content.innerHTML = ajax.responseText
			}
		}
		ajax.send(null)
	}
	
	$('.bgliked').click(function(){
		if($.cookie("omb_session")=="loggedin")
		{
			$('.likebtn').show();
			$('.bgliked').hide();
		
			objDivParent = $(this).parent("div");
			var idPlace = objDivParent.attr("id");
			
			var valAction=$(this).attr("data");
			
			contenido("like", valAction, idPlace);
		} else {
			$('.fancybox-close').css('display','none');
			openPopup();
		}
	});
	
	$('.likebtn').click(function(){
		if($.cookie("omb_session")=="loggedin")
		{
			$('.bgliked').show();
			$('.likebtn').hide();
			
			objDivParent = $(this).parent("div");
			var idPlace = objDivParent.attr("id");
			
			var valAction=$(this).attr("data");
			
			contenido("like", valAction, idPlace);
		} else {
			$('.fancybox-close').css('display','none');
			openPopup();
		}
	});

	$('#datphotospop').click(function(){
		
		$('.datphotospop').fadeIn('slow');
		$('.datphotospop div ul').show();
		$('#datphotospop').addClass('on');

		$('.datmappop').hide();
		$('.datstreetpop').hide();

		$('#datstreetpop').removeClass('on');
		$('#datmappop').removeClass('on');        

	});

	$('#datmappop').click(function(){
		
		$('.datmappop').fadeIn('slow');
		$('.datmappop').show();
		$('#datmappop').addClass('on');

		$('.datphotospop').hide();
		$('.datstreetpop').hide();

		$('#datphotospop').removeClass('on');
		$('#datstreetpop').removeClass('on');

		$("#map").gmap3({
			action: 'addMarker',
			address: "{$place.street} {$place.route}, {$place.city}, {$place.state} {$place.postal}",
			map:{
			center: true,
			zoom: 14,
			mapTypeId: google.maps.MapTypeId.MAP,
			mapTypeControl: true,
			navigationControl: true,
			scrollwheel: true,
			streetViewControl: false
			},
			marker:{
			options:{
				icon:new google.maps.MarkerImage('https://onmyblock.com/media/images/map_icon.png',
				new google.maps.Size(24, 36),
				new google.maps.Point(0,0),
				new google.maps.Point(12, 32)),
				shadow:new google.maps.MarkerImage('https://onmyblock.com/media/images/map_icon_shadow.png',
				new google.maps.Size(19, 13),
				new google.maps.Point(0,0),
				new google.maps.Point(1, 10)),
				draggable: false
				}
			}
		});
	});

	$('#datstreetpop').click(function(){
		
		$('.datstreetpop').fadeIn('slow');
		$('.datstreetpop div').show();
		$('#datstreetpop').addClass('on');

		$('.datphotospop').hide();
		$('.datmappop').hide();

		$('#datphotospop').removeClass('on');
		$('#datmappop').removeClass('on');                

	});
</script>