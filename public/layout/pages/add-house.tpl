{strip}
{* 

This is the add-house page. It is still under construction. 

Variables assgined:
$user (user info)
$added (boolean to indicate the user added a valid address or not)

*} 
{include file='../header4.tpl'}
{literal}
<link rel="stylesheet" href="http://54.241.12.168/media/css/savecss/sexyalertbox.css" type="text/css"/>
<script src="http://54.241.12.168/media/js/libs/savejs/jquery.easing.1.3"></script>
<script src="http://54.241.12.168/media/js/libs/savejs/sexyalertbox.v1.2.jquery.mini.js"></script>
{/literal}
{literal}
<style type="text/css">
@media screen and (-webkit-min-device-pixel-ratio:0) {
	.blue-button {
		right:0 !important;
	}
}
</style>
{/literal}

{if $step == 2}
{literal}

<script>
$(document).ready(function() {

	statusImg = localStorage.status;
	
	if(statusImg==1)
	{
		Sexy.alert('Please upload at least one picture of the house!'); 
	}
	

	$("#add_house_map").gmap3({
		action: 'addMarker',
		address: "{/literal}{$address.street} {$address.route} {$address.city}, {$address.state} {$address.postal}{literal}",
		map:{
			center: true,
			zoom: 14,
			mapTypeId: google.maps.MapTypeId.MAP,
			mapTypeControl: false,
			navigationControl: false,
			scrollwheel: false,
			streetViewControl: false
		},
		marker:{
			options:{
				icon:new google.maps.MarkerImage('https://onmyblock.com/media/images/map_icon.png',
				new google.maps.Size(24, 36),
				new google.maps.Point(0,0),
				new google.maps.Point(12, 32)),
				shadow:new google.maps.MarkerImage('https://onmyblock.com/media/images/map_icon_shadow.png',
				new google.maps.Size(19, 13),
				new google.maps.Point(0,0),
				new google.maps.Point(1, 10)),
				draggable: false
				}
		}

	});
	
	$("#BoxAlertBtnOk").on("click",function(){
		url = "http://54.241.12.168/add-house";
	
		//$(location).attr('href',url);
	});
});
</script>
{/literal}
{/if}
{literal}
<script>
$(document).ready(function() {

	var statusPlace = "{/literal}{$slow}{literal}";

	if(statusPlace==1)
	{
		Sexy.alert('House already exists at this location! You can view the house profile if you would like.<a class="wishlistBlank" href="https://onmyblock.com/place?id={/literal}{$placeId}{literal}" target="_blank"  style="margin-left: 68px;margin-top: 42px;float: right;position: absolute;left: 188px;top: 20px;width:117px;"><p style="margin-left: 14px;" class="noselect">View Profile</p></a> '); 
	}

	var input = document.getElementById('searchTextField');     
	var autocomplete = new google.maps.places.Autocomplete(input, {
		types: ["geocode"],
		componentRestrictions: {country: 'USA'}
	});
	
	
});
</script>
{/literal}

<div id="main" style="text-align:center;width: 100%;">

        <div style="margin: auto;border-top: 3px solid #CCC;margin-top: 30px;width: 920px;"> 
			<h6 style="margin: auto;margin-top: -14px;background: #f6f6f6;width: 366px;color: black;font-size: 45px;" >Create a House</h6>
		</div>
                {if $step < 2}
                <div style="position:relative; height: 206px; margin: auto; width: 454px; padding:186px 0;">
                {if $step == 1}
                <!--h3> We're sorry, but the address is not correct. Please try again.</h3-->
                <br />
                {/if}
                <form method="post" action="add-house" id="form-1" class="jqTransform">
					<input type="text" style="width:350px;padding: 6px 5px;" id="searchTextField" value=""  name="address"><input type="submit" class="blue-button" style="padding: 8px 14px; top:1px;" value="Continue">
                    <h5 style="color:#ababab; font-size:11px; margin-left:10px;">Example: 1234 Main st. San Diego</h5>
                </form>
                </div>

                
                {elseif $step == 2}
                
                <div id="add_house_map" style="width:100%; height:420px; position:relative; top:-36px; border-bottom:1px solid #ccc;"></div>
                <div style="position:relative;margin-left:50%; left:-400px; padding-bottom:30px;">
                <form method="post" action="add-house">
                    <input type="text" style="width:350px;float: center;" id="add_house_address" value="{$address.street} {$address.route}, {$address.city}, {$address.state} {$address.postal} " name="address">
                    
                    <input type="submit" class="blue-button" style="padding:8px 15px; top:-4px; right:170px;" value="Change">
                </form>
                    <div style="margin: 10px 20px;">
                    <form method="post" action="add-house">
                    <!--input type="radio" name="resident" value="current"><h5 style="display:inline-block; padding-right:10px; padding-left:5px;">I live here</h5>
                    <input type="radio" name="resident" value="past"><h5 style="display:inline-block; padding-right:10px; padding-left:5px;">I used to live here</h5>
                    <input type="radio" name="resident" checked="true" value="none"><h5 style="display:inline-block; padding-right:10px; padding-left:5px;">I don't live here</h5-->
                    <div style="width:600px; margin-top:50px;">
						<h1 style="display:inline-block;">House Information </h1><h5 style="display:inline-block; padding-top:3px;">(Optional)</h5>
						<br />
						<div class="place_edit_co">
							<div class="place_edit_item">
							<h5>House Name: </h5><input type="text" class="place_edit_text" name="house_name" value="{$place.name}"/>
							</div>
							<div class="place_edit_item">
							<h5>Unit Number: </h5><input type="text" class="place_edit_text" name="house_unit" value="{$place.unit}"/>
							</div>
							<div class="place_edit_item">
							<h5>House Size (ft): </h5><input type="text" class="place_edit_text" name="house_size" value="{$place.size}"/>
							</div>
							<div class="place_edit_item">
							<h5>Neighborhood: </h5><input type="text" class="place_edit_text" name="house_neighborhood" value="{$address.neighborhood}"/>
							</div>
						</div>
						<div class="place_edit_co">
							<div class="place_edit_item">
							<h5>Monthly Rent: </h5><input type="text" class="place_edit_text" name="house_price" value="{$place.price}"/>
							</div>
							<div class="place_edit_item">
							<h5>Number of Bedrooms: </h5><input type="text" class="place_edit_text" name="house_rooms" value="{$place.rooms}"/>
							</div>
							<div class="place_edit_item">
							<h5>Number of Bathrooms: </h5><input type="text" class="place_edit_text" name="house_bathrooms" value="{$place.bathrooms}"/>
							</div>
						</div>
						
						<h1 style="display:inline-block;">Realtor Information </h1><h5 style="display:inline-block; padding-top:3px;">(Optional)</h5>
						<br />
							<div class="place_edit_co">
							<div class="place_edit_item">
							<h5>Realtor Name: </h5><input type="text" class="place_edit_text" name="realtor_name" value="{$place.realtor.name}"/>
							</div>
							<div class="place_edit_item">
							<h5>Phone Number: </h5><input type="text" class="place_edit_text" name="realtor_phone" value="{$place.realtor.phone}"/>
							</div>
						</div>
						<div class="place_edit_co">
							<div class="place_edit_item">
								<h5>Email Address: </h5><input type="text" class="place_edit_text" name="realtor_email" value="{$place.realtor.email}"/>
							</div>
							<div class="place_edit_item">
								<h5>Website: </h5><input type="text" class="place_edit_text" name="realtor_link" value="{$place.realtor.link}"/>
							</div>
							<input type="text" name="create_form" value="create" style="display:none" />
							<input type="text" name="address" value="{$address.street} {$address.route} {$address.city}, {$address.state} {$address.postal}" style="display:none" />
						</div>
						
						<h1 style="display:inline-block;">Images </h1><h5 style="display:inline-block; padding-top:3px;">(Required)</h5>
						<iframe id="photo_iframe" src="/photos_temp"  scrolling="no" marginwidth="0" marginheight="0" frameborder="0" vspace="0" hspace="0" style="overflow:visible; width:100%;height: 454px;margin-left:22px;"></iframe>
	                                	
	                                	<input type="submit" class="blue-button" value="Create" style="width:80px; margin-left:50%; left:-40px; position:relative" />
                        </form>

                    </div>
                    </div>
                </form>
                </div>
                
                {/if}

</div>
{include file='../footer4.tpl'}
{/strip}