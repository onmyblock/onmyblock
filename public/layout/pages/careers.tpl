<!doctype html>
<!--[if lt IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html lang="es" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html lang="es" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="es" class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<title>OnMyBlock</title>
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="author" content="">
		<!-- //metas para facebook -->
		<meta property="og:title" content="">
		<meta property="og:description" content="">
		<meta property="og:type" content="website">
		<meta property="og:url" content="">
		<meta property="og:image" content=" ">
		<meta property="og:site_name" content="">

		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
		<link rel="stylesheet" href="/media/css/main.css?v=1">
		<link rel="stylesheet" href="/media/css/jquery.vegas.css" type="text/css" media="all"> 

		

		<script src="/media/js/libs/modernizr-2.0.6.min.js"></script>
		<script>
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
	</head>
	<body>
		<header id="header">
			<article>
				<div class="logo"></div>
				{if $smarty.cookies.omb_session eq 'loggedin'}
					<div id="contentlogin">

						<div class="topminipic">
							<img src="https://graph.facebook.com/{$user.username}/picture?square">
						</div>

						<div class="idnametop">
							<a href="javascript:">{$user.first_name} {$user.last_name}</a>
						</div>

						<div style="display:none;" class="despledat">
							<ul>
								<li><a href="">Profile</a></li>
								<li><a href="">Logout</a></li>
							</ul>
						</div>
					</div>

					<div class="menu">            
						<ul>
							<li class="hide home"><a href="home">HOME</a></li>
							<li class="myblock"><a class="on" href="myblock">MYBLOCK</a></li>
							<li class="browse"><a href="browse">BROWSE</a></li>
							<li class="map"><a href="map">MAP</a></li>  
						</ul>
					</div>
				{else}
					<div class="works"><a href="howitworks.html">How it Works</a></div>
				{/if}
			</article>
		</header>
		<div id="wrapper">
			<section id="section-5">
				<article>
					<div class="boxhowitworks">
						<h1>Company Careers</h1>
						<h2>Join Our Team!</h2>
						<span>We build OnMyBlock with one mission in mind</span><br/>
							Make finding the right college pad an exciting and fun experience. After all, we are college students ourselves who too enjoy looking for great college pads.<br/><br/>
						<p>
							- iOS Developer - San Diego, CA<br/>

							- Web Designer - San Diego, CA<br/>

							- Campus Founder - University of San Diego<br/>

							- Campus Founder - San Diego State<br/>

							- Campus Founder - UCSD<br/>

							- Campus Founder - Point Loma University<br/>
						</p>
					</div>
				</article>
			</section>
		</div>

		{include file='../footer.tpl'}

		<script src="/media/js/libs/jquery-1.9.0.min.js"></script>
		<script src="/media/js/main.js"></script>
		<script src="//connect.facebook.net/en_US/all.js"></script>
		<script type="text/javascript" src="{$onmyblock}/media/js/libs/jquery.vegas.js"></script>
		<script src="/media/js/libs/jquery.easing.1.3.js" type="text/javascript"></script>
		<script src="/media/js/init.js?v=1"></script>
		<script>
			(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=561406290571363";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>

	</body>
</html>