{strip}
{* 
This file only contains the black footer and ends the html tags.
*}
<footer>
    <aside>
        <div class="row-lists">
                <div class="wrapper">
                    <article>
                        <h5 class="aside-title">OnMyBlock</h5>
                        <ul class="list-1">
                            <li><a href="howitworks">How it Works</a></li><span>.</span>
                            <li><a href="help">Help</a></li><span>.</span>
                            <li><a href="team">Team</a></li><span>.</span>
                            <li><a href="careers">Careers</a></li><span>.</span>
                            <li><a href="privacy">Privacy Policy</a></li><span>.</span>
                            <li><a href="terms">Terms of Service</a></li>
                        </ul>
                    </article>
                </div>
        </div>
    </aside>
</footer>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-34903792-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  	FB.init({
		appId: '246041178762859',
		frictionlessRequests: true,
		status: true,
		cookie: true
	});
	FB.getLoginStatus(function(response) {
		checkStatus(response);
	});
</script>
{literal}
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2612.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
{/literal}
</body>
</html>
{/strip}