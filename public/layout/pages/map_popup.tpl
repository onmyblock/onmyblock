{strip}
{*
This is a complete page used as a popup in the map page.
*}
<body style="overflow:hidden;" scroll="no">
{literal}
<script>
if(self==top)
{
window.open('', '_self', ''); 

}
</script>
{/literal}
<link rel="stylesheet" href="https://onmyblock.com/media/css/style.css" type="text/css">
<!--[if IE]><link rel="stylesheet" href="https://onmyblock.com/media/css/ie.css" type="text/css"><![endif]-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" ></script>
<script src="https://onmyblock.com/media/js/cufon-yui.js" ></script>
<script src="https://onmyblock.com/media/js/Kozuka_Gothic_Pro_OpenType_400.font.js" ></script>
<script src="https://onmyblock.com/media/js/cufon-replace.js" ></script>

<script src="https://onmyblock.com/media/js/jquery.jqtransform.js"></script>
<script src="https://connect.facebook.net/en_US/all.js"></script>
<div class="map_popup_container">
{if $photos > 0}
<div class="map_popup_container_border">
<a href="place?id={$place.id}" target="_parent"><img src="https://s3-us-west-1.amazonaws.com/onmyblock/places/{$place.id}/profile.jpg" /></a>
</div>
{else}
<div class="map_popup_container_border">
<a href="place?id={$place.id}" target="_parent"><img src="https://onmyblock.com/media/images/house.png" /></a>
</div>
{/if}
<div class="map_popup_content">
<a href="place?id={$place.id}" target="_parent"><h2>{$place.name}</h2></a>
<h5>{if $place.rooms == 1}1 Bedroom {elseif $place.rooms > 1}{$place.rooms} Bedrooms {/if}{if $place.rooms > 0 && $place.price > 100}-{/if}{if $place.price > 100} ${$place.price}/m{/if}</h5>
<!--iframe src="action?object=rating&id={$place.id}" scrolling="no" frameborder="0" style="border:none; overflow:hidden; visibility:hidden; margin-left:-24px;  width:170px; height:25px;" onload="this.style.visibility='visible';" allowTransparency="true"></iframe-->
<a href="place?id={$place.id}" id="viewHouse" class="blue-button" style="margin-top:10px;" target="_parent">View House Profile</a>
</div>
</div>
</body>
{strip}