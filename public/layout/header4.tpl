{strip}

<!doctype html>

<html lang="en">
<head>
    <title>{$title}</title>
    <meta charset="utf-8">
    <meta name="description" content="Find your next college place to live.">
    <link rel="stylesheet" href="/media/css/savecss/style.css" type="text/css" />
		<link type="image/x-icon" href="/media/images/map_icon_tab.png" rel="shortcut icon" />
    <link rel="stylesheet" href="/media/icons/style.css" />
    <link rel="stylesheet" type="text/css" href="/media/css/jquery.fancybox.css"/>
	<link rel="stylesheet" type="text/css" href="/media/css/coin-slider-styles.css" /> 
    <link rel="stylesheet" type="text/css" href="/media/css/basestyle.css"/>
    <!--[if IE]><link rel="stylesheet" href="/media/css/ie.css" type="text/css"><![endif]-->
    <!--[if lte IE 7]><script src="/media/icons/lte-ie7.js"></script><![endif]-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" ></script>
	<script src="https://maps.google.com/maps/api/js?libraries=places&region=uk&language=en&sensor=false"></script>
    <script src="/media/js/libs/coin-slider.js"></script>
    <script src="https://connect.facebook.net/en_US/all.js"></script>
    <script src="/media/js/libs/savejs/jquery.isotope.min.js"></script>
    <script src="/media/js/libs/savejs/main.js"></script>
	<script src="/media/js/libs/gmap3.min.js"></script>
    <script src="/media/js/libs/savejs/browse.js"></script>
    <script src="/media/js/libs/jquery.fancybox.js"></script>
    <script src="/media/js/initr.js"></script>
    
</head>

<body>

    <header style="margin-bottom:0px;">
		
        <div id="left_header"></div>
        
        <div id="middle_header">
        
			<div id="top_container">
				<a href="/">
					<div id="logo"></div>
				</a>
				
				{if $page != 'register'}
					
					{if  $filename != 'home'}
						<div class="menu_buttons">
							{if $smarty.cookies.omb_session == 'loggedin'}
							
							<a {if $filename == 'myblock'} class="selected" {/if} href="myblock">
								<span class="fs1" data-icon="&#x25;"></span>MYBLOCK
								
								<div style="width:24px;height:24px;margin-top: -30px;margin-left: -21px;">
									
									<div id="target-dom" style="margin-left: 115px;display:{if $number_notification neq 0}block{else}none{/if};width:16px;height:16px;background: #d16394;border-radius:10px;font-size:10px;color: white;left: 9%;margin-top: -8px;float:left;text-align:center;" ><p id="numNotification" class="notransformar" style="margin-top: -4px;">{$number_notification}</p></div> 
								</div>
							</a>
							
							{else}
								<a {if $filename == 'home'} class="selected" {/if} href="/" style="text-align: center;"><span class="fs1" data-icon="&#x25;"></span>Home</a>
							{/if}
							<a {if $filename == 'browse'} class="selected"{else} class="ses"  {/if} href="browse" data="browse"  style="text-align: center;"><span class="fs1" data-icon="&#x30;" style="font-size: 15px top:-1px;margin-left: -8px;"></span>BROWSE</a>
							<a {if $filename == 'map'} class="selected"{else} class="ses" {/if} href="map" data="map" style="text-align: center;margin-left: -22px;" ><span class="fs1" data-icon="&#x2f;" style="top:-1px;"></span>MAP</a>
						</div> 

						
						{if $smarty.cookies.omb_session == 'loggedin'}
							<div id="user_login_container">
								<ul id="user_login">
									<li>
										<a href="#">
										<img src="https://graph.facebook.com/{$user.username}/picture?square">
												<h6>{$user.first_name} {$user.last_name}</h6>
											</a>
											<ul>  
												<li><a href="myblock"><h5>Profile</h5></a></li>								
												<li><a onclick="fbLogout()"><h5>Logout</h5></a></li>
											</ul>
									</li>
								</ul>
							</div>
						{else}
							<!--onclick="fbLogin()"-->
							<div style="width: 245px;float: left;text-align: center;">
								<a id="buttonFb" class="buttonFb blue-button" style="width: 130px; margin-top: 19px; margin-left: 105px; text-align:center; padding:5px; font-size:12px;">Login with Facebook</a>
							</div>
						{/if}
					{else}
						{if $smarty.cookies.omb_session == 'loggedin'}
							<div class="menu_buttons">								
								<a href="myblock">
									<span class="fs1" data-icon="&#x25;"></span>MYBLOCK
									
									<div style="width:24px;height:24px;margin-top: -30px;margin-left: -21px;">
										<div id="target-dom" style="margin-left: 115px;display:none;width:16px;height:16px;background: #d16394;border-radius:10px;font-size:10px;color: white;left: 9%;margin-top: -8px;float:left;text-align:center;" ><p id="numNotification" class="notransformar" style="margin-top: -4px;"></p></div>
									</div>
								</a>
								<a class="selected" data="browse" href="browse"  style="text-align: center;"><span class="fs1" data-icon="&#x30;" style="font-size: 15px top:-1px;margin-left: -8px;"></span>BROWSE</a>
								<a data="map" href="map" style="text-align: center;margin-left: -22px;" ><span class="fs1" data-icon="&#x2f;" style="top:-1px;"></span>MAP</a>
							</div> 

							<div id="user_login_container">
								<ul id="user_login">
									<li>
										<a href="#">
										<img src="https://graph.facebook.com/{$user.username}/picture?square">
												<h6>{$user.first_name} {$user.last_name}</h6>
											</a>
											<ul>  
												<li><a href="myblock"><h5>Profile</h5></a></li>
												<li><a href="myblock"><h5>Settings</h5></a></li>
												<li><a onclick="fbLogout()"><h5>Logout</h5></a></li>
											</ul>
									</li> 
								</ul>
							</div>
						{else}
							<div class="menu_buttons"></div>
							<div class="menu_buttons">
								<a style="margin-left: 400px;text-transform: none; " href="howitworks">How it Works</a>
							</div>
						{/if}
					{/if}
				{/if}
			</div>

        </div>
        <div id="right_header"></div>
        
		
		{if $smarty.cookies.omb_session != 'loggedin' and $smarty.cookies.omb_access == '' }
		<div class="temp_popup_container" style="margin-top: 0px;">
		</div>
		<div class="temp_popup" style="margin-left: -298px; background: transparent;opacity: 1;" >
			<div style="background-image: url('http://onmyblock.com/media/images/login/popup.png'); background-repeat:no-repeat; margin:auto; border-radius: 10px; width:547px; height:448px; text-align: center;">
				<div style="height: 100%; width: 100%;">
					<a onclick="fbLogin()"><div style="background-image: url('http://onmyblock.com/media/images/v0-1/btnFacebook.png'); background-repeat:no-repeat; width:295px; height:49px; margin-top: 220px; margin-left: 127px; position: absolute;" ></div></a>
					<div style="background-image: url('http://onmyblock.com/media/images/login/dont-have-a-login.png'); background-repeat:no-repeat;width:186px; height:21px;margin-top: 304px;margin-left: 125px;position: absolute;" ></div>
					<a onclick="fbLogin()"><div style="background-image: url('http://onmyblock.com/media/images/login/signUp.png'); background-repeat:no-repeat;width:186px; height:21px;margin-top: 304px;margin-left: 319px;position: absolute;" ></div></a>
					<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FOnMyBlock&amp;send=false&amp;layout=standard&amp;width=480&amp;show_faces=true&amp;action=like&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; margin-top: 360px;margin-left: 8px; width:480px; height:80px;" allowTransparency="true"></iframe>
				</div>
			</div>
			<a onclick="closePopup()"><div style="background-image: url('http://onmyblock.com/media/images/login/X.png'); background-repeat:no-repeat;position: absolute;width:15px; height:14px;margin-left: 512px;margin-top: -433px;" ></div></a>
		</div>
		
		{/if}
		
    </header>
{/strip}  
