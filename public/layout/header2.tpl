{strip}
{*  

This is the header of all pages.
It contains links to the style sheets and scripts and the top banner.

Variables assigned come depanding on the requested page.

*}
<!DOCTYPE html>

<html lang="en">
<head>
	<!--[if IE]><link rel="stylesheet" href="http://54.241.12.168/media/css/ie.css" type="text/css"><![endif]-->
	<!--[if lte IE 7]><script src="http://54.241.12.168/media/icons/lte-ie7.js"></script><![endif]-->

    <title>{$title}</title>
    <meta charset="utf-8">
    <meta name="description" content="Find your next college place to live.">
    <link rel="stylesheet" href="http://54.241.12.168/media/css/style.css" type="text/css">
	<link type="image/x-icon" href="http://54.241.12.168/media/images/map_icon_tab.png" rel="shortcut icon">
    
    
    <link rel="stylesheet" href="http://54.241.12.168/media/icons/style.css" />	
	<link rel="stylesheet" href="http://54.241.12.168/media/css/jquery.vegas.css" type="text/css" media="all">
	<link rel="stylesheet" href="http://54.241.12.168/media/css/jquery-ui-1.10.1.custom.min.css" />

    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" ></script>
    <script type="text/javascript" src="http://54.241.12.168/media/js/jquery.vegas.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="http://maps.google.com/maps/api/js?libraries=places&region=uk&language=en&sensor=false"></script>
    
    <script src="http://54.241.12.168/media/js/gmap3.min.js"></script>
    <script src="https://connect.facebook.net/en_US/all.js"></script>
    <script src="http://54.241.12.168/media/js/cufon-yui.js" ></script>
    <script src="http://54.241.12.168/media/js/Kozuka_Gothic_Pro_OpenType_400.font.js" ></script>
    <script src="http://54.241.12.168/media/js/cufon-replace.js" ></script>
    <script src="http://54.241.12.168/media/js/jquery.isotope.min.js"></script>
    <script src="http://54.241.12.168/media/js/main.js"></script>
        
    
    <script src="http://54.241.12.168/media/js/jquery.jqtransform.js"></script>
    <!--<script src="http://54.241.12.168/media/js/home.js"></script>-->

    <script src="http://54.241.12.168/media/js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="https://loader.engage.gsfn.us/loader.js"></script>     
    <script src="http://54.241.12.168/media/js/jquery-ui-1.10.1.custom.min.js"></script>    	
	<script type="text/javascript" src="http://54.241.12.168/media/js/init.js"></script>
    
   
</head>

<body {if $filename eq 'map'} style="/*overflow: hidden;*/"{/if} style="/*min-width:800px;*/{if $filename eq 'home' }/*height:822px;*/{/if}{if $smarty.cookies.omb_session== 'loggedin'} /*overflow:scroll;*/{/if}" >
{if $filename != 'add_house'} 
	<!--div style="position:fixed; margin-top:200px; z-index:9999999;"><a id="feedback" ><img src="https://onmyblock.com/media/images/feedback.png" /></a></div-->
	
	<div id="getsat-widget-4815"></div>
	
{/if}
	{if $filename eq 'browse'}
	<div id="frame_app" style="background-image: url('http://onmyblock.com/media/images/v0-1/background_friends.png');height:49px;width:100%;text-align:center;">
		<div style="margin: auto;width: 690px;height: 40px;">
			<div style="background-image: url('http://onmyblock.com/media/images/v0-1/text_friends.png');background-repeat:no-repeat;width: 520px;height: 24px;float: left;margin-top: 16px;"></div>
			<div id="invite_btn" style="background-image: url('http://onmyblock.com/media/images/v0-1/btn_inviteNow.png');background-repeat:no-repeat;width: 109px;height: 35px;float: left;margin-top: 6px;"></div>
		</div>
		<div style="float:right;width:90px;height:40px;margin-top: -36px;">
			<div id="close_btn" style="background-image: url('http://onmyblock.com/media/images/v0-1/btn_close.png');background-repeat:no-repeat;width: 19px; height: 19px;margin-top: 11px;"></div>
		</div>
	</div>
	{/if}
    <header {if $filename eq 'browse'}style="margin-bottom:0px;"{/if}>
		
        <div id="left_header"></div>
        
        <div id="middle_header">
        
			<div id="top_container">
			
			<a id="animate">
				<div id="logo" {if $filename == 'home' and $smarty.cookies.omb_session != 'loggedin'}style="margin-left:-110px;"{/if}></div>
			</a>
			
			{if $page != 'register'}
				
				{if  $filename != 'home'}
					<div class="menu_buttons">
						{if $smarty.cookies.omb_session == 'loggedin'}
						
						<a {if $filename == 'myblock'} class="selected" {/if} href="myblock">
							<span class="fs1" data-icon="&#x25;"></span>MYBLOCK
							
							<div style="width:24px;height:24px;margin-top: -30px;margin-left: -21px;">
								<div id="target-dom" style="margin-left: 115px;display:none;width:16px;height:16px;background: #d16394;border-radius:10px;font-size:10px;color: white;left: 9%;margin-top: -8px;float:left;text-align:center;" ><p id="numNotification" class="notransformar" style="margin-top: -4px;"></p></div>
							</div>
						</a>
						
						{else}
							<a {if $filename == 'home'} class="selected" {/if} href="/" style="text-align: center;"><span class="fs1" data-icon="&#x25;"></span>Home</a>
						{/if}
						<a {if $filename == 'browse'} class="selected"{else} class="ses"  {/if} href="browse" data="browse"  style="text-align: center;"><span class="fs1" data-icon="&#x30;" style="font-size: 15px top:-1px;margin-left: -8px;"></span>BROWSE</a>
						<a {if $filename == 'map'} class="selected"{else} class="ses" {/if} href="map" data="map" style="text-align: center;margin-left: -22px;" ><span class="fs1" data-icon="&#x2f;" style="top:-1px;"></span>MAP</a>
					</div> 

					
					{if $smarty.cookies.omb_session == 'loggedin'}
						<div id="user_login_container">
							<ul id="user_login">
								<li>
									<a href="#">
									<img src="https://graph.facebook.com/{$user.username}/picture?square">
											<h6>{$user.first_name} {$user.last_name}</h6>
										</a>
										<ul>  
											<li><a href="myblock"><h5>Profile</h5></a></li>
											<li><a href="myblock"><h5>Settings</h5></a></li>
											<li><a onclick="fbLogout()"><h5>Logout</h5></a></li>
										</ul>
								</li>
							</ul>
						</div>
					{else}
						<!--onclick="fbLogin()"-->
						<div style="width: 245px;float: left;text-align: center;">
							<a id="buttonFb" class="buttonFb blue-button" style="width: 130px; margin-top: 19px; margin-left: 105px; text-align:center; padding:5px; font-size:12px;">Login with Facebook</a>
						</div>
					{/if}
				{else}
						{if $smarty.cookies.omb_session == 'loggedin'}
						
							<div class="menu_buttons">								
								<a {if $filename == 'myblock'} class="selected" {/if} href="myblock">
									<span class="fs1" data-icon="&#x25;"></span>MYBLOCK
									
									<div style="width:24px;height:24px;margin-top: -30px;margin-left: -21px;">
										<div id="target-dom" style="margin-left: 115px;display:none;width:16px;height:16px;background: #d16394;border-radius:10px;font-size:10px;color: white;left: 9%;margin-top: -8px;float:left;text-align:center;" ><p id="numNotification" class="notransformar" style="margin-top: -4px;"></p></div>
									</div>
								</a>
								
								
								<a {if $filename == 'browse'} class="selected"{else} class="ses" {/if} data="browse" href="browse"  style="text-align: center;"><span class="fs1" data-icon="&#x30;" style="font-size: 15px top:-1px;margin-left: -8px;"></span>BROWSE</a>
								<a {if $filename == 'map'} class="selected"{else} class="ses" {/if} data="map" href="map" style="text-align: center;margin-left: -22px;" ><span class="fs1" data-icon="&#x2f;" style="top:-1px;"></span>MAP</a>
							</div> 

							<div id="user_login_container">
								<ul id="user_login">
									<li>
										<a href="#">
										<img src="https://graph.facebook.com/{$user.username}/picture?square">
												<h6>{$user.first_name} {$user.last_name}</h6>
											</a>
											<ul>  
												<li><a href="myblock"><h5>Profile</h5></a></li>
												<li><a href="myblock"><h5>Settings</h5></a></li>
												<li><a onclick="fbLogout()"><h5>Logout</h5></a></li>
											</ul>
									</li> 
								</ul>
							</div>
						{else}
							<div class="menu_buttons"></div>
							<div class="menu_buttons">
								<a style="margin-left: 400px;text-transform: none; " href="howitworks">
									How it Works
								</a>
							</div>
						{/if}
					
				{/if}
			{/if}
			
			</div>

        </div>
        <div id="right_header"></div>
        
		
		{if $smarty.cookies.omb_session != 'loggedin' and $smarty.cookies.omb_access == '' }
		<!--div class="temp_popup_blocker">
		</div-->
		<div class="temp_popup_container">
		</div>
		<div class="temp_popup" style="margin-left: -298px; background: transparent;opacity: 1;" >
			<div style="background-image: url('http://54.241.12.168/media/images/login/popup.png'); background-repeat:no-repeat; margin:auto; border-radius: 10px; width:547px; height:448px; text-align: center;">
				<div style="height: 100%; width: 100%;">
					<a onclick="fbLogin()"><div style="background-image: url('http://onmyblock.com/media/images/v0-1/btnFacebook.png'); background-repeat:no-repeat; width:295px; height:49px; margin-top: 220px; margin-left: 127px; position: absolute;" ></div></a>
					<div style="background-image: url('http://54.241.12.168/media/images/login/dont-have-a-login.png'); background-repeat:no-repeat;width:186px; height:21px;margin-top: 304px;margin-left: 125px;position: absolute;" ></div>
					<a onclick="fbLogin()"><div style="background-image: url('http://54.241.12.168/media/images/login/signUp.png'); background-repeat:no-repeat;width:186px; height:21px;margin-top: 304px;margin-left: 319px;position: absolute;" ></div></a>
					<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FOnMyBlock&amp;send=false&amp;layout=standard&amp;width=480&amp;show_faces=true&amp;action=like&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; margin-top: 360px;margin-left: 8px; width:480px; height:80px;" allowTransparency="true"></iframe>
				</div>
			</div>
			<a onclick="closePopup()"><div style="background-image: url('http://54.241.12.168/media/images/login/X.png'); background-repeat:no-repeat;position: absolute;width:15px; height:14px;margin-left: 512px;margin-top: -433px;" ></div></a>
		</div>
		
		{/if}
		
		{*if $smarty.cookies.omb_access eq 'return'}
		<div class="temp_popup_blocker">
		</div>
		<div class="temp_popup_container">
		</div>
		<div class="temp_popup">

		We're sorry, your Facebook account does not say you are in the University of San Diego network. However, we plan to expand to other universities soon, so please submit your email below to receive company updates!
		<br />
		<input type="text" id="email_beta" class="slides_search_city" value="Email" onfocus="this.value = (this.value=='Email')? '' : this.value;" style="color:#6d6d6d; width:290px; font-size:16px; padding:5px; margin:10px; margin-top:20px;" />
		<input type="submit" id="send_beta_email" class="blue-button" value="Submit" style="font-size:17px;" />

		<div id="thankyou_message"></div>
		<br />
		<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FOnMyBlock&amp;send=false&amp;layout=standard&amp;width=480&amp;show_faces=true&amp;action=like&amp;colorscheme=dark&amp;font&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:480px; height:80px;" allowTransparency="true"></iframe>
		<div style="text-align:center; font-size:11px; margin-top:-30px;"><a onclick="fbLogin()">Login through Facebook if you're already a beta user</a></div>
		</div>

		{/if*}
		
    </header>
{/strip}  
