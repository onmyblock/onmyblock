<?php

if (!class_exists("wp_twitter_list_core")) {

	class wp_twitter_list_core {
	
		var $wp_twitter_list_default = array();
		
		function wp_twitter_list_core()
		{
			$this->__construct();
		}
		
		function __construct()
		{
			$this->wp_twitter_list_default = array(
				account 		=> "juliusdesign",
				tweet_count 	=> "5",
				delay_effect 	=> "1000",
				text_loading 	=> "Load last news...",
				tweet_enable 	=> "yes",
				
				div_ul 			=> "twitt-box",
				ul_id 			=> "twitt-ul",
				class_li 		=> "twitt-li",
				class_text_li 	=> "twitt-li-text"
			);
			
			
			if( is_admin() )
			{
				add_action('plugins_loaded', array( &$this, 'plugins_loaded' ) );
				add_action('admin_menu', array( &$this, 'install' ));
				add_action('init', array( &$this, 'update_settings' ));
			}
		}
		
		
		function plugins_loaded()
		{	
			if( !get_option( 'wp_twitter_list' ) ) 
			{
				$wp_twitter_list['account'] 		= $this->wp_twitter_list_default['account'];
				$wp_twitter_list['tweet_count'] 	= $this->wp_twitter_list_default['tweet_count'];
				$wp_twitter_list['delay_effect'] 	= $this->wp_twitter_list_default['delay_effect'];
				$wp_twitter_list['text_loading'] 	= $this->wp_twitter_list_default['text_loading'];
				$wp_twitter_list['tweet_enable'] 	= $this->wp_twitter_list_default['tweet_enable'];
				
				$wp_twitter_list['div_ul'] 			= $this->wp_twitter_list_default['div_ul'];
				$wp_twitter_list['ul_id'] 			= $this->wp_twitter_list_default['ul_id'];
				$wp_twitter_list['class_li'] 		= $this->wp_twitter_list_default['class_li'];
				$wp_twitter_list['class_text_li'] 	= $this->wp_twitter_list_default['class_text_li'];
				
				add_option('wp_twitter_list', $wp_twitter_list);
			}
			
		}
	
		
		function install()
		{
			add_options_page('WP Twitter List','WP Twitter List','manage_options','wp-twitter-list',array( &$this, 'config' ));
		}
		
		
		function config()
		{
			$wp_twitter_list = get_option('wp_twitter_list');
			include(dirname(__FILE__).'/../templates/option-page.php');
		}
		
		
		function update_settings()
		{
			
			if(isset($_POST['update']))
			{
				$wp_twitter_list['account'] 		= $this->int_is_null(htmlspecialchars($_POST['account']),$this->wp_twitter_list_default['account']);
				$wp_twitter_list['tweet_count'] 	= $this->int_is_null(htmlspecialchars($_POST['tweet_count']),$this->wp_twitter_list_default['tweet_count']);
				$wp_twitter_list['delay_effect'] 	= $this->int_is_null(htmlspecialchars($_POST['delay_effect']),$this->wp_twitter_list_default['delay_effect']);
				$wp_twitter_list['text_loading'] 	= $this->int_is_null(htmlspecialchars($_POST['text_loading']),$this->wp_twitter_list_default['text_loading']);
				$wp_twitter_list['tweet_enable'] 	= $this->int_is_null(htmlspecialchars($_POST['tweet_enable']),$this->wp_twitter_list_default['tweet_enable']);
				
				$wp_twitter_list['div_ul'] 			= $this->int_is_null(htmlspecialchars($_POST['div_ul']),$this->wp_twitter_list_default['div_ul']);
				$wp_twitter_list['ul_id'] 			= $this->int_is_null(htmlspecialchars($_POST['ul_id']),$this->wp_twitter_list_default['ul_id']);
				$wp_twitter_list['class_li'] 		= $this->int_is_null(htmlspecialchars($_POST['class_li']),$this->wp_twitter_list_default['class_li']);
				$wp_twitter_list['class_text_li'] 	= $this->int_is_null(htmlspecialchars($_POST['class_text_li']),$this->wp_twitter_list_default['class_text_li']);
				
				update_option('wp_twitter_list', $wp_twitter_list);
			}
			
			if(isset($_POST['restore']))
			{
				$wp_twitter_list['account'] 		= $this->wp_twitter_list_default['account'];
				$wp_twitter_list['tweet_count'] 	= $this->wp_twitter_list_default['tweet_count'];
				$wp_twitter_list['delay_effect'] 	= $this->wp_twitter_list_default['delay_effect'];
				$wp_twitter_list['text_loading'] 	= $this->wp_twitter_list_default['text_loading'];
				$wp_twitter_list['tweet_enable'] 	= $this->wp_twitter_list_default['tweet_enable'];
				
				$wp_twitter_list['div_ul'] 			= $this->wp_twitter_list_default['div_ul'];
				$wp_twitter_list['ul_id'] 			= $this->wp_twitter_list_default['ul_id'];
				$wp_twitter_list['class_li'] 		= $this->wp_twitter_list_default['class_li'];
				$wp_twitter_list['class_text_li'] 	= $this->wp_twitter_list_default['class_text_li'];
				
				update_option('wp_twitter_list', $wp_twitter_list);
			}
			
		}
		
		
		function int_is_null($p_int, $p_default="0")
		{
			$p_int = trim($p_int);
			if($p_int == null || $p_int == "")
				return $p_default;
			else
				return $p_int;
		}
		
	}
}
$wp_twitter_list_core = new wp_twitter_list_core;

function wp_tweet_list($atts) {
	$wp_twitter_list = get_option('wp_twitter_list');
	
	if ( $wp_twitter_list['tweet_enable'] == "yes" )
		include(dirname(__FILE__).'/../templates/header-page.php');
}
add_shortcode ('wp_tweet_list', 'wp_tweet_list');

function wp_tweet_load_scripts() {
	wp_enqueue_script('jquery');
}
add_action('wp_enqueue_scripts', 'wp_tweet_load_scripts');

?>
