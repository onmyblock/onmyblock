<div id='<?php echo $wp_twitter_list['div_ul'];?>'>
	<ul id='<?php echo $wp_twitter_list['ul_id'];?>'>
		<li id='tweet-loading' class='<?php echo $wp_twitter_list['class_li'];?>'>
			<span class="<?php echo $wp_twitter_list['class_text_li'];?>">
				<?php echo $wp_twitter_list['text_loading'];?>
			</span>
		</li>
		<?php
		$format='xml';
		$tweet=simplexml_load_file("http://api.twitter.com/1/statuses/user_timeline/{$wp_twitter_list['account']}.{$format}");
		for($i=0;$i<5;$i++)
		{
			$res = $tweet->status[$i]->text;
			?>
			<li class='<?php echo $wp_twitter_list['class_li'];?>' style='display:none'>
				<span class='<?php echo $wp_twitter_list['class_text_li'];?>'>
					<?php echo twitterify($res); ?>
				</span>
			</li>
			<?php
		}
		?>
   </ul>
</div>
<?php
function twitterify($ret) 
{
	$ret = preg_replace("#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t< ]*)#", "\\1<a href=\"\\2\" >\\2</a>", $ret);
	$ret = preg_replace("#(^|[\n ])((www|ftp)\.[^ \"\t\n\r< ]*)#", "\\1<a href=\"http://\\2\" >\\2</a>", $ret);
	$ret = preg_replace("/@(\w+)/", "<a href=\"http://www.twitter.com/\\1\" >@\\1</a>", $ret);
	$ret = preg_replace("/#(\w+)/", "<a href=\"http://search.twitter.com/search?q=\\1\" >#\\1</a>", $ret);
	return $ret;
}
?>
<script type="text/javascript">
jQuery(function () {
	jQuery('#<?php echo $wp_twitter_list['ul_id'];?> li').hide();
	InOut( jQuery('#<?php echo $wp_twitter_list['ul_id'];?> li:first'));
});
function InOut( elem )
{
	elem.delay()
		.fadeIn()
		.delay(<?php echo $wp_twitter_list['delay_effect'];?>)
		.fadeOut(
			function(){
				if(elem.next().length > 0)
				{
					InOut( elem.next() );
				}
				else
				{
					jQuery('#tweet-loading').remove();
					InOut( elem.siblings(':first'));
				}
           });
}
</script>
