<style>
.bb{
	font-weigth:bold;
	color:red;
}
</style>
<div class='wrap'>
	<form method='post' action=''>
		<div id='icon-options-general' class='icon32'>
			<br/>
		</div>
		<h2>WP Twitter List</h2>
		<div class='metabox-holder'>
			<div class='meta-box-sortables ui-sortable'>
				<div class='postbox'>
					<h3 class='hndle'>
						<span>Options</span>
					</h3>
					<div class='inside' style='display: block;'>
							<table>
								<tr>
									<td width=200>Username Twitter</td>
									<td>https://twitter.com/<input type='text' name='account' id='account' value='<?php echo $wp_twitter_list['account']; ?>' ></td>
								</tr>
								<tr>
									<td>Number of tweet to show</td>
									<td><input type='text' name='tweet_count' id='tweet_count' value='<?php echo $wp_twitter_list['tweet_count']; ?>' onkeyup="return(check_num(this,this.name))" ></td>
								</tr>
								<tr>
									<td>Text loading twitt</td>
									<td><input type='text' name='text_loading' id='text_loading' value='<?php echo $wp_twitter_list['text_loading']; ?>' ></td>
								</tr>
								<tr>
									<td>Delay effect</td>
									<td><input type='text' name='delay_effect' id='delay_effect' value='<?php echo $wp_twitter_list['delay_effect']; ?>' onkeyup="return(check_num(this,this.name))" >
									<small>millisecond</small>
									</td> 
								</tr>
								<tr>
									<td>Enable Plugin</td>
									<td>
									<?php
									$yes_selected = " selected = 'selected' ";
									$no_selected = " selected = 'selected' ";
									switch( $wp_twitter_list['tweet_enable'] )
									{
										case 'yes':
											$no_selected = "";
											break;
										
										case 'no':
											$yes_selected = "";
											break;											
									}
									?>
									<select name='tweet_enable' id='tweet_enable'>
										<option value='yes' <?php echo $yes_selected;?> >yes</option>
										<option value='no' <?php echo $no_selected;?>>no</option>
									</select>
									</td>
								</tr>
							</table>
					</div>
				</div><!-- postbox -->
				<div class='postbox'>
					<h3 class='hndle'>
						<span>Advanced Options*</span>
					</h3>
					<div class='inside' style='display: block;'>
							<table>
								<tr>
									<td width=200>ID of Div container</td>
									<td><input type='text' name='div_ul' id='div_ul' value='<?php echo $wp_twitter_list['div_ul']; ?>' ></td>
								</tr>
								<tr>
									<td>ID of UL</td>
									<td><input type='text' name='ul_id' id='ul_id' value='<?php echo $wp_twitter_list['ul_id']; ?>' ></td>
								</tr>
								<tr>
									<td>Class of LI</td>
									<td><input type='text' name='class_li' id='class_li' value='<?php echo $wp_twitter_list['class_li']; ?>' ></td>
								</tr>
								<tr>
									<td>Class Text of LI</td>
									<td><input type='text' name='class_text_li' id='class_text_li' value='<?php echo $wp_twitter_list['class_text_li']; ?>' ></td>
								</tr>
							</table>
					</div>
				</div><!-- postbox -->

				<small><p>* this option required HTML knowledge.</p></small>
				
			</div>
			<br />
			<h2>Shortcode</h2>
			<p>Inside post and page write this: <code>
			[wp_tweet_list]
			</code></p>
			<p>Inside index.php or other page .php, write this:
			<code>
			&lt;?php echo do_shortcode("[wp_tweet_list]"); ?&gt;
			</code></p>
			<br />
			<h2>Structure of generated code</h2>
<pre>
&lt;div id='<span class='bb'>twitt-box</span>'&gt;
   &lt;ul id='<span class='bb'>twitt-ul</span>'&gt;
      &lt;li class='<span class='bb'>twitt-li</span>'&gt;
         &lt;span class='<span class='bb'>twitt-li-text</span>'&gt;Load last news...&lt;/span&gt;
      &lt;/li&gt;
   &lt;/u&gt;
&lt;/div&gt;
</pre>
			<br/><br/>
			<input type='submit' class='button' id='restore' name='restore' value='Restore Default' onclick="return confirm('Are you sure to restore default options?')"/>
			<input type='submit' class='button' id='update' name='update' value='Update' />
			<br /><br />
		</div>
	</form>
</div>
<script> 
function check_num(campo,tipo){
	var lunghezza = campo.value.length;
	var str = campo.value;
	var strfinal="";
 	var i=0;
 	for( i=0 ; i < lunghezza ; i++ )
 	{
 		var codascii=str.charCodeAt(i);
	 	if (codascii>=48 && codascii<=57)
	 	{
	  		strfinal = strfinal + String.fromCharCode(codascii);
	 	}
	 	else
	 	{
	  		strfinal = strfinal;
	 	}
	}
	campo.value = strfinal;
	return true;
}
</script>
	