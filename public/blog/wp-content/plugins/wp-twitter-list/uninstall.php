<?php
/*
Part of Plugin: Wp Twitter List
Uninstall procedure

*/

// Make sure that we are uninstalling
if ( !defined('WP_UNINSTALL_PLUGIN') ) {
    exit();
}

// Leave no trail
delete_option('wp_twitter_list');

?>