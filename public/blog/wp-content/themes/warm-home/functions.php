<?php

if(!isset($content_width)){
    $content_width = 500;
}

// Set Widgets
function warmHome_widgets_init() {    

	register_sidebar(array(
	    'id' => 'sidebar_1',
		'name' => __( 'sidebar_1', 'warm-home' ),
		'before_widget' => '<li>', 
		'after_widget' => '</li>',
		'before_title' => '<h2>', 
		'after_title' => '</h2>' 
	));
 
}

add_action( 'widgets_init', 'warmHome_widgets_init');



function warmHome_setup() {
// Set languages
	load_theme_textdomain( 'warm-home', TEMPLATEPATH . '/languages' );
    
	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );
	
	add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );
    
    	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );
	
	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'warm-home' ),
	) );

}	


function warmHome_new_excerpt_length($length) {
return 20;
}
add_filter('excerpt_length', 'warmHome_new_excerpt_length',999);


add_filter( 'use_default_gallery_style', '__return_false' );

function warmHome_remove_gallery_css( $css ) {
	return preg_replace( "#<style type='text/css'>(.*?)</style>#s", '', $css );
}
// Backwards compatibility with WordPress 3.0.
if ( version_compare( $GLOBALS['wp_version'], '3.1', '<' ) )
	add_filter( 'gallery_style', 'warmHome_remove_gallery_css' );
	



if ( ! function_exists( 'warmHome_posted_in' ) ) :
/**
 * Prints HTML with meta information for the current post (category, tags and permalink).
 */
function warmHome_posted_in() {
	// Retrieves tag list of current post, separated by commas.
	$tag_list = get_the_tag_list( '', ', ' );
	if ( $tag_list ) {
		$posted_in = __( 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'warm-home' );
	} elseif ( is_object_in_taxonomy( get_post_type(), 'category' ) ) {
		$posted_in = __( 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'warm-home' );
	} else {
		$posted_in = __( 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'warm-home' );
	}
	// Prints the string, replacing the placeholders.
	printf(
		$posted_in,
		get_the_category_list( ', ' ),
		$tag_list,
		get_permalink(),
		the_title_attribute( 'echo=0' )
	);
}
endif;	
	
add_action( 'after_setup_theme', 'warmHome_setup'); 



function warmHome_the_thumbnail() {   
 
global $post;   

if ( has_post_thumbnail() ) {   
echo '<a href="'.get_permalink().'" title="Read More">';   
the_post_thumbnail('thumbnail');   
echo '</a>';   
} else {   

$content = $post->post_content;   
preg_match_all('/<img.*?(?: |\\t|\\r|\\n)?src=[\'"]?(.+?)[\'"]?(?:(?: |\\t|\\r|\\n)+.*?)?>/sim', $content, $strResult, PREG_PATTERN_ORDER);   
$n = count($strResult[1]);  
 
if($n > 0){    
            echo '<a href="'.get_permalink().'" title="Read More"><img src="'.$strResult[1][0].'" alt="thumbnail" /></a>';   
        }else {      
            echo '<a href="'.get_permalink().'" title="Read More"><img src="'.get_bloginfo('template_url').'/images/thumbnail.png" alt="thumbnail" /></a>';  
       }   
  }    
}  
  
?>