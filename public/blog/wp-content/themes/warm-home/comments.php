<div id="comments">
<?php if ( post_password_required() ) : ?>
				<p class="nopassword"><?php _e( 'This post is password protected. Enter the password to view any comments.','warm-home' ); ?></p>
			</div><!-- #comments -->
<?php
		
		return;
	endif;
?>

<?php
	// You can start editing here -- including this comment!
?>

<?php if ( have_comments() ) : ?>
			<h4 id="widgettitle "><?php
			printf( _n( 'One Response to %2$s', '%1$s Responses to %2$s', get_comments_number(), 'warm-home' ),
			number_format_i18n( get_comments_number() ), '<em>' . get_the_title() . '</em>' );
			?></h4>

<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<div class="comments-navigation">
				<?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Older Comments', 'warm-home' ) ); ?>
				<?php next_comments_link( __( 'Newer Comments <span class="meta-nav">&rarr;</span>', 'warm-home' ) ); ?></div>
<!-- .navigation -->
<?php endif; // check for comment navigation ?>
 
<ol class="commentlist">
 <?php wp_list_comments();?>
</ol>
<?php else : 
	if ( ! comments_open()  && is_single()  ) :
?>
	<p class="nocomments"><?php _e( 'Comments are closed.', 'warm-home' ); ?></p>
<?php endif; // end ! comments_open() ?>

<?php endif; // end have_comments() ?>

<?php comment_form(); ?>

</div><!-- #comments -->
