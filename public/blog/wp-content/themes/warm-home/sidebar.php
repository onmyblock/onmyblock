<div id="sidebar_1">
  <ul >
    <?php
	  
		if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar_1') ) :
	 ?>
    <li class="widget_1">
      <h2>
        <?php _e('categories','warm-home');?>
      </h2>
      <ul>
        <?php wp_list_categories('sort_colum=name&optioncount=1&hierarchical=o');?>
      </ul>
    </li>
    <li>
      <h2>
        <?php _e('Archives','warm-home');?>
      </h2>
      <ul>
        <?php wp_get_archives('type=monthly');?>
      </ul>
    </li>
    <li>
      <h2>
        <?php _e('search','warm-home');?>
      </h2>
      <?php get_search_form() ;?>
    </li>
    <?php wp_list_bookmarks();?>
    <li>
      <h2>
        <?php _e('Meta','warm-home');?>
      </h2>
      <ul>
        <?php wp_register();?>
        <li>
          <?php wp_loginout();?>
        </li>
        <?php wp_meta();?>
      </ul>
    </li>
    <li id="calendar">
      <h2>
        <?php _e('calendar','warm-home');?>
      </h2>
      <?php get_calendar();?>
    </li>
    <?php endif; ?>
  </ul>
</div>
<!--sidebar_1 end-->

<div id="sidebar_2">
  <div id="advertarea">
    <div class="advert"><img src="<?php echo get_template_directory_uri('template_directory'); ?>/images/pic01.jpg" alt="advert1" /></div>
    <div class="advert"><img src="<?php echo get_template_directory_uri('template_directory'); ?>/images/pic02.jpg" alt="advert2" /></div>
  </div>
</div>
