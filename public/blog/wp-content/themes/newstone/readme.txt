
Theme Name: NewStone
Theme URI: http://www.newstonetheme.com
Author: Terry Lin
Author URI: http://www.pcdiy.com
Version: 1.0 
Last Updated: June, 16, 2011

Thank you for choosing Newstone theme! This is my first design work for a theme of WordPress 3.x , so I spent very lone time to design this theme, I hope you can like it.

If you have any question in using Newstone theme, you could open a qustion thread on my forum ( http://forum.pcdiy.com ), I will try my best to help you.