<?php

	$rsUser = mysqli_fetch_row($user->db->query("select user_admin.status from user_admin where user_id={$user->id}"));
	
	$status = $rsUser[0];
	
	if($status==99)
	{
		$html = '<li><label for="email" class="required">No Places</label><input type="checkbox" id="statusUser" name="checkNoPlaces" value="1"></li>';

		$htmlNav = '<li class="active ac" data="global" ><a href="#">Places</a></li>
					<li class="ac" data="graphics" ><a href="#">Graphics</a></li>
					<li class="ac" data="users" ><a href="#">Users</a></li>
					<li class="ac" data="adminusers" ><a href="#">Admin Users</a></li>
					<li class="ac" data="noPlaces" ><a href="#">Places no </a></li>';
					
		/*USD*/
		$placesUSD = mysqli_fetch_all($user->db->query("select count(id) from places 
														where `status` = 1 and (city like '%Mission Beach%' or neighborhood like '%Mission Beach%' 
														or city like '%Pacific Beach%' or neighborhood like '%Pacific Beach%' 
														or city like '%Morena%' or neighborhood like '%Morena%' 
														or city like '%Linda Vista%' or neighborhood like '%Linda Vista%'
														or city like '%Ocean Beach%' or neighborhood like '%Ocean Beach%'
														or city like '%Point Loma%' or neighborhood like '%Point Loma%'
														or city like '%La Jolla%' or neighborhood like '%La Jolla%'
														or city like '%Mission Valley%' or neighborhood like '%Mission Valley%'
														or city like '%University Heights%' or neighborhood like '%University Heights%'
														or city like '%Hillcrest%' or neighborhood like '%Hillcrest%'
														or city like '%North Park%' or neighborhood like '%North Park%'
														or city like '%Downtown%' or neighborhood like '%Downtown%')"));
		
		/*San Diego State University*/
		$placesSanDiego = mysqli_fetch_all($user->db->query("select count(id) from places 
															where city like '%College wes%' or neighborhood like '%College wes%' 
															or city like '%Baja Drive%' or neighborhood like '%Baja Drive%' 
															or city like '%SDSU%' or neighborhood like '%SDSU%' 
															or city like '%College area%' or neighborhood like '%College area%'"));	
		
		/*Chapman University*/
		$placesChapman = mysqli_fetch_all($user->db->query("select count(id) from places 
															where city like '%Old Towne Orange%' or neighborhood like '%Old Towne Orange%' 
															or city like '%Chapman and Tus%' or neighborhood like '%Chapman and Tus%' 
															or city like '%Costa Mesa%' or neighborhood like '%Costa Mesa%' 
															or city like '%Villa Park%' or neighborhood like '%Villa Park%' 
															or city like '%Glassell%' or neighborhood like '%Glassell%'
															or city like '%Collins%' or neighborhood like '%Collins%'"));	
															
		/*University of Southern California*/
		$placesSCalifornia = mysqli_fetch_all($user->db->query("select count(id) from places 
															where city like '%Menlo%' or neighborhood like '%Menlo%' 
															or city like '%Irvine Are%' or neighborhood like '%Irvine Are%' 
															or city like '%Ellendale%' or neighborhood like '%Ellendale%' 
															or city like '%USC%' or neighborhood like '%USC%' 
															or city like '%University Villag%' or neighborhood like '%University Villag%'
															or city like '%Adams & Ellendale%' or neighborhood like '%Adams & Ellendale%'
															or city like '%W 29th St%' or neighborhood like '%W 29th St%'"));	
															
		/*University of California at Los Angeles*/
		$placesCaliforniaAngeles = mysqli_fetch_all($user->db->query("select count(id) from places 
																		where city like '%Westwood Village%' or neighborhood like '%Westwood Village%' 
																		or city like '%Beverly HIlls%' or neighborhood like '%Beverly HIlls%' 
																		or city like '%Westwood Hills%' or neighborhood like '%Westwood Hills%' 
																		or city like '%UCLA%' or neighborhood like '%UCLA%' 
																		or city like '%Greenfield%' or neighborhood like '%Greenfield%'"));															
																		
		/*Cal Poly San Luis Obispo*/
		$placesSanLuisObispo = mysqli_fetch_all($user->db->query("select count(id) from places 
																where city like '%San Luis Obispo%' or neighborhood like '%San Luis Obispo%' 
																or city like '%Downtown SLO%' or neighborhood like '%Downtown SLO%' 
																or city like '%Grover Beach%' or neighborhood like '%Grover Beach%' 
																or city like '%UCLA%' or neighborhood like '%UCLA%' 
																or city like '%Pismo Beach%' or neighborhood like '%Pismo Beach%'"));

		/*Loyola Marymount University */
		$placesLoyolaMarymountUniversity = mysqli_fetch_all($user->db->query("select count(id) from places 
																				where city like '%Marina Del Ray%' or neighborhood like '%Marina Del Ray%' 
																				or city like '%La Tijera%' or neighborhood like '%La Tijera%' 
																				or city like '%Manchester%' or neighborhood like '%Manchester%' 
																				or city like '%Westchester%' or neighborhood like '%Westchester%' 
																				or city like '%Culver City%' or neighborhood like '%Culver City%'"));
																	
		/*UCSB*/
		$placesUCSB = mysqli_fetch_all($user->db->query("select count(id) from places 
														where city like '%Isla Vista%' or neighborhood like '%Isla Vista%' 
														or city like '%Goleta%' or neighborhood like '%Goleta%' 
														or city like '%Fortuna%' or neighborhood like '%Fortuna%' 
														or city like '%Goleta Beach%' or neighborhood like '%Goleta Beach%'"));
														
		/*Santa Clara*/
		$placesSantaClara = mysqli_fetch_all($user->db->query("select count(id) from places 
																where city like '%Santa Clara%' or neighborhood like '%Santa Clara%' 
																or city like '%San Jose%' or neighborhood like '%San Jose%' 
																or city like '%Milpitas%' or neighborhood like '%Milpitas%' "));
																
		/*Stanford */
		$placesStanford = mysqli_fetch_all($user->db->query("select count(id) from places 
															where city like '%Stanford%' or neighborhood like '%Stanford%' 
															or city like '%Palo Alto%' or neighborhood like '%Palo Alto%' 
															or city like '%Menlo Park%' or neighborhood like '%Menlo Park%' 
															or city like '%Mountain View%' or neighborhood like '%Mountain View%' 
															or city like '%Redwood City%' or neighborhood like '%Redwood City%' "));
															
		/*UCSF*/
		$placesUCSF = mysqli_fetch_all($user->db->query("select count(id) from places 
															where city like '%Richmond%' or neighborhood like '%Richmond%' 
															or city like '%Inner sunset%' or neighborhood like '%Inner sunset%' 
															or city like '%UCSF%' or neighborhood like '%UCSF%' 
															or city like '%Parkside%' or neighborhood like '%Parkside%'  "));
															
		$cadenaData  = '{ "y":"San Diego State University","a": '.$placesSanDiego[0][0].'},';
		$cadenaData .= '{ "y":"USD","a": '.$placesUSD[0][0].'},';
		$cadenaData .= '{ "y":"Chapman University","a": '.$placesChapman[0][0].'},';
		$cadenaData .= '{ "y":"University of Southern California","a": '.$placesSCalifornia[0][0].'},';
		$cadenaData .= '{ "y":"University of California at Los Angeles","a": '.$placesCaliforniaAngeles[0][0].'},';
		$cadenaData .= '{ "y":"Cal Poly San Luis Obispo","a": '.$placesSanLuisObispo[0][0].'},';
		$cadenaData .= '{ "y":"Loyola Marymount University","a": '.$placesLoyolaMarymountUniversity[0][0].'},';
		$cadenaData .= '{ "y":"UCSB","a": '.$placesUCSB[0][0].'},';
		$cadenaData .= '{ "y":"Santa Clara","a": '.$placesSantaClara[0][0].'},';
		$cadenaData .= '{ "y":"Stanford","a": '.$placesStanford[0][0].'},';
		$cadenaData .= '{ "y":"UCSF","a": '.$placesUCSF[0][0].'}';
		
		$jsonData = '[ '.$cadenaData.' ]';
		
		$smarty->assign('data', $jsonData);
		$smarty->assign('htmlAcces', $html);
		$smarty->assign('htmlNav', $htmlNav);
		
		$smarty->display('pages/report.tpl');
	}
	elseif($status==1)
	{
		$htmlNav = '<li class="active ac" data="noPlaces" ><a href="#">Places no </a></li>';
		
		$smarty->assign('htmlNav', $htmlNav);
		
		$smarty->display('pages/report.tpl');
	}
	
	
