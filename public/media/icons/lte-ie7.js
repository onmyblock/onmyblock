/* Use this script if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'omb_icons\'">' + entity + '</span>' + html;
	}
	var icons = {
			'omb-icon-chat' : '&#x22;',
			'omb-icon-bookmark' : '&#x21;',
			'omb-icon-apple' : '&#x23;',
			'omb-icon-loading' : '&#x27;',
			'omb-icon-bookmark-2' : '&#x29;',
			'omb-icon-bookmark-3' : '&#x28;',
			'omb-icon-eye' : '&#x2a;',
			'omb-icon-location' : '&#x25;',
			'omb-icon-heart' : '&#x24;',
			'omb-icon-thumbs-up' : '&#x26;',
			'omb-icon-backspace' : '&#x2b;',
			'omb-icon-facebook' : '&#x2c;',
			'omb-icon-link' : '&#x2d;',
			'omb-icon-star' : '&#x2e;',
			'omb-icon-earth' : '&#x2f;',
			'omb-icon-search' : '&#x30;',
			'omb-icon-untitled' : '&#x32;',
			'omb-icon-calendar' : '&#x31;',
			'omb-icon-checkbox-unchecked' : '&#x33;',
			'omb-icon-checkbox' : '&#x34;',
			'omb-icon-contact' : '&#x35;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; i < els.length; i += 1) {
		el = els[i];
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/omb-icon-[^\s'"]+/);
		if (c) {
			addIcon(el, icons[c[0]]);
		}
	}
};