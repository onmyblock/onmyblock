$(function() {

	$("#scroll_top").hide();
	$("#back_browse").hide();
	$('#scroll_top').click(function() {
		
		$('body,html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});
	window.onbeforeunload = function() {
		$.cookie("browseLoadCount", 0);
		$.cookie("currentPricingOptions", '');
		$.cookie("currentRoomOptions", '');
		$.cookie("omb_browse_location", '');
	};



	

	nIntervId = setInterval(function() {
		reloadItems();
	}, 1000);
	
	
	/*$('#btnSearch').click(function(){
		//alert('aaa');
		$('#browse_container').isotope('fnSearch').isotope({
			sortBy: '*'
		});
		//fnSearch();
		//$('#browse_container').isotope('fnSearch');
	});*/
	
	/*function fnSearch()
	{
		
		var dataBrowse = $("#searchTextField").val();
			
			$.ajax({
				type: "POST",
				url: "searchPlace",
				data: { searchData: dataBrowse }				
			}).done(function(data) {
					
					$('.browse_item').remove();
					
					$('#browse_container').append(data);
					
					//response = $.trim(data);
					
					$('#browse_container').append($newData).isotope('appended', $newData);
					
					if(response=='')
					{
						Sexy.info('<p>Sorry, we came up with no results for your search. Try a different search!'); 
					}
					
					//$('#browse_container').css('height','auto');  
					 
					$("#searchTextField").val("");
			}); 
			
			//$.cookie("multiload_blocker", "block"); 
			
			$.cookie("omb_set","browse_search");
			
			$.cookie("omb_browse_location","browse_search");
	}*/
		
	function reloadItems() {
		if ($(this).scrollTop() > 1000) {
			$('#scroll_top').fadeIn();
		} else {
			$('#scroll_top').fadeOut();
		}
		
		if($.cookie("omb_browse_location")!='browse_search')
		{
			$.cookie("omb_set",null);
			$.cookie("multiload_blocker", null);
		}
			
		if($.cookie("omb_browse_location")=='friends')
		{
			if($.cookie("omb_session")!='loggedin')
			{
				openPopup();
				
				clearInterval(nIntervId);
			}
			
			if($.cookie("browseLoadCount")>0)
			{
				$.cookie("multiload_blocker", "block"); 
			}
		}
		
		
		
		if (($(document).height() - 1500) < ($(window).height() + $(document).scrollTop()) || $(document).height() < 1500) {
			if ($.cookie("multiload_blocker") != 'block' ) {
				var $count = $.cookie("browseLoadCount");
				$.cookie("multiload_blocker", 'block');
				$.ajax({
					type: "POST",
					url: "browse_items",
					data: {
						count: $count,
						price: $.cookie("currentPricingOptions"),
						rooms: $.cookie("currentRoomOptions")
					}
				}).done(function(data) {
					if ($.trim(data) == 1) {} else {
						$newData = $(data);
						$('#browse_container').append($newData).isotope('appended', $newData);
					}
				});
				$count++;
				$.cookie("browseLoadCount", $count);
				$.cookie("multiload_blocker", null);
			}
		}
	}
	var $container = $('#browse_container');
	$body = $('body'), colW = 316, columns = null;
	$container.isotope({
		resizable: true,
		masonry: {
			columnWidth: colW
		},
		animationEngine: 'best-available',
		itemSelector: '.browse_item',
		filter: '*'
	});
	$(window).smartresize(function() {
		var currentColumns = Math.floor(($body.width() - 10) / colW);
		if (currentColumns !== columns) {
			columns = currentColumns;
			$container.width(columns * colW).isotope('reLayout');
		}
	}).smartresize();
	var $currentPricingOptions = $.cookie("currentPricingOptions");
	var $optionSets = $('#sorting_price .browse_sorting_price'),
		$optionLinks = $optionSets.find('a');
	$optionLinks.click(function() {
		$('.browse_runout').remove();
		var $this = $(this);
		$.cookie("currentPricingOptions", $this.attr('data-option-value'));
		$roomOptions = $.cookie("currentRoomOptions");
		var $optionSet = $this.parents('.browse_sorting_price');
		if ($this.hasClass('selected')) {
			$this.removeClass('selected');
			$.cookie("currentPricingOptions", '');
			var options = {},
				key = $optionSet.attr('data-option-key'),
				value = $.cookie("currentRoomOptions");
		} else {
			$optionSet.find('.selected').removeClass('selected');
			$this.addClass('selected');
			if ($roomOptions.indexOf(".") >= 0) {
				var options = {},
					key = $optionSet.attr('data-option-key'),
					value = ($.cookie("currentRoomOptions") + ':' + $.cookie("currentPricingOptions"));
			} else {
				var options = {},
					key = $optionSet.attr('data-option-key'),
					value = $.cookie("currentPricingOptions");
			}
		}
		value = value === 'false' ? false : value;
		options[key] = value;
		if (key === 'layoutMode' && typeof changeLayoutMode === 'function') {
			changeLayoutMode($this, options)
		} else {
			$container.isotope(options);
		}
		return false;
	});
	var $optionSets = $('#sorting .browse_sorting'),
		$optionLinks = $optionSets.find('a');
	$optionLinks.click(function() {
		$('.browse_runout').remove();
		var $this = $(this);
		var $currentRoomOptions = $.cookie("currentRoomOptions");
		var $optionSet = $this.parents('.browse_sorting');
		$.cookie("currentRoomOptions", $this.attr('data-option-value'));
		$pricingOptions = $.cookie("currentPricingOptions");
		if ($this.hasClass('selected')) {
			$this.removeClass('selected');
			$.cookie("currentRoomOptions", '');
			var options = {},
				key = $optionSet.attr('data-option-key'),
				value = $.cookie("currentPricingOptions");
		} else {
			$optionSet.find('.selected').removeClass('selected');
			$this.addClass('selected');
			if ($pricingOptions.indexOf(".") >= 0) {
				var options = {},
					key = $optionSet.attr('data-option-key'),
					value = ($.cookie("currentRoomOptions") + ':' + $.cookie("currentPricingOptions"));
			} else {
				var options = {},
					key = $optionSet.attr('data-option-key'),
					value = $.cookie("currentRoomOptions");
			}
		}
		value = value === 'false' ? false : value;
		options[key] = value;
		if (key === 'layoutMode' && typeof changeLayoutMode === 'function') {
			changeLayoutMode($this, options)
		} else {
			$container.isotope(options);
		}
		return false;
	});
	var $optionSets = $('#options .browse_nav'),
		$optionLinks = $optionSets.find('a');
	$optionLinks.click(function() {
		var $this = $(this);
		$.cookie("omb_browse_location", $this.attr('data-option-value'));
		$.cookie("browseLoadCount", 0);
		if ($this.hasClass('selected')) {
			return false;
		}
		$.cookie("currentPricingOptions", '');
		$.cookie("currentRoomOptions", '');
		$('.browse_sorting').find('.selected').removeClass('selected');
		$('.browse_sorting_price').find('.selected').removeClass('selected');
		var $optionSet = $this.parents('.browse_nav');
		$optionSet.find('.selected').removeClass('selected');
		$("#floatbar #options .browse_nav").find('.selected').removeClass('selected');
		$("#floatbar #options .browse_nav").find($this.attr('loc')).addClass('selected');
		$this.addClass('selected');
		$('.browse_item').remove();
		$('#browse_container').isotope('reloadItems').isotope({
			sortBy: '*'
		}); 
		
		return false;
	});
	
	$("#btnSearch").click(function() {
		$.cookie("omb_browse_location","browse_search");
		$.cookie("browseLoadCount", 0);

		$.cookie("currentPricingOptions", '');
		$.cookie("currentRoomOptions", '');
		
		$.cookie("val_search",$("#searchTextField").val());
		$('.browse_item').remove();
		$('#browse_container').isotope('reloadItems').isotope({
			sortBy: '*'
		}); 
		
		return false;
	});
	
	$("#searchTextField").keyup(function(event){
		if(event.keyCode == 13){
			$.cookie("omb_browse_location","browse_search");
			$.cookie("browseLoadCount", 0);

			$.cookie("currentPricingOptions", '');
			$.cookie("currentRoomOptions", '');
			
			$.cookie("val_search",$("#searchTextField").val());
			$('.browse_item').remove();
			$('#browse_container').isotope('reloadItems').isotope({
				sortBy: '*'
			}); 
			
			return false;
		}
	});
	
	$('#optionsf').click(function() {
		
		/* var $this = $(this);
		$.cookie("omb_browse_location", $this.attr('data-option-value'));
		$.cookie("browseLoadCount", 0);
		if ($this.hasClass('selected')) {
			return false;
		}
		$.cookie("currentPricingOptions", '');
		$.cookie("currentRoomOptions", '');
		$('.browse_sorting').find('.selected').removeClass('selected');
		$('.browse_sorting_price').find('.selected').removeClass('selected');
		var $optionSet = $this.parents('.browse_nav');
		$optionSet.find('.selected').removeClass('selected');
		$("#floatbar #options .browse_nav").find('.selected').removeClass('selected');
		$("#floatbar #options .browse_nav").find($this.attr('loc')).addClass('selected');
		$this.addClass('selected');
		$('.browse_item').remove();
		$('#browse_container').isotope('reloadItems').isotope({
			sortBy: '*'
		});
		
		return false; */
	});
});

$(function() {
	$('.browse_item').live('click', function() {
		$('.browse_popup_shadow').remove();
		$('.browse_popup').remove();
		var place_id = $(this).attr("place_id");
		if(place_id!=0)
		{
			$('body').append("<div class='browse_popup_shadow'></div><div id='popup_loading_box'><span>loading...</span></div><iframe class='browse_popup' src='browse_popup?id=" + place_id + "' scrolling='no' allowTransparency='true' onload=" + '"' + "this.style.visibility='visible';$('#back_browse').show();" + '"' + "></iframe>");
			$('#popup_loading_box').hide();
			$('.browse_popup_shadow').hide();
			$('#popup_loading_box').fadeIn("slow");
			$('.browse_popup_shadow').fadeIn("slow");
		}
	});
	$('#back_browse').live('click', function() {
		$('#back_browse').hide();
		$('#popup_loading_box').remove();
		$('.browse_popup_shadow').hide();
		$('.browse_popup').slideToggle("normal");
		return false;
	});
	$('.browse_popup_shadow').live('click', function() {
		$('#back_browse').hide();
		$('#popup_loading_box').remove();
		$('.browse_popup_shadow').hide();
		$('.browse_popup').slideToggle("normal");
		return false;
	});
	$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			$('#back_browse').hide();
			$('#popup_loading_box').remove();
			$('.browse_popup_shadow').hide();
			$('.browse_popup').slideToggle("normal");
			return false;
		}
	});
});
$(function() {
	$('#close_browse_sidebar').click(function() {
		if ($(this).hasClass("closed")) {
			$('.browse_sorting_container').fadeIn(300);
			$(this).val(">");
			$(this).removeClass("closed");
		} else {
			$('.browse_sorting_container').fadeOut(300);
			$(this).val("<");
			$(this).addClass("closed");
		}
	});
	
	$(".browse_fav").live('click', function() {
		var $fav_button = $(this);
		$.ajax({
			type: "POST",
			url: "action",
			data: {
				ajax_action: "fav",
				user_id: $fav_button.parent().attr("user_id"),
				place_id: $fav_button.parent().attr("place_id")
			}
		}).done(function(response) {
			if ($.trim(response) == "1") {
				$fav_button.css("backgroundPosition", "5px 1px");
			} else {
				$fav_button.css("backgroundPosition", "5px -27px");
			}
		});
		e.stopPropogation();
	});

	function notificationItem(floating)
	{
		//alert('asda');
		$.ajax({
			type: "POST",
			url: "svcNotification",
			data:	{
				type: "insert"
			}
		}).done(function(data) {
			if(data!=0)
			{
				if(floating==true)
				{
					$('#target-domm').css('display','block');
					//alert(data);
					notificationFloatingItem(data);
				}

				$('#target-dom').css('display','block');
				
				//$('#target-dom').addClass("animate");
				
				$("#numNotification").empty();
			
				$("#numNotification").text($.trim(data));
				
				var el = $("#target-dom"), newone = el.clone(true);
   
				el.before(newone);
				
				el.css('display','none');
				
				$("." + el.attr("class") + ":last").remove();
				

			}
		});
	}

	function notificationItemDown(floating)
	{
		//alert('asda');
		$.ajax({
			type: "POST",
			url: "svcNotification",
			data:	{
				type: "down"
			}
		}).done(function(data) {
			if(data>0)
			{
				if(floating==true)
				{
					$('#target-domm').css('display','block');
					//alert(data);
					notificationFloatingItem(data);
				}

				$('#target-dom').css('display','block');
				
				//$('#target-dom').addClass("animate");
				
				$("#numNotification").empty();
			
				$("#numNotification").text($.trim(data));
				
				var el = $("#target-dom"), newone = el.clone(true);
   
				el.before(newone);
				
				el.css('display','none');
				
				$("." + el.attr("class") + ":last").remove();

			}else{
				$('#target-domm').css('display','none');
				$('#target-dom').css('display','none');
			}
		});
	}
		
	function notificationFloatingItem(data)
	{
		//$('#target-domm').addClass("animate");
		
		$("#numNotificationm").empty();
	
		$("#numNotificationm").text($.trim(data));
		
		var el = $("#target-domm"), newone = el.clone(true);

		el.before(newone);
		
		el.css('display','none');
	}

	$(".myblock_tipsy").live('click', function() {
	
		var session = $.cookie('omb_session');
		
		var $fav_button = $(this);
		
		var place_id = $fav_button.parent().attr("place_id");
		var user_id = $fav_button.parent().attr("user_id");
		
		if(session=='loggedin')
		{
			
			$.ajax({
				type: "POST",
				url: "action",
				data: {
					ajax_action: "fav",
					user_id: $fav_button.parent().attr("user_id"),
					place_id: $fav_button.parent().attr("place_id")
				}
			}).done(function(response) {
				if ($.trim(response) == "1") {
					//insert
					//$fav_button.css("backgroundPosition", "5px 1px");
					$fav_button.parent().attr("id","Added to Favorites");
					var obj = {'background':'#68ab64','background':'url(https://onmyblock.com/media/images/icons/myblock_w.png) 4px 2px no-repeat #68ab64','opacity': '0.6'};
					$fav_button.css(obj);
					$fav_button.attr("data-animation","1");
			
				} else {
					//delete
					//$fav_button.css("backgroundPosition", "5px -27px");
					$fav_button.parent().attr("id","Add to Favorites");
					var obj = {'background':'#F3F3F3','background':'url(https://onmyblock.com/media/images/icons/myblock.png) 4px 2px no-repeat #F3F3F3','opacity': '1'};
					$fav_button.css(obj);
					$fav_button.attr("data-animation","0");
				}
			});

			
		}else{
			openPopup();
		}

		var data = $fav_button.attr('data-animation');
		
		var floating=true;

		if(data==0)
		{
			//alert('asda');
			notificationItem(floating);
		}

		if(data==1)
		{
			//alert('asda');
			notificationItemDown(floating);
		}

		e.stopPropogation();
		
	});
	
	$(".browse_like").live('click', function() {
		
		var $like_button = $(this);
		$.ajax({
			type: "POST",
			url: "action",
			data: {
				ajax_action: "like",
				user_id: $like_button.parent().attr("user_id"),
				place_id: $like_button.parent().attr("place_id")
			}
		}).done(function(response) {
			if ($.trim(response) == "1") {
				$like_button.css("backgroundPosition", "7px -23px");
			} else if ($.trim(response) == "0") {
				$like_button.css("backgroundPosition", "7px 5px");
			}
		});
		e.stopPropogation();
	
	});
	
	$(".browselike").live('click', function() {
		var session = $.cookie('omb_session');
		
		var $like_button = $(this);
			
		var place_id = $like_button.parent().attr("place_id");
		var user_id = $like_button.parent().attr("user_id");
		
		if(session=='loggedin')
		{
			$.ajax({
				type: "POST",
				url: "action",
				data: {
					ajax_action: "like",
					user_id: $like_button.parent().attr("user_id"),
					place_id: $like_button.parent().attr("place_id")
				}
			}).done(function(response) {
				if ($.trim(response) == "1") {
					//$like_button.css("background", "7px -23px");
					$like_button.parent().attr("id","Liked");
					var obj = {'background':'#d16394','background':'url(https://onmyblock.com/media/images/thumb_up_w.png) 9px 5px no-repeat #d16394','opacity': '0.6'};
					$like_button.css(obj);
					
					like = $("#p"+place_id).text();
					like=parseInt(like)+1;
					$("#p"+place_id).empty();
					$("#p"+place_id).text(like);
					$like_button.attr("data-animation","1");
					
					
				} else if ($.trim(response) == "0") {
					//$like_button.css("backgroundPosition", "7px 5px");
					$like_button.parent().attr("id","Like");
					var obj = {'background':'#F3F3F3','background':'url(https://onmyblock.com/media/images/thumb_up.png) 9px 5px no-repeat #F3F3F3','opacity': '1'};
					$like_button.css(obj);
					
					like = $("#p"+place_id).text();
					like=parseInt(like)-1;
					$("#p"+place_id).empty();
					$("#p"+place_id).text(like);
					$like_button.attr("data-animation","0");
					//$("#p"+place_id).fadeOut(complete: function(){ $("#p"+place_id).text(like);});
					//$("#p"+place_id).fadeIn($("#p"+place_id).text(like););
					//$("#p"+place_id).fadeIn(3000, function(){ $("#p"+place_id).text(like);});
				}
			});
		}else{
			openPopup();
		}
		
		e.stopPropogation();
	});
});

	
$(function() {
	//alert('asd');
	$('.tipsy').tipsy({gravity: 'sw', fade: true, title: 'id'});
}); 
