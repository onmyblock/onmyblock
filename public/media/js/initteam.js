 /**
 *  @description [Site OnMyBlock]
 *  @author [Ronald Mori G]
 *  @version [3.0]
 *  @copyright [Tecla Labs]
 *  
 */


/**S
 * [main description]
 * @type {Object}
 */
Main = (function() {    
  

    /**
     * [onReady Inicializando la experiencia del usuario ]
     * @return {[type]} [description]
     */
    onReady = function() {              
    
      $('#favorites').click(function(){

        $('#contfavorite').fadeIn('slow');
        $('#contfavorite').show();

        $('#favorites a').addClass('on');
        $('#profile a').removeClass('on');
        $('#places a').removeClass('on');


        $('#contprofile').hide();
        $('#contplaces').hide();    
        
      });

      $('#profile').click(function(){

        $('#contprofile').fadeIn('slow');
        $('#contprofile').show();

        $('#profile a').addClass('on');
        $('#favorites a').removeClass('on');
        $('#places a').removeClass('on');

        $('#contfavorite').hide();
        $('#contplaces').hide();


      });

      $('#places').click(function(){

        $('#contplaces').fadeIn('slow');
        $('#contplaces').show();

        $('#places a').addClass('on');
        $('#profile a').removeClass('on');
        $('#favorites a').removeClass('on');

        $('#contprofile').hide();
        $('#contfavorite').hide();


      });

    $('#contentlogin').mouseenter(function(){

      $('.despledat').fadeIn('slow');

      }).mouseleave(function(){

            $('.despledat').fadeOut('slow');

      });    
   
        

    /*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
    f[z]=function(){
    (a.s=a.s||[]).push(arguments)};var a=f[z]._={
    },q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
    f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
    0:+new Date};a.P=function(u){
    a.p[u]=new Date-a.p[0]};function s(){
    a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
    hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
    return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
    b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
    b.contentWindow[g].open()}catch(w){
    c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
    var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
    b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
    loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
    /* custom configuration goes here (www.olark.com/documentation) */
    olark.identify('7080-590-10-3591');/*]]>*/    
        

      // Additional JS functions here
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '561406290571363', // App ID
      channelUrl : 'https://onmyblock.com/', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });

    // Additional init code here

  };

  // Load the SDK asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));


    window.fbAsyncInit = function() {
  FB.init({
    appId      : '561406290571363', // App ID
    channelUrl : 'https://onmyblock.com/', // Channel File
    status     : true, // check login status
    cookie     : true, // enable cookies to allow the server to access the session
    xfbml      : true  // parse XFBML
  });

  // Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
  // for any auth related change, such as login, logout or session refresh. This means that
  // whenever someone who was previously logged out tries to log in again, the correct case below 
  // will be handled. 
  FB.Event.subscribe('auth.authResponseChange', function(response) {
    // Here we specify what we do with the response anytime this event occurs. 
    if (response.status === 'connected') {
      // The response object is returned with a status field that lets the app know the current
      // login status of the person. In this case, we're handling the situation where they 
      // have logged in to the app.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // In this case, the person is logged into Facebook, but not into the app, so we call
      // FB.login() to prompt them to do so. 
      // In real-life usage, you wouldn't want to immediately prompt someone to login 
      // like this, for two reasons:
      // (1) JavaScript created popup windows are blocked by most browsers unless they 
      // result from direct user interaction (such as a mouse click)
      // (2) it is a bad experience to be continually prompted to login upon page load.
      FB.login();
    } else {
      // In this case, the person is not logged into Facebook, so we call the login() 
      // function to prompt them to do so. Note that at this stage there is no indication
      // of whether they are logged into the app. If they aren't then they'll see the Login
      // dialog right after they log in to Facebook. 
      // The same caveats as above apply to the FB.login() call here.
      FB.login();
    }
  });
  };

  // Load the SDK asynchronously
  (function(d){
   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement('script'); js.id = id; js.async = true;
   js.src = "//connect.facebook.net/en_US/all.js";
   ref.parentNode.insertBefore(js, ref);
  }(document));

  // Here we run a very simple test of the Graph API after login is successful. 
  // This testAPI() function is only called in those cases. 
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      console.log('Good to see you, ' + response.name + '.');
    });
  }

}  
         


     /**
      * Public API
      */
    return {
        onReady : onReady
    }
 
})();

$( function() {
  $.vegas({
    src:'images/bg-image6.jpg'
  });  
});
 

/**
 * Execute OnReady
 */
Main.onReady();

