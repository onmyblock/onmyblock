$(document).ready(function() {/*
	$('.temp_popup').css("display", "none");
	$('.temp_popup_container').css("display", "none");
	
	var blocked = false;
	
	if($.cookie("omb_access")=='return')
	{
		$('.temp_popup').css("display", "block");
		$('.temp_popup_container').css("display", "block");
	} */
	
	/* $('.buttonFb').click(function() {
	
		if (!blocked) {
			
			var blocked = false;
			$('.temp_popup').css("display", "block");
			$('.temp_popup').hide();
			$('.temp_popup').fadeIn(500);
			$('.temp_popup_container').css("display", "block");
			$('.temp_popup_container').hide();
			$('.temp_popup_container').fadeIn(500);
			blocked = true;
		}
	}); */

	$('.logout').click(function(){
		fbLogout();
	});
	
	$('.sweet').click(function() {
		var session = $.cookie('omb_session');
		
		if(session=='loggedin')
		{
			if ($('.sweet').hasClass("sweetnd")) {
				$('.sweet').removeClass("sweetnd");
				$("#iconLike").remove();
				$("img.iconLike").remove();
				
				$(".spanLike").append('<img id="iconLike" class="iconLike" src="https://onmyblock.com/media/images/thumb-down-icon_32x32px.png" style="width: 20px;height: 20px;" />');
				$('.sweet p').text("Like");
				$(this).attr("data-animation","1");
				
				$.ajax({
					type: "POST",
					url: "action",
					data: {
						ajax_action: 'place_like',
						action: 'unlike',
						user_id: $(this).attr("data-user-id"),
						place_id: $(this).attr("data-place-id")
					}
				}).done(function(data) {
					var numLikes = $.trim(data);
					if (numLikes == 0) {
						$("#num_sweets").html("No Likes");
					} else if (numLikes == 1) {
						$("#num_sweets").html("1 Like");
					} else if (numLikes > 1) {
						$("#num_sweets").html(numLikes + " Likes");
					}
				});
			} else {
				$('.sweet').addClass("sweetnd");
				$("#iconLike").remove();
				$("img.iconLike").remove();
				
				$(".spanLike").append('<img id="iconLike" class="iconLike" src="https://onmyblock.com/media/images/thumb-down-icon_32x32w.png" style="width: 20px;height: 20px;" />');
				$('.sweet p').text("Liked");
				$(this).attr("data-animation","0");
				
				$.ajax({
					type: "POST",
					url: "action",
					data: {
						ajax_action: 'place_like',
						action: 'like',
						user_id: $(this).attr("data-user-id"),
						place_id: $(this).attr("data-place-id")
					}
				}).done(function(data) {
					var numLikes = $.trim(data);
					if (numLikes == 0) {
						$("#num_sweets").html("No Likes");
					} else if (numLikes == 1) {
						$("#num_sweets").html("1 Like");
					} else if (numLikes > 1) {
						$("#num_sweets").html(numLikes + " Likes");
					}
				});
			}
		}else{
			openPopup();
		}
	});
	$('.wishlist').click(function() {
		var session = $.cookie('omb_session');
		
		if(session=='loggedin')
		{
			if ($(this).hasClass("wishlisted")) {
				$("#myblock").remove();
				$(this).find("span").append('<img style="float:left;" id="myblock" src="https://onmyblock.com/media/images/icons/myblock.png" />');
				$(this).removeClass("wishlisted");
				$('.wishlist p').text("Add to Favorites");
				$(this).attr("data-animation","1");
				//$(this).find("p").html("Add This House to Your Wishlist");
				$.ajax({
					type: "POST",
					url: "action",
					data: {
						ajax_action: 'favorite',
						action: 'unfavorite',
						user_id: $(this).attr("data-user-id"),
						place_id: $(this).attr("data-place-id")
					}
				});
			} else {
				$(this).addClass("wishlisted");
				$("#myblock").remove();
				$(this).find("span").append('<img style="float:left;" id="myblock" src="https://onmyblock.com/media/images/icons/myblock_w.png" />');
				$('.wishlist p').text("Added to Favorites");
				$(this).attr("data-animation","0");
				//$(this).find("p").html("This House Is In Your Wishlist");
				$.ajax({
					type: "POST",
					url: "action",
					data: {
						ajax_action: 'favorite',
						action: 'favorite',
						user_id: $(this).attr("data-user-id"),
						place_id: $(this).attr("data-place-id")
					}
				});
			}
		}else{
			openPopup();
		}
	});
});
 
function closePopup() {
	$('.temp_popup').css("display", "none");
	$('.temp_popup').hide();
	$('.temp_popup').fadeOut(500);
	$('.temp_popup_container').css("display", "none");
	$('.temp_popup_container').hide();
	$('.temp_popup_container').fadeOut(500);
	fbLogout();
}

function openPopup() {
	$('.temp_popup').css("display", "block");
	$('.temp_popup').hide();
	$('.temp_popup').fadeIn(500);
	$('.temp_popup_container').css("display", "block");
	$('.temp_popup_container').hide();
	$('.temp_popup_container').fadeIn(500);
}

function fbLogin() { 

	FB.init({
		appId: '246041178762859',
		frictionlessRequests: true,
		status: true,
		cookie: true
	}),
	FB.login(function(response) {
		if (response.authResponse) {
			FB.api('/me', function(response) {
				$.cookie("omb_session", 'loggedin');
				
				window.location.reload();
			});
		} else {  }
	});
}

function fbLoginin()
{
	var autoLoginFbook = function(response) {
		if (response.status !== 'connected') {
            return;
        }
        var uid = response.authResponse.userID;
        var accessToken = response.authResponse.accessToken;
        $.post("/login/facebook/", {
            access_token: accessToken,
            fb_id: uid},
            function(resp) {
                if (resp.status === "success") {
                    window.location.reload()
                }
            });
    };
    window.fbAsyncInit = function() {
        FB.init({
            appId: '246041178762859',
            cookie: true
        });
        FB.getLoginStatus(autoLoginFbook);
    };
    (function() {
        var e = document.createElement('script');
        e.async = true;
        e.src = document.location.protocol +
        '//connect.facebook.net/en_US/all.js';
        var scriptTag = document.getElementsByTagName('script')[0];
        scriptTag.parentNode.appendChild(e);
    } ());
}

function fbLogout() {
	FB.init({
		appId: '246041178762859',
		frictionlessRequests: true,
		status: true,
		cookie: true
	});
	FB.logout(function(response) {
		$.cookie("omb_session", 'loggedout');
		$.cookie('omb_access','');
		window.location.reload();
	});
}

function inviteFriends() {
	FB.api({
		method: 'fql.query',
		query: "SELECT uid FROM user WHERE uid IN (SELECT uid1 FROM friend WHERE uid2=me()) AND 'San Diego' IN affiliations AND is_app_user=0"
	}, function(response) {
		var collegeFriends;
		for (i = 0; i < response.length; i++) {
			collegeFriends = collegeFriends + "," + response[i]['uid'];
		}
		if (collegeFriends == undefined) {
			alert('All of your college friends are already using OnMyBlock!');
		} else {
			FB.ui({
				method: 'apprequests',
				message: 'Join OnMyBlock to see where your friends want to live next year!',
				filters: [{
					name: 'College Friends',
					user_ids: collegeFriends
				}]
			});
		}
	});
}

function inviteFriendsAll() {
	FB.api({
		method: 'fql.query',
		query: "SELECT uid FROM user WHERE uid IN (SELECT uid1 FROM friend WHERE uid2=me()) AND is_app_user=0"
	}, function(response) {
		var collegeFriends;
		for (i = 0; i < response.length; i++) {
			collegeFriends = collegeFriends + "," + response[i]['uid'];
		}
		if (collegeFriends == undefined) {
			alert('All of your college friends are already using OnMyBlock!');
		} else {
			FB.ui({
				method: 'apprequests',
				message: 'Join OnMyBlock to see where your friends want to live next year!',
				filters: [{
					name: 'College Friends',
					user_ids: collegeFriends
				}]
			});
		}
	});
}

function addCurrentResidents() {

	$this = $('.livehere');
	$place_id = $this.attr("data-place-id");
	$from_user = $this.attr("data-user-id");

	FB.api({
		method: 'fql.query',
		query: "SELECT uid FROM user WHERE uid IN (SELECT uid1 FROM friend WHERE uid2=me()) AND 'San Diego' IN affiliations"
	}, function(response) {
		var collegeFriends;
		for (i = 0; i < response.length; i++) {
			collegeFriends = collegeFriends + "," + response[i]['uid'];
		}
		FB.ui({
			method: 'apprequests',
			message: 'House Resident',
			filters: [{
				name: 'Friends',
				user_ids: collegeFriends
			}]
		}, function(response) {
		
			$.ajax({
				type: "POST",
				url: "action",
				data: {
					ajax_action: 'current_resident_friends',
					place_id: $place_id,
					from_user: $from_user,
					user_ids: JSON.stringify(response['to'])
				}
			}).done(function(data) {
					location.reload();
			});
		});
	});
}

function addPastResidents() {

	$this = $('.used_to_livehere');
	$place_id = $this.attr("data-place-id");

	FB.api({
		method: 'fql.query',
		query: "SELECT uid FROM user WHERE uid IN (SELECT uid1 FROM friend WHERE uid2=me()) AND 'San Diego' IN affiliations"
	}, function(response) {
		var collegeFriends;
		for (i = 0; i < response.length; i++) {
			collegeFriends = collegeFriends + "," + response[i]['uid'];
		}
		FB.ui({
			method: 'apprequests',
			message: 'House Resident',
			filters: [{
				name: 'Friends',
				user_ids: collegeFriends
			}]
		}, function(response) {
		
			$.ajax({
				type: "POST",
				url: "action",
				data: {
					ajax_action: 'past_resident_friends',
					place_id: $place_id,
					user_ids: JSON.stringify(response['to'])
				}
			}).done(function(data) {
					location.reload();
			});
		});
	});
}

function checkStatus(response) {
	if (response.status === 'connected') {
		if ($.cookie("omb_registeration") == 'rejected') {
			if ($.cookie("omb_session") == 'loggedin') {
				$.cookie("omb_session", 'loggedout');
				window.location.reload();
			}
		} else {
			if ($.cookie("omb_session") == 'loggedout') {
				$.cookie("omb_session", 'loggedin');
				$.cookie("omb_registeration", '');
				window.location.reload();
			}
		}
		var uid = response.authResponse.userID;
		var accessToken = response.authResponse.accessToken;
	} else if (response.status === 'not_authorized') {
		if ($.cookie("omb_session") == 'loggedin') {
			$.cookie("omb_session", 'loggedout');
			window.location.reload();
		} else {
			$.cookie("omb_session", 'loggedout');
		}
	} else {
		if ($.cookie("omb_session") == 'loggedin') {
			$.cookie("omb_session", 'loggedout');
			window.location.reload();
		} else {
			$.cookie("omb_session", 'loggedout');
		}
	}
}(function($, document, undefined) {
	var pluses = /\+/g;

	function raw(s) {
		return s;
	}

	function decoded(s) {
		return decodeURIComponent(s.replace(pluses, ' '));
	}
	var config = $.cookie = function(key, value, options) {
			if (value !== undefined) {
				options = $.extend({}, config.defaults, options);
				if (value === null) {
					options.expires = -1;
				}
				if (typeof options.expires === 'number') {
					var days = options.expires,
						t = options.expires = new Date();
					t.setDate(t.getDate() + days);
				}
				value = config.json ? JSON.stringify(value) : String(value);
				return (document.cookie = [encodeURIComponent(key), '=', config.raw ? value : encodeURIComponent(value), options.expires ? '; expires=' + options.expires.toUTCString() : '', options.path ? '; path=' + options.path : '', options.domain ? '; domain=' + options.domain : '', options.secure ? '; secure' : ''].join(''));
			}
			var decode = config.raw ? raw : decoded;
			var cookies = document.cookie.split('; ');
			for (var i = 0, parts;
			(parts = cookies[i] && cookies[i].split('=')); i++) {
				if (decode(parts.shift()) === key) {
					var cookie = decode(parts.join('='));
					return config.json ? JSON.parse(cookie) : cookie;
				}
			}
			return null;
		};
	config.defaults = {};
	$.removeCookie = function(key, options) {
		if ($.cookie(key) !== null) {
			$.cookie(key, null, options);
			return true;
		}
		return false;
	};
})(jQuery, document);
eval(function(p, a, c, k, e, d) {
	e = function(c) {
		return (c < a ? '' : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36))
	};
	if (!''.replace(/^/, String)) {
		while (c--) {
			d[e(c)] = k[c] || e(c)
		}
		k = [function(e) {
			return d[e]
		}];
		e = function() {
			return '\\w+'
		};
		c = 1
	};
	while (c--) {
		if (k[c]) {
			p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c])
		}
	}
	return p
}('(2($){$.R.B=2(z){7 t={f:\'Q\',3:\'B\',G:c,8:c,M:T,F:P,4:\'\',C:\'V\',h:5,n:5,i:E,k:E},0=$.O({},t,z);r j.U(2(){7 1=$(j);6(1.b(\'9\')){7 4=1.b(\'9\')}11{7 4=0.4}7 m=2(){$(\'W\').s("<y u=\'"+0.3+"\' D=\'"+0.C+"\'><p D=\'v\'>"+4+"</p></y>");6(4&&0.8){$(\'#\'+0.3+\' p.v\').s("<a u=\'"+0.f+"\' Z=\'#\' 10=\'w\'>w</a>")}},e=2(){$(\'#\'+0.3).L({o:(1.x().o-$(\'#\'+0.3).I()-0.n)+\'A\',q:(1.x().q+1.12()+0.h)+\'A\'}).K().14(0.M,2(){6($.J(0.i)){0.i(1)}})},l=2(){$(\'#\'+0.3).K().Y(0.F,2(){$(j).g();6($.J(0.k)){0.k(1)}})};6(4&&!0.8){1.15(2(){$(\'#\'+0.3).g();1.b({9:\'\'});m();e()},2(){l()})}6(4&&0.8){1.H(2(d){$(\'#\'+0.3).g();1.b({9:\'\'});m();e();$(\'#\'+0.f).H(2(){l();r c});r c})}6(!0.G&&!0.8){1.13(2(d){$(\'#\'+0.3).L({o:(d.N-$(\'#\'+0.3).I()-0.n),q:(d.S+0.h)})})}})}})(X);', 62, 68, 'settings|obj|function|toolTipId|tipContent||if|var|clickIt|title||attr|false|el|positionaToolTip|closeTipBtn|remove|xOffset|onShow|this|onHide|removeaToolTip|buildaToolTip|yOffset|top||left|return|append|defaults|id|aToolTipContent|close|offset|div|options|px|aToolTip|toolTipClass|class|null|outSpeed|fixed|click|outerHeight|isFunction|stop|css|inSpeed|pageY|extend|100|aToolTipCloseBtn|fn|pageX|200|each|defaultTheme|body|jQuery|fadeOut|href|alt|else|outerWidth|mousemove|fadeIn|hover'.split('|'), 0, {}))
$(function() {
	$('.normaltip').aToolTip()
	$('.fixedtip').aToolTip({
		fixed: true
	})
	$('.clicktip').aToolTip({
		clickIt: true,
		tipContent: 'Hello I am aToolTip with content from param'
	})
});
jQuery.fn.extend({
	getUrlParam: function(strParamName) {
		strParamName = escape(unescape(strParamName));
		var returnVal = new Array();
		var qString = null;
		if ($(this).attr("nodeName") == "#document") {
			if (window.location.search.search(strParamName) > -1) {
				qString = window.location.search.substr(1, window.location.search.length).split("&");
			}
		} else if ($(this).attr("src") != "undefined") {
			var strHref = $(this).attr("src")
			if (strHref.indexOf("?") > -1) {
				var strQueryString = strHref.substr(strHref.indexOf("?") + 1);
				qString = strQueryString.split("&");
			}
		} else if ($(this).attr("href") != "undefined") {
			var strHref = $(this).attr("href")
			if (strHref.indexOf("?") > -1) {
				var strQueryString = strHref.substr(strHref.indexOf("?") + 1);
				qString = strQueryString.split("&");
			}
		} else {
			return null;
		}
		if (qString == null) return null;
		for (var i = 0; i < qString.length; i++) {
			if (escape(unescape(qString[i].split("=")[0])) == strParamName) {
				returnVal.push(qString[i].split("=")[1]);
			}
		}
		if (returnVal.length == 0) return null;
		else if (returnVal.length == 1) return returnVal[0];
		else return returnVal;
	}
});
