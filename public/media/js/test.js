$(document).ready(function(){
	
	$(".bdgs").click(function(){
		var valBadge=$(this).attr('data');
		var valStatus=$(this).attr('data-status');
		var idPlace=$(this).parent("ul");
		
		if(valStatus==0) {
			$(this).attr('data-status',1);
		} else if(valStatus==1) {
			$(this).attr('data-status',0);
		}
			
		$.ajax({
			type: 	"POST",
			url:	"svcAction/badge",
			data:	{action:valStatus,idBadge:valBadge,place_id:idPlace}
		}).done(function(data) {
		   /*
			*
			*/
		});
	});
	
	
	$(".btnFav").on("click",function(){
		var valAction=$(this).attr("data");
		var idPlace=$(this).attr("data-placeid");
		
		if(valAction==0) {
			$(this).attr('data',1);
		} else if(valAction==1) {
			$(this).attr('data',0);
		}
		
		$.ajax({
			type: 	"POST",
			url:	"svcAction/favorite",
			data:	{action:valAction,place_id:idPlace}
		}).done(function(data) {
		   /*
			*
			*/
		});
	});
	
	
	$(".btnLike").on("click",function(){
		var valAction=$(this).attr("data");
		var idPlace=$(this).attr("data-placeid");
		
		if(valAction==0) {
			$(this).attr('data',1);
		} else if(valAction==1) {
			$(this).attr('data',0);
		}
		
		$.ajax({	
			type: 	"POST",
			url:	"svcAction/like",
			data:	{action:valAction,place_id:idPlace}
		}).done(function(data) {
		   /*
			*
			*/
		});
		
	});
	
	
});