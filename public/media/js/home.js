

$(function() {

	var input = document.getElementById('searchText');     
	var autocomplete = new google.maps.places.Autocomplete(input, {
		types: ["geocode"],
		componentRestrictions: {country: 'USA'}
	}); 

	$("#searchText").keyup(function(event){
		if(event.keyCode == 13){
			//longLat();
		}
	});
	
	$("#slide_submit_search").on("click",function(){
		longLat();
	});
     
    function longLat()
    {
        var firstResult = $("#searchText").val();
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({"address":firstResult }, 
            function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var lat = results[0].geometry.location.lat(),
                        lng = results[0].geometry.location.lng(),
                        placeName = results[0].address_components[0].long_name,
                        latlng = new google.maps.LatLng(lat, lng);
                        
                        $("#latitude").val(lat);
                        $("#longitude").val(lng);

                        $('#form-1').submit();
                }
            });
    }
	
	var input = document.getElementById('searchTextField');     
	var autocomplete = new google.maps.places.Autocomplete(input, {
		types: ["geocode"],
		componentRestrictions: {country: 'USA'}
	}); 

	$('#send_beta_email').click(function() {
		var email = $('#email_beta').val();
		function validateEmail(email) {
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		}
		if (validateEmail(email)) {
			$.ajax({
				type: "POST",
				url: "email_beta.php",
				data: {
					email: email
				}
			});
			$(this).fadeOut(500);
			$('#email_beta').fadeOut(500);
			$("#thankyou_message").append("<p style='font-size:14px;'>Thank You, we'll contact you soon!</>");
		} else {
			alert("Please Enter A Valid Email Address!");
		}
	});
	var blocked = false;
	//1=active||0=inactive
	var status=1;

	$('.buttonFb').click(function() {
		
		if (!blocked) {
			$('.temp_popup').css("display", "block");
			$('.temp_popup').hide();
			$('.temp_popup').fadeIn(500);
			$('.temp_popup_container').css("display", "block");
			$('.temp_popup_container').hide();
			$('.temp_popup_container').fadeIn(500);
			blocked = true;
		}
	});

	function callNotification(status)
	{
		if(status==1)
		{
			$.ajax({
				type:"POST",
				url:"svcNotification",
				data: {
					type: "result"
				}
			}).done(function(data) {
				if(data!=0)
				{
					$('#target-domm').css('display','block');

					$("#numNotificationm").empty();

					$("#numNotificationm").text($.trim(data));
				} else {
					$('#target-domm').css('display','none');
				}
			});
		}
	}

	setInterval(function() {
		$floatBar = $('.floatbar_home');
		
		if($(document).scrollTop() > 750){
			if(!$floatBar.hasClass('floating')){
				$floatBar.hide();
				$floatBar.addClass('floating'); 
				$(".browse_options").css("margin-top","0px");
				$("#front_menu").css("margin-top","0px");
				$floatBar.css({'top':'0', 'position':'fixed'}, function(){});
				$floatBar.fadeIn('normal'); 
				$(".browse_nav").css('margin-top','-2px');
				
				$(".menuBtn").css("display","block");

				callNotification(status);

				status=0;
			}
		} else {
			if($floatBar.hasClass('floating')){
				$floatBar.hide();
				$floatBar.removeClass('floating');
				$(".browse_options").css("margin-top","-82px");
				$("#front_menu").css("margin-top","135px");
				$floatBar.css({'top':'618px', 'position':''}, function(){});
				$floatBar.fadeIn('normal');
				$(".menuBtn").css("display","none");
				$(".browse_nav").css('margin-top','0px');

				status=1;
			}
		}
	}, 100);
	
});
