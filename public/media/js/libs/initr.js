 /**
 *  @description [Site OnMyBlock]
 *  @author [Ronald Mori G]
 *  @version [3.0]
 *  @copyright [Tecla Labs]
 *  
 */


/**
 * [main description]
 * @type {Object}
 */
Main = (function() {   
  
		
    /**
     * [onReady Inicializando la experiencia del usuario ]
     * @return {[type]} [description]
     */
    onReady = function() {              
				
		$('#favorites').click(function(){

			$('#contfavorite').fadeIn('slow');
			$('#contfavorite').show();

			$('#favorites a').addClass('on');
			$('#profile a').removeClass('on');
			$('#places a').removeClass('on');


			$('#contprofile').hide();
			$('#contplaces').hide();    
        
		});

		$('#profile').click(function(){

			$('#contprofile').fadeIn('slow');
			$('#contprofile').show();

			$('#profile a').addClass('on');
			$('#favorites a').removeClass('on');
			$('#places a').removeClass('on');

			$('#contfavorite').hide();
			$('#contplaces').hide();


		});

		$('#places').click(function(){

			$('#contplaces').fadeIn('slow');
			$('#contplaces').show();

			$('#places a').addClass('on');
			$('#profile a').removeClass('on');
			$('#favorites a').removeClass('on');

			$('#contprofile').hide();
			$('#contfavorite').hide();


		});
		
		$("#searchTextField").keyup(function(event){
			if(event.keyCode == 13){
				pag=1;
				pagTwo=0;
				
				var valSearch = $("#searchTextField").val();
				
				$.cookie("val_search",valSearch);
				
				datFilter = $(this).attr("data-option-value");
				
				$('#container').empty();
				
				$.ajax({
					type: 	"POST",
					url:	"browse_items",
					data:	{ dataFilter:datFilter,pagina:pagTwo } 
				}).done(function(data) {
				
					var $container = $('#container');

					$container.masonry({
						itemSelector: '.box',
						columnWidth: 2,
						isAnimated: !Modernizr.csstransitions
					});
					
					var $boxes = $( data );
					$container.append( $boxes ).masonry( 'appended', $boxes );
					
				});
				pagTwo++;
			}
		});

		$('#contentlogin').mouseenter(function(){

			$('.despledat').fadeIn('slow');

		}).mouseleave(function(){

            $('.despledat').fadeOut('slow');

		});
	

		$('.closepop').click(function(){
			var data = $(this).attr("id");
			var nextPlace = parseInt(data)+1;
			var idPlace = $(this).attr("data");
			
			$.ajax({
				type: 	"GET",
				url:	"svcAction/favorite",
				data:	{action:0,place_id:idPlace}
			}).done(function(data) {
			   /*
				*
				*/
			});
			
			$('#fav01-'+data).fadeOut('slow');
			$('#fav01-'+nextPlace).animate({ 'margin-top': '0px' }, 1000, 'easeInBack');   
		});

		$('.viewmore').click(function(){
			$('.hide').fadeIn('slow');                     
			$('.viewless').show();
			$('.viewmore').hide();
			$('.listbadges ul').animate({'height':'336px'}, 1000);
		});

		$('.viewless').click(function(){        
			$('.hide').fadeOut('slow');        
			$('.viewmore').show();
			$('.viewless').hide();
			$('.listbadges ul').animate({'height':'178px'},1000);
		});
		
		$('.transition').mouseenter(function(){

			var idPlace = $(this).attr("data");
			
            $('#'+idPlace).fadeIn('slow');
				
		}).mouseleave(function(){

			var idPlace = $(this).attr("data");
			
            $('#'+idPlace).fadeOut('slow');

		});
		
		
		$('.fav').click(function(){
	  
			objDivParent = $(this).parent("div");
			var idPlace = objDivParent.attr("id");
		
			var valAction=$(this).attr("data");
			
			$('.fav-'+idPlace).css('background-position','0 30px;');
			//$('.addfav-'+idPlace).hide();
			
			$.ajax({
				type: 	"GET",
				url:	"svcAction/favorite",
				data:	{action:valAction,place_id:idPlace}
			}).done(function(data) {
				
			});

		});

		$('.like').click(function(){
	  
			objDivParent = $(this).parent("div");
			var idPlace = objDivParent.attr("id");
			
			var valAction=$(this).attr("data");

			$('.nolike-'+idPlace).fadeIn("slow");
			$('.like-'+idPlace).hide();
			
			$.ajax({	
				type: 	"GET",
				url:	"svcAction/like",
				data:	{action:valAction,place_id:idPlace}
			}).done(function(data) {
				
			});
		});
		
		$('.nofavorite').click(function(){
			
			objDivParent = $(this).parent("div");
			var idPlace = objDivParent.attr("id");

			$('.addfav-'+idPlace).fadeIn("slow");
			$('.nofav-'+idPlace).hide();
		});
                                   
		$('.nolike').click(function(){
			
			objDivParent = $(this).parent("div");
			var idPlace = objDivParent.attr("id");

			$('.like-'+idPlace).fadeIn("slow");
			$('.nolike-'+idPlace).hide();			
			
		});

		//Solucion tabs 1//

		

		

      

      

		//Solucion tabs 2//

		$('#datphotos2').click(function(){

			$('.datphotos2').fadeIn('slow');
			$('.datphotos2').show();

			$('#datphotos2').addClass('on');

			$('#datmap2').removeClass('on');
			$('#datstreet2').removeClass('on');
			$('#datedit').removeClass('on');

			$('.datmap2').hide();
			$('.datstreet2').hide();
			$('.datedit2').hide();   

		});

		$('#datmap2').click(function(){

			$('.datmap2').fadeIn('slow');
			$('.datmap2').show();

			$('#datmap2').addClass('on');

			$('#datphotos2').removeClass('on');
			$('#datstreet2').removeClass('on');
			$('#datedit2').removeClass('on');

			$('.datphotos2').hide();
			$('.datstreet2').hide();
			$('.datedit2').hide();   

		});

		$('#datstreet2').click(function(){

			$('.datstreet2').fadeIn('slow');
			$('.datstreet2').show();

			$('#datstreet2').addClass('on');

			$('#datphotos2').removeClass('on');
			$('#datmap2').removeClass('on');
			$('#datedit2').removeClass('on');

			$('.datphotos2').hide();
			$('.datmap2').hide();
			$('.datedit2').hide();   

		});

		$('#datedit2').click(function(){

			$('.datedit2').fadeIn('slow');
			$('.datedit2').show();

			$('#datedit2').addClass('on');

			$('#datphotos2').removeClass('on');
			$('#datmap2').removeClass('on');
			$('#datstreet2').removeClass('on');

			$('.datphotos2').hide();
			$('.datmap2').hide();
			$('.datstreet2').hide();   

		});            

		
		//Solucion popup//			
        	
		
		
		$('#dataposted').click(function(){

			$('#dataposted').animate({'height':'150px'},'slow');
			$('#replydat').fadeIn('slow');

		  
		}).mouseleave(function(){

			$('#replydat').fadeOut('slow');
			  
		});

		$('.likes').click(function(){

			$('.silike').css('background-position','0 0');

		}); 

		$('#save').click(validacionsave);

		$('input').click(function () {
			$(this).css('border', '1px solid #D9D9D9');
			$('.error').text('');
		});
		
		
		$('.addfav').click(function(){

            $('.addfav').hide();
            $('.bgadd').show();
		});

      $('.bgadd').click(function(){

            $('.addfav').show();
            $('.bgadd').hide();
       });  

      $('.likebtn').click(function(){

            $('.bgliked').show();
            $('.likebtn').hide();

      });

      $('.bgliked').click(function(){

            $('.likebtn').show();
            $('.bgliked').hide();

      });

      $('.bad01').click(function(){

        $('.bad01').css('background-position' , '0 -333px');        

      });

      $('.bad02').click(function(){

        $('.bad02').css('background-position' , 'right -333px');        

      });

      $('.bad03').click(function(){

        $('.bad03').css('background-position' , '0 -415px');        

      });

      $('.bad04').click(function(){

        $('.bad04').css('background-position' , 'right -415px');        

      });

      $('.bad05').click(function(){

        $('.bad05').css('background-position' , '0 -497px');        

      });

      $('.bad06').click(function(){

        $('.bad06').css('background-position' , 'right -497px');        

      });

      $('.bad07').click(function(){

        $('.bad07').css('background-position' , '0 -579px');        

      });

      $('.bad08').click(function(){

        $('.bad08').css('background-position' , 'right -581px');        

      });
	
   }
        

    /*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
    f[z]=function(){
    (a.s=a.s||[]).push(arguments)};var a=f[z]._={
    },q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
    f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
    0:+new Date};a.P=function(u){
    a.p[u]=new Date-a.p[0]};function s(){
    a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
    hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
    return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
    b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
    b.contentWindow[g].open()}catch(w){
    c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
    var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
    b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
    loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
    /* custom configuration goes here (www.olark.com/documentation) */
    olark.identify('7080-590-10-3591');/*]]>*/    
        

      // Additional JS functions here
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '561406290571363', // App ID
      channelUrl : 'https://onmyblock.com/', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });

    // Additional init code here

  };

  // Load the SDK asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));


    window.fbAsyncInit = function() {
  FB.init({
    appId      : '561406290571363', // App ID
    channelUrl : 'https://onmyblock.com/', // Channel File
    status     : true, // check login status
    cookie     : true, // enable cookies to allow the server to access the session
    xfbml      : true  // parse XFBML
  });

  // Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
  // for any auth related change, such as login, logout or session refresh. This means that
  // whenever someone who was previously logged out tries to log in again, the correct case below 
  // will be handled. 
  FB.Event.subscribe('auth.authResponseChange', function(response) {
    // Here we specify what we do with the response anytime this event occurs. 
    if (response.status === 'connected') {
      // The response object is returned with a status field that lets the app know the current
      // login status of the person. In this case, we're handling the situation where they 
      // have logged in to the app.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // In this case, the person is logged into Facebook, but not into the app, so we call
      // FB.login() to prompt them to do so. 
      // In real-life usage, you wouldn't want to immediately prompt someone to login 
      // like this, for two reasons:
      // (1) JavaScript created popup windows are blocked by most browsers unless they 
      // result from direct user interaction (such as a mouse click)
      // (2) it is a bad experience to be continually prompted to login upon page load.
      FB.login();
    } else {
      // In this case, the person is not logged into Facebook, so we call the login() 
      // function to prompt them to do so. Note that at this stage there is no indication
      // of whether they are logged into the app. If they aren't then they'll see the Login
      // dialog right after they log in to Facebook. 
      // The same caveats as above apply to the FB.login() call here.
      FB.login();
    }
  });
  };

  // Load the SDK asynchronously
  (function(d){
   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement('script'); js.id = id; js.async = true;
   js.src = "//connect.facebook.net/en_US/all.js";
   ref.parentNode.insertBefore(js, ref);
  }(document));

  // Here we run a very simple test of the Graph API after login is successful. 
  // This testAPI() function is only called in those cases. 
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
      console.log('Good to see you, ' + response.name + '.');
    });
  }


         


     /**
      * Public API
      */
    return {
        onReady : onReady
    }
 
})();

 

/**
 * Execute OnReady
 */
Main.onReady();

$('.logo').on('click',function(){
	var url = "http://54.241.12.168/";    
	$(location).attr('href',url);
});

(function ($) {
    $.fn.validCampoInput = function (cadena) {
        $(this).on({
            keypress: function (e) {
                var key = e.which,
            keye = e.keyCode,
            tecla = String.fromCharCode(key).toLowerCase(),
            letras = cadena;
                if (letras.indexOf(tecla) == -1 && keye != 9 && (key == 37 || keye != 37) && (keye != 39 || key == 39) && keye != 8 && (keye != 46 || key == 46) || key == 161) {
                    e.preventDefault();
                }
            }
        });
    };
})(jQuery);

$(function () {

    $('#housename,#address_value').validCampoInput('0123456789abcdefghijklmnopqrstuvwxyziou, ');
    $('#neighborhood').validCampoInput('abcdefghijklmnopqrstuvwxyziou ');
    $('#number,#housesize,#monthly,#numbed,#numbad').validCampoInput('0123456789');
    $('#namerealtor').validCampoInput('abcdefghijklmnopqrstuvwxyziou ');
    $('#numphone').validCampoInput('0123456789-() ');
    $('#email').validCampoInput('0123456789abcdefghijklmnopqrstuvwxyziou@.-_');
    $('#website').validCampoInput('0123456789abcdefghijklmnopqrstuvwxyziou.-_');
});

function validacionsave() {   
    var error = 0;

    if ($('#email').val() == "") {
         return true;      
    }
    else {
        var email = $("#email").val();
        if (!EmailValidacion(email)) {
            $("#email").next().text('Por favor ingresa un correo válido.');
            $("#email").css("border", "1px solid #ff0000");
            error++;
        }
    }

    if (error > 0) {    
            return false;
        } else {   
            return true;        
        }
}

function EmailValidacion(email){
  var reg = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
  if (reg.test(email) == false) {
      return false;
  }
  else {
      return true;
  }
} 

   
