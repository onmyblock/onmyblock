function selectURL() {
	var text_input = document.getElementById('short_url');
	text_input.focus();
	text_input.select();
}
$(document).ready(function() {
	$used_to = $('.used_to_livehere');
	$user_id = $used_to.attr("data-user-id");
	$place_id = $used_to.attr("data-place-id");
	$user_name = $used_to.attr("data-user-name");
	$used_to.click(function() {
		if (!$(this).hasClass('checked')) {
			$.ajax({
				type: "POST",
				url: "action",
				data: {
					ajax_action: 'past_resident',
					action: 'add',
					user_id: $user_id,
					place_id: $used_to.attr("data-place-id")
				}
			}).done(function(data) {
				$(".used_to_livehere span").remove();
				$used_to.prepend('<span class="fs1" data-icon="&#x34;" style="margin-right:5px;" ></span>');
				$('#past_list').append('<div id="resident_' + $user_id + '" style="width: 140px; height:100px; background:url(https://graph.facebook.com/' + $user_id + '/picture?width=140&height=100) no-repeat center top #000; background-size:140px; margin:3px; display:inline-block;"><div class="place_username_box"><a href="profile?id=' + $user_id + '">' + $user_name + '</a></div></div>');
				$used_to.addClass('checked');
			});
		} else {
			$.ajax({
				type: "POST",
				url: "action",
				data: {
					ajax_action: 'past_resident',
					action: 'remove',
					user_id: $user_id,
					place_id: $used_to.attr("data-place-id")
				}
			}).done(function(data) {
				$(".used_to_livehere span").remove();
				$used_to.prepend('<span class="fs1" data-icon="&#x33;" style="margin-right:5px;" ></span>');
				$('#resident_' + $user_id).remove();
				$used_to.removeClass('checked');
			});
		}
	});
	$livehere = $('.livehere');
	$user_id = $livehere.attr("data-user-id");
	$place_id = $livehere.attr("data-place-id");
	$user_name = $livehere.attr("data-user-name");
	$livehere.click(function() {
		if (!$(this).hasClass('checked')) {
			$.ajax({
				type: "POST",
				url: "action",
				data: {
					ajax_action: 'current_resident',
					action: 'add',
					user_id: $user_id,
					place_id: $livehere.attr("data-place-id")
				}
			}).done(function(data) {
					location.reload();
			});
		} else {
			$.ajax({
				type: "POST",
				url: "action",
				data: {
					ajax_action: 'current_resident',
					action: 'remove',
					user_id: $user_id,
					place_id: $livehere.attr("data-place-id")
				}
			}).done(function(data) {
				$(".livehere span").remove();
				$livehere.prepend('<span class="fs1" data-icon="&#x33;" style="margin-right:5px;" ></span>');
				$('#resident_' + $user_id).remove();
				$livehere.removeClass('checked');
			});
		}
	});
	$('.place_wall_post').click(function() {
		$(this).animate({
			height: '130px'
		}, 300, function() {});
		$(this).mouseleave(function() {
			$(this).animate({
				height: '85px'
			}, 200, function() {});
		})
	});
	
});


function confirmResident(user_id, place_id, owner) {
	$.ajax({
		type: "POST",
		url: "action",
		data: {
			ajax_action: 'confirm_resident',
			user_id: user_id,
			place_id: place_id,
			owner: owner
		}
	}).done(function(data) {
		location.reload();
	});
}

function viewMoreBadges() {
	if (!$('#badge_frame').hasClass("opened")) {
		$('#badge_frame').addClass("opened");
		$('#badge_view_more').hide();
		$('#badge_view_less').show();
		$('#badge_frame').animate({
			height: '365px',
		}, 800, function() {});
	}
}
$(function() {
	place_tabs.init();
	$("#contact_realtor dt").click(function() {
		$(this).next("#contact_realtor dd").slideToggle("slow").siblings("#contact_realtor dd:visible").slideUp("slow");
		$(this).toggleClass("active");
		$(this).siblings("#contact_realtor dt").removeClass("active");
		return false
	})
	$('#badge_view_less').hide();
	$('#badge_view_more').click(function() {
		viewMoreBadges();
	});
	$('#badge_view_less').click(function() {
		$('#badge_frame').removeClass("opened");
		$('#badge_view_less').hide();
		$('#badge_view_more').show();
		$('#badge_frame').animate({
			height: '192px',
		}, 800, function() {});
	});
});
place_tabs = {
	init: function() {
		$('.place_tabs').each(function() {
			$(this).find('.tab-content').not($(this).find('ul.nav .selected a').attr('href')).hide();
			$(this).find('ul.nav li').click(function() {
				if ($(this).hasClass('selected')) {
					return false
				} else {
					$(this).parents('.place_tabs').find('.tab-content').hide();
					$($(this).find('>a').attr('href')).fadeIn(300);
					$(this).addClass('selected').siblings().removeClass('selected');
					return false;
				}
			});
		});
	}
};

function iResize() {
	var iframeids = ["photo_iframe"]
	//Should script hide iframe from browsers that don't support this script (non IE5+/NS6+ browsers. Recommended):
	var iframehide = "yes"
	var getFFVersion = navigator.userAgent.substring(navigator.userAgent.indexOf("Firefox")).split("/")[1]
	var FFextraHeight = parseFloat(getFFVersion) >= 0.1 ? 16 : 0 //extra height in px to add to iframe in FireFox 1.0+ browsers

	function dyniframesize() {
		var dyniframe = new Array()
		for (i = 0; i < iframeids.length; i++) {
			if (document.getElementById) { //begin resizing iframe procedure
				dyniframe[dyniframe.length] = document.getElementById(iframeids[i]);
				if (dyniframe[i] && !window.opera) {
					dyniframe[i].style.display = "block"
					if (dyniframe[i].contentDocument && dyniframe[i].contentDocument.body.offsetHeight) //ns6 syntax
					dyniframe[i].height = dyniframe[i].contentDocument.body.offsetHeight + FFextraHeight;
					else if (dyniframe[i].Document && dyniframe[i].Document.body.scrollHeight) //ie5+ syntax
					dyniframe[i].height = dyniframe[i].Document.body.scrollHeight;
				}
			}
			//reveal iframe for lower end browsers? (see var above):
			if ((document.all || document.getElementById) && iframehide == "no") {
				var tempobj = document.all ? document.all[iframeids[i]] : document.getElementById(iframeids[i])
				tempobj.style.display = "block"
			}
		}
	}
	if (window.addEventListener) window.addEventListener("load", dyniframesize, false)
	else if (window.attachEvent) window.attachEvent("onload", dyniframesize)
	else window.onload = dyniframesize
	$("#photo_iframe").css('height', dyniframesize);
}
