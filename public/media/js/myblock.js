$(function() {
	/* $.ajax({
		type: "POST",
		url: "svcNotification",
		data:	{
			type: "reset"
		}
	}).done(function(data) {
		$("#numNotification").empty();
	}); */
	
	$('#target-dom').css('display','none');

	$("#scroll_top").hide();
	$('#scroll_top').click(function() {
		$('body,html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});
	$("#places dt").click(function() {
		$(this).next("#places dd").slideToggle("slow").siblings("#places dd:visible").slideUp("slow");
		$(this).toggleClass("active");
		$(this).siblings("#places dt").removeClass("active");
		return false
	})
	$(".delete_from_wishlist").click(function() {
		$.ajax({
			type: "POST",
			url: "action?object=delete_from_wishlist",
			data: {
				user_id: $(this).attr("user_id"),
				place_id: $(this).attr("place_id")
			}
		});
		$parent = $(this).parent();
		$(this).remove();
		$parent.slideToggle("slow");
	});
	home_tabs.init();
});
home_tabs = {
	init: function() {
		$.cookie("streamTab", 1);
		$('.home_tabs').each(function() {
			$(this).find('.tab-content').not($(this).find('ul.nav .selected a').attr('href')).hide();
			$(this).find('ul.nav li').click(function() {
				if ($(this).hasClass('selected')) {
					return false
				} else {
					if ($(this).find('a').attr('href') == '#stream') {
						$.cookie("streamTab", 1);
					} else {
						$.cookie("streamTab", 0);
					}
					$(this).parents('.home_tabs').find('.tab-content').hide();
					$($(this).find('>a').attr('href')).fadeIn(300);
					$(this).addClass('selected').siblings().removeClass('selected');
					return false;
				}
			});
		});
	}
};
$(function() {
	window.onbeforeunload = function() {
		$.cookie("streamLoad", 0);
		$.cookie("multiload_blocker", null);
		$.cookie("currentPricingOptions", '');
		$.cookie("currentRoomOptions", '');
		$.cookie("omb_browse_location", '');
	};
	setInterval(function() {
		addEvents()
	}, 1000);

	function addEvents() {
		if ($(this).scrollTop() > 2000) {
			$('#scroll_top').fadeIn();
		} else {
			$('#scroll_top').fadeOut();
		}
		if (($(document).height() - 300) < ($(window).height() + $(document).scrollTop()) && $.cookie("streamTab") == 1) {
			if ($.cookie("multiload_blocker") != 'block') {
				$.cookie("multiload_blocker", 'block');
				var $load = $.cookie("streamLoad");
				$load++;
				$.cookie("streamLoad", $load);
				$.ajax({
					type: "POST",
					url: "stream_items",
					data: {
						load: $load,
						user_id: $("#stream_container").attr('user_id')
					}
				}).done(function(data) {
					if ($.trim(data) == 1) {} else {
						$newData = $(data);
						$('#stream_container').isotope('insert', $newData);
					}
				});
				$.cookie("multiload_blocker", null);
			}
		}
	}
});
$(function() {
	var $container = $('#stream_container');
	$body = $('body'), columns = null;
	$container.isotope({
		resizable: false,
		layoutMode : 'straightDown',
		itemSelector: '.event',
		transformsEnabled: false,
		columnWidth: 400,
		getSortData: {
			name: function($elem) {
				return $elem.find('.friends').text();
			},
			symbol: function($elem) {
				return $elem.find('.network').text();
			}
		}
	});
	var $optionSets = $('#stream_options .stream_nav'),
		$optionLinks = $optionSets.find('a');
	$optionLinks.click(function() {
		var $this = $(this);
		if ($this.hasClass('selected')) {
			return false;
		}
		var $optionSet = $this.parents('.stream_nav');
		$optionSet.find('.selected').removeClass('selected');
		$this.addClass('selected');
		var options = {},
			key = $optionSet.attr('data-option-key'),
			value = $this.attr('data-option-value');
		value = value === 'false' ? false : value;
		options[key] = value;
		if (key === 'layoutMode' && typeof changeLayoutMode === 'function') {
			changeLayoutMode($this, options)
		} else {
			$container.isotope(options);
		}
		return false;
	});
});