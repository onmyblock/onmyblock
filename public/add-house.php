<?php

/*
error_reporting(E_ALL);
ini_set('display_errors', '1');
*/
require_once "../classes/sdk.class.php";

$address = $_POST['address'];

$step = 0;

if($address != ''){
	if($_POST['create_form'] != 'create'){
		$user->db->query("delete from temp_photos where user={$user->id}");
	}
	
	$arrayAddressGoogle = explode(',',$address);
	
	$arrayAddress = explode(' ',$arrayAddressGoogle[0]);
	
	$countArray = count($arrayAddress);
	
	for($i = 1; $i <= $countArray; $i++)
	{
		switch($arrayAddress[$i])
		{
			case 'Court':
					$arrayAddress[$i] = 'Ct';
				break;
				
			case 'street':
					$arrayAddress[$i] = 'St';
				break;
				
			case 'Street':
					$arrayAddress[$i] = 'St';
				break;
			case 'Avenue':
					$arrayAddress[$i] = 'Ave';
				break;
				
			case 'Boulevard':
					$arrayAddress[$i] = 'Blvd';
				break;
				
			case 'Road':
					$arrayAddress[$i] = 'Rd';
				break;
				
			case 'Lane':
					$arrayAddress[$i] = 'Ln';
				break;
			
			case 'Drive':
					$arrayAddress[$i] = 'Dr';
				break;
				
			case 'Highway':
					$arrayAddress[$i] = 'Hwy';
				break;
			
			case 'Parkway':
					$arrayAddress[$i] = 'Pkwy';
				break;
		}
	
		$routeArray .= $arrayAddress[$i].' ';
	}
	
	
	
	echo "<script>localStorage.status = 0;</script>"; 
	
	if(is_numeric($arrayAddress[0]))
	{
		// get address info
		require('../classes/geo_parser.php');
		$geo = get_ggeocoder_json($_POST['address']);
		$street =  $geo->find_address_components('street_number');
		$route =  $geo->find_address_components('route');
		$neighborhood =  $geo->find_address_components('neighborhood');
		$city =  $geo->find_address_components('locality');
		$state =  $geo->find_address_components('administrative_area_level_1');
		$postal_code =  $geo->find_address_components('postal_code');

		if($geo->results['status'] == 'OK' && $street->short_name != '' && $route->short_name != '' && $state->short_name == 'CA')
		{
			$address = array(
				'street' => $street->short_name,
				'route' => $route->short_name,
				'neighborhood' => $neighborhood->short_name,
				'city' => $city->short_name,
				'state' => $state->short_name,
				'postal' => $postal_code->short_name,
				'latitude' => $geo->results['latitude'],
				'longitude' => $geo->results['longitude']
			);
			
			$rsX = $user->db->query("select exists (select 1 from places where street = ".$arrayAddress[0]." and route = '".$routeArray."' ) as result");

			$array = array();
			
			while ($fila = $rsX->fetch_assoc()) {
				
				array_push($array,$fila);
			}
			
			$result = $array[0]['result'];
			
			$smarty->assign('address', $address);
			
			$slow=0;
			
			$step = 2;
				
			if($result!=1)
			{
				
				if($_POST['create_form'] == 'create'){
					
					$tempPlace = mysqli_fetch_row($user->db->query("select id_user_temp, id from temp_place where id_user_temp = {$user->id} "));
					
					$rsPhotos = mysqli_fetch_array($user->db->query("select * from temp_photos where place = {$tempPlace[1]}"));
					
					if(count($rsPhotos)>0)
					{
						while (true)
						{
							$id = rand(1000000, 9999999);
							$id = 1000000000+$id;

							$sql = mysqli_fetch_array($user->db->query("select id from places where id = {$id}"));
							if($sql[0] != $id){break;}
						}
						
						$query = 'insert into realtors values (null, ?, ?, ?, ?)';
						
						$stmt = $user->db->connection->prepare($query);

						if(strlen($_POST['realtor_phone']) > 9){
							$realtor_phone = preg_replace("/[^0-9]/", '', $_POST['realtor_phone']);
						}

						mysqli_stmt_bind_param($stmt, "ssss", $_POST['realtor_name'], $_POST['realtor_email'], $realtor_phone, $_POST['realtor_link']);
						$stmt->execute();
						//print_r($user->db->connection->insert_id);

						//$query = "insert into places values ({$id}, {$user->db->connection->insert_id}, ?, now(), ?, ?, ?, ?, ?, ?, ?, ?, ?, null, ?, ?, ?, ?)";
						
						$parametersPlace = array('realtor'=>$user->db->connection->insert_id,
												 'name'=>$_POST['house_name'],
												 'date'=>'now()',
												 'street'=>$address['street'],
												 'route'=>$address['route'],
												 'state'=>$address['state'],
												 'unit'=>$_POST['house_unit'],
												 'city'=>$address['city'],
												 'neighborhood'=>$_POST['house_neighborhood'],
												 'postal'=>$address['postal'],
												 'latitude'=>$address['latitude'],
												 'longitude'=>$address['longitude'],
												 'size'=>$_POST['house_size'],
												 'rooms'=>$_POST['house_rooms'],
												 'bathrooms'=>$_POST['house_bathrooms'],
												 'price'=>$_POST['house_price']
												);
												
						$arrayString = array('name','route','state','city','neighborhood','type','description','unit');
						  
						foreach($parametersPlace as $key => $value)
						{
							if($value!='')
							{
								$queryOne .= $key.',';
								
								if(in_array($key, $arrayString))
								{
									$queryTwo .= "'".$value."',";
								}
								else
								{
									$queryTwo .= $value.",";
								}
							}
						}
						
						$queryOne = substr($queryOne, 0, -1);
						$queryTwo = substr($queryTwo, 0, -1);
						
						$queryTest = "INSERT INTO `main`.`places` (".$queryOne.")
													  VALUES (".$queryTwo.")";
						
						//$stmt = $user->db->connection->prepare($query);

						//mysqli_stmt_bind_param($stmt, "sssd", $_POST['house_name'], $address['street'], $address['route'], $address['state'], $_POST['house_unit'], $address['city'], $address['nighborhood'],  $address['postal'], $address['latitude'], $address['longitude'], $_POST['house_size'], $_POST['house_rooms'], $_POST['house_bathrooms'], $_POST['house_price']);
						
						if($user->db->query($queryTest)){
							
							$id = $user->db->connection->insert_id;
							
							$tempPlaceId = $tempPlace[1];
							
							$photos = $user->db->query("select * from temp_photos where place = {$tempPlaceId} && dir not like '%profile%' order by id desc");
							
							$i=0;
							
							$bucket = 'onmyblock';
							
							$s3 = new AmazonS3();
							
							while($photo = mysqli_fetch_assoc($photos))
							{
								$name = $id.'-'.$i;
								
								$nameTemp = $name.'.jpg';
								
								$query = "insert into photos ( place, user, dir, width, height )values ( {$id}, {$user->id}, '{$nameTemp}', {$photo['width']}, {$photo['height']}  )";
								
								$user->db->query($query);
							
								$dir = "places/".$photo['place']."/".$photo['dir'];
								
							
								$response = $s3->copy_object(
									array( // Source
										'bucket'   => $bucket,
										'filename' => $dir
									),
									array( // Destination
										'bucket'   => $bucket,
										'filename' => 'places/'.$id.'/'.$name.'.jpg'
									),
									array( // Optional parameters
										'acl'  => AmazonS3::ACL_PUBLIC,
									)
								);
							
								if ($response->isOK())
								{
									//deleteS3
									if($dir)
									{
										$bucket = 'onmyblock';
										
										$s3 = new AmazonS3();
										
										$response = $s3->delete_object($bucket, $dir);
										
										if ($response->isOK())
										{
											$user->db->query("delete from temp_photos where place = {$tempPlaceId}");
											$user->db->cache->delete("place_{$tempPlaceId}");
										}
									}
								} else {
									//echo 'the place '.$place.' with the name '.$name.' had a error';
								}
								
								$i++;
							}
							
							$rand = rand(1, 100000);
							$short = mysqli_fetch_array($user->db->query("select short from short_url where place_id is null limit {$rand}, 1"));
							$user->db->query("UPDATE `main`.`short_url` SET `place_id`={$id} WHERE `short`='{$short[0]}'");
							$user->db->query("insert into events values (null, {$user->id}, 0, {$id}, now())");

							if($_POST['resident'] == 'current'){

								$user->db->query("insert into residents values ({$user->id}, {$id}, 1, now())");

							} elseif ($_POST['resident'] == 'past'){

								$user->db->query("insert into residents values ({$user->id}, {$id}, 0, now())");

							}

							header("Location:https://onmyblock.com/place?id={$id}");
						}
					}else{
						echo "<script>localStorage.status = 1;</script>"; 
					}
				}
			} else {
				$rsIdPlace = mysqli_fetch_array($user->db->query("select * from places where street = ".$arrayAddress[0]." and route = '".$routeArray."'"));
				
				$slow=1;
				$step = 1;
				echo "<script>localStorage.status = 0;</script>"; 
			}
		} else {
			$step = 1;
			echo "<script>localStorage.status = 0;</script>"; 
		}
	}else{
		$step = 1;
		echo "<script>localStorage.status = 0;</script>"; 
	}
}


// Steps
//  0 = First visit
//  1 = Address is wrong
//  2 = Address is correct, input details
//  2 && action is add = add house and redirect
$timestamp = time();
$md5timestamp = md5('unique_salt' . $timestamp);

$smarty->assign('slow', $slow);
$smarty->assign('timestamp', $timestamp);
$smarty->assign('md5timestamp', $md5timestamp);
$smarty->assign('placeId', $rsIdPlace['id']);
$smarty->assign('step', $step);
$smarty->assign('user', $user->get_info());
$smarty->assign('title', 'OnMyBlock - Add New House');
$smarty->assign('filename', 'add_house');
$smarty->display('pages/add-house.tpl');



?>