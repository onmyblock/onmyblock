<?php

if($user->id == 597597919 or $user->id == 1348620030 or $user->id == 1310384220 or $user->id == 1063140506 or $user->id == 1063140531 or $user->id == 580950903 )
{
	$idUniversity = $_GET['idUniversity'];

	switch($idUniversity)
	{
		case 1:
				$where = "where city like '%College wes%' or neighborhood like '%College wes%' 
							or city like '%Baja Drive%' or neighborhood like '%Baja Drive%' 
							or city like '%SDSU%' or neighborhood like '%SDSU%' 
							or city like '%College area%' or neighborhood like '%College area%'";
			break;
		case 2:
				$where = "where city like '%Old Towne Orange%' or neighborhood like '%Old Towne Orange%' 
							or city like '%Chapman and Tus%' or neighborhood like '%Chapman and Tus%' 
							or city like '%Costa Mesa%' or neighborhood like '%Costa Mesa%' 
							or city like '%Villa Park%' or neighborhood like '%Villa Park%' 
							or city like '%Glassell%' or neighborhood like '%Glassell%'
							or city like '%Collins%' or neighborhood like '%Collins%'";
			break;
		case 3:
				$where = "where city like '%Menlo%' or neighborhood like '%Menlo%' 
							or city like '%Irvine Are%' or neighborhood like '%Irvine Are%' 
							or city like '%Ellendale%' or neighborhood like '%Ellendale%' 
							or city like '%USC%' or neighborhood like '%USC%' 
							or city like '%University Villag%' or neighborhood like '%University Villag%'
							or city like '%Adams & Ellendale%' or neighborhood like '%Adams & Ellendale%'
							or city like '%W 29th St%' or neighborhood like '%W 29th St%'";
			break;
		case 4:
				$where = "where city like '%Westwood Village%' or neighborhood like '%Westwood Village%' 
							or city like '%Beverly HIlls%' or neighborhood like '%Beverly HIlls%' 
							or city like '%Westwood Hills%' or neighborhood like '%Westwood Hills%' 
							or city like '%UCLA%' or neighborhood like '%UCLA%' 
							or city like '%Greenfield%' or neighborhood like '%Greenfield%'";
			break;
		case 5:
				$where = "where city like '%San Luis Obispo%' or neighborhood like '%San Luis Obispo%' 
							or city like '%Downtown SLO%' or neighborhood like '%Downtown SLO%' 
							or city like '%Grover Beach%' or neighborhood like '%Grover Beach%' 
							or city like '%UCLA%' or neighborhood like '%UCLA%' 
							or city like '%Pismo Beach%' or neighborhood like '%Pismo Beach%'";
			break;
		case 6:
				$where = "where city like '%Marina Del Ray%' or neighborhood like '%Marina Del Ray%' 
							or city like '%La Tijera%' or neighborhood like '%La Tijera%' 
							or city like '%Manchester%' or neighborhood like '%Manchester%' 
							or city like '%Westchester%' or neighborhood like '%Westchester%' 
							or city like '%Culver City%' or neighborhood like '%Culver City%'";
			break;
		case 7:
				$where = "where city like '%Isla Vista%' or neighborhood like '%Isla Vista%' 
							or city like '%Goleta%' or neighborhood like '%Goleta%' 
							or city like '%Fortuna%' or neighborhood like '%Fortuna%' 
							or city like '%Goleta Beach%' or neighborhood like '%Goleta Beach%'";
			break;
		case 8:
				$where = "where city like '%Santa Clara%' or neighborhood like '%Santa Clara%' 
							or city like '%San Jose%' or neighborhood like '%San Jose%' 
							or city like '%Milpitas%' or neighborhood like '%Milpitas%' ";
			break;
			
		case 9:
				$where = "where city like '%Stanford%' or neighborhood like '%Stanford%' 
							or city like '%Palo Alto%' or neighborhood like '%Palo Alto%' 
							or city like '%Menlo Park%' or neighborhood like '%Menlo Park%' 
							or city like '%Mountain View%' or neighborhood like '%Mountain View%' 
							or city like '%Redwood City%' or neighborhood like '%Redwood City%'";
			break;
			
		case 10:
				$where = "where city like '%Richmond%' or neighborhood like '%Richmond%' 
							or city like '%Inner sunset%' or neighborhood like '%Inner sunset%' 
							or city like '%UCSF%' or neighborhood like '%UCSF%' 
							or city like '%Parkside%' or neighborhood like '%Parkside%'";
			break;
			
		case 11:
				$where = "where `status` = 1";
			break;
			
		case 12:
				$where = "where DATE(date) ='{$_GET['date']}' ";
	}
	
	$arrayUniversity = array('San Diego State University',
							 'Chapman University',
							 'University of Southern California',
							 'University of California at Los Angeles',
							 'Cal Poly San Luis Obispo',
							 'Loyola Marymount University',
							 'UCSB',
							 'Santa Clara',
							 'Stanford',
							 'UCSF',
							 'University of San Diego'
							);
	
	$rsPlaces = mysqli_fetch_all($user->db->query("select id, street, route, city, neighborhood, DATE(date) from places {$where}"));
	
	/*USD*/
	$placesUSD = mysqli_fetch_all($user->db->query("select count(id) from places where `status` = 1"));
	
	/*San Diego State University*/
	$placesSanDiego = mysqli_fetch_all($user->db->query("select count(id) from places 
														where city like '%College wes%' or neighborhood like '%College wes%' 
														or city like '%Baja Drive%' or neighborhood like '%Baja Drive%' 
														or city like '%SDSU%' or neighborhood like '%SDSU%' 
														or city like '%College area%' or neighborhood like '%College area%'"));	
	
	/*Chapman University*/
	$placesChapman = mysqli_fetch_all($user->db->query("select count(id) from places 
														where city like '%Old Towne Orange%' or neighborhood like '%Old Towne Orange%' 
														or city like '%Chapman and Tus%' or neighborhood like '%Chapman and Tus%' 
														or city like '%Costa Mesa%' or neighborhood like '%Costa Mesa%' 
														or city like '%Villa Park%' or neighborhood like '%Villa Park%' 
														or city like '%Glassell%' or neighborhood like '%Glassell%'
														or city like '%Collins%' or neighborhood like '%Collins%'"));	
														
	/*University of Southern California*/
	$placesSCalifornia = mysqli_fetch_all($user->db->query("select count(id) from places 
														where city like '%Menlo%' or neighborhood like '%Menlo%' 
														or city like '%Irvine Are%' or neighborhood like '%Irvine Are%' 
														or city like '%Ellendale%' or neighborhood like '%Ellendale%' 
														or city like '%USC%' or neighborhood like '%USC%' 
														or city like '%University Villag%' or neighborhood like '%University Villag%'
														or city like '%Adams & Ellendale%' or neighborhood like '%Adams & Ellendale%'
														or city like '%W 29th St%' or neighborhood like '%W 29th St%'"));	
														
	/*University of California at Los Angeles*/
	$placesCaliforniaAngeles = mysqli_fetch_all($user->db->query("select count(id) from places 
																	where city like '%Westwood Village%' or neighborhood like '%Westwood Village%' 
																	or city like '%Beverly HIlls%' or neighborhood like '%Beverly HIlls%' 
																	or city like '%Westwood Hills%' or neighborhood like '%Westwood Hills%' 
																	or city like '%UCLA%' or neighborhood like '%UCLA%' 
																	or city like '%Greenfield%' or neighborhood like '%Greenfield%'"));															
																	
	/*Cal Poly San Luis Obispo*/
	$placesSanLuisObispo = mysqli_fetch_all($user->db->query("select count(id) from places 
															where city like '%San Luis Obispo%' or neighborhood like '%San Luis Obispo%' 
															or city like '%Downtown SLO%' or neighborhood like '%Downtown SLO%' 
															or city like '%Grover Beach%' or neighborhood like '%Grover Beach%' 
															or city like '%UCLA%' or neighborhood like '%UCLA%' 
															or city like '%Pismo Beach%' or neighborhood like '%Pismo Beach%'"));

	/*Loyola Marymount University */
	$placesLoyolaMarymountUniversity = mysqli_fetch_all($user->db->query("select count(id) from places 
																			where city like '%Marina Del Ray%' or neighborhood like '%Marina Del Ray%' 
																			or city like '%La Tijera%' or neighborhood like '%La Tijera%' 
																			or city like '%Manchester%' or neighborhood like '%Manchester%' 
																			or city like '%Westchester%' or neighborhood like '%Westchester%' 
																			or city like '%Culver City%' or neighborhood like '%Culver City%'"));
																
	/*UCSB*/
	$placesUCSB = mysqli_fetch_all($user->db->query("select count(id) from places 
													where city like '%Isla Vista%' or neighborhood like '%Isla Vista%' 
													or city like '%Goleta%' or neighborhood like '%Goleta%' 
													or city like '%Fortuna%' or neighborhood like '%Fortuna%' 
													or city like '%Goleta Beach%' or neighborhood like '%Goleta Beach%'"));
													
	/*Santa Clara*/
	$placesSantaClara = mysqli_fetch_all($user->db->query("select count(id) from places 
															where city like '%Santa Clara%' or neighborhood like '%Santa Clara%' 
															or city like '%San Jose%' or neighborhood like '%San Jose%' 
															or city like '%Milpitas%' or neighborhood like '%Milpitas%' "));
															
	/*Stanford */
	$placesStanford = mysqli_fetch_all($user->db->query("select count(id) from places 
														where city like '%Stanford%' or neighborhood like '%Stanford%' 
														or city like '%Palo Alto%' or neighborhood like '%Palo Alto%' 
														or city like '%Menlo Park%' or neighborhood like '%Menlo Park%' 
														or city like '%Mountain View%' or neighborhood like '%Mountain View%' 
														or city like '%Redwood City%' or neighborhood like '%Redwood City%' "));
														
	/*UCSF*/
	$placesUCSF = mysqli_fetch_all($user->db->query("select count(id) from places 
														where city like '%Richmond%' or neighborhood like '%Richmond%' 
														or city like '%Inner sunset%' or neighborhood like '%Inner sunset%' 
														or city like '%UCSF%' or neighborhood like '%UCSF%' 
														or city like '%Parkside%' or neighborhood like '%Parkside%'  "));
	

	$users = array();
	
	$rs = mysqli_fetch_all($user->db->query("select users.id ,users.first_name, users.last_name, users.gender, users.email from users"));
	
	foreach($rs as $item)
	{
		if($item[0]!=1)
		{
			$rsItem = mysqli_fetch_all($user->db->query("select networks.`name` from affiliations join networks on affiliations.network=networks.id where affiliations.`user`={$item[0]}"));
			
			$array = array('first_name'=>$item[1],
						   'last_name'=>$item[2],
						   'gender'=>$item[3],
						   'email'=>$item[4],
						   'school'=>$rsItem[0][0]
							);
			
			array_push($users, $array);
		}
	}
	
	$cadenaData  = '{ "y":"San Diego State University","a": '.$placesSanDiego[0][0].'},';
	$cadenaData .= '{ "y":"USD","a": '.$placesUSD[0][0].'},';
	$cadenaData .= '{ "y":"Chapman University","a": '.$placesChapman[0][0].'},';
	$cadenaData .= '{ "y":"University of Southern California","a": '.$placesSCalifornia[0][0].'},';
	$cadenaData .= '{ "y":"University of California at Los Angeles","a": '.$placesCaliforniaAngeles[0][0].'},';
	$cadenaData .= '{ "y":"Cal Poly San Luis Obispo","a": '.$placesSanLuisObispo[0][0].'},';
	$cadenaData .= '{ "y":"Loyola Marymount University","a": '.$placesLoyolaMarymountUniversity[0][0].'},';
	$cadenaData .= '{ "y":"UCSB","a": '.$placesUCSB[0][0].'},';
	$cadenaData .= '{ "y":"Santa Clara","a": '.$placesSantaClara[0][0].'},';
	$cadenaData .= '{ "y":"Stanford","a": '.$placesStanford[0][0].'},';
	$cadenaData .= '{ "y":"UCSF","a": '.$placesUCSF[0][0].'}';
	
	$jsonData = '[ '.$cadenaData.' ]';
	
	$smarty->assign('places', $rsPlaces);
	$smarty->assign('university', $arrayUniversity);
	$smarty->assign('idUniversity', $idUniversity);
	$smarty->assign('users', $users);
	$smarty->assign('data', $jsonData);
	$smarty->assign('date', $_GET['date']);
	
		
	$smarty->display('pages/report.tpl');

}
