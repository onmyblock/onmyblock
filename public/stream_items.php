<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

$limit = 5*$_POST['load'];

$stream_query = $user->db->query("select events.id as event_id, events.user as user_id, events.action, events.target as place_id, UNIX_TIMESTAMP(events.date) as time from events left join friends on friends.user = {$_POST['user_id']} and friends.friend = events.user join affiliations as firstAff on firstAff.user = {$_POST['user_id']} join affiliations as secondAff on secondAff.network = firstAff.network where events.user = secondAff.user and friends.friend is null and events.user <> {$_POST['user_id']} and events.action <> 5 and events.action <> 9 and  events.action <> 10 and events.action <> 11 and events.action <> 15 and events.action <> 16 order by events.id desc limit {$limit}, 5");

$stream_friends_query = $user->db->query("select events.id as event_id, events.user as user_id, events.action, events.target as place_id, UNIX_TIMESTAMP(events.date) as time from events join friends on friends.user = {$_POST['user_id']} where events.user = friends.friend and events.user <> {$_POST['user_id']} and events.action <> 5 and events.action <> 9 and  events.action <> 10 and events.action <> 11 and events.action <> 15 and events.action <> 16 order by events.id desc limit {$limit} , 5");


$i = 0;
while($event = mysqli_fetch_assoc($stream_query)){

	$event_details = $user->db->cache->get("event_{$event['event_id']}");

	if(!isset($event_details['user_id'])){
		$user_info = $user->db->get_user($event['user_id']);
		$place = ($event['place_id'] > 0 ? $user->db->get_place($event['place_id']) : null);
		$photos = ($event['place_id'] > 0 && $event['action'] < 13 ? mysqli_fetch_all($user->db->query("select dir from photos where place = {$event['place_id']} and dir not like '%profile%' limit 3")) : null);

		$details = null;

		switch ($event['action']){

		case 2: // badge

			$badge = mysqli_fetch_array($user->db->query("select badge from badges where user = {$event['user_id']} and place = {$event['place_id']} and UNIX_TIMESTAMP(date) = {$event['time']}"));
			$details['badge'] = $badge[0];

			break;

		case 3: // posted on house wall

			$post = mysqli_fetch_array($user->db->query("select entry from posts where user = {$event['user_id']} and place = {$event['place_id']} and UNIX_TIMESTAMP(date) = {$event['time']}"));
			$details['post'] = $post[0];

			break;


		}

		$x = 0;
		foreach($photos as $photo){
			$details['photos'][$x] = $photo[0];
			$x++;
		}


		$event_details = array(
			'user_id' => $event['user_id'],
			'first_name' => $user_info['first_name'],
			'last_name' => $user_info['last_name'],
			'event_type' => $event['action'],
			'place_id' => $event['place_id'],
			'place_name' => $place['name'],
			'place_street' => $place['street'],
			'place_route' => $place['route'],
			'time' => $event['time'],
			'details' => $details,
		);

		$user->db->cache->add("event_{$event['event_id']}", $event_details, false, 86400);

	}
	$stream[$i] = $event_details;
	$i++;
}



$i = 0;
while($event = mysqli_fetch_assoc($stream_friends_query)){

	$event_details = $user->db->cache->get("event_{$event['event_id']}");

	if(!isset($event_details['user_id'])){
		$user_info = $user->db->get_user($event['user_id']);
		$place = ($event['place_id'] > 0 ? $user->db->get_place($event['place_id']) : null);
		$photos = ($event['place_id'] > 0 && $event['action'] < 13 ? mysqli_fetch_all($user->db->query("select dir from photos where place = {$event['place_id']} and dir not like '%profile%' limit 3")) : null);

		$details = null;

		switch ($event['action']){

		case 2: // badge

			$badge = mysqli_fetch_array($user->db->query("select badge from badges where user = {$event['user_id']} and place = {$event['place_id']} and UNIX_TIMESTAMP(date) = {$event['time']}"));
			$details['badge'] = $badge[0];

			break;

		case 3: // posted on house wall

			$post = mysqli_fetch_array($user->db->query("select entry from posts where user = {$event['user_id']} and place = {$event['place_id']} and UNIX_TIMESTAMP(date) = {$event['time']}"));
			$details['post'] = $post[0];

			break;


		}

		$x = 0;
		foreach($photos as $photo){
			$details['photos'][$x] = $photo[0];
			$x++;
		}


		$event_details = array(
			'user_id' => $event['user_id'],
			'first_name' => $user_info['first_name'],
			'last_name' => $user_info['last_name'],
			'event_type' => $event['action'],
			'place_id' => $event['place_id'],
			'place_name' => $place['name'],
			'place_street' => $place['street'],
			'place_route' => $place['route'],
			'time' => $event['time'],
			'details' => $details,
		);

		$user->db->cache->add("event_{$event['event_id']}", $event_details, false, 86400);

	}
	$stream_friends[$i] = $event_details;
	$i++;
}


for($i = 0; $i < 10; $i++){
	if(isset($stream[$i])){
		$stream[$i]['time_epoch'] = $stream[$i]['time'];
		$stream[$i]['time'] = $user->db->time_format($stream[$i]['time'])."ago";
		$stream[$i]['sort'] = 'network';
	}
}

for($i = 10; $i < 20; $i++){
	if(isset($stream_friends[$i-10])){
		$stream[$i] = $stream_friends[$i-10];
		$stream[$i]['time_epoch'] = $stream[$i]['time'];
		$stream[$i]['time'] = $user->db->time_format($stream[$i]['time'])."ago";
		$stream[$i]['sort'] = 'friends';
	}
}


function aasort (&$array, $key) {
	$sorter=array();
	$ret=array();
	reset($array);
	foreach ($array as $ii => $va) {
		$sorter[$ii]=$va[$key];
	}
	asort($sorter);
	foreach ($sorter as $ii => $va) {
		$ret[$ii]=$array[$ii];
	}
	$array=$ret;
}

aasort($stream,"time_epoch");
$stream = array_reverse($stream);



$smarty->assign('stream', $stream);



$smarty->display('pages/stream_items.tpl');



?>


