{strip}
{include file='../header.tpl'}
{*
This is the home page and it is still under construction
*}
<div id="main">
{if $smarty.cookies.omb_session != 'loggedin'}
<div class="temp_popup_blocker">
</div>
<div class="temp_popup_container">
</div>
<div class="temp_popup">

Hey there we just launched to a group of private users!
<br />
We're excited to be moving forward and if you would like to be invited to the private beta, please enter in your email!
<br />
<input type="text" id="email_beta" class="slides_search_city" value="Email" onfocus="this.value = (this.value=='Email')? '' : this.value;" style="color:#6d6d6d; width:290px; font-size:16px; padding:5px; margin:10px; margin-top:20px;" />
<input type="submit" id="send_beta_email" class="blue-button" value="Submit" style="font-size:17px;" />

<div id="thankyou_message"></div>
<br />
<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FOnMyBlock&amp;send=false&amp;layout=standard&amp;width=480&amp;show_faces=true&amp;action=like&amp;colorscheme=dark&amp;font&amp;height=80" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:480px; height:80px;" allowTransparency="true"></iframe>
<div style="text-align:center; font-size:11px; margin-top:-30px;"><a onclick="fbLogin()">Login through Facebook if you're already a beta user</a></div>
</div>

{/if}
            <div class="front_slides">
	            
	            <div class="front_slides_container">
	            <div class="slide_item" id="slide_0" style="background:url('https://onmyblock.com/media/images/slides/s{$slide.num}.jpg') no-repeat;">
		            
	            <span class="slide_info">
	            <a href="https://onmyblock.com/place?id={$slide.id}" style="color:#fff;">
		            <span style="font-size:18px;"><p style="font-weight:bold;">{$slide.name}, ${$slide.price}</p>
		            <span style="font-size:12px;">{$slide.city}, {$slide.state}</span>
		            </span>
	            </a>
		            <br />
		            <img src="https://onmyblock.com/media/images/ps_views_white.png" /> {$slide.views} views <img src="https://onmyblock.com/media/images/ps_comments_white.png" /> {$slide.comments} comments <img src="https://onmyblock.com/media/images/ps_likes_white.png" /> {$slide.likes} likes
		            
	            </span>
		            
	            </div> 
	            </div>


	            <div id="slide_0_content" class="slide_content">
	            
	            	    <div class="slide_hook">
		            Find Your Next College Pad.
	            </div>
	            
	            
	            
	            <div class="slides_search">
	            <form  id="form-1" class="jqTransform" method="post" action="map" enctype="multipart/form-data">
		            <input type="text" border="0" id="slides_search_city" />

                   <div class="width-1">
		            <select style="display:none;">
					  <option value="1">1+ Bedroom</option>
					  <option value="2">2+ Bedrooms</option>
					  <option value="3">3+ Bedrooms</option>
					  <option value="4">4+ Bedrooms</option>
					</select>
                   </div>
                   
                   <div class="width-1">
		            <select style="display:none;">
					  <option value="0-1000">$0-$1000</option>
					  <option value="1000-2000">$1000-$2000</option>
					  <option value="2000-3000">$2000-$3000</option>
					  <option value="3000+">$3000+</option>
					</select>
                   </div>
                                     
		            <input type="submit" value="Find" class="blue-button" id="slide_submit_search" />
	            </form>
	            </div>
	   

	           

	            </div>
	       
	            
	            


	            
            </div>
            

<a id="scroll_top">Scroll Top</a>
<div id="front_menu">
<div id="front_menu_container">
        <div id="options" class="browse_options">
            <ul class="browse_nav" data-option-key="filter">
                <li><a data-option-value="" class="selected">New Pads</a></li>
                <li><a data-option-value="beach">Beaches</a></li>
                <li><a data-option-value="school">Close to School</a></li>
            </ul>
        </div>
</div>
</div>
<div style="min-height:1000px; margin-bottom:30px;">
    <div id="browse_container">
        {foreach from=$places item='place'} {assign var=photo value=","|explode:$place.4}
        {if {$place.height} > 50}
        <div style="height:{$place.height}px; background:url(https://s3-us-west-1.amazonaws.com/onmyblock/places/{$place.0}/profile.jpg);" class="browse_item {if $place.4 < 2} 1br 
        						{elseif $place.4 == 2} 2br 
        						{elseif $place.4 == 3} 3br 
        						{elseif $place.4 > 3} 4br {/if}
        						{if $place.5 < 1000} 0-1 
        						{elseif $place.5 >= 1000 && $place.5 < 2000} 1-2 
        						{elseif $place.5 >= 2000 && $place.5 < 3000} 2-3 
        						{elseif $place.5 >= 3000} 3-* {/if}" place_id="{$place.0}">
            <div class="browse_box">
                <div class="browse_item_header" user_id="{$user.id}" place_id="{$place.0}">
	                <input type="button" class="blue-button" style="float:right; padding:7px; font-size:14px;" value="${$place.5}" />
	                <input type="button" class="browse_fav" title="Add to Wishlist" {if $place.favorited eq 1}id="browse_favorited"{/if}/>
	                <input type="button" class="browse_like" title="Like this House"  {if $place.liked eq 1}id="browse_liked"{/if}/>
                </div>
                <span>{$place.6} {$place.7} <br /> {$place.1}</span>
                <div class="browse_item_info">
                <div class="bi">
                <div class="bi0">{$place.views}</div>
                <div class="bi1">{$place.likes}</div>
                <div class="bi2">{$place.comments}</div>
                </div>
                </div>
            </div>
        </div>
        {/if}
        {/foreach}
    </div>
</div>
</div>
{include file='../footer.tpl'} {/strip}