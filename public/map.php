<?php

$location = $_POST['location'];
$rooms = $_POST['rooms'];
$price = $_POST['price'];

$focus = false;
$p=$price;
$r=$rooms;

if($price == 1){
	$price = "price < 1000";
} elseif($price == 2){
	$price = "price > 1000 and price < 2000";
} elseif($price == 3){
	$price = "price > 2000 and price < 3000";
} elseif($price == 4){
	$price = "price > 3000";
} else {
	$price = "";
}

if($rooms == 1){
	$rooms = "rooms = 1";
} elseif($rooms == 2){
	$rooms = "rooms = 2";
} elseif($rooms == 3){
	$rooms = "rooms = 3";
} elseif($rooms == 4){
	$rooms = "rooms > 3";
} else {
	$rooms = "";
}

	
if ($location != '' && $location != 'Please enter city or neighborhood' ){
	
	$focus = true;
	
	if($price!="")
	{
		$queryPrice = " ( ".$price." )";
	}
	
	if($rooms!="")
	{
		if($price!=""){
			$queryRooms = " and ";
		}
		$queryRooms .= " ( ".$rooms." ) ";
		
		//echo $queryRooms;
	}
	
	/*$criterio = $location;
	
	if($criterio!='')
	{
		$arrayCriterio = explode(',',$criterio);
	}
	
	$places = array();
	
	foreach($arrayCriterio as $item)
	{
		$item = trim($item);
		$route = '';
		
		$query .= "( city like '%{$item}%' or neighborhood like '%{$item}%' or state like '%{$item}%' ";
		
		$arraystreet = explode(' ',$item);
			
		for($i=1; $i<count($arraystreet); $i++)
		{
			$route.=$arraystreet[$i].' ';
		}
		
		$route = trim($route);
		
		if(is_numeric($arraystreet[0]))
		{
			$query .= " or street={$arraystreet[0]} ";
		}
		
		if($route)
		{
			$query .= " or route like '%{$route}%' ";
		}
		
		$query .= ") and ";
	}
	,((ACOS(SIN({$_POST['latitude']} * PI() / 180) * SIN(`latitude` * PI() / 180) + COS({$_POST['latitude']} * PI() / 180) * COS(`latitude` * PI() / 180) * COS(({$_POST['longitude']} - `longitude`) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS distance 
	// limit 300
	$query = rtrim($query,"and ");¨
	*/
	
	if($price!="" or $rooms!="")
	{
		$where = 'where ';
	}
	
	$queryf = "select id, latitude, longitude, name, street, route, price, size, rooms, bathrooms, type from places {$where} {$queryPrice}  {$queryRooms} and status = 1 group by id ORDER BY date desc;";
	
	//$queryPlaces="SELECT id, latitude, longitude, name, street, route, price, size, rooms, bathrooms, type ,((ACOS(SIN({$_GET['latitude']} * PI() / 180) * SIN(`latitude` * PI() / 180) + COS({$_GET['latitude']} * PI() / 180) * COS(`latitude` * PI() / 180) * COS(({$_GET['longitude']} - `longitude`) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS distance FROM places WHERE (`latitude` BETWEEN ({$_GET['latitude']} - 10) AND ({$_GET['latitude']} + 10) AND `longitude` BETWEEN ({$_GET['longitude']} - 10) AND ({$_GET['longitude']} + 10)) {$queryPrice} {$queryrooms} group by distance ORDER BY distance ASC, date desc limit 1500;";

	$places = mysqli_fetch_all($user->db->query($queryf));
	//print_r($places);
	$arrayLocation = explode(',',$location);
	
	$quantitySearch = count($arrayLocation);
	
	switch($quantitySearch)
	{
		case 1:
			if($_POST['home']=='myblock')
			{
				$zoom=14;
			}
			break;
		case 2:
				$zoom=7;
			break;
		case 3:
				$zoom=10;
			break;
		case 4:
				$zoom=14;
			break;
	}
	
	if($user->id == 597597919)
	{	
		//print_r($zoom);
		//echo $_POST['latitude'].'--'.$_POST['longitude'];
	}
	
	$center['latitude'] = $_POST['latitude'];
	$center['longitude'] = $_POST['longitude'];
	$smarty->assign('center', $center);
	$smarty->assign('zoom', $zoom);

} else {
	$big_bar = true;
	$latitude = 32.771904;
	$longitude = -117.189124;
	$radius = 30;
	$limit = 1500;
	//$focus = false;
	
	$queryPrice="";
	$queryRooms="";
	
	if($price!="")
	{
		$queryPrice = " and ( ".$price." )";
	}
	
	if($rooms!="")
	{
		//echo $rooms;
		$queryRooms = " and ( ".$rooms." ) ";
		
		//echo $queryRooms;
	}

	$queryPlaces="SELECT id, latitude, longitude, name, street, route, price, size, rooms, bathrooms, type ,((ACOS(SIN({$latitude} * PI() / 180) * SIN(`latitude` * PI() / 180) + COS({$latitude} * PI() / 180) * COS(`latitude` * PI() / 180) * COS(({$longitude} - `longitude`) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS `distance` FROM places WHERE (`latitude` BETWEEN ({$latitude} - {$radius}) AND ({$latitude} + {$radius}) AND `longitude` BETWEEN ({$longitude} - {$radius}) AND ({$longitude} + {$radius})) {$queryPrice}  {$queryRooms} and status = 1  ORDER BY distance ASC, date desc ;";//limit {$limit}
	
	if($user->id == 597597919)
	{
		//echo $queryPlaces;
	}
	
	$places = mysqli_fetch_all($user->db->query($queryPlaces));
}

$queryMax = mysqli_fetch_row($user->db->query("select max(price) from places"));
$queryMin = mysqli_fetch_row($user->db->query("select min(price) from places"));

if($location == ''){
	$location = 'Enter city name or neighborhood';
}

if(!$places || $places[0][11] > 1){
	echo "<script style='position:fixed;' type='text/javascript'>alert('No results found');window.location.href = 'map';</script>";
}

$smarty->assign('focus', $focus);
$smarty->assign('location', $location);
$smarty->assign('places', $places);
$smarty->assign('big_bar', $big_bar);
$smarty->assign('user', $user->get_info());
$smarty->assign('filename', 'map');
$smarty->assign('title', 'OnMyBlock - Map');
$smarty->assign('rooms', $r);
$smarty->assign('price', $p);
$smarty->assign('pricemax', $queryMax[0]);
$smarty->assign('pricemin', $queryMin[0]);

$smarty->display('pages/map.tpl');


?>
